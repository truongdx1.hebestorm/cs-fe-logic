(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./src/assets/images/logos/logo_2.png":
/*!********************************************!*\
  !*** ./src/assets/images/logos/logo_2.png ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"06479ba43b423f3595ac2fc57817ff12.png\";\n\n//# sourceURL=webpack:///./src/assets/images/logos/logo_2.png?");

/***/ }),

/***/ "./src/layouts/Minimal/Minimal.js":
/*!****************************************!*\
  !*** ./src/layouts/Minimal/Minimal.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ \"./node_modules/prop-types/index.js\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _material_ui_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/styles */ \"./node_modules/@material-ui/styles/esm/index.js\");\n/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components */ \"./src/layouts/Minimal/components/index.js\");\n\n\n\n\nvar useStyles = Object(_material_ui_styles__WEBPACK_IMPORTED_MODULE_2__[\"makeStyles\"])(function () {\n  return {\n    root: {\n      paddingBottom: '50px',\n      height: '100%'\n    },\n    content: {\n      height: '100%'\n    }\n  };\n});\n\nvar Minimal = function Minimal(props) {\n  var children = props.children;\n  var classes = useStyles();\n  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: classes.root\n  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"main\", {\n    className: classes.content\n  }, children), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null));\n};\n\nMinimal.propTypes = {\n  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,\n  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (Minimal);\n\n//# sourceURL=webpack:///./src/layouts/Minimal/Minimal.js?");

/***/ }),

/***/ "./src/layouts/Minimal/components/Topbar/Topbar.js":
/*!*********************************************************!*\
  !*** ./src/layouts/Minimal/components/Topbar/Topbar.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"./node_modules/react-router-dom/es/index.js\");\n/* harmony import */ var clsx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! clsx */ \"./node_modules/clsx/dist/clsx.m.js\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ \"./node_modules/prop-types/index.js\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _material_ui_styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/styles */ \"./node_modules/@material-ui/styles/esm/index.js\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ \"./node_modules/@material-ui/core/esm/index.js\");\n/* harmony import */ var _assets_images_logos_logo_2_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../assets/images/logos/logo_2.png */ \"./src/assets/images/logos/logo_2.png\");\n/* harmony import */ var _assets_images_logos_logo_2_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_images_logos_logo_2_png__WEBPACK_IMPORTED_MODULE_6__);\nvar _excluded = [\"className\"];\n\nfunction _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }\n\nfunction _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }\n\nfunction _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }\n\n\n\n\n\n\n\n\nvar useStyles = Object(_material_ui_styles__WEBPACK_IMPORTED_MODULE_4__[\"makeStyles\"])(function () {\n  return {\n    root: {\n      boxShadow: 'none'\n    }\n  };\n});\n\nvar Topbar = function Topbar(props) {\n  var className = props.className,\n      rest = _objectWithoutProperties(props, _excluded);\n\n  var classes = useStyles();\n  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"AppBar\"], _extends({}, rest, {\n    className: Object(clsx__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(classes.root, className),\n    color: \"primary\",\n    position: \"fixed\"\n  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"Toolbar\"], {\n    style: {\n      minHeight: 80,\n      backgroundColor: '#2c6835',\n      border: '2px solid #fff',\n      borderLeft: 0,\n      borderRight: 0,\n      borderTop: 0\n    },\n    variant: \"regular\"\n  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/admin\"\n  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"img\", {\n    alt: \"Logo\",\n    height: 50,\n    src: _assets_images_logos_logo_2_png__WEBPACK_IMPORTED_MODULE_6___default.a\n  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", {\n    style: {\n      marginLeft: 10,\n      fontWeight: 'bold',\n      color: 'white'\n    }\n  }, \"Pet Health\")));\n};\n\nTopbar.propTypes = {\n  className: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (Topbar);\n\n//# sourceURL=webpack:///./src/layouts/Minimal/components/Topbar/Topbar.js?");

/***/ }),

/***/ "./src/layouts/Minimal/components/Topbar/index.js":
/*!********************************************************!*\
  !*** ./src/layouts/Minimal/components/Topbar/index.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Topbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Topbar */ \"./src/layouts/Minimal/components/Topbar/Topbar.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return _Topbar__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n\n\n//# sourceURL=webpack:///./src/layouts/Minimal/components/Topbar/index.js?");

/***/ }),

/***/ "./src/layouts/Minimal/components/index.js":
/*!*************************************************!*\
  !*** ./src/layouts/Minimal/components/index.js ***!
  \*************************************************/
/*! exports provided: Topbar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Topbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Topbar */ \"./src/layouts/Minimal/components/Topbar/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Topbar\", function() { return _Topbar__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n\n\n//# sourceURL=webpack:///./src/layouts/Minimal/components/index.js?");

/***/ }),

/***/ "./src/layouts/Minimal/index.js":
/*!**************************************!*\
  !*** ./src/layouts/Minimal/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Minimal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Minimal */ \"./src/layouts/Minimal/Minimal.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return _Minimal__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n\n\n//# sourceURL=webpack:///./src/layouts/Minimal/index.js?");

/***/ })

}]);