/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { Suspense } from 'react'
import { Router, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { history, store } from '../_helpers'
import { alertActions } from '../_actions'
import { PrivateRoute } from '../_components'
import { IntlProvider, addLocaleData } from 'react-intl'
import en from 'react-intl/locale-data/en'
import vi from 'react-intl/locale-data/vi'
import { localeSet } from '../_actions'
// import 'react-progress-2/main.css'
import messages from '../messages';
import _ from 'lodash'
// import theme from '../theme'; 
import {ThemeProvider } from '@material-ui/styles';
// import '../assets/scss/index.scss';
import { createTheme } from '@material-ui/core/styles';
// import '../style.css';
import "../App.css";
import '../index.css';
import Routes from '../Routes';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap-daterangepicker/daterangepicker.css';
import Backdrop from '@material-ui/core/Backdrop';
import { userConstants, groupAccess } from '../_constants'
import 'rsuite/dist/styles/rsuite-default.css';
import Cookies from 'universal-cookie'
import config from 'config'
import { request } from '../_services/request'

const cookies = new Cookies()
// import 'react-big-calendar/lib/sass/styles.scss';
// import 'react-big-calendar/lib/addons/dragAndDrop/styles.scss'

addLocaleData(en);
addLocaleData(vi);
if (localStorage.feroshLng) {
  store.dispatch(localeSet(localStorage.feroshLng));
}
const theme = createTheme({
  palette: {
    primary: {
      main: '#219653',
      
    },
    secondary: {
      main: "#EFEFEF",
    },
  },
});
class App extends React.Component {
  constructor(props) {
    super(props)

    const { dispatch } = this.props
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear())
    })
  }
  onRouteChange(math, name, isClear) {
    const { breadcrums, dispatch } = this.props
    // console.log(math,'math')
    var data = []
    math['name'] = math.params.id != null ? (math.params.id != -1 ? name : 'New') : name
    if (breadcrums.data != null) {
      data = Object.assign([], breadcrums.data.data)
    }
    data = [math]
    dispatch({ type: 'SET_BREADCRUM', data })
  }


  componentDidMount() {
    const { dispatch } = this.props
    if (cookies.get('user_hebecore')) {
      const userObj = cookies.get('user_hebecore')
      request.get(`${config.apiLoginUrl}/api/v1/user_profile`).then(res => {
        if (res.data.success) {
          const user = {data: { ...res.data.data, token: userObj.token }}
          dispatch({ type: userConstants.LOGIN_SUCCESS, user })
          // localStorage.setItem('user_hebecore', JSON.stringify(user))
          if (_.intersection(res.data.data.groups.map(e => e.code), groupAccess).length == 0 && window.location.pathname != '/not-found') {
            console.log('11111')
            history.push('/not-found')
          }
        }
      })
    }
  }

  render() {
    const { lang, loadingBar } = this.props

    return (
      <IntlProvider
        locale={lang}
        messages={messages[lang]}
      >
        <ThemeProvider theme={theme}>
          <div>
            {/* <Backdrop style={{
              zIndex: 9000,
              color: 'rgba(0, 0, 0, 0)',
              opacity: 0,
            }} open={loadingBar.default == 1} onClick={() => { }}>
            </Backdrop> */}
            <Router history={history}>
              <Suspense fallback={'Loading'}>
                <Switch>
                  <PrivateRoute 
                    path='/' 
                    component={Routes} 
                    onRouteChange={(pageId, name, isClear) => this.onRouteChange(pageId, name, isClear)}
                  />
                </Switch>
              </Suspense>
            </Router>
          </div>

        </ThemeProvider>
      </IntlProvider>
    )
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    alert: state.alert,
    lang: state.locale.lang,
    breadcrums: state.breadcrums,
    loadingBar: state.loadingBar,
    user
  }
}

const connectedApp = connect(mapStateToProps)(App)
export { connectedApp as App }
