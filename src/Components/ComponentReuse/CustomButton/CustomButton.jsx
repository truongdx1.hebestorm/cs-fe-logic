import React from 'react';
import {Button} from "antd"
const CustomButton = React.forwardRef(({icons , text , background ,open, ...props  }, ref) => (
    <button className={`${background} button p-1 rounded hover:opacity-60 capitalize px-2 mr-2 flex-nowrap gap-2 flex items-center`} ref={ref} {...props}>
      {icons} {text}
    </button>
  ));
export default CustomButton;