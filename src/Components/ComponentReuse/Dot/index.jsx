import React from 'react';
const DotComponent = ({ bgColor }) => {
	return (
		<>
			<div className={`items-center mt-0.5`}>
				<span className={`w-2.5 h-2.5 rounded-full inline-block m-1 items-center ${bgColor}`}></span>
			</div>
		</>
	);
};
export default DotComponent;
