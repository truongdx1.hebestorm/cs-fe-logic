import OpenIcons from '../../../Icons/OpenIcons';
import React from 'react';
import AddIcons from '../../../Icons/AddIcons';
import CalendarIcons from '../../../Icons/CalendarICons';
import { Button } from 'antd';
const ListOptionFilterDay = ({ listChoosen, link , onClick }) => {
	return (
		<>
			<div className="flex justify-between mobile:flex-wrap items-baseline">
				<div className="flex gap-2 2xl:flex-nowrap xl:flex-wrap xl:mb-4 mobile:flex-wrap">
					<div className="flex xl:flex-nowrap mobile:flex-wrap gap-2">
						<Button
							className="border flex gap-2 rounded-md px-3 py-1 bg-gray text-sm items-center mobile:flex-nowrap btn-filter-days"
							icon={<CalendarIcons />}
							onClick={() => onClick("today")}
						>
							Today
						</Button>
						{listChoosen.map((item) => (
							<Button
								className="border inline rounded-md px-3 py-1 bg-gray text-sm btn-filter-days btn-option"
								key={item}
								onClick={() => onClick(item.value)}
							>
								{item.title}
							</Button>
						))}
					</div>
				</div>
				<div className="flex gap-2 xl:flex-nowrap md:flex-wrap mobile:my-2 xl:my-0">
					<Button
						size="large"
						className="border px-3 py-2 flex flex-nowrap gap-2 rounded-md bg-gray text-sm btn-export items-center"
						icon={<OpenIcons />}
					>
						Export
					</Button>
					<Button
						size="large"
						className="border px-4 py-2 rounded-md bg-btn-primary text-white text-sm flex gap-2 flex-nowrap btn-export items-center"
						onClick={link}
						icon={<AddIcons styled="icons-white inline ml-2" />}
					>
						New Case
					</Button>
				</div>
			</div>
		</>
	);
};

export default ListOptionFilterDay;
