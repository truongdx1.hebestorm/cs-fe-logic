import React from 'react';
import { Menu, Dropdown, Button } from 'antd';
import { AppstoreOutlined } from '@ant-design/icons';
import DownIcons from '../../../Icons/DownIcons';

const { SubMenu } = Menu;

const MenuComponent = ({ title, list, icons, key, onClick, className }) => {
	const handleClick = (e) => {
		console.log(e);
	};
	return (
		<>
			<Dropdown
				overlay={
					<Menu
						className={className}
						keyPath={key}
						style={{ width: 'auto' }}
						mode={'vertical'}
						theme={'light'}
						subMenuOpenDelay={0.1}
						// triggerSubMenuAction={true}
						onClick={onClick}
						openKeys={list}
					>
						{list?.map((item) => (
							<Menu.Item key={item}>{item}</Menu.Item>
						))}
					</Menu>
				}
			>
				<Button
					className={`border flex rounded-5px items-center ${className}`}
				>
					{title}
					{icons}
				</Button>
			</Dropdown>
		</>
	);
};
export default MenuComponent;
