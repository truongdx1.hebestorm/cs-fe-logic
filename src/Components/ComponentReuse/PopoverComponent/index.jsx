import React from 'react';
import { Popover } from 'antd';

const PopoverComponent = ({ text, item, bg , styled }) => {
	const content = (
		<>
			<div>
				<span
					style={{
						backgroundColor: bg,
					}}
					dangerouslySetInnerHTML={{ __html: text }}
				>
					
				</span>
			</div>
		</>
	);

	return (
		<>
			<Popover content={content}>
				<span
					style={{
						backgroundColor: bg,
					}}
                    className={styled}
					dangerouslySetInnerHTML={{ __html: text }}
				>
				</span>
			</Popover>
		</>
	);
};
export default PopoverComponent;
