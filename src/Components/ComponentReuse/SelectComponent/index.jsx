import React from 'react';
const SelectComponent = ({ list, text }) => {
	return (
		<>
			<select className="border rounded-5px w-36">
				<option className="block" disabled>{text}</option>
				{list.map((item, index) => (
					<option key={index} value={item} className="py-3 px-1 hover:bg-gray-300 block">
						{item}
					</option>
				))}
			</select>
		</>
	);
};
export default SelectComponent;
