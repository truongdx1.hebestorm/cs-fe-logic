import React from 'react';
const TableOrderInfor = ({className}) => {
	return (
		<table className={className}>
			<thead className="bg-skype h-auto ">
				<tr className="text-start text-10px mobile:overflow-hidden border-tr">
					<td className="xl:py-2 mobile:px-2 none-border">Order ID</td>
					<td className="px-2 none-border">Code</td>
					<td className="px-2 none-border">Level</td>
					<td className="px-2 none-border">Kind</td>
					<td className="px-2 none-border">Title</td>
					<td className="px-2 none-border">Note</td>
					<td className="px-2 none-border">Create At</td>
					<td className="px-2 none-border">Forward To</td>
				</tr>
			</thead>
			<tbody className="text-10px xl:truncate text-start">
				<tr className="text-10px border-tr">
					<td className="py-1 px-2 none-border">3421492</td>
					<td className="px-2 none-border">OI-0086679</td>
					<td className="px-2 none-border">T5</td>
					<td className="truncate px-2 none-border">Danaged/Wrong/Missing</td>
					<td className="px-2 none-border">#6362334</td>
					<td className="truncate px-2 none-border">
						KH mua 1 xanh va 1 do flying orb nhung chi nhu...
					</td>
					<td className="truncate px-2 none-border">2022-01-10T14:31:30</td>
					<td className="px-2 none-border">Alex</td>
				</tr>
			</tbody>
		</table>
	);
}
export default TableOrderInfor;