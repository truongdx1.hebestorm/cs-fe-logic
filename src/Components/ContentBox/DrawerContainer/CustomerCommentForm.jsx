import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileVideo } from '@fortawesome/free-solid-svg-icons/faFileVideo';
import { Button } from 'antd';
import DownloadIcons from '../../../Icons/DownloadIcons';
import ForwardIcons from '../../../Icons/ForwardIcons';
import TrashIcons from '../../../Icons/TrashIcons';
import React from 'react';

const CustomerCommentForm = () => {
	return (
		<div className="m-6 border rounded bg-green">
			<div className="flex items-center mx-4">
				<div className="h-8 w-8 mr-2  mt-2 avatarA font-normal text-center items-center rounded-5px ">
					<span className={`inline-block mt-1.5 text-sm`}>A</span>
				</div>
				<div className="basis-10/12 inline mt-3 ">
					<div className="inline">
						<p className="blue text-sm">Allison Kienlen</p>
						<p className="text-xs text-gray-400 w-4/5">
							2 days ago , (Fri, 14 Jan 2022 at 1:43PM)
						</p>
					</div>
				</div>
			</div>
			<div className="m-2 mx-4 leading-relaxed text-sm">
				<p>
					I would like to know where this is??? It is supporsed to be a
					Christmas present and needs to be here by December 24th. Where is it?
				</p>

				<p>Thanks you,</p>

				<p>Renee Seibert</p>
			</div>
			<div className="my-4  mx-4 flex xl:flex-nowrap md:flex-nowrap items-center 2xl:flex-nowrap 2xl:w-64 xl:w-48  border border-gray-400 rounded-5px p-3 shadow drop-shadow-lg ">
				<div className="flex py-2">
					<div className="px-2">
						<FontAwesomeIcon icon={faFileVideo} size="3x" className="mx-1" />
					</div>
					<div>
						<span className="text-sm">Video.mov</span>
						<span className="text-xs text-gray-400 block">6.69MB</span>
					</div>
				</div>
				<div className="flex">
					<button className="btn-none-border">
						<DownloadIcons className="mx-1.5" />
					</button>
					<button className="btn-none-border">
						<ForwardIcons className="mx-1.5" />
					</button>
					<button className="btn-none-border">
						<TrashIcons margin="mx-1.5" />
					</button>
				</div>
			</div>
		</div>
	);
};
export default CustomerCommentForm;
