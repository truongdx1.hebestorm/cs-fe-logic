import React from 'react';
import FormSendMail from '../DrawerContainer/FormSendMail';
import CloseIcon from '@material-ui/icons/Close';
import OrderDetailForward from './OrderDetailForward';
import CustomerCommentForm from './CustomerCommentForm';
import ForwardIcons from '../../../Icons/ForwardIcons';
import { Button } from 'antd';
import { Drawer } from '@material-ui/core';

export default function FormFoward({ text, styled, styleIcons }) {
	const [state, setState] = React.useState({ right: false });

	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}

		setState({ ...state, [anchor]: open });
	};
	const list = () => (
		<div className="mobile:w-full mobile:overflow-auto">
			<div className="flex justify-end">
				<Button
					size="large"
					className="border-none btn-icon"
					onClick={toggleDrawer('right', false)}
				>
					<CloseIcon />
				</Button>
			</div>
			<div
				sx={{
					width: '100%',
					flexShrink: 1,
					'& .Mui': {
						width: '50%',
						boxSizing: 'border-box',
						backgroundColor: 'white',
					},
				}}
				role="presentation"
				onKeyDown={toggleDrawer('right', false)}
			>
				<div className="mx-6 border rounded-5px">
					<OrderDetailForward />
				</div>
				<div className="m-6">
					<FormSendMail />
				</div>
				<div className="w-full">
					<CustomerCommentForm />
				</div>
			</div>
		</div>
	);
	return (
		<div className="w-full">
			<React.Fragment key={'right'}>
				<Button
					onClick={toggleDrawer('right', true)}
					className={`border rounded-5px hover:opacity-60 capitalize items-center text-center  ${styled}`}
					icon={
						<ForwardIcons
							className={styleIcons ? styleIcons : 'icons-gray mx-2'}
						/>
					}
				>
					{text ? text : null}
				</Button>
				<Drawer
					sx={{
						'& .MuiDrawer-paper': {
							width: '50%',
							boxSizing: 'border-box',
							backgroundColor: 'white',
						},
					}}
					width="100%"
					anchor={'right'}
					open={state['right']}
					onClose={toggleDrawer('right', false)}
					variant="temporary"
				>
					{list('right')}
				</Drawer>
			</React.Fragment>
		</div>
	);
}
