import Popover from './Popover';
// import TinyEditorComponent from './TinyForm';
import { Button } from 'antd';
import ForwardIcons from '../../../Icons/ForwardIcons';
import { TinyEditorComponent } from '../../../_components';

const FromSendMail = () => {
	// Test
	return (
		<>
			<div className="h-auto box-border">
				<h2 className="capitalize font-normal inline text-sm">foward to:</h2>
				<div className="flex w-full border bg-gray rounded-5px items-baseline justify-between">
					<div className="basis-10/12 p-1 text-sm">
						<span className="font-semibold ">Alex</span>(alex.pod@supplier.com)
					</div>
					<div className="items-center text-center mr-2">
						<div>
							<Popover />
						</div>
					</div>
				</div>
				<div className="my-2 border rounded-5px">
					<div className="flex m-2 items-center">
						<div className="h-8 w-8 mx-2 mt-2 font-normal rounded-5px avatarJ items-center text-center">
							<span className="inline-block my-auto text-xs pt-2">J</span>
						</div>
						<div className="basis-10/12 inline mt-3 ">
							<div className="inline">
								<p className="blue text-sm">Jessica N</p>
								<p className="text-xs text-gray-400 w-4/5">
									Fri , 15 Jan 2022 at 1:43 PM
								</p>
							</div>
						</div>
					</div>
					<div className="p-4">
						<TinyEditorComponent />
						<div className="items-end pt-3 w-full flex flex-row-reverse">
							<Button
							size="large"
								icon={<ForwardIcons className="icons-white inline ml-2" />}
								className="btn-submit text-white"
							>
								Forward
							</Button>
							<Button size="large" className="btn-cancel bg-gray border mx-2">Cancel</Button>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
export default FromSendMail;
