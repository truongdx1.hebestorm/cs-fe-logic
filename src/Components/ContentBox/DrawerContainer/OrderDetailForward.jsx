import { Checkbox } from 'antd';
const OrderDetailForward = () => {
	return (
		<div className="inline rounded-5px md:w-full mobile:w-full mobile:overflow-auto">
			<div className="py-2.5 mx-4 p-1 text-sm items-baseline flex justify-between">
				<h2 className="inline font-semibold text-emerald-700 capitalize my-auto">
					Order detail
				</h2>
				<a href="/" className="blue my-auto">
					Edit
				</a>
			</div>
			<div className="mobile:overflow-auto">
				<table className="mobile:table-auto border w-full bg-white mb-3 mobile:overflow-x-scroll">
					<thead className="bg-gray text-start">
						<tr className="text-start items-center border-y">
							<td className="text-10px font-semibold none-border flex flex-nowrap items-center p-1.5 none-border ">
								<Checkbox />
								<span className="mx-2">Lineitem SKU</span>
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Lineitem Name
							</td>
							<td className="px-2 text-10px font-semibold none-border truncate none-border">
								Link Design <br /> Link Ali/Sup
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Level
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Supplier
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Tracking
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Carrier
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								TKN Uploaded At
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								TKN Status
							</td>
							<td className="px-2 text-10px font-semibold none-border">
								Est Time
							</td>
						</tr>
					</thead>
					<tbody className="text-start mobile:overflow-x-scroll">
						<tr className="text-sm border-tr">
							<td className="text-10px flex flex-nowrap items-center none-border p-1.5">
								<Checkbox />
								<span className="mx-2">POD_20217641</span>
							</td>
							<td className="px-2 truncate none-border text-10px">
								Fleece Blanket Id{' '}
							</td>
							<td className="px-2 truncate none-border text-10px">Link</td>
							<td className="px-2 truncate none-border text-10px">L7.8</td>
							<td className="px-2 truncate none-border text-10px">Alex</td>
							<td className="px-2 truncate none-border text-10px">
								YT2135121236038136
							</td>
							<td className="px-2 truncate none-border text-10px">
								Yun Express
							</td>
							<td className="px-2 truncate none-border text-10px">
								2022-01-10T14:31:30
							</td>
							<td className="px-2 truncate none-border text-10px">In trasit</td>
							<td className="px-2 truncate none-border text-10px">
								Jan 29,2022
							</td>
						</tr>
						<tr className="bg-gray text-sm border-tr">
							<td className="text-10px flex flex-nowrap items-center none-border p-1.5">
								<Checkbox />
								<span className="mx-2">POD_20217641</span>
							</td>
							<td className="px-2 truncate none-border text-10px">
								Fleece Blanket Id
							</td>
							<td className="px-2 truncate none-border text-10px">Link</td>
							<td className="px-2 truncate none-border text-10px">L7.8</td>
							<td className="px-2 truncate none-border text-10px">Kyle Cal</td>
							<td className="px-2 truncate none-border text-10px">
								YT2135121236038136
							</td>
							<td className="px-2 truncate none-border text-10px">
								Yun Express
							</td>
							<td className="px-2 truncate none-border text-10px">
								2022-01-10T14:31:30
							</td>
							<td className="px-2 truncate none-border text-10px">In trasit</td>
							<td className="px-2 truncate none-border text-10px">
								Jan 28,2022
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
};
export default OrderDetailForward;
