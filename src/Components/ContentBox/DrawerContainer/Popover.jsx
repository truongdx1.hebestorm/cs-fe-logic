import * as React from 'react';

import {Button} from 'antd';
import FormSearchTemplate from '../../Header/Issues/LeftContent/FormSearchTemplate';
import DownIcons from '../../../Icons/DownIcons';
import UpIcons from '../../../Icons/UpIcons';
import { Popover, Typography } from '@material-ui/core';
const arr = ['Alex', 'ZootopBear', 'Kyle Cai', 'Lucas', 'Tony'];
export default function BasicPopover(props) {
	const { text } = props;
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [changeIcons, setChangeIcons] = React.useState(false);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
		setChangeIcons(!changeIcons);
	};
	const handleClose = () => {
		setAnchorEl(null);
		setChangeIcons(!changeIcons);
	};

	const open = Boolean(anchorEl);
	const id = open ? 'simple-popover' : undefined;

	return (
		<div className="m-0">
			<Button
		        className="border-none bg-gray"
				aria-describedby={id}
				onClick={handleClick}
				icon={changeIcons ? <UpIcons /> : <DownIcons />}
			></Button>
			<Popover
				id={id}
				open={open}
				anchorEl={anchorEl}
				onClose={handleClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'left',
					width: '100%',
				}} 
			>
				<Typography>
					<FormSearchTemplate
						text={arr}
						icons={null}
						close={handleClose}
						styledSearch="w-full flex border bg-gray rounded-5px py-1 px-2"
						styledList="px-3 py-0.5  w-full hover:bg-gray-200 rounded-5px hover:cursor-pointer"
					/>
				</Typography>
			</Popover>
		</div>
	);
}
