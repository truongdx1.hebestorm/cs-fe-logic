import { Editor } from '@tinymce/tinymce-react';

import tinymce from 'tinymce/tinymce';

import 'tinymce/themes/silver';

import 'tinymce/icons/default';

import 'tinymce/skins/ui/oxide/skin.min.css';

import { useRef } from 'react';

export default function TinyEditorComponent() {
	const editorRef = useRef(null);

	return (
		<>
			<Editor
				onInit={(evt, editor) => (editorRef.current = editor)}
				initialValue="<p>Please take a look at ticket CS20211221009681 raised  by Allison Kienlen (bkearse1982@gmail.com).</p>"
				init={{
					height: 150,
					menubar: false,
					plugins: ['code'],
					toolbar:
						'bold italic underline icons | alignjustify aligncenter link image',
					content_style:
						'body { font-family: Inter, sans-serif; border-radius: 10px;}',
				}}
			/>
		</>
	);
}
