import { useState } from 'react';
import { Button } from 'antd';
import React from "react";

const AuthorizedComponent = () => {
	const [selectedValue, setSelectedValue] = useState('a');
	const handleChange = (event) => {
		setSelectedValue(event.target.value);
	};
	const controlProps = (item) => ({
		checked: selectedValue === item,
		onChange: handleChange,
		value: item,
		name: 'color-radio-button-demo',
		inputProps: { 'aria-label': item },
	});
	return (
		<div className="border m-4 rounded">
			<div className="items-center flex flex-nowrap mt-2">
				<div className="w-7 h-7 bg-yellow-300  rounded-full relative mx-4 items-center text-center">
					<div className="border-2 border-dashed transform-box h-5 w-5 top-1 left-1 absolute bg-white border-yellow-600 rounded-full mr-4">
						<span className="absolute"></span>
					</div>
				</div>
				<p className="inline">
					<span className="font-semibold text-base">Authorized</span>
				</p>
			</div>
			<div>
				<div className="flex flex-col gap-3 py-4 mx-4 border-b">
					<div className="flex w-full text-start">
						<div className="basis-4/12">Discount</div>
						<div className="basis-7/12">VT_M4o9pPzk5d</div>
						<div className="basis-1/12">Applied</div>
					</div>
					<div className="flex">
						<div className="basis-4/12">Subtotal</div>
						<div className="basis-7/12">1 item</div>
						<div className="basis-1/12">$13.99</div>
					</div>
					<div className="flex ">
						<div className="basis-4/12">Shipping</div>
						<div className="basis-7/12">Secure Shipping (Include)</div>
						<div className="basis-1/12">$5.99</div>
					</div>
					<div className="flex">
						<div className="basis-4/12 font-semibold">Total</div>
						<div className="basis-7/12">Tracking (0.05 kg)</div>
						<div className="basis-1/12 font-semibold">$19.98</div>
					</div>
				</div>
				<div className="flex px-4 py-8 border-b">
					<p className="inline basis-11/12">Paid by customer</p>
					<p className="inline basis-1/12">$19.98</p>
				</div>
				<div className="p-5 flex flex-row-reverse">
					<Button size='large' className="p-2 border rounded btn-submit text-white">
						Capture Payment
					</Button>
				</div>
			</div>
		</div>
	);
};
export default AuthorizedComponent;
