import React, { useState } from 'react';
import CheckCircle from '../../../../Icons/CheckCircle';
const FulfilledComponent = () => {
	const [selectedValue, setSelectedValue] = useState('a');

	const handleChange = (event) => {
		setSelectedValue(event.target.value);
	};

	const controlProps = (item) => ({
		checked: selectedValue === item,
		onChange: handleChange,
		value: item,
		name: 'color-radio-button-demo',
		inputProps: { 'aria-label': item },
	});
	return (
		<div className=" rounded py-4 border m-4 items-center">
			<div className="items-center flex flex-nowrap">
				<div className="w-7 h-7 opacity-80 rounded-full relative mx-4">
					<div className="transform-box top-0.5 left-0.5 absolute rounded-full">
					<CheckCircle/>
					</div>
				</div>
				<p className="inline text-sm">
					<span className="font-semibold text-base">Fulfilled (3)</span>{' '}
					<span className="text-sm">#21204149-GBS-F1</span>
				</p>
			</div>
			<div className="flex m-4 items-center gap-4 ">
				<div className="basis-1/12">
					<img
						className="w-14 h-14"
						src="https://s3-alpha-sig.figma.com/img/89c9/0e46/6195d794d17225f4afe7467294b1f71c?Expires=1645401600&Signature=Iv9gxPpsfZx6bB~FNBeQYwXVcF0kSk8iEkVY4FRW6CE7wcmZ4sj~NRzRDTA9rlmbeE2~HzXFLeUvqWAsTdvF7oIbhVz56nURB8EeXj-MGYTaDgRvSDUA10WzgSo7gdXSuec1CxIl16TeKTvdaCOXzHzrcd~a0ZsnJOnUWmcuEKXG0fE1gbZ2U4FUFMCPsXJ3pePK7dOK2hYHrGq7H1Efgtl61gYzM~Z~qytJK0b1w7GcGeoRMythuetR6Pjn5oBbs6DTv9eifv01mX94Ha3gyYa-kPpwkEEa5-B43~PbST5YMW3YZO3lqb-8gMeFt1HKo9TBNcuw1XigU4hYCWc-bg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
						alt="img"
					/>
				</div>
				<div className="basis-11/12 flex gap-4 text-sm">
					<div className="basis-8/12 leading-6">
						<p className="uppercase blue">Hippie life 0022</p>
						<p className="capitalize">Face mask 3.5D /26cm X 16cm / 1PC</p>
						<p>SKU: POD_202013958</p>
						<p className="capitalize">Custom info : name: "PeterParker"</p>
					</div>
					<div className="basis-3/12">$13.99 x 1</div>
					<div className="basis-1/12 text-end">$13.99</div>
				</div>
			</div>
		</div>
	);
};
export default FulfilledComponent;
