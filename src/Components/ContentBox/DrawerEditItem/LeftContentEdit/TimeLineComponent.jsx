
import {Checkbox} from "antd";
import { Timeline } from 'antd';
import React from "react";
import SearchForm from '../../DrawerLineItem/SearchForm';

const steps = [
	{
		time: 'january 09',
		description:
			'We sent a order confirmation email to Machael Meyers (meyers41250@gmail.com)',
		hour: '21:22',
	},
	{
		time: '',
		description: 'A $85.84USD payment was processed on PayPal Checkout.',
		hour: '21:21',
	},
	{
		time: '',
		description:
			'Michael Meyers placed this order Online Store (checkout #4538)',
		hour: '21:20',
	},
	{
		time: '',
		description: 'Content is hide in design so not show in the viewer',
		hour: '21:19',
	},
];
const TimeLineComponent = () => {
	return (
		<div className="items-baseline mx-4">
			<div className="mx-4 items-center flex border-b py-4 justify-between">
				<div className="inline">
					<h2 className="inline text-lg font-semibold p-3">Timeline</h2>
				</div>
				<form className="">
					<label className="text-sm">
						<Checkbox defaultChecked size="small" />
						<span className="ml-2">Show comments</span>
					</label>
				</form>
			</div>
			<div className="w-full my-6 mx-4">
				<Timeline align="left" className="my-4">
					<Timeline.Item color="gray" style={{ marginRight: '40px' }}>
						<SearchForm text="Send" styled="w-10/12 px-2 border rounded"/>
					</Timeline.Item>
					{steps.map((item, index) => (
						<Timeline.Item className="my-4" key={index} color="gray">
							<div className="ml-2">
								<div className="flex justify-between mr-8">
									<p className="inline basis-4/5 uppercase">{item.time}</p>
									<p>{item.hour}</p>
								</div>
								<div className="mr-4">
									<p>{item.description}</p>
								</div>
							</div>
						</Timeline.Item>
					))}
				</Timeline>
			</div>
		</div>
	);
};
export default TimeLineComponent;
