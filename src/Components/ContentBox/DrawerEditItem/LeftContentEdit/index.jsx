import UnfulfilledComponent from "./UnfulfilledComponent";
import FulfilledComponent from "./FulfilledComponent";
import AuthorizedComponent from "./AuthorizedComponent";
import React from "react";
import TimeLineComponent from "./TimeLineComponent";
const LeftContentEdit = () => {
    return (<div>
        <div><UnfulfilledComponent/></div>
        <div><FulfilledComponent /></div>
        <div><AuthorizedComponent/></div>
        <div><TimeLineComponent/></div>
    </div>)
}
export default LeftContentEdit;