import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import { ClickAwayListener, Grow, ListItemText, Menu, MenuItem, MenuList, Paper, Popper } from '@material-ui/core';
import ArrowDownIcons from '../../../Icons/ArrowDownIcons';


const StyledMenu = styled((props) => (
	<Menu
		elevation={0}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'right',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'right',
		}}
		{...props}
	/>
))(({ theme }) => ({
	'& .MuiPaper-root': {
		borderRadius: '5px',
		marginTop: theme.spacing(1),
		minWidth: 180,
		color:
			theme.palette.mode === 'light'
				? 'rgb(55, 65, 81)'
				: theme.palette.grey[300],
		boxShadow:
			'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
		'& .MuiMenu-list': {
			padding: '4px 0',
		},
		'& .MuiMenuItem-root': {
			'& .MuiSvgIcon-root': {
				fontSize: 18,
				color: theme.palette.text.secondary,
				marginRight: theme.spacing(1.5),
			},
			'&:active': {
				backgroundColor: alpha(
					theme.palette.primary.main,
					theme.palette.action.selectedOpacity
				),
			},
		},
	},
}));

export default function CustomizedMenus() {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const anchorRef = React.useRef(null);
	const [open, setOpen] = React.useState(false);
	const handleToggle = () => {
		setOpen((prevOpen) => !prevOpen);
	};

	const handleClose = (event) => {
		if (anchorRef.current && anchorRef.current.contains(event.target)) {
			return;
		}

		setOpen(false);
	};

	function handleListKeyDown(event) {
		if (event.key === 'Tab') {
			event.preventDefault();
			setOpen(false);
		}
	}

	const handleClick = () => {
		// handle menu click here

		setOpen(false);
	};

	return (
		<div className="overflow-y-auto">
			<button
				ref={anchorRef}
				aria-controls={open ? 'menu-list-grow' : undefined}
				aria-haspopup="true"
				onClick={handleToggle}
				className="capitalize  border 2xl:px-4 2xl:py-1.5 md:px-0 md:py-0.5 rounded-md bg-gray hover:opacity-60 inline mobile:basis-full md:basis-8/12 2xl:basis-full text-start items-start"
			>
				<span className="font-normal">More Action</span>
				<ArrowDownIcons className="2xl:ml-2 xl:ml-0" />
			</button>
			<Popper open={open} anchorEl={anchorRef.current} transition disablePortal>
				{({ TransitionProps, placement }) => (
					<Grow
						{...TransitionProps}
						style={{
							transformOrigin:
								placement === 'bottom' ? 'center top' : 'center bottom',
						}}
					>
						<Paper>
							<ClickAwayListener onClickAway={handleClose}>
								<MenuList
									autoFocusItem={open}
									id="menu-list-grow"
									onKeyDown={handleListKeyDown}
								>
									<MenuItem onClick={handleClick}>
										<ListItemText primary="Sent mail" />
									</MenuItem>
									<MenuItem onClick={handleClick}>
										<ListItemText primary="Call Me" />
									</MenuItem>
								</MenuList>
							</ClickAwayListener>
						</Paper>
					</Grow>
				)}
			</Popper>
		</div>
	);
}
