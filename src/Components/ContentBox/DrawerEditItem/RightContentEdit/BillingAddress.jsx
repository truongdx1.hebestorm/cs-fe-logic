import ClipboardList from "../../../../Icons/ClipboardList";
import React from "react";
const BillingAddress = () => {
    return (
        <div className="mx-4 py-4">
            <div className="flex py-2">
                <p className="inline basis-10/12 capitalize">Billing Address</p>
                <a href ="# "className="basis-2/12 blue">Edit</a>
            </div>
            <div className="flex">
                <p className="basis-10/12 ">Same as Shipping address</p>
                <a href="mailto:abc@gmail.com" className="basis-2/12 text-black"><ClipboardList/></a>
            </div>
        </div>
    )
}
export default BillingAddress;