import ClipboardList from "../../../../Icons/ClipboardList";
import React from "react";
const ContactInfo = () => {

    return (
        <div className="mx-4 py-4">
        <div className="flex py-2 leading-6">
            <p className="uppercase basis-10/12">contact information</p>
            <a href="# " className="basis-2/12 blue">Edit</a>
        </div>
        <div className="flex items-baseline">
            <p className="inline mr-2 basis-10/12">barbrachurchil@gmail.com</p>
            <a href="mailto:barbrachurchil@gmail.com" className="basis-2/12 text-black"><ClipboardList/></a>
        </div>
        
        </div>
    )
}
export default ContactInfo;