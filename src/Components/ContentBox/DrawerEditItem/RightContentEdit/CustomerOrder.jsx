import ContactInfo from "./ContactInfo"
import ShippingAddress from "./ShippingAddress";
import BillingAddress from "./BillingAddress";
import React from "react";
const CustomerOrder = () => {
    return (
        <div className="border my-4 mr-4 rounded">
            <div className="p-4 border-b leading-6">
                <p className="py-4 font-semibold">Customer</p>
                <a className="blue underline my-2" href="# ">Barbra Churchill</a>
                <p className="py-2">1 order</p>
            </div>
            <div className="border-b"><ContactInfo/></div>
            <div className="border-b"><ShippingAddress/></div>
            <div><BillingAddress/></div>
           
            
        </div>
    )
}
export default CustomerOrder;