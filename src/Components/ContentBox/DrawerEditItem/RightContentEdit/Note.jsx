import React from "react";

const Notes = () => {
   return (
       <div className="py-4 border mr-4 rounded">
           <div className="flex p-2 px-4">
               <p className="basis-10/12 inline font-semibold ">Notes</p>
               <a href = "# " className="basis-2/12 blue">Edit</a>
           </div>
           <div className="py-2 px-4">
               <p>No notes from customer</p>
           </div>
       </div>
   )
}
export default Notes;