import ClipboardList from "../../../../Icons/ClipboardList";
import React from "react";
const ShippingAddress = () => {
	return (
		<div className="mx-4 py-4">
			<div className="flex py-2">
				<p className="inline uppercase basis-10/12">shipping address</p>
				<a href="# " className="basis-2/12 blue">
					Edit
				</a>
			</div>
			<div>
				<div className="flex items-baseline">
					<p className="inline basis-10/12">Barbra Churchill </p>
					<a href="mailto:barbrachurchill@gmail.com" className="basis-2/10 text-black">
						<ClipboardList />
					</a>
				</div>
				<div className="leading-6 pb-4">
					<p>5549 Oldham Street</p>
					<p>Lincoln NE 68506</p>
					<p>United States</p>
					<p>(402) 570-2216</p>
				</div>
			</div>
		</div>
	);
};
export default ShippingAddress;
