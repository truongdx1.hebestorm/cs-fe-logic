
import Notes from "./Note";
import CustomerOrder from "./CustomerOrder";
import React from "react";


const RightContentEdit = () => {
    return (
        <div className="text-sm">
        <div><Notes/></div>
        <div><CustomerOrder/></div>
        </div>
    )
}
export default RightContentEdit;