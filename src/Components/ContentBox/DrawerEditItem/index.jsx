import * as React from "react";

import LeftContentEdit from "./LeftContentEdit";
import RightContentEdit from "./RightContentEdit";
import MenuComponent from "../../ComponentReuse/MenusComponent/MenusComponent";
import DownIcons from "../../../Icons/DownIcons";
import { Button } from "antd";
import { styled } from "@material-ui/styles";
import { Box, CssBaseline, Drawer } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  })
);

export default function DrawEditItem(props) {
  const { text } = props;
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <span
        className="cursor-pointer"
        aria-label="open drawer"
        onClick={handleDrawerOpen}
        edge="start"
        sx={{ ...(open && { display: "flex" }) }}
      >
        {text}
      </span>
      <Drawer
        sx={{
          width: "100%",
          flexShrink: 1,
          "& .MuiDrawer-paper": {
            width: "50%",
            boxSizing: "border-box",
            backgroundColor: "white",
          },
        }}
        variant="persistent"
        anchor="right"
        open={open}
        className="mobile:w-screen"
      >
        <div className="items-end">
          <Button
            className="float-right mx-2 border-none"
            onClick={handleDrawerClose}
          >
            <CloseIcon className="" />
          </Button>
        </div>
        <div className="flex xl:justify-between md:justify-between items-center mx-4 my-2 mobile:flex-wrap xl:flex-nowrap gap-4 ">
          <div className="flex flex-nowrap md:basis-12/12 2xl:basis-2/3 mobile:flex-wrap">
            <div className="mobile:flex-wrap md:flex-wrap xl:flex xl:gap-x-4 md:gap-1   gap-2">
              <div className="">
                <div className="flex gap-2 items-center mobile:flex-wrap  md:flex-nowrap">
                  <span className="inline 2xl:text-xl md:text-sm font-medium">
                    #072630-GBS
                  </span>
                  <div className="bg-surface rounded-5px w-auto gap-1 flex h-5 ">
                    <span className="border-circle w-3 h-3 rounded-full my-1 ml-1 "></span>
                    <span className="md:text-xs h-auto m-0.5">
                      Payment pending
                    </span>
                  </div>
                  <div className="bg-yellow md:p-0 rounded-5px flex gap-1 text-sm h-5 ">
                    <span className="border-circle w-3 h-3 rounded-full my-1 ml-1"></span>
                    <span className="text-center sm:flex sm:text-xs m-0.5">
                      Unfulfilled
                    </span>
                  </div>
                </div>
                <p className="my-1 text-xs font-light text-secondary">
                  October 10,2021 at 09:06 pm
                </p>
              </div>
            </div>
          </div>
          <div className="xl:flex mobile:flex-wrap md:flex-nowrap mobile:w-full 2xl:flex-nowrap  gap-2 md:justify-start items-baseline  mobile:basis-full 2xl:basis-1/3 2xl:items-end">
            <Button className="md:basis-2/12  px-4 border py-1 rounded-md hover:bg-gray-300 mobile:mx-2 xl:mx-0 btn-edit">
              Edit
            </Button>
            <Button className="text-red-500 btn-refund md:basis-2/12 border py-1 rounded-md border-red-500 px-4 hover:bg-gray-300 mobile:my-2 md:mr-2 xl:mr-0 xl:my-0 ">
              Refund
            </Button>
            <MenuComponent title="More Action" icons={<DownIcons />} />
          </div>
        </div>
        <div className="flex mobile:flex-wrap xl:flex-nowrap">
          <div className="md:basis-8/12 mobile:w-full">
            <LeftContentEdit />
          </div>
          <div className="md:basis-4/12 mobile:w-full">
            <RightContentEdit />
          </div>
        </div>
      </Drawer>
      <Main open={open}></Main>
    </Box>
  );
}
