import React from "react";
const RelatedIssues = ({styled}) => {
	return (
		<div className={`border rounded-5px w-full ${styled}`}>
			<div className="p-3 mx-1 font-semibold text-sm text-emerald-700">
				<p>Related Issues</p>
			</div>
			<div className="overflow-auto h-auto border-b rounded-b-lg">
				<table className="md:table-fixed xl:w-full 2xl:w-full md:w-auto bg-white text-start border sm:table-fixed ">
					<thead className="bg-gray h-auto">
						<tr className="text-start text-xs sm:overflow-visible">
							<td className="w-auto xl:py-3 2xl:py-4 2xl:px-2">Order ID</td>
							<td className="2xl:py-4 2xl:px-2">Code</td>
							<td className="2xl:py-4 2xl:px-2">Level</td>
							<td className="2xl:py-4 2xl:px-2">Kind</td>
							<td className="2xl:py-4 2xl:px-2">Title</td>
							<td className="2xl:py-4 2xl:px-2">Note</td>
							<td className="2xl:py-4 2xl:px-2">Create At</td>
							<td className="2xl:py-4 2xl:px-2">Forward To</td>
						</tr>
					</thead>
					<tbody className="text-sm xl:truncate">
						<tr className="h-6 text-xs">
							<td className="p-2">3421492</td>
							<td className="p-2">OI-0086679</td>
							<td className="p-2">T5</td>
							<td className="md:truncate p-2">Danaged/Wrong/Missing</td>
							<td className="p-2">#6362334</td>
							<td className="mobile:truncate p-2 ">
								KH mua 1 xanh va 1 do flying orb nhung chi nhu...
							</td>
							<td className="mobile:truncate p-2">2022-01-10T14:31:30</td>
							<td className="p-2">Alex</td>
						</tr>
						<tr className="h-6 text-xs bg-gray">
							<td className="p-2">3421492</td>
							<td className="p-2">OI-0086679</td>
							<td className="p-2">T5</td>
							<td className="mobile:truncate p-2">Unmatched supplier</td>
							<td className="p-2">#6362334</td>
							<td className="mobile:truncate p-2 "></td>
							<td className="mobile:truncate p-2">2022-01-10T14:31:30</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
};
export default RelatedIssues;
