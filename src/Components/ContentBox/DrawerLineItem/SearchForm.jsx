import { Button } from "antd";
import React from "react";

const SearchForm = (props) => {
     const {text , styled } = props;
     return(
         <div className="w-full box-border">
            <form className="flex gap-2  ">
                 <input placeholder="Leave a comment..." className={`grow ${styled ? styled :"w-10/12 px-2 border rounded"}`}/>
                 <Button className="py-2 px-4 bg-blue text-white border rounded w-auto block btn-submit">{text}</Button>
            </form>
         </div>)

}
export default SearchForm;