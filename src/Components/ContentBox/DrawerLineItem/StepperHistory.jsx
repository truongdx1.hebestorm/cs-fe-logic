import * as React from 'react';
import { Timeline } from 'antd';
import SearchForm from './SearchForm';
import 'antd/dist/antd.css';
const steps = [
	{
		label: 'January 17',
		todo: [
			{
				name: 'Commit action',
				description: 'update_solution',
				user: 'Kanos Service',
				status: true,
				from: 'on issue',
				task: [
					{
						taskName: 'Solution',
						descriptionTask: 'Update design link => Process',
					},
				],
				time: '11:22',
			},
			{
				name: 'Commit action',
				description: 'add_ofer',
				user: 'Caroline N',
				status: true,
				from: 'on issue',
				task: [
					{
						taskName: 'Offer',
						descriptionTask: "Hold order and wait on Customer's feedback",
					},
					{
						taskName: 'Assignee',
						descriptionTask: 'Caroline N (linhntt.hebestorm@gmail.com)',
					},
				],
				time: '11:22',
			},
		],
	},
	{
		label: 'January 18',
		todo: [
			{
				name: 'Commit action',
				description: 'update_solution',
				user: 'Kanos Service',
				status: true,
				from: 'on issue',
				task: [
					{
						taskName: 'Offered At',
						descriptionTask: "Hold order and wait on Customer's feedback",
					},
					{
						taskName: 'Link contact offer',
						descriptionTask: 'https://hebestorm.freshdesk.com/a/tickets/187356',
					},
				],
				time: '11:22',
			},
		],
	},
];
export default function TimeLineHistory() {
	return (
		<div className="w-full m-6 h-auto">
			<Timeline align="left" className="my-4 h-auto">
				<div>
					<Timeline.Item color="gray" className="h-auto">
						<SearchForm text="Post" />
					</Timeline.Item>
				</div>
				<div className=" w-full">
					<Timeline.Item color="gray">
						<div className="mx-2">
							<p className="uppercase ">January 17</p>
							<div className="text-sm w-full">
								<div>
									<ul className="flex gap-4 ">
										<p className="basis-10/12">
											<span className="font-semibold inline">
												Kanos Service
											</span>{' '}
											finished
										</p>
										<span className="p-1 px-5  ml-4">11:22</span>
									</ul>
									<p className="">
										<span>Commit action </span>
										<em className="font-semibold">update_solution</em>
										<span> on issue</span>
									</p>
									<div>
										<ul className="list-item mx-8">
											<li>
												<span className="inline font-semibold">
													Solution:A7.
												</span>
												<p className="inline">
													{' '}
													Update design link ={'>'} process
												</p>
											</li>
										</ul>
									</div>
								</div>
								<div className="my-4">
									<div className="flex gap-4">
										<div className="basis-10/12">
											<span className="font-semibold inline">Caroline N</span>{' '}
											finished
										</div>
										<span className="p-1 px-5 ml-4">19:22</span>
									</div>
									<p className="">
										<span>Commit action </span>
										<em className="font-semibold">add_ofer</em>
										<span> on issue</span>
									</p>
									<div>
										<ul className="list-item mx-8">
											<li>
												<span className="inline font-semibold">Offered : </span>
												Hold order and wait on Customer's feedback
											</li>
											<li>
												<span className="inline font-semibold">Assignee </span>
												Caroline N (linhntt.hebestorm@gmail.com)
											</li>
										</ul>
										<div className="list-item"></div>
									</div>
								</div>
							</div>
						</div>
					</Timeline.Item>
				</div>
				<div className="">
					<Timeline.Item color="gray">
						<div className="mx-2">
							<p className="uppercase ">january 19</p>
							<div className="text-sm float-left w-full">
								<div>
									<div className="flex gap-4">
										<div className="basis-10/12">
											<span className="font-semibold inlinbe">
												Kanos Service
											</span>{' '}
											finished
										</div>
										<span className="p-1 px-5 ml-4">11:22</span>
									</div>
									<p className="">
										<span>Commit action </span>
										<em className="font-semibold">update_solution</em>
										<span> on issue</span>
									</p>
									<div>
										<ul className="list-item mx-8">
											<li>
												<span className="inline font-semibold">
													Offered at:{' '}
												</span>{' '}
												Hold order and wait on Customer's feedback
											</li>
											<li>
												<span className="inline font-semibold">
													Link contact offer:{' '}
												</span>
												https:hebestorm.freshdesk.com/a/tickets/187356
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</Timeline.Item>
				</div>
				<Timeline.Item color="gray"  activeDot={false}></Timeline.Item>
			</Timeline>
		</div>
	);
}
