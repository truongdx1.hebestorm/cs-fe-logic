import * as React from "react";
import RelatedIssues from "./RelatedIssues";
import StepperHistory from "./StepperHistory";
import { Button, Checkbox } from "antd";
import { styled, useTheme } from "@material-ui/styles";
import { Box, Drawer, FormGroup } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  })
);

export default function PersistentDrawerLeft(props) {
  const { id, name } = props;
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ boxSizing: "border-box", width: "100%" }}>
      {/* <CssBaseline /> */}
      <p
        className="cursor-pointer"
        aria-label="open drawer"
        onClick={handleDrawerOpen}
        edge="start"
        sx={{ ...(open && { display: "flex" }) }}
      >
        {id}
      </p>
      <Drawer
        sx={{
          width: "50%",
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: "50%",
            boxSizing: "border-box",
            backgroundColor: "white",
            overflow: "hidden",
          },
        }}
        variant="persistent"
        anchor="right"
        open={open}
      >
        <div className="overflow-hidden">
          <div className="items-end my-2">
            <div className="flex justify-between text-justify">
              <h2 className="text-sm font-semibold inline mx-4 my-2.5">
                #702630-GBS - {name}
              </h2>
              <Button
                className="mr-4"
                onClick={handleDrawerClose}
                icon={<CloseIcon className="" />}
              ></Button>
            </div>
          </div>

          <div className="w-full px-4">
            <RelatedIssues styled="" />
          </div>
          <div className="items-baseline text-sm flex mx-4 border-b py-4 ">
            <div className="inline mx-2 xl:basis-10/12 md:basis-8/12">
              <h2 className="inline text-sm font-semibold text-emerald-700 p-3">
                History
              </h2>
            </div>
            <div className="xl:basis-2/12 md:basis-4/12">
              <FormGroup>
                <label className="float-right inline">
                  <Checkbox defaultChecked size="small" />
                  Show history
                </label>
              </FormGroup>
            </div>
          </div>
        </div>
        <StepperHistory />
      </Drawer>
      <Main open={open}></Main>
    </Box>
  );
}
