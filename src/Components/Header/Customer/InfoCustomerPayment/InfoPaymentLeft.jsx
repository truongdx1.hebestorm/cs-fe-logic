import React from "react"
import { Button } from 'antd';
import TimeLinePayment from './TimeLinePayment';

const InfoPayment = ({ name, address, issue }) => {
	return (
		<div className="font-normal">
			<div className="border rounded-5px w-full">
				<div className="p-4 border-b my-2">
					<span className="font-semibold text-lg block mb-6">Barbra Churchill </span>
					<span className="text-sm block mb-2">Lincoln NE, United States</span>
					<span className="text-sm">Customer for 10 months</span>
				</div>
				<div className="flex justify-between w-10/12 mx-auto py-5">
					<div className="text-center ">
						<a className="underline block text-sm font-normal my-2">
							Last Order
						</a>
						<span className="font-semibold block my-2">10 months ago</span>
						<span className="text-xs my-2">From Online Store</span>
					</div>
					<div className="text-center ">
						<span className="block text-sm my-2">Total spent to date</span>
						<span className="font-semibold block my-2">$19.98</span>
						<span className="text-xs my-2">1 order</span>
					</div>
					<div className="text-center ">
						<span className="block text-sm my-2">Average order value</span>
						<span className="font-semibold my-2">$19.98</span>
					</div>
					<div className="text-center ">
						<span className="block text-sm my-2">Issues</span>
						<a className="font-semibold blue underline my-2">2 Issues</a>
					</div>
				</div>
			</div>
			<div className="border rounded-5px my-4">
				<div className="p-4">
					<span className="font-semibold text-sm">Last order placed</span>
				</div>
				<div className="flex justify-between p-4 mobile:items-center">
					<div className="flex items-baseline">
						<span className="text-sm blue">Order #381120-DOA </span>
						<div className="bg-gray xl:mx-4 flex items-center rounded-2xl text-center mt-1 text-13px px-1 mobile:text-xs">
							<span
								className="w-3 h-3 rounded-full inline-block m-1"
								style={{ backgroundColor: '#6D7175' }}
							></span>
							<span className="pr-1 ">Fulfilled</span>
						</div>
					</div>
					<div>
						<span>Mar 5, 2021 at 04:54 PM</span>
					</div>
				</div>
				<div className="mx-4">
					<span>$19.98 from Online Store</span>
				</div>
				<div className="flex my-4 items-center gap-4 border-b ">
					<div className=" ml-4">
						<img
							className="w-14 h-14"
							src="https://s3-alpha-sig.figma.com/img/89c9/0e46/6195d794d17225f4afe7467294b1f71c?Expires=1645401600&Signature=Iv9gxPpsfZx6bB~FNBeQYwXVcF0kSk8iEkVY4FRW6CE7wcmZ4sj~NRzRDTA9rlmbeE2~HzXFLeUvqWAsTdvF7oIbhVz56nURB8EeXj-MGYTaDgRvSDUA10WzgSo7gdXSuec1CxIl16TeKTvdaCOXzHzrcd~a0ZsnJOnUWmcuEKXG0fE1gbZ2U4FUFMCPsXJ3pePK7dOK2hYHrGq7H1Efgtl61gYzM~Z~qytJK0b1w7GcGeoRMythuetR6Pjn5oBbs6DTv9eifv01mX94Ha3gyYa-kPpwkEEa5-B43~PbST5YMW3YZO3lqb-8gMeFt1HKo9TBNcuw1XigU4hYCWc-bg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
							alt="img"
						/>
					</div>
					<div className=" text-sm py-4">
						<div className="basis-8/12 leading-6">
							<p className="uppercase blue">Hippie life 0022</p>
							<p className="capitalize">Face mask 3.5D /26cm X 16cm / 1PC</p>
							<p>SKU: POD_202013958</p>
						</div>
					</div>
				</div>
				<div className="flex flex-row-reverse">
					<Button size="large"className="btn-submit px-4 py-2 border rounded m-4 text-white">
						Create Order
					</Button>
				</div>
			</div>
			<div>
				<TimeLinePayment />
			</div>
		</div>
	);
};
export default InfoPayment;
