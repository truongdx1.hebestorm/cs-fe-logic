import React from "react";
import ClipboardList from "../../../../Icons/ClipboardList";

const InfoPaymentRight = ({ name, address, mailAddress , status }) => {
	return (
		<div>
			<div className="border rounded font-normal">
				<div className="border-b ">
					<div className="flex justify-between items-baseline p-4">
						<span className="font-semibold mt-4">Customer</span>
						<a>Edit</a>
					</div>
					<div className="flex justify-between items-center px-4 pb-4 my-2.5">
						<span className="blue ">barbrachurchill@gmail.com</span>
						<ClipboardList className="" />
					</div>
				</div>
				<div className="border-b my-4 p-4">
					<div className="flex justify-between mb-4">
						<span className="uppercase">Default address</span>
						<span className="blue capitalize">manage</span>
					</div>
					<div className="leading-6 my-2.5">
						<p>Barbra Churchill</p>
						<p>5549 Oldham Street</p>
						<p>Lincoln NE 68506</p>
						<p>United States</p>
						<p>(402) 570-2216</p>
					</div>
					<p className="blue">Add new address</p>
				</div>
				<div className="my-4 p-4">
					<div className="flex justify-between items-center">
						<span className="uppercase">tax settings</span>
						<span className="blue capitalize my-5">manage</span>
					</div>
					<p>No exemptions</p>
				</div>
			</div>
			<div className="p-4 border rounded my-4">
				<div className="flex justify-between mb-2.5">
					<span className="font-semibold">Status</span>
					<span className="blue">Edit status</span>
				</div>
                <span className="px-2 py-0.5 bg-primary rounded-xl text-xs text-13px mb-5 ">{"Active"}</span>
			</div>
		</div>
	);
};
export default InfoPaymentRight;
