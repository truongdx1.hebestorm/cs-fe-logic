import React from 'react';
import { FormGroup } from '@material-ui/core';
import { Checkbox } from 'antd';
import { Timeline } from 'antd';
import SearchForm from '../../../ContentBox/DrawerLineItem/SearchForm';

const steps = [
	{
		time: 'january 09',
		description:
			'We sent a order confirmation email to Machael Meyers (meyers41250@gmail.com)',
		hour: '21:22',
	},
	{
		time: '',
		description: 'A $85.84USD payment was processed on PayPal Checkout.',
		hour: '21:21',
	},
	{
		time: '',
		description:
			'Michael Meyers placed this order Online Store (checkout #4538)',
		hour: '21:20',
	},
	{
		time: '',
		description: 'Content is hide in design so not show in the viewer',
		hour: '21:19',
	},
];
const TimeLineComponent = () => {
	return (
		<div className="items-baseline ">
			<div className="items-baseline flex border-b py-4 justify-between mobile:flex-nowrap ">
				<div className="inline mx-2 md:basis-8/12 mobile:basis-1/2">
					<h2 className="inline text-lg font-semibold p-3">Timeline</h2>
				</div>
				<FormGroup className="">
					<label className="float-right  text-sm mr-6 ">
						<Checkbox defaultChecked size="small" />
						<span className="ml-2">Show comments</span>
					</label>
				</FormGroup>
			</div>
			<div className="w-full my-6">
				<Timeline align="left" className="my-4">
					<Timeline.Item color="gray">
						<div className="mr-4">
							<SearchForm text="Post" />
						</div>
					</Timeline.Item>
					{steps.map((item, index) => (
						<Timeline.Item className="my-10" key={index} color="gray">
							<div className="ml-2 mr-6">
								<div className="flex justify-between mobile:flex-nowrap">
									<div className="mobile:w-2/3 md:4/5">
										<p className="inline basis-4/5 uppercase">{item.time}</p>
										<p>{item.description}</p>
									</div>
									<p className="mr-6">{item.hour}</p>
								</div>
							</div>
						</Timeline.Item>
					))}
				</Timeline>
			</div>
		</div>
	);
};
export default TimeLineComponent;
