import * as React from 'react';
import InfoPaymentLeft from './InfoPaymentLeft';
import InfoPaymentRight from './InfoPaymentRight';
import { withRouter, useHistory } from 'react-router-dom';
import { Button } from 'antd';
import { Box, List } from '@material-ui/core';
import ArrowBackwardIcons from '../../../../Icons/ArrowBackwardIcons';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
const InfoCustomerPayment = (props) => {
	const { text, address, name, issue, mailAddress, createAt, status, shop } =
		props;
	const [open, setOpen] = React.useState(false);
	const [state, setState] = React.useState({
		bottom: open,
	});
	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}
		setOpen(true);
		setState({ ...state, [anchor]: open });
	};
	const handleKeyPress = (event) => {
		event.preventDefault();
		if (event.key === 'ESC') {
			setOpen(false);
		}
	};

	const list = (anchor, index) => (
		<Box
			key={index}
			sx={{ width: '100%' }}
			role="presentation"
			onClick={toggleDrawer(anchor, open)}
			onKeyDown={toggleDrawer(anchor, open)}
		>
			<List>{text}</List>
		</Box>
	);
	const history = useHistory();
	const handleDrawerClose = () => {
		setOpen(false);
		history.push('/customers');
	};

	return (
		<>
			<div className="md:w-8/12 mobile:w-screen mobile:box-border mx-auto my-4">
				<div className="">
					<div className="flex justify-between items-center">
						<div className="basis-8/12 items-baseline">
							<Button
								size="large"
								onClick={() => handleDrawerClose()}
								className="border p-2 mr-2 rounded btn-direct"
							>
								<ArrowBackwardIcons />
							</Button>
							<span className="text-base font-semibold mb-2">Barbra Churchill</span>
						</div>
						<div>
							<Button
								className="border p-2 mr-2 rounded btn-direct"
								size="large"
							>
								<ArrowBackIcon />
							</Button>
							<Button className="border p-2 rounded btn-direct" size="large">
								<ArrowForwardIcon />
							</Button>
						</div>
					</div>
				</div>
				<div className="flex my-4 gap-4 md:flex-wrap xl:flex-nowrap hidescroll mobile:flex-wrap">
					<div className="xl:w-2/3 md:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll ">
						<InfoPaymentLeft name={name} address={address} issue={issue} />
					</div>
					<div className="xl:w-1/3 md:w-full mobile:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll">
						<InfoPaymentRight
							name={name}
							address={address}
							mailAddress={mailAddress}
							status={status}
						/>
					</div>
				</div>
			</div>
		</>
	);
};
export default withRouter(InfoCustomerPayment);
