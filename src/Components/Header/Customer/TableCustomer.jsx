import React from "react";
import { listCustomer } from '../../../Data/dataInfoCustomer';
import { Checkbox } from 'antd';
import { PaginationMemo } from '../../Pagination';
import { useState, useEffect, useCallback } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Table } from 'antd';
const infoCustomer = {
	name: '',
	address: '',
	mailAddress: '',
	createAt: '',
	issue: '',
	status: '',
	shop: '',
};
const columns = [
	{
		title: 'Customer',
		dataIndex: 'name',
	},
	{
		title: 'Email',
		dataIndex: 'email',
	},
	{
		title: 'Created At',
		dataIndex: 'createdAt',
	},
	{
		title: 'Issues',
		dataIndex: 'issues',
	},
	{
		title: 'Status',
		dataIndex: 'status',
	},
	{
		title: 'From Shop',
		dataIndex: 'fromShop',
	},
];
const rowSelection = {
	onChange: (selectedRowKeys, selectedRows) => {
		console.log(
			`selectedRowKeys: ${selectedRowKeys}`,
			'selectedRows: ',
			selectedRows
		);
	},
	onSelect: (record, selected, selectedRows) => {
		console.log(record, selected, selectedRows);
	},
	onSelectAll: (selected, selectedRows, changeRows) => {
		console.log(selected, selectedRows, changeRows);
	},
};

const TableCustomer = () => {
	const [postsPerPage, setPostsPerPage] = useState(20);
	const [totalEntries, setTotalEntries] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	const [customerInfo, setCustomerInfo] = useState(infoCustomer);
	useEffect(() => {
		setTotalEntries(listCustomer.length);
	}, []);
	const history = useHistory();
	const handleBack = useCallback(() => {
		if (currentPage > 1) {
			setCurrentPage(currentPage - 1);
		}
	}, [currentPage]);
	const onClickInfo = (
		name,
		address,
		mailAddress,
		createAt,
		issue,
		status,
		shop
	) => {
		setCustomerInfo({
			name: name,
			address: address,
			mailAddress: mailAddress,
			createAt: createAt,
			issue: issue,
			status: status,
			shop: shop,
		});
		console.log(
			'name:' + name,
			'address:' + address,
			'mailAddress:' + mailAddress,
			'createAt:' + createAt,
			'issue:' + issue,
			'status:' + status,
			'shop:' + shop
		);
		history.push('/customers/info-payments', customerInfo);
	};
	console.log(customerInfo);
	const handleNext = () => {
		if (currentPage < Math.ceil(totalEntries / postsPerPage)) {
			setCurrentPage(currentPage + 1);
		}
		console.log('abc');
	};
	const handleChoosePage = useCallback((page) => {
		setCurrentPage(page);
	}, []);
	const handleSetPostsPerPage = useCallback((entries) => {
		setPostsPerPage(entries);
		setCurrentPage(1);
	}, []);

	return (
		<>
			{/* <Table
				rowSelection={{ ...rowSelection, checkStrictly: true }}
				columns={columns}
				dataSource={listCustomer}
				pagination={
					<PaginationMemo
						handleBack={handleBack}
						handleNext={handleNext}
						handleChoosePage={handleChoosePage}
						handleSetPostsPerPage={handleSetPostsPerPage}
						postsPerPage={postsPerPage}
						totalPosts={totalEntries}
						currentPage={currentPage}
					/>
				}
			/> */}
			<table className="table-auto w-full mobile:overflow-auto border-collapse">
				<thead>
					<tr className="bg-gray text-sm xl:w-2/12 border-tr">
						<td className="font-semibold py-4 px-2  flex gap-2 mobile:flex-nowrap mobile:items-center">
							<Checkbox />
							Customer
						</td>
						<td className="font-semibold p-2  xl:w-2/12">Email</td>
						<td className="font-semibold p-2  xl:w-2/12">Created at</td>
						<td className="font-semibold p-2  xl:w-2/12">Issues</td>
						<td className="font-semibold p-2  xl:w-2/12">Status</td>
						<td className="font-semibold p-2  xl:w-2/12">From Shop</td>
					</tr>
				</thead>
				<tbody>
					{listCustomer.map((item, index) => {
						if (
							index >= (currentPage - 1) * postsPerPage &&
							index < currentPage * postsPerPage
						) {
							return (
								<tr key={index} className="text-sm border-tr">
									<td className="p-2 flex items-center  mobile:flex-nowrap gap-2">
										<Checkbox />
										<div
											onClick={() => {
												onClickInfo(
													item.name,
													item.address,
													item.email,
													item.createdAt,
													item.issues,
													item.status,
													item.fromShop
												);
											}}
											className="cursor-pointer"
										>
											<span className="font-semibold">{item.name}</span>
											<span className="text-sm block">{item.address}</span>
										</div>
									</td>
									<td className="p-2 ">{item.email}</td>
									<td className="p-2 ">{item.createdAt}</td>
									<td className="p-2 ">{item.issues}</td>
									<td className="p-2 ">
										{item.status ? (
											<span
												style={{ backgroundColor: '#AAF2C8' }}
												className="rounded-xl px-2 py-0.5 text-sm"
											>
												Active
											</span>
										) : (
											<span
												style={{ backgroundColor: '#E0E0E0' }}
												className="rounded-xl px-2 py-0.5 text-sm"
											>
												Unactive
											</span>
										)}
									</td>
									<td className="p-2 ">{item.fromShop}</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>

			<div className="p-5">
				<PaginationMemo
					handleBack={handleBack}
					handleNext={handleNext}
					handleChoosePage={handleChoosePage}
					handleSetPostsPerPage={handleSetPostsPerPage}
					postsPerPage={postsPerPage}
					totalPosts={totalEntries}
					currentPage={currentPage}
				/>
			</div>
		</>
	);
};
export default TableCustomer;
