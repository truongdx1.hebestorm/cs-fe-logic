import React from "react";
import MenusComponent from '../../ComponentReuse/MenusComponent/MenusComponent';
import TableCustomer from './TableCustomer';
import DownIcons from './../../../Icons/DownIcons';
import MoreIcons from '../../../Icons/MoreIcons';
import SearchIcons from './../../../Icons/SearchIcons';
const Customer = () => {
	return (
		<div>
			<div className="m-4 border rounded-5px">
				<div className="flex m-4 gap-2 mobile:flex-wrap xl:flex-nowrap items-center">
					<div className="mobile:w-full flex-grow xl:basis-10/12 md:basis-12/12 flex gap-2 md:my-4 xl:my-0">
						<MenusComponent icons={<MoreIcons />} />
						<div className="w-full mobile:flex mobile:w-full md:flex bg-gray border rounded p-1 overflow-hidden items-center ">
							<SearchIcons styled={"ml-2 mr-1"} />
							<input
								placeholder={`Search template...`}
								className="border-none mobile:w-full md:w-full inline visited:border-none bg-gray focus:outline-none ml-1 "
							/>
						</div>
					</div>
					<div className="flex gap-2 mobile:items-center md:items-center">
						<MenusComponent title="Status" icons={<DownIcons />} />
						<MenusComponent title="From Shop" icons={<DownIcons />} />
						<MenusComponent icons={<MoreIcons />} />
					</div>
				</div>
				<div className="mobile:overflow-scroll mobile:w-full md:overflow-auto">
					<TableCustomer />
				</div>
			</div>
		</div>
	);
};
export default Customer;
