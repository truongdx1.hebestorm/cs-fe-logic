import LeftContent from "./LeftContent/LeftContent";
import RightContent from "./RightContent/RightContent";
import React from "react"
const Issues = () => {
    return (
        <div className="flex mt-4 xl:flex-nowrap mobile:flex-wrap mx-4 lg:gap-4">
            <div className="xl:w-6/12 moblie:w-full md:w-full mobile:overflow-hidden xl:overflow-scroll md:h-auto xl:h-screen hidescroll">
                <LeftContent />
            </div>
            <div className="xl:w-6/12 mobile:w-full md:w-full xl:overflow-scroll hidescroll md:h-auto xl:h-screen">
                <RightContent />
            </div>
        </div>
    );
};
export default Issues;