import { dataCustomer } from '../../../../Data/dataCustomer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileVideo } from '@fortawesome/free-solid-svg-icons/faFileVideo';
import ForwardIcons from '../../../../Icons/ForwardIcons';
import { Popup } from 'reactjs-popup';
import FormSearchTemplate from './FormSearchTemplate';
import FormFoward from '../../../ContentBox/DrawerContainer/FormFoward';
import AddIcons from '../../../../Icons/AddIcons';
import TableOrderInfor from '../../../../Components/ComponentReuse/TableOrderInfo';
import TrashIcons from '../../../../Icons/TrashIcons';
import DownloadIcons from '../../../../Icons/DownloadIcons';
import ChatIcons from '../../../../Icons/ChatIcons';
import NoteIcons from '../../../../Icons/NoteIcons';
import DocumentIcons from '../../../../Icons/DocumentIcons';
import { Button } from 'antd';
import React from "react";

const avatarDefaultByName = (str) => {
	return str[0];
};
const li = [
	'Tracking number active',
	'Order not found',
	'Ask for proof',
	"HoachId's Template",
];
const CustomerComment = () => {
	const onHandleClick = (e) => {
		e.preventDefault();
	};
	return (
		<>
			<div className="md:text-xs md:overflow-hidden xl:text-base ">
				{dataCustomer.map((item, index) => (
					<div
						key={item.id}
						className={`${
							index % 2 === 0 ? 'bg-white' : 'bg-gray1'
						} box-border h-auto`}
					>
						<div className="flex mx-2 pt-4 items-center justify-between">
							<div className="flex items-center">
								<div
									className={`h-8 w-8 mx-2 mt-1 text-center font-bold text-secondary rounded-5px ${
										index % 2 === 0 ? 'avatarA' : 'avatarJ'
									}`}
								>
									<span className={`inline-block my-auto md:p-2 xl:p-1`}>
										{avatarDefaultByName(item.name)}
									</span>
								</div>
								<div className="inline ">
									<span className="blue text-sm">{item.name}</span>
									<span className="text-xs text-gray-400 block">
										2 days ago({item.createAt})
									</span>
								</div>
							</div>
							<div className="xl:basis-1/12 text-end m-2 mobile:flex-row-reverse flex xl:flex-nowrap md:flex-wrap">
								<div>
									<FormFoward />
								</div>
								{item.id % 2 === 0 ? (
									<Button
										icon={<TrashIcons margin="ml-1.5 icons-gray" />}
										className="border items-center text-center justify-center rounded-5px  hover:opacity-70 mr-3"
									></Button>
								) : null}
							</div>
						</div>
						<div className="leading-7 mx-4 my-2">
							<span className="font-bold text-sm block">
								{item.comment.title ? item.comment.title : ''}
							</span>
							<span className="text-sm">{item.comment.content}</span>
							<br />
							<span className="text-sm  block mt-4 mb-2 pb-4">
								{item.comment.end ? item.comment.end : ''}
							</span>
							<span className="mb-2  text-sm pb-2">
								{item.comment.signature ? item.comment.signature : ''}
							</span>
						</div>
						{item.id === 1 ? (
							<div className="px-2">
								<div className="my-4 flex xl:flex-nowrap md:flex-nowrap 2xl:flex-nowrap w-64  border border-gray-400 mx-2 gap-2 rounded-5px p-3 items-center">
									<div className="flex py-2">
										<div className="px-2">
											<FontAwesomeIcon
												icon={faFileVideo}
												size="3x"
												className="mx-1"
											/>
										</div>
										<div>
											<span className="text-sm">Video.mov</span>
											<span className="text-xs text-gray-400 block">
												6.69MB
											</span>
										</div>
									</div>
									<div className="flex">
										<Button
											icon={<DownloadIcons className="mx-1.5" />}
											className="btn-none-border"
										></Button>
										<Button
											icon={<ForwardIcons className="mx-1.5" />}
											className="btn-none-border"
										></Button>
										<Button
											icon={<TrashIcons margin="mx-1.5" />}
											className="btn-none-border"
										></Button>
									</div>
								</div>
								<div className=" m-2 overflow-auto">
									<TableOrderInfor className="table-auto 2xl:w-full xl:w-auto overflow-auto font-normal mx-auto bg-white border border-gray-300 mb-5 md:w-full" />
								</div>
							</div>
						) : null}
					</div>
				))}
				<div className="flex bg-gray1 p-4 items-center mobile:flex-wrap md:flex-nowrap">
					<div className="h-8 w-8 mr-4 text-center items-center font-bold text-avatar justify-center rounded-5px avatarJ mobile:mb-4 md:mb-0">
						<span className="inline-block my-auto p-1">J</span>
					</div>
					<div className="mobile:mb-4 md:mb-0 mobile:text-xs">
						<Button
							icon={<ChatIcons />}
							className="bg-gray text-sm border btn-cancel mr-2 capitalize"
						>
							reply
						</Button>
					</div>
					<div className="mobile:mb-4 md:mb-0 mobile:text-xs">
						<Button icon={<NoteIcons />} className="btn-cancel capitalize mr-2">
							add note
						</Button>
					</div>
					<div className="mobile:mb-4 md:mb-0 mobile:text-xs">
						<Popup
							trigger={(open) => (
								<Button
									icon={<DocumentIcons />}
									className="btn-cancel mr-2 capitalize"
								>
									template
								</Button>
							)}
							position="top center"
							closeOnDocumentClick
							open={false}
							onClose={() => onHandleClick()}
						>
							{(close) => (
								<FormSearchTemplate
									show={false}
									text={li}
									icons={<AddIcons />}
									close={close}
								/>
							)}
						</Popup>
					</div>
					<div className="bg-gray mb-4 mobile:text-sm xl:mt-4 w-26">
						<FormFoward
							text="forward"
							styled="w-full btn-cancel "
							styleIcons="ml-2.5 inline"
						/>
					</div>
				</div>
			</div>
		</>
	);
};
export default CustomerComment;
