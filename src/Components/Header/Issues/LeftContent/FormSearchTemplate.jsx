import SearchIcons from '../../../../Icons/SearchIcons';
import React from "react";
const FormSearchTemplate = ({
	show,
	text,
	icons,
	close,
	styledSearch,
	styledList,
}) => {
	return (
		<div className="bg-white border shadow drop-shadow-lg rounded-5px font-inter">
			<form className="mobile:ml-40 md:ml-0">
				<div className="flex m-2 items-center text-sm">
					<div
						className={
							styledSearch
								? styledSearch
								: 'w-10/12 flex bg-secondary border rounded p-1 overflow-hidden '
						}
					>
						<SearchIcons />
						<input
							placeholder={`Search template...`}
							className="w-full border-none visited:border-none bg-secondary focus:outline-none ml-1"
						/>
					</div>
					{icons}
				</div>
				<div className="m-2 text-sm">
					<ul>
						{text.map((item) => (
							<li
								className={
									styledList
										? styledList
										: 'hover:bg-gray-100 w-10/12 rounded p-1 list-none'
								}
							>
								<a href="/" className="block text-black">
									{item}
								</a>
							</li>
						))}
						<li
							className={
								styledList
									? styledList
									: 'hover:bg-gray-100 w-10/12 rounded p-1 cursor-pointer close list-none hover:cursor-pointer'
							}
							onClick={close}
						>
							<span>Cancel</span>
						</li>
					</ul>
					<p></p>
				</div>
			</form>
		</div>
	);
};

export default FormSearchTemplate;
