
import React ,{ useState, useEffect } from 'react';
import DownIcons from '../../../../Icons/DownIcons';
import UpIcons from '../../../../Icons/UpIcons';
import EyeIcons from '../../../../Icons/EyeIcons';
import InformationIcons from '../../../../Icons/InformationIcons';
import CustomerComment from './CustomerComment';
import { useHistory } from 'react-router-dom';
import { Button } from 'antd';
import ArrowBackwardIcons from '../../../../Icons/ArrowBackwardIcons';

const LeftContent = () => {
	const [chevron, setChevron] = useState(false);
	const [showTab, setShowTab] = useState('customer');
	const handleClick = () => {
		setChevron(!chevron);
	};
	const handleChangeTab = (text) => {
		setShowTab(text);
	};
	const history = useHistory();
	const onRedirectIssue = () => {
		history.push('/issues');
	};
	return (
		<div className="box-border sm:overflow-hidden">
			<div className="my-4 flex items-center sm:gap-0 md:text-xs mobile:flex-wrap mobile:border-b md:border-none mobile:w-full xl:text-base xl:flex-nowrap ">
				<Button
					size="large"
					className="rounded border  mobile:p-0 btn-direct"
					onClick={onRedirectIssue}
					// icon={}
				>
					<ArrowBackwardIcons />
				</Button>
				<div className=" mx-3 mobile:text-xs md:text-sm 2xl:text-sm  mr-1">
					<span className="font-semibold text-emerald-600 text-sm">
						CS20211221009681
					</span>
				</div>
				<div className="md:text-xs 2xl:text-sm md:truncate mr-6 mobile:text-xs">
					<span>Reported at: Jan 18,2022 at 09:23:53-GMT-8</span>
				</div>
				<div className=" md:text-xs md:truncate 2xl:text-sm">
					<span className="bg-new px-1.5 text-10px py-1  mr-1 rounded-md capitalize ">
						new
					</span>
					<span className="text-xs 2xl:text-sm capitalize inline">
						First response due by mon , 17 Jan 2022 , 09:16AM
					</span>
				</div>
			</div>
			<div className="flex md:text-xs md:flex-wrap xl:flex-wrap 2xl:flex-nowrap justify-between items-baseline mobile:flex-wrap">
				<div className="grow flex basis-8/12 2xl:flex-nowrap xl:flex-wrap  mobile:flex-wrap items-baseline">
					<div className="mr-4 xl:flex-nowrap md:block xl:inline text-16px md:flex-nowrap">
						<span className="inline  mobile:text-base flex-nowrap">
							<span className="border-r-2 font-medium  border-black pr-2 mr-2 ">
								Ask for Help
							</span>
							<span className="font-medium inline">#702630-GBS</span>
						</span>
					</div>
					<div className="flex gap-2 text-center md:text-xs items-baseline text-13px">
						<div className="bg-primary flex items-center rounded-2xl text-center mt-1  px-1">
							<span className="bg-emerald-800 w-3 h-3 rounded-full inline-block m-1"></span>
							<span className="pr-1">Paid</span>
						</div>
						<div className="bg-gray flex items-center rounded-2xl mt-1  px-1">
							<span className="bg-gray-500 w-3 h-3 rounded-full inline-block m-1"></span>
							<span className="pr-1">Canceled</span>
						</div>
						<div className="bg-yellow flex items-center rounded-2xl mt-1 px-1">
							<span className="border-yellow-500 border-2 w-3 h-3 rounded-full inline-block m-1"></span>
							<span className="pr-1">Unfulfilled</span>
						</div>
					</div>
				</div>
				<div className="md:basis-3/12 mobile:basis-1/2 mobile:mt-2 grid justify-items-end 2xl:basis-1/5">
					<Button
						icon={<EyeIcons />}
						className="md:flex btn-cancel mobile:flex-nowrap whitespace-nowrap mobile:w-full md:text-sm xl:text-sm  xl:ml-7 items-center text-sm inline border rounded bg-gray p-1 hover:-translate-y-0.5"
					>
						Customer screen
					</Button>
				</div>
			</div>
			<div
				className="mobile:items-center mobile:flex xl:w-full justify-between text-sm px-2 py-2 rounded-5px my-2 bg-sky border-sky "
				onClick={() => handleClick()}
			>
				<div className="items-center flex w-full justify-between">
					<InformationIcons />
					<div className="flex-nowrap flex  grow mobile:line-clamp-2">
						<span className="font-semibold md:mx-1">Note</span>
						<span>
							{`: 16/1: Nhận sai sp => check Alex
							http://apps.hebestorm.com/issues/93839`}
						</span>
					</div>
				</div>
				<span className="inline ">{chevron ? <DownIcons /> : <UpIcons />}</span>
			</div>
			<div className="border rounded-5px">
				<div className="flex mobile:flex-wrap gap-4 border-b">
					<div
						onClick={() => handleChangeTab('customers')}
						style={{
							borderBottom:
								showTab === 'customers' ? '2px solid rgb(5 150 105)' : '',
							padding: '12px 16px',
							fontWeight: 400,
						}}
						className=" md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 "
					>
						<span>Customer</span>
					</div>
					<div
						onClick={() => handleChangeTab('supplier')}
						style={{
							borderBottom:
								showTab === 'supplier' ? '2px solid rgb(5 150 105)' : '',
							padding: '12px 16px',
							fontWeight: 400,
						}}
						className=" md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 "
					>
						<span className="cursor-pointer capitalize">supplier</span>
					</div>
					<div
						onClick={() => handleChangeTab('designer')}
						className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 "
						style={{
							borderBottom:
								showTab === 'designer' ? '2px solid rgb(5 150 105)' : '',
							padding: '12px 16px',
							fontWeight: 400,
						}}
					>
						<span className="cursor-pointer capitalize">designer</span>
					</div>
					<div
						onClick={() => handleChangeTab('seller')}
						activeStyle={{
							borderBottom: '2px solid rgb(5 150 105)',
							fontWeight: '600',
						}}
						style={{
							borderBottom:
								showTab == 'seller' ? '2px solid rgb(5 150 105)' : '',
							padding: '12px 16px',
							fontWeight: 400,
						}}
						className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 "
					>
						<span className="cursor-pointer capitalize">seller</span>
					</div>
					<div
						onClick={() => handleChangeTab('csTeam')}
						style={{
							borderBottom:
								showTab == 'csTeam' ? '2px solid rgb(5 150 105)' : '',
							padding: '12px 16px',
							fontWeight: 400,
						}}
						className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 "
					>
						<span className="cursor-pointer capitalize">CS team</span>
					</div>
				</div>
				{showTab === 'customers' && <CustomerComment />}
			</div>
		</div>
	);
};
export default LeftContent;
