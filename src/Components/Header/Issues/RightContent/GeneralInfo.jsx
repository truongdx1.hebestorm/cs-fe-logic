import AtSignIcons from "../../../../Icons/AtSignIcons"
import ClipboardList from "../../../../Icons/ClipboardList";
import DrawerEditItem from '../../../ContentBox/DrawerEditItem';
import React from "react"
const GeneralInfo = () => {
	return (
		<div className="border rounded-5px text-sm" style={{ fontWeight:400,}}>
			<div className=" py-3 border-b">
				<h2 className="inline my-2 mx-4 font-semibold text-emerald-700 capitalize">
					General Info
				</h2>
				<div className="float-right blue mr-4">
					<DrawerEditItem text="Edit" />
				</div>
			</div>
			<div className="mx-4 m-2">
				<div className="flex border-b">
					<div className="md:basis-4/12 mobile:basis-1/2">
						<span className="font-semibold py-2.5">Order name</span>
					</div>
					<div className="md:basis-8/12 mobile:basis-1/2">
						<span className="py-2.5">
							#212704149-GBS <AtSignIcons className="mx-2 mb-2" fontSize='small'/>
						</span>
					</div>
				</div>

				<div className="flex border-b py-2 overflow-hidden">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Created At</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2">
						<span>
							January 14, 2022 at 9:24 pm from{' '}
							<a href="/" className="blue underline">
								Gasbystore
							</a>
						</span>
					</div>
				</div>

				<div className="flex border-b py-2 overflow-hidden">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Customer name</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2">
						<a href="/" className="blue underline">
							Allison Kienlen{' '}
						</a>
					</div>
				</div>

				<div className="flex border-b py-2 overflow-hidden">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Contact info</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2 mobile:flex md:inline mobile:flex-wrap">
						<span className="inline mr-6">bkearse1982@gmail.com</span>
						<button>
							<ClipboardList fontSize="small" />
						</button>
					</div>
				</div>
				<div className="flex border-b py-2 overflow-hidden">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Phone</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2">
						<span>(864) 381-9665</span>
					</div>
				</div>
				<div className="flex border-b py-2">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Shipping Address</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2">
						<span className="inline mr-6">Tony Swift </span>
						<button>
							<ClipboardList fontSize="small" />
						</button>
						<span className="block py-1">2312 Kellong Place</span>
						<span className="block py-1">Eureka CA 95503</span>
						<span className="block py-1">United States</span>
					</div>
				</div>
				<div className="border-b py-2">
					<div className="flex py-2">
						<div className="md:basis-4/12 mobile:w-1/2">
							<span className="font-semibold">Subtotal</span>
						</div>
						<div className="md:basis-7/12 mobile:w-1/4">
							<span>3 Items</span>
						</div>
						<div className="md:basis-1/12 mobile:w-1/4 ">
							<span className="mobile:pl-10 md:pl-0">$58.50</span>
						</div>
					</div>
					<div className="flex py-2">
						<div className="md:basis-4/12 mobile:w-1/2">
							<span className="font-semibold">Shipping Fee</span>
						</div>
						<div className="md:basis-7/12 mobile:1/4 mobile:overflow-auto">
							<span>Secured Shipping (include Tracking) (0.04 Kg)</span>
						</div>
						<div className="md:basis-1/12 mobile:w-1/4 text-end">
							<span className="mobile:pl-10 md:pl-0">$6.99</span>
						</div>
					</div>
					<div className="flex py-2">
						<div className="md:basis-4/12 mobile:w-1/2">
							<span className="font-semibold">Tip</span>
						</div>
						<div className="md:basis-7/12 mobile:w-1/4 overflow-auto">
							<span></span>
						</div>
						<div className="md:basis-1/12 text-end mobile:w-1/4">
							<span className="mobile:pl-10 md:pl-0">$5.00</span>
						</div>
					</div>
					<div className="flex py-2">
						<div className="md:basis-4/12 mobile:w-1/2">
							<span className="font-semibold">Total</span>
						</div>
						<div className="md:basis-7/12 mobile:w-1/4"></div>
						<div className="md:basis-1/12 text-end mobile:w-1/4">
							<span className="mobile:pl-10 md:pl-0">$65.49</span>
						</div>
					</div>
				</div>
				<div className="flex pb-4 mt-2">
					<div className="md:basis-4/12 mobile:w-1/2">
						<span className="font-semibold">Note</span>
					</div>
					<div className="md:basis-8/12 mobile:w-1/2 ">
						<em>Please ship it soon</em>
					</div>
				</div>
			</div>
		</div>
	);
};
export default GeneralInfo;
