import DrawerEditItem from '../../../ContentBox/DrawerEditItem';
import React from "react";
const IssueInfo = () => {
	return (
		<div className="border my-2 rounded-5px text-sm">
			<div className="py-3 border-b ">
				<h2 className="inline my-2 mx-4 font-bold text-emerald-700 capitalize">
					Issue info
				</h2>
				<div className="float-right blue mr-4">
					<DrawerEditItem text="Edit" />
				</div>
			</div>
			<div className="w-full">
				<div className="mx-4 m-2 border-b flex ">
					<span className="py-1 md:basis-4/12 mobile:basis-1/2 font-semibold">Company</span>
					<span className="py-1 md:basis-8/12 mobile:basis-1/2">HBS</span>
				</div>
				<div className="mx-4 border-b flex">
					<div className="md:basis-4/12 mobile:basis-1/2 py-1 font-semibold">
						<span className="font-semibold">Agent</span>
					</div>
					<div className="md:basis-8/12 mobile:basis-1/2 py-1">
						<span>Alice P</span>
					</div>
				</div>
				<div className="m-2 mx-4 border-b  flex">
					<span className="md:basis-4/12 mobile:basis-1/2 font-semibold py-1">Level 1</span>
					<span className="md:basis-8/12 mobile:basis-1/2 py-1">Khiếu nại</span>
				</div>
				<div className="m-2 mx-4 border-b flex">
					<div className="md:basis-4/12 mobile:basis-1/2  py-1">
						<span className="font-semibold">Level 2</span>
					</div>
					<div className="md:basis-8/12 mobile:basis-1/2 py-1">
						<span>Chất lượng sản phẩm</span>
					</div>
				</div>
				<div className="my-1 mx-4 flex">
					<div className="md:basis-4/12 mobile:basis-1/2 font-semibold py-1">
						<span className="font-semibold">Level 3</span>
					</div>
					<div className="md:basis-8/12 mobile:basis-1/2 py-1">
						<span>Sai mẫu mã</span>
					</div>
				</div>
			</div>
		</div>
	);
};
export default IssueInfo;
