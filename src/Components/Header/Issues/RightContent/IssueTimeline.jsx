import { Avatar } from '@material-ui/core';
import { dataIssueTimeline } from '../../../../Data/dataCustomer';
import React from "react"
export const avatarUser = (str) => {
	const reg = /(?=.*[A-Z])/;
	const newArr = [];
	for (let i = 0; i < str.length; i++) {
		if (reg.test(str[i])) {
			newArr.push(str[i]);
		}
	}
	return newArr.join('');
};
const IssueTimeline = () => {
	return (
		<div className="border my-2 rounded-5px ">
			<div className="pt-4 text-sm">
				<span className="font-semibold text-emerald-700 mx-4">
					Issue Timeline
				</span>
			</div>
			<div className="items-baseline">
				{dataIssueTimeline.map((item, index) => (
					<div
						className="my-2 mx-4 rounded-5px border sm:overflow-hidden box-border items-baseline"
						key={index}
					>
						<div className="flex items-baseline mx-1 ">
							<div className="m-1 text-10px">
								<div direction="row" spacing={1}>
									<Avatar
										sx={{
											bgcolor: '#FFD79D',
											padding: '4px',
											marginTop: '4px',
											width: '24px',
											height: '24px',
											fontSize: '10px',
											color: '#000000',
											fontWeight:"normal",
										}}
									>
										{avatarUser(item.user)}
									</Avatar>
								</div>
							</div>
							<span className="font-semibold text-sm mr-2 ml-1">
								{item.user}
							</span>
							<span className="text-10px mr-2">{item.createAt}</span>
							<button
								className={` border rounded-md text-white text-10px px-1 py-1 items-center ${
									item.status === 'Closed'
										? 'bg-close'
										: item.status === 'Resolved'
										? 'bg-resolve'
										: 'bg-open'
								}`}
							>
								{item.status}
							</button>
						</div>
						<div className="pb-3 mx-3">
							<span className="m-2 text-sm">{item.note}</span>
						</div>
					</div>
				))}
			</div>
		</div>
	);
};
export default IssueTimeline;
