import DrawerLineItem from '../../../ContentBox/DrawerLineItem';
import React from "react";
const OrderDetail = () => {
	return (
		<div className="border rounded-5px my-2 overflow-auto box-border">
			<div className="py-2 text-sm flex justify-between">
				<h2 className="inline m-2 font-semibold text-emerald-700 capitalize mx-4">
					Order detail
				</h2>
				<a href="/" className="mr-4 blue">
					Edit
				</a>
			</div>
			<table className="table-auto border-none 2xl:w-full font-normal xl:w-full mx-auto bg-white text-xs md:w-full">
				<thead className="bg-gray text-10px border-thead">
					<tr className="border-tr">
						<td className="py-4 px-2 none-border">Lineitem SKU</td>
						<td className="p-2 none-border">Lineitem Name</td>
						<td className="p-2 none-border">
							Link Design<p>Link Ali/Sup</p>
						</td>
						<td className="p-2 none-border">Level</td>
						<td className="p-2 none-border">Supplier</td>
						<td className="p-2 none-border">Tracking</td>
						<td className="p-2 none-border">Carrier</td>
						<td className="p-2 none-border">TKN Uploaded At</td>
						<td className="p-2 none-border">TKN Status</td>
						<td className="p-2 none-border">Est Time</td>
					</tr>
				</thead>
				<tbody className="text-start">
					<tr className="text-10px border-tr">
						<td className="p-2 none-border">
							<DrawerLineItem id="POD_20217641" name="Fleece Blanket Id" />
						</td>
						<td className=" px-2 none-border truncate">Fleece Blanket Id </td>
						<td className="px-2 none-border">Link</td>
						<td className="px-2 none-border">L7.8</td>
						<td className="px-2 none-border">Alex</td>
						<td className=" px-2 none-border truncate">YT2135121236038136</td>
						<td className="px-2 none-border">YunExpress</td>
						<td className=" px-2 none-border truncate">2022-01-10T14:31:30</td>
						<td className=" px-2 none-border truncate">In trasit</td>
						<td className="px-2 none-border truncate overflow-ellipsis">
							Jan 29,2022
						</td>
					</tr>
					<tr className="bg-gray text-10px border-tr">
						<td className="p-2 none-border">
							<DrawerLineItem id="POD_20217641" name="Fleece Blanket Id" />
						</td>
						<td className=" px-2 none-border truncate">Fleece Blanket Id</td>
						<td className="px-2 none-border">Link</td>
						<td className="px-2 none-border">L7.8</td>
						<td className="px-2 none-border">Kyle Cal</td>
						<td className=" px-2 none-border truncate">YT2135121236038136</td>
						<td className="px-2 none-border">YunExpress</td>
						<td className=" px-2 none-border truncate">2022-01-10T14:31:30</td>
						<td className=" px-2 none-border truncate">In trasit</td>
						<td className="px-2 none-border truncate overflow-ellipsis">
							Jan 28,2022
						</td>
					</tr>
					<tr className="text-10px none-border">
						<td className="p-2 none-border">
							<DrawerLineItem id="POD_20217641" name="Fleece Blanket Id" />
						</td>
						<td className="px-2 none-border truncate">Fleece Blanket Id</td>
						<td className="px-2 none-border">Link</td>
						<td className="px-2 none-border">L7.8</td>
						<td className="px-2 none-border">Alex</td>
						<td className="px-2 none-border truncate">YT2135121236038136</td>
						<td className="px-2 none-border">YunExpress</td>
						<td className="px-2 none-border truncate">2022-01-10T14:31:30</td>
						<td className="px-2 none-border truncate">In trasit</td>
						<td className="px-2 none-border truncate">
							Jan 29,2022
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
};
export default OrderDetail;
