import IssueInfo from "./IssueInfo";
import OrderDetail from "./OrderDetail"
import GeneralInfo from "./GeneralInfo";
import IssueTimeline from "./IssueTimeline";
import React from "react";
const RightContent = () => {
    return (
        <>
        <div className="w-full">
            <div className="w-full"><IssueInfo/></div>
            <div className=""><GeneralInfo/></div>
            <div className=""><OrderDetail/></div>
            <div className=""><IssueTimeline/></div>
        </div>

        </>
    ); 
};

export default RightContent;