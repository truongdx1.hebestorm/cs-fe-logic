import { Button, Checkbox } from "antd";
import { NavLink, Route, Switch, useHistory } from "react-router-dom";
import React, { useState, useEffect, useCallback } from "react";
import { dataCustomerIssue, dataIssue } from "../../../../Data/dataIssue";
import { PaginationMemo } from "./../../../Pagination/index";
import DotComponent from "../../../ComponentReuse/Dot";
import PopoverComponent from "../../../ComponentReuse/PopoverComponent";
import { request } from "../../../../_services";
import config from "config";
import * as types from "../../../../_actions/user.actions.js";
// import dataUser from "../../../../_reducers/users.reducer.js";
import parse from "html-react-parser";

const removeTagHtml = (text) => {
  const regex = /(<([^>]+)>)/gi;
  const result = text.replace(regex, "");
  return result;
};
const TableCustomerIssue = () => {
  const [postsPerPage, setPostsPerPage] = useState(20);
  const [totalEntries, setTotalEntries] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [userList, setUserList] = useState([]);
  const [watch, setWatch] = useState(false);
  const [listIssue , setListIssue] = useState([]);
  useEffect(() => {
    request
      .get(`${config.issueApiUrl}/api/cs_issues`)
      .then((res) => setUserList(res.data.data.result))
      .catch((err) => console.log(err));
  }, []);
  //
  useEffect(() => {
    setTotalEntries(userList.length);
  }, [userList]);
  const handleBack = useCallback(() => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }, [currentPage]);
  const handleNext = () => {
    if (currentPage < Math.ceil(totalEntries / postsPerPage)) {
      setCurrentPage(currentPage + 1);
      request
        .get(`${config.issueApiUrl}/api/cs_issues`, { limit: 1, page: 1 })
        .then((res) => setUserList(res.data.data.result))
        .catch((err) => {
          err;
        });
    }
    console.log(userList);
  };

  const handleChoosePage = useCallback((page) => {
    setCurrentPage(page);
  }, []);
  const handleSetPostsPerPage = useCallback((entries) => {
    setPostsPerPage(entries);
    setCurrentPage(1);
  }, []);
  const backgroundTheme = (text) => {
    let background = "";
    if (text.toLowerCase() === "new") {
      background = "#AEE9D1";
    } else if (text.toLowerCase() === "wait_on_3rd_party") {
      background = "#8E59FF";
    } else if (text.toLowerCase() === "wait_for_customer") {
      background = "#2F80ED";
    } else if (text.toLowerCase() === "pending") {
      background = "#F2C94C";
    } else if (text.toLowerCase() === "resolved") {
      background = "#836100";
    } else if (text.toLowerCase() === "closed") {
      background = "#6F767E";
    } else background = "#27AE60";
    return background;
  };
  console.log(listIssue);
  const history = useHistory();
  const handleClickToInforIssues = () => {
    history.push("/issues/issue-info");
  };
  const changeText = (text) => {
    return text ? text.replaceAll("_", " ") : "";
  };
  const watchingStatus = () => {
    setWatch(true);
  };
  return (
    <>
      <div className="mobile:overflow-auto">
        <table className="xl:table-fixed mobile:table-auto w-full rounded-5px hide-order mobile:overflow-auto">
          <thead>
            <tr className="bg-gray border-y">
              <td className="px-2 py-2.5 none-border">
                <Checkbox />
                <span className="mx-2">Code</span>
              </td>
              <td className="px-2 py-2.5 border-y none-border truncate">
                Order name
              </td>
              <td className="px-2 py-2.5 none-border truncate">
                Customer Message
              </td>
              <td className="px-2 py-2.5 none-border">Kind</td>
              <td className="px-2 py-2.5 none-border truncate">Created at</td>
              <td className="px-2 py-2.5 none-border truncate">Agent Note</td>
              <td className="px-2 py-2.5 none-border truncate">
                Time Remaining
              </td>
              <td className="px-2 py-2.5 none-border truncate">CS State</td>
              <td className="px-2 py-2.5 none-border ">Agent</td>
              <td className="px-2 py-2.5 none-border truncate">Kind level 1</td>
              <td className="px-2 py-2.5 none-border truncate">Kind level 2</td>
              <td className="px-2 py-2.5 none-border truncate">Kind level 3</td>
            </tr>
          </thead>
          <tbody>
            {userList
              ? userList.map((item, index) => {
                  if (
                    index >= (currentPage - 1) * postsPerPage &&
                    index < currentPage * postsPerPage
                  ) {
                    return (
                      <tr
                        key={item.customer_id}
                        className={
                          index % 2 !== 0
                            ? "bg-gray border-tr"
                            : "bg-white border-tr"
                        }
                      >
                        <td className="px-2 py-2.5 none-border truncate  cursor-pointer">
                          <Checkbox />
                          <span
                            className="mx-2"
                            onClick={() => handleClickToInforIssues()}
                          >
                            {item.code}
                          </span>
                        </td>
                        <td className="px-2 py-2.5 none-border truncate ">
                          {item.order_name}
                        </td>
                        <td className="px-2 py-2.5 none-border cursor-pointer ">
                          <PopoverComponent text={item.content} styled="line-clamp-1" />
                        </td>
                        <td className="px-2 py-2.5 none-border truncate cursor-pointer capitalize">
                          <PopoverComponent text={changeText(item.kind)} />
                        </td>
                        <td className="px-2 py-2.5 none-border truncate ">
                          {item.created_at}
                        </td>
                        <td className="px-2 py-2.5 none-border truncate cursor-pointer first-letter">
                          <PopoverComponent
                            text={item.note}
                            styled={"capitalize"}
                          />
                        </td>
                        <td className="py-3.5 my-auto none-border truncate  flex flex-nowrap ">
                          <DotComponent
                            bgColor={watch ? "bg-red" : "bg-gray2"}
                          />
                          <span className="" onClick={watchingStatus}>
                            {item.cs_state_overdue_at}
                          </span>
                        </td>
                        <td className="px-2 py-2.5 none-border truncate cursor-pointer">
                          <PopoverComponent
                            text={changeText(item.cs_state)}
                            item={changeText(item.cs_state)}
                            bg={backgroundTheme(item.cs_state)}
                            styled={`px-1 rounded-5px py-0.5 text-10px capitalize  ${
                              item.state == "New" ? "text-black" : "text-white"
                            }`}
                          />
                        </td>
                        <td className="px-2 py-2.5 none-border truncate ">
                          {item.customer_support
                            ? item.customer_support.name
                            : ""}
                        </td>
                        <td className="px-2 py-2.5 none-border truncate capitalize">
                          {changeText(item.kind_level_1)}
                        </td>
                        <td className="px-2 py-2.5 none-border truncate capitalize">
                          {changeText(item.kind_level_2)}
                        </td>
                        <td className="px-2 py-2.5 none-border truncate capitalize	">
                          {changeText(item.kind_level_3)}
                        </td>
                      </tr>
                    );
                  }
                })
              : null}
          </tbody>
        </table>
      </div>
      <div className="p-5">
        <PaginationMemo
          handleBack={handleBack}
          handleNext={handleNext}
          handleChoosePage={handleChoosePage}
          handleSetPostsPerPage={handleSetPostsPerPage}
          postsPerPage={postsPerPage}
          totalPosts={totalEntries}
          currentPage={currentPage}
        />
      </div>
    </>
  );
};
export default TableCustomerIssue;
