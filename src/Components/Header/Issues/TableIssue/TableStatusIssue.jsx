import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { dataStatusIssue } from "../../../../Data/dataIssue";
import ArrowDownIcons from "../../../../Icons/ArrowDownIcons";
import ArrowUpIcons from "../../../../Icons/ArrowUpIcons";
import ChervonBackwardIcons from "../../../../Icons/ChervonBackwardIcons";
import ChervonForwardIcons from "../../../../Icons/ChevronForwardIcons";
// import {userActions} from "../../../../_actions/user.actions.js"
const searchKey = (obj, text) => {
  if (obj.hasOwnProperty(text)) {
    return obj[text];
  }
};
const sumTotalStatus = (textSearch) => {
  return dataStatusIssue
    .map((item) => searchKey(item, textSearch))
    .reduce((sum, item) => sum + item);
};
const TableStatusIssue = ({ listIssue }) => {
  const [show, setShow] = useState(false);
  const [showTable, setShowTable] = useState(false);
  const handleShowTable = () => {
    return setShowTable(!showTable);
  };

  // const userList = useSelector(userActions.getAll());
  return (
    <>
      <div className="border rounded-5px">
        <div
          className="flex justify-between py-1 cursor-pointer"
          onClick={handleShowTable}
        >
          <span className="capitalize font-semibold mx-2 text-sm ">
            quick filter
          </span>
          {showTable ? <ArrowUpIcons /> : <ArrowDownIcons />}
        </div>
        <div
          className={`w-full ${
            showTable ? "overflow-auto" : "overflow-hidden"
          }`}
        >
          <table
            className={`w-full border-none table-auto ${
              showTable ? "overflow-auto" : "overflow-hidden"
            }`}
          >
            <thead>
              {listIssue ? (
                <tr className="m-2 font-semibold bg-gray text-sm text-center border-y">
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate">
                    Agent
                  </td>
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate">
                    Unresolved{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.count}
                    </span>
                  </td>
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate ">
                    Need 1st response{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.need_1st_response}
                    </span>
                  </td>
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate">
                    Open{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.open}
                    </span>
                  </td>
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate">
                    Wait for Customer{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.wait_for_customer}
                    </span>
                  </td>
                  <td className="table-head px-2 py-2.5  flex-nowrap truncate">
                    Wait for Third Party{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.wait_for_third_party}
                    </span>
                  </td>
                  <td className="table-head px-2 py-2.5 flex-nowrap truncate">
                    Pending{" "}
                    <span className="text-10px bg-white number rounded-lg inline-flex">
                      {listIssue.count?.unresolved?.pending}
                    </span>
                  </td>
                  <td
                    className="py-2.5 w-full bg-red mobile:flex cursor-pointer whitespace-nowrap  table-head  px-2 justify-between items-center flex-nowrap truncate md:text-center"
                    onClick={() => setShow(!show)}
                  >
                    <span>
                      Overdue{" "}
                      <span className="text-10px number bg-white text-black rounded-lg border-none inline-flex  mx-auto items-center">
                        {listIssue.count?.overdue?.count}
                      </span>
                    </span>
                    {show ? (
                      <ChervonBackwardIcons styled={"icons-white"} />
                    ) : (
                      <ChervonForwardIcons styled={"icons-white"} />
                    )}
                  </td>
                  {show ? (
                    <>
                      <td className="table-head px-2 py-2.5 bg-red flex-nowrap truncate">
                        Need 1st Response{" "}
                        <span className="text-10px bg-white number rounded-lg inline-flex text-black">
                          {listIssue.count?.overdue?.need_1st_response}
                        </span>
                      </td>
                      <td className="table-head px-2 py-2.5 bg-red flex-nowrap truncate">
                        Open{" "}
                        <span className="text-10px bg-white number rounded-lg inline-flex text-black">
                          {listIssue.count?.overdue?.open}
                        </span>
                      </td>
                      <td className="table-head px-2 py-2.5 bg-red flex-nowrap truncate">
                        Unresolved ({`>20days`}){" "}
                        <span className="text-10px bg-white number rounded-lg inline-flex text-black">
                          {listIssue.count?.overdue?.unresolved}
                        </span>
                      </td>
                      <td className="table-head px-2 py-2.5 bg-red flex-nowrap truncate">
                        Unclosed ({`>60days`}){" "}
                        <span className="text-10px bg-white number rounded-lg inline-flex text-black">
                          {listIssue.count?.overdue?.unclosed}
                        </span>
                      </td>
                    </>
                  ) : null}
                </tr>
              ) : null}
            </thead>
            {showTable ? (
              <tbody>
                {listIssue
                  ? listIssue.agents.map((item, index) => (
                      <tr
                        key={index}
                        className={
                          index % 2 !== 0
                            ? "text-sm bg-gray text-center border-tr"
                            : "text-sm bg-white text-center border-tr"
                        }
                      >
                        <td className="px-2 py-2.5 none-border whitespace-nowrap">
                          {item.agent?.name}
                        </td>
                        <td className="px-2 py-2.5 none-border">
                          {item.unresolved.count}
                        </td>
                        <td className="px-2 py-2.5 none-border">
                          {item.unresolved.need_1st_response}
                        </td>
                        <td className="px-2 py-2.5 none-border">{item.unresolved.open}</td>
                        <td className="px-2 py-2.5 none-border">
                          {item.unresolved.wait_for_customer}
                        </td>
                        <td className="px-2 py-2.5 none-border">
                          {item.unresolved.wait_for_third_party}
                        </td>
                        <td className="px-2 py-2.5 none-border">
                          {item.unresolved.pending}
                        </td>
                        <td className="px-2 py-2.5 none-border">
                          {item.overdue.count}
                        </td>
                        {show ? (
                          <>
                            <td className="px-2 py-2.5 none-border">
                              {item.overdue.need_1st_response}
                            </td>
                            <td className="px-2 py-2.5 none-border">
                              {item.overdue.open}
                            </td>
                            <td className="px-2 py-2.5 none-border">
                              {item.overdue.unresolved}
                            </td>
                            <td className="px-2 py-2.5 none-border">
                              {item.overdue.unclosed}
                            </td>
                          </>
                        ) : null}
                      </tr>
                    ))
                  : null}
              </tbody>
            ) : null}
          </table>
        </div>
      </div>
      <div className="my-4"></div>
    </>
  );
};
export default TableStatusIssue;
