import { dataStatusIssue } from "../../../Data/dataIssue";
import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import ArrowDownIcons from "../../../Icons/ArrowDownIcons";
import ArrowUpIcons from "../../../Icons/ArrowUpIcons";
import TableStatusIssue from "./TableIssue/TableStatusIssue";
import ChervonForwardIcons from "../../../Icons/ChevronForwardIcons";
import ChervonBackwardIcons from "./../../../Icons/ChervonBackwardIcons";
import ListOptionFilterDay from "../../ComponentReuse/ListOptionFilterDay";
import TableCustomerIssue from "./TableIssue/TableCustomerIssue";
import MoreIcons from "../../../Icons/MoreIcons";
import SearchIcons from "../../../Icons/SearchIcons";
import MenuComponent from "../../ComponentReuse/MenusComponent/MenusComponent";
import DownIcons from "../../../Icons/DownIcons";
import { getDate } from "date-fns";
import { request } from "../../../_services";
import config from "config";
import queryString from "query-string";
import toastr from "../../../common/toastr";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  fields,
  stateOptions,
  issueFields,
} from "../../../views/Issues/List/constants";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { Checkbox } from "antd";
import { useDispatch } from "react-redux";
import moment from "moment";
const useStyles = (theme) => ({
  root: {
    padding: "0 1rem",
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: "1px solid #D8D8D8",
    backgroundColor: "#fff",
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  notchedOutline: {
    borderRadius: 6,
  },
  inputRoot: {
    "&.MuiInput-underline:hover:before": {
      border: "1px solid #3476D9",
    },
  },
  input: {
    fontSize: 14,
  },
  headName: {
    fontWeight: "bold",
    borderRight: "1px solid #D8D8D8",
    borderTop: "1px solid #D8D8D8",
    borderBottom: "1px solid #D8D8D8",
    background: "#ECECEC",
    "&:last-child": {
      borderRight: "none",
    },
  },
  cellContent: {
    borderRight: "1px solid #D8D8D8",
    padding: "8px",
    "&:last-child": {
      borderRight: "none",
    },
  },
});
const ListCSState = [
  "All",
  "New",
  "Open",
  "Pending",
  "Wait on customer",
  "Wait on 3rd party",
  "Resolved",
  "Closed",
];
const listAgents = ["All", "Ashley", "Alice P", "Caroline N", "Celina V"];
const listIssueKind = [
  "All",
  "Address Confirmation",
  "Ask For Help",
  "Cancel order",
  "Change product details",
  "Change shipping details",
  "Contact PO",
  "Dispute",
  "Fraud",
  "Item not as described/Merchandise",
  "Item not received",
  "Other",
  "Out of stock",
  "Pending",
  "Request for new address",
  "Specify type of product",
  "Unauthorized transaction",
  "Unfulfilled/Late Fulfilled",
];
const listCompany = [
  "All",
  "Address Confirmation",
  "Ask For Help",
  "Cancel order",
  "Change product details",
  "Change shipping details",
  "Contact PO",
  "Dispute",
  "Fraud",
  "Item not as described/Merchandise",
  "Item not received",
  "Other",
  "Out of stock",
  "Pending",
  "Request for new address",
  "Specify type of product",
  "Unauthorized transaction",
  "Unfulfilled/Late Fulfilled",
];
const listChoosen = [
  {
    title: "Yesterday",
    value: "yesterday",
  },
  {
    title: "Last 7 days",
    value: "last_7_days",
  },
  {
    title: "Last 30 days",
    value: "last_30_days",
  },

  {
    title: "Last 90 days",
    value: "last_90_days",
  },
];

const backgroundTheme = (text) => {
  let background = "";
  if (text.lowerCase() === "new") {
    background = "#AEE9D1";
  } else if (text.lowerCase() === "wait on 3rd party") {
    background = "#8E59FF";
  } else if (text.lowerCase() === "wait on customer") {
    background = "#2F80ED";
  } else if (text.lowerCase() === "pending") {
    background = "#F2C94C";
  } else if (text.lowerCase() === "resolved") {
    background = "#836100";
  } else if (text.lowerCase() === "closed") {
    background = "#EFEFEF";
  } else background = "#27AE60";
  return background;
};
const hashUrl = (query) => {
  const newUrl = window.location.pathname + "?" + queryString.stringify(query);
  window.history.pushState({}, "", newUrl);
};
const getTabFromUrl = () => {
  const query = queryString.parse(window.location.search);
  var tab = "customer";
  if (
    query["tab"] &&
    ["customer", "system", "supplier", "seller", "designer"].includes(
      query["tab"]
    )
  ) {
    tab = query["tab"];
  } else {
    query["tab"] = tab;
  }
  hashUrl(query);
  return tab;
};

const IssueComponents = ({ props }) => {
  const [currentTab, setCurrentTab] = useState("customer");
  const [dayFilter, setDayFilter] = useState("");
  const [listIssue, setListIssue] = useState([]);
  const [loadingFilter, setLoadingFilter] = useState(true);
  const [dataFilter, setDataFilter] = useState({});
  const [formState, setFormState] = useState({
    values: {
      page: 1,
      limit: 15,
      tab: getTabFromUrl(),
    },
    // query: ""
  });
  const [totals, setTotal] = useState();
  const [count, setCount] = useState();
  const [levelCount, setLevelCount] = useState();
  const [type, setType] = useState("");
  const [right, setRight] = useState();
  const [items, setItems] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [expandFilterColumn, setExpandFilterColumn] = useState();
  const [expandFilterRow, setExpandFilterRow] = useState();
  const [dateAction, setDateAction] = useState();
  //   const dispatch = useDispatch();
  const query = (queryObject) => {
    // dispatch(showLoading());
    request.get(`${config.apiUrl}/api/cs_issues`, queryObject).then(
      (res) => {
        if (res.data.success) {
          //   dispatch(hideLoading());
          //   this.setState({
          //     items: res.data.data.result,
          //     totals: res.data.data.total,
          //     count: res.data.data.count,
          //     type: "cs_issues",
          //   });
          setItems(res.data.data.result);
          setTotal(res.data.data.total);
          setCount(res.data.data.count);
          setType("cs_issues");
          // setLevelCount(res.data.data.level_count);
        } else {
          //   dispatch(hideLoading());
          toastr.error(res.data.msg);
        }
      },
      (err) => {
        toastr.error(err);
        // dispatch(hideLoading());
      }
    );
  };
  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedItems.indexOf(id);
    let newselectedItems = [];

    if (selectedIndex === -1) {
      newselectedItems = newselectedItems.concat(selectedItems, id);
    } else if (selectedIndex === 0) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(1));
    } else if (selectedIndex === selectedItems.length - 1) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(0, -1));
    } else if (selectedIndex > 0) {
      newselectedItems = newselectedItems.concat(
        selectedItems.slice(0, selectedIndex),
        selectedItems.slice(selectedIndex + 1)
      );
    }
    setSelectedItems(newselectedItems);
  };

  const handleSelectAll = (event) => {
    let selectedItems;

    if (event.target.checked) {
      selectedItems = items?.map((t) => t.id);
    } else {
      selectedItems = [];
    }
    setSelectedItems(selectedItems);
  };
  const queryIssue = (queryObject) => {
    dispatch(showLoading());
    request.get(`${config.apiUrl}/api/issues`, queryObject).then(
      (res) => {
        if (res.data.success) {
          //   dispatch(hideLoading());
          //   this.setState({
          //     items: res.data.data.result,
          //     totals: res.data.data.total,
          //     count: res.data.data.count,
          //     type: "issues",
          //     levelCount: res.data.data.level_count,
          //   });
          setItems(res.data.data.result);
          setTotal(res.data.data.total);
          setCount(res.data.data.count);
          setType("issues");
          setLevelCount(res.data.data.level_count);
        } else {
          //   dispatch(hideLoading());
          toastr.error(res.data.msg);
        }
      },
      (err) => {
        toastr.error(err);
        // dispatch(hideLoading());
      }
    );
  };
  const handleChangeTab = (tab) => {
    setCurrentTab(tab);
  };
  useEffect(() => {
    request
      .get(`${config.issueApiUrl}/api/cs_issues/realtime_report`)
      .then((res) => setListIssue(res.data.data.result))
      .catch((err) => console.log(err));
  }, []);
  const getQuickFilters = (query1) => {
    const queryObject = Object.assign({}, {});
    var keys = query ? Object.keys(query1) : "";
    keys.map((key, index) => {
      if (typeof query1[key] == "array" || query1[key] instanceof Array) {
        if (query1[key].length > 0) {
          const arr = [];
          query1[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format("YYYY-MM-DD"));
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (
        typeof query1[key] == "object" ||
        query1[key] instanceof Object
      ) {
        queryObject[key] = query1[key].id;
      } else {
        queryObject[key] = query1[key];
      }
    });
    setLoadingFilter(true);
    setDataFilter(() => {
      request
        .get(`${config.apiUrl}/api/cs_issues/realtime_report`, queryObject)
        .then(
          (res) => {
            // this.setState({ isLoadingFilter: false });
            setLoadingFilter(false);
            if (res.data.success) {
              setDataFilter(res.data.data.result);
            } else {
              toastr.error(res.data.msg);
              setDataFilter({});
            }
          },
          (err) => {
            toastr.error(err);
            // this.setState({ isLoadingFilter: false, dataFilters: {} });
            setLoadingFilter(false);
            setDataFilter({});
          }
        );
    });
    // this.setState({ isLoadingFilter: true }, () => {

    // });
  };

  const getListItems = (query1) => {
    const queryObject = Object.assign({}, {});
    var keys = Object.keys(query1);
    keys.map((key, index) => {
      if (typeof query1[key] == "array" || query1[key] instanceof Array) {
        if (query1[key].length > 0) {
          const arr = [];
          query1[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format("YYYY-MM-DD"));
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (
        typeof query1[key] == "object" ||
        query1[key] instanceof Object
      ) {
        queryObject[key] = query1[key].id;
      } else {
        queryObject[key] = query1[key];
      }
    });
    if (formState.values.tab == "customer") {
      query(queryObject);
      getQuickFilters(formState.values);
    } else {
      queryIssue(queryObject);
    }
  };
  useEffect(() => {
    getListItems(formState.values);
    console.log(dataFilter);
  }, []);
  const filterDateAction = (date_action) => {
    console.log(date_action);
    const formState1 = Object.assign({}, formState);
    switch (date_action) {
      case "today":
        formState1.values["created_at"] = [
          moment(new Date()).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "yesterday":
        formState1.values["created_at"] = [
          moment(new Date(moment().subtract(1, "days"))).format("YYYY-MM-DD"),
          moment(new Date(moment().subtract(1, "days"))).format("YYYY-MM-DD"),
        ];
        break;
      case "last_7_days":
        formState1.values["created_at"] = [
          moment(new Date(moment().subtract(6, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "last_30_days":
        formState1.values["created_at"] = [
          moment(new Date(moment().subtract(29, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "last_90_days":
        formState1.values["created_at"] = [
          moment(new Date(moment().subtract(89, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "":
        delete formState1.values["created_at"];
        break;
      default:
        break;
    }
    // this.setState({ formState, date_action }, () => {
    //   this.getListItems(this.state.formState.values)
    // })
    setFormState(() => getListItems(formState1.values));
    setDateAction(() => getListItems(formState1.values));
  };
  return (
    <>
      <div className="m-4">
        <TableStatusIssue listIssue={listIssue} />
      </div>
      <div className="w-full px-4 ">
        <ListOptionFilterDay
          listChoosen={listChoosen}
          onClick={filterDateAction}
        />
      </div>
      <div className="border rounded-5px mx-4">
        <div>
          <div className="flex mobile:flex-wrap gap-4 capitalize border-b">
            <div
              onClick={() => handleChangeTab("customer")}
              style={{
                padding: "12px 16px",
                fontWeight: 400,
                borderBottom:
                  currentTab == "customer"
                    ? "2px solid rgb(5 150 105)"
                    : "none",
              }}
              className="md:ml-2 md:p-2 mobile:p-0 mobile:ml-0 mobile:mb-0 cursor-pointer hover:border-b-2 text-black text-sm"
            >
              Customer
            </div>
            <div
              onClick={() => handleChangeTab("supplier")}
              style={{
                padding: "12px 16px",
                fontWeight: 400,
                borderBottom:
                  currentTab == "supplier"
                    ? "2px solid rgb(5 150 105)"
                    : "none",
              }}
              className="md:ml-2 md:p-2 mobile:p-0 mobile:ml-0 mobile:mb-0 cursor-pointer hover:border-b-2 text-black text-sm"
            >
              Supplier
            </div>
            <div
              onClick={() => handleChangeTab("designer")}
              className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 cursor-pointer "
              style={{
                borderBottom:
                  currentTab == "designer"
                    ? "2px solid rgb(5 150 105)"
                    : "none",
                padding: "12px 16px",
                fontWeight: 400,
              }}
              expandFilterRow
            >
              designer
            </div>
            <div
              onClick={() => handleChangeTab("seller")}
              style={{
                borderBottom:
                  currentTab == "seller" ? "2px solid rgb(5 150 105)" : "none",
                padding: "12px 16px",
                fontWeight: 400,
              }}
              className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 cursor-pointer "
            >
              seller
            </div>
            <div
              onClick={() => handleChangeTab("csTeam")}
              activeStyle={{
                borderBottom: "2px solid rgb(5 150 105)",
                fontWeight: "600",
              }}
              style={{
                borderBottom:
                  currentTab == "csTeam" ? "2px solid rgb(5 150 105)" : "none",
                padding: "12px 16px",
                fontWeight: 400,
              }}
              className="md:p-2 hover:border-b-2 text-black text-sm mobile:p-0 mobile:ml-0 mobile:mb-0 cursor-pointer "
            >
              CS team
            </div>
          </div>
        </div>
        <div className="flex mx-2 mobile:flex-wrap xl:flex-nowrap items-center md:items-center">
          <div className="flex items-center grow ">
            <MenuComponent
              icons={<MoreIcons />}
              list={["Assign to CS", "Change CS State"]}
            />
            <div className="inline-flex grow m-2 items-center text-sm w-full">
              <div
                className={
                  "w-full flex bg-gray1 border rounded-5px p-1 overflow-hidden items-center"
                }
              >
                <SearchIcons styled="" />
                <input
                  placeholder={`Search template...`}
                  className="w-full border-none visited:border-none bg-gray1 focus:outline-none ml-2"
                />
              </div>
            </div>
          </div>
          <div className="flex gap-2 mobile:flex-wrap xl:flex-nowrap mobile:mb-2 md:mb-0 ">
            <MenuComponent
              title="CS State"
              list={ListCSState}
              icons={<DownIcons />}
            />
            <MenuComponent
              title="Agents"
              list={listAgents}
              icons={<DownIcons />}
            />
            <MenuComponent
              title="Issue Kind"
              list={listIssueKind}
              icons={<DownIcons />}
            />
            <MenuComponent
              title="Company"
              list={listCompany}
              icons={<DownIcons />}
            />
          </div>
        </div>

        <div className="">
          {currentTab == "customer" && <TableCustomerIssue />}
        </div>
        <div>
          <TableContainer>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{ width: "3%" }}
                    align="center"
                    padding="checkbox"
                    className="flex w-full "
                  >
                    <Checkbox
                      checked={selectedItems?.length === items.length}
                      color="secondary"
                      indeterminate={
                        selectedItems?.length > 0 &&
                        selectedItems?.length < items.length
                      }
                      onChange={handleSelectAll}
                    />
                  </TableCell>
                  {type == "cs_issues" &&
                    fields.map((field) => (
                      <TableCell
                        key={field.name}
                        style={{ width: field.width }}
                        align="left"
                        className=""
                      >
                        {field.label}
                      </TableCell>
                    ))}
                  {type == "issues" &&
                    issueFields.map((field) => (
                      <TableCell
                        key={field.name}
                        style={{ width: field.width }}
                        align="left"
                        className=""
                      >
                        {field.label}
                      </TableCell>
                    ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map((item, index) => (
                  <TableRow
                    key={item.id}
                    className="cursor-pointer"
                    style={{
                      backgroundColor: index % 2 == 1 ? "#ECECEC" : "#fff",
                    }}
                    onClick={() =>
                      this.props.history.push(`/${type}/${item.id}`, {
                        from: window.location.pathname + window.location.search,
                      })
                    }
                  >
                    <TableCell
                      onClick={(e) => e.stopPropagation()}
                      padding="checkbox"
                      align="center"
                      className=""
                    >
                      <Checkbox
                        checked={selectedItems?.indexOf(item.id) !== -1}
                        color="secondary"
                        onChange={(event) => handleSelectOne(event, item.id)}
                        value="true"
                      />
                    </TableCell>
                    {type == "cs_issues" &&
                      fields.map((field) => (
                        <TableCell key={field.name} className="">
                          {field.renderItem(item)}
                        </TableCell>
                      ))}
                    {type == "issues" &&
                      issueFields.map((field) => (
                        <TableCell key={field.name} className="">
                          {field.renderItem(item)}
                        </TableCell>
                      ))}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    </>
  );
};
export default withStyles(useStyles)(IssueComponents);
