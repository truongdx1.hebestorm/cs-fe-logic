import React from "react";
import { Timeline } from 'antd';
import { avatarUser } from '../../Issues/RightContent/IssueTimeline';
import SmileIcons from '../../../../Icons/SmileIcons';
import AtSignIcons from '../../../../Icons/AtSignIcons';
import AttachmentIcons from '../../../../Icons/AttachmentIcons';
import { Avatar } from '@material-ui/core';
const steps = [
	{
		time: 'january 09',
		comment: [
			{
				user: 'Jessica Nguyen',
				desc: 'Check gấp case này khách nóng lắm rồi nhé.',
				hour: '21:22',
			},
			{
				user: 'Alice P',
				desc: 'Khách không muốn nhận hàng',
				hour: '21:22',
			},
		],
	},
	{
		time: 'january 06',
		comment: [
			{
				user: 'Jessica Nguyen',
				desc: '17.1: Khách chưa nhận được hàng, muốn được refund',
				hour: '21:22',
			},
			{
				user: 'Alice P',
				desc: 'Khách không muốn nhận hàng',
				hour: '21:22',
			},
			{
				user: 'Alice P',
				desc: 'Đồng ý nhận resend',
				hour: '21:22',
			},
		],
	},
];
const AgentComment = () => {
	return (
		<>
			<div className="border rounded-5px">
				<span className="text-sm font-semibold border-b p-4 block">
					Agent Comments
				</span>
				<div className="m-4 mr-6">
					<div className="w-full m-6">
						<Timeline align="left" className="my-4">
							<Timeline.Item color="gray" style={{ marginRight: '' }}>
								<div className="w-full box-border">
									<div className="flex justify-between border mx-2 mr-4 rounded">
										<input
											placeholder="Leave a comment..."
											className="ml-2 py-2 px-1 mobile:w-20 md:w-auto focus:outline-none xl:w-full"
										/>
										<div className="flex gap-4 items-center">
											<SmileIcons />
											<AtSignIcons />
											<AttachmentIcons />
											<button className="h-8 w-16 px-4 bg-blue text-white border rounded mr-1 my-1">
												Post
											</button>
										</div>
									</div>
								</div>
							</Timeline.Item>
							{steps.map((item, index) => (
								<Timeline.Item className="my-4" key={index} color="gray">
									<div className="ml-2 overflow-hidden">
										<div className="flex">
											<p className="inline basis-4/5 uppercase">{item.time}</p>
										</div>
										<div className="">
											{item.comment.map((comment, index) => (
												<div
													className="border mr-4 my-3 rounded overflow-hidden"
													key={index}
												>
													<div className="flex gap-4 justify-between px-4 py-2">
														<div className="flex text-center items-baseline ">
															<Avatar
																sx={{
																	width: 24,
																	height: 24,
																	fontSize: 10,
																	color: '#000000',
																	backgroundColor:
																		avatarUser(comment.user) === 'AP'
																			? '#FFD8D3'
																			: '#FFD79D',
																}}
															>
																{avatarUser(comment.user)}
															</Avatar>
															<span className="mx-2 capitalize font-semibold text-xs">
																{comment.user}
															</span>{' '}
															<span className="text-10px">{comment.hour}</span>
														</div>
														<div>
															<button>
																<ClearIcon sx={{ color: '#6F767E' }} />
															</button>
														</div>
													</div>
													<div className="pb-2">
														<span className="text-sm mx-4 mb-2">
															{comment.desc}
														</span>
													</div>
												</div>
											))}
										</div>
									</div>
								</Timeline.Item>
							))}
							<Timeline.Item color="gray"></Timeline.Item>
						</Timeline>
					</div>
				</div>
			</div>
		</>
	);
};
export default AgentComment;
