import TableOrderInfor from '../../../ComponentReuse/TableOrderInfo';
import FormFoward from './../../../ContentBox/DrawerContainer/FormFoward';
import EditIcons from "../../../../Icons/EditIcons";
import React from "react";

const BuyerComponent = () => {
	return (
		<div className="my-4 break-words">
			<div className="border-b px-5 pb-2.5 ư-full">
				<span className="font-semibold text-sm capitalize">buyer comment</span>
			</div>
			<div className="my-4 px-4 w-full">
				<div className="md:flex flex- justify-between mobile:flex-nowrap  gap-4 break-words mobile:w-full mb-4">
					<div className="xl:basis-10/12 h-20  break-words word-wrap mobile:w-1/2">
						<span className="break-all mobile:text-xs xl:text-sm line-clamp-3">
							I am sending this , as I have notr yet received an email from Sean
							Paypal Customer Service.asdasdasdasda sdasdasdadsasda sdasdasdasd
							adsasdasdadasdasdasdasda
						</span>
					</div>
					<div className="xl:basis-1/12 mobile:w-1/2 xl:pl-12">
						<FormFoward />
					</div>
				</div>
				<div className="w-full mobile:overflow-auto xl:overflow-auto">
					<TableOrderInfor className="table-auto w-full mobile:overflow-x-scroll border-top-left" />
				</div>
				<div className="bg-agent w-full my-4">
					<div className="flex px-3 py-3 justify-between items-baseline">
						<span className="font-semibold text-sm my-4 capitalize">
							agent note
						</span>
						<button className=" rounded-md flex bg-orange text-sm py-1.5 px-3 text-white">
							<EditIcons fontSize="small" className="p-0.5 mr-2" />
							Edit
						</button>
					</div>
					<div className="mx-3 pb-5 flex-col overflow-hidden leading-6">
						<span className="line-clamp-3">
							Báo SUP resend + thuyết phục KH close kiện sau khi nhận TKN
							replacement
						</span>
						<span className="block line-clamp-3">
							Báo Kh bên ngoài yêu cầu close kiện
						</span>
						<span className="block line-clamp-3">18/1: TKN chưa active </span>
					</div>
				</div>
			</div>
		</div>
	);
};
export default BuyerComponent;
