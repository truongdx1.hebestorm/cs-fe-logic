import { Button } from "antd";
import React from "react";




const CaseDetailsEdit = ({ hideEdit }) => {
	return (
		<>
			<div className="text-sm mobile:w-full ">
				<div className="flex justify-between border-b">
					<span className="px-4 py-3 font-semibold text-title capitalize">
						case details
					</span>
					<div className="px-4 py-3 blue ">
						<span onClick={hideEdit} className="font-bold">
							Save
						</span>
					</div>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:basis-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						Company:
					</span>
					<span className="md:basis-2/3 py-2 mobile:basis-1/2">HBS</span>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:w-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						Argent:
					</span>
					<input
						type="text"
						className="border-none focus:border-none bg-gray w-full my-1 px-2 rounded-5px"
						placeholder="Jessica N"
					/>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:basis-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						Buyer Details:
					</span>
					<div className="md:basis-2/3 py-2 mobile:basis-1/2">
						<span className="block">Lisa Perl</span>
						<span>Lisaaperl@gmail.com</span>
					</div>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:basis-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						Disputed Amount:
					</span>
					<span className="md:basis-2/3 py-2 mobile:basis-1/2">
						$131.99 USD
					</span>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:basis-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						Transaction ID
					</span>
					<a className="md:basis-2/3 py-2 font-semibold underline mobile:basis-1/2">
						8XP20407Y33328438
					</a>
				</div>
				<div className="flex mx-4 border-b overflow-hidden">
					<span className="md:w-1/3 font-semibold capitalize py-2 mobile:basis-1/2">
						link case:
					</span>
					<input
						type="text"
						className="border-none focus:border-none bg-gray w-full my-1 px-2 rounded-5px "
						placeholder="https://www.paypal.com/resolutioncenter/view/PP-D-138831018"
					/>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:w-1/3 font-semibold capitalize py-2 underline mobile:basis-1/2">
						link email
					</span>
					<input
						type="text"
						placeholder="https://link nào đó"
						className="border-none focus:border-none bg-gray w-full my-1 px-2 rounded-5px "
					/>
				</div>
				<div className="flex mx-4 border-b">
					<span className="md:basis-1/3 font-semibold capitalize py-2 mobile:w-1/2">
						check date
					</span>
					<span className="md:basis-2/3 py-2 mobile:w-1/2">
						2022-01-21 thêm nút edit
					</span>
				</div>
				<div className="flex mx-4 items-baseline">
					<span className="md:basis-1/3 font-semibold capitalize mobile:basis-1/2">
						shipping label
					</span>
					<div className="md:basis-2/3 mb-2 mobile:w-1/2">
						<div direction="row" alignItems="center" spacing={2}>
							<label
								htmlFor="contained-button-file"
								className="flex mobile:flex-wrap xl:flex-nowrap mobile:items-center"
							>
								<input
									accept="image/*"
									id="contained-button-file"
									multiple
									type="file"
									className="hidden"
								/>
								<Button
									variant="contained"
									component="span"
									className="border bg-gray rounded px-2 p-1 my-2 mr-1 text-center item-baseline"
								>
									Upload
								</Button>
								<span> hoặc lấy thẳng từ thông tin của sup trả</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
export default CaseDetailsEdit;
