import * as React from 'react';


import CaseIDLeftContent from './CaseIdLeftContent';
import CaseIdRightContent from './CaseIdRightContent';
import { useHistory } from 'react-router-dom';
import { Box } from '@material-ui/core';
export default function CaseIdInfo(props) {
	const { text } = props;
	const [open, setOpen] = React.useState(false);
	const [state, setState] = React.useState({
		bottom: open,
	});
	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}
		setOpen(true);
		setState({ ...state, [anchor]: open });
	};

	const list = (anchor, index) => (
		<Box
			key={index}
			sx={{ width: '100%' }}
			role="presentation"
			onClick={toggleDrawer(anchor, open)}
			onKeyDown={toggleDrawer(anchor, open)}
		>
			<span>{text}</span>
		</Box>
	);
	const history = useHistory()
	const handleDrawerClose = () => {
		setOpen(false);
        history.push("/paygate-disputes" , [text])
	};

	return (
		<div className="flex mx-4 gap-4 mobile:flex-wrap xl:flex-nowrap">
			<div className="xl:w-1/2 md:w-full mobile:w-full mobile:overflow-hidden xl:overflow-scroll xl:h-screen hidescroll">
				<CaseIDLeftContent handleDrawerClose={handleDrawerClose} text={text} />
			</div>
			<div className="xl:w-1/2 md:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll mobile:w-full">
				<CaseIdRightContent />
			</div>
		</div>
	);
}
