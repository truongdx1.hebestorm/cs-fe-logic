import StepperComponent from './StepperComponent';
import BuyerComponent from './BuyerComment';
import AgentComment from './AgentComment';
import CaseResponse from './CaseResponse';
import { Button } from 'antd';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import React from "react";


const CaseIDLeftContent = ({ handleDrawerClose, text }) => {
	return (
		<>
			<div className="my-4 flex items-baseline sm:gap-0 mobile:text-xs mobile:flex-wrap xl:text-base xl:flex-nowrap md:overflow-hidden ">
				<Button
				    size='large'
					className="border rounded p-2 inline btn-direct"
					onClick={() => handleDrawerClose()}
				>
					<ArrowBackIcon />
				</Button>
				<div className="mx-3 md:text-sm 2xl:text-sm inline mr-1 text-ellipsis whitespace-nowrap">
					<p className="font-semibold text-emerald-600 text-sm">{`PP-D136530710`}</p>
				</div>
				<div className="md:text-xs 2xl:text-sm md:truncate mr-6">
					<span>Reported at: Jan 18,2022 at 09:23:53-GMT-8</span>
				</div>
				<div className=" md:text-xs md:truncate 2xl:text-sm">
					<span className="bg-new px-1.5 text-10px py-1  mr-1 rounded-md capitalize ">
						new
					</span>
					<span className="text-xs 2xl:text-sm capitalize inline">
						First response due by mon , 17 Jan 2022 , 09:16AM
					</span>
				</div>
			</div>
			<div className="w-full before:box-border after:box-border">
				<div className="block w-full">
					<h2 className="font-semibold text-base">
						<span className=" border-r-2 font-semibold pr-2">Claim</span> Not as
						described
					</h2>
				</div>
				<div className="w-full">
					<StepperComponent />
				</div>
				<div className="w-full break-words box-border border rounded-5px my-2 shadow shadow-slate-300 drop-shadow-md ">
					<BuyerComponent />
				</div>
				<div className="w-full shadow-md shadow-slate-300 rounded-5px">
					<AgentComment />
				</div>
				<div className="w-full shadow-md shadow-slate-300 rounded-5px">
					<CaseResponse />
				</div>
			</div>
		</>
	);
};
export default CaseIDLeftContent;
