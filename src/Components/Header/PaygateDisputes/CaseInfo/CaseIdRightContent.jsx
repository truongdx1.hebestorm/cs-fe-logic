import React, {useState} from "react"
import CaseDetails from './CaseDetails';
import GeneralInfo from './../../Issues/RightContent/GeneralInfo';
import OrderDetail from './../../Issues/RightContent/OrderDetail';
import RelatedIssues from '../../../ContentBox/DrawerLineItem/RelatedIssues';
import CaseDetailsEdit from "./CaseDetailsEdit";
const CaseIdRightContent = () => {
    const [showEdit , setShowEdit] = useState(false);

    const onClickShowEdit = () => {
        
        setShowEdit(true);
    }
    const onClickHideEdit = () => {
        setShowEdit(false);
    }
	return (
		<div className="w-full mobile:overflow-hidden">
			<div className=" border rounded-5px my-2 ">
				{showEdit ? <CaseDetailsEdit hideEdit={onClickHideEdit}/> : <CaseDetails showEdit={onClickShowEdit}/>}
			</div>
            <div className="">
                <GeneralInfo/>
            </div>
            <div>
                <OrderDetail/>
            </div>
            <div className="mb-4">
                <RelatedIssues styled={""}/>
            </div>
		</div>
	);
};
export default CaseIdRightContent;