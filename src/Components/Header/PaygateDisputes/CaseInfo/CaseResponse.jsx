import React, { useState } from 'react';
import EditIcons from '../../../../Icons/EditIcons';
import ClipboardList from '../../../../Icons/ClipboardList';
import UpIcons from '../../../../Icons/UpIcons';
import Button from "antd";

const CaseResponse = () => {
	const [charactorText, setCharacterText] = useState(0);
	const [focus, setForcus] = useState(false);
	const [changeText, setChangeText] = useState(false);
	const handleChange = (e) => {
		e.preventDefault();
		const value = e.target.value;
		if (value) {
			setForcus(true);
		}
		return setCharacterText(value.length);
	};

	const handleChangeText = () => {
		return setChangeText(!changeText);
	};
	return (
		<div className="my-4 border">
			<div className="border-b py-4">
				<span className="font-semibold capitalize text-sm p-4">
					case response
				</span>
			</div>
			<div className="m-4">
				<form onSubmit={() => {}}>
					<textarea
						className="border rounded-5px p-2 w-full h-52"
						maxLength={2000}
						placeholder=""
						rows={9}
						onChange={handleChange}
					/>
				</form>
				<div className="flex justify-between my-1 text-sm">
					<p>{charactorText}/2000</p>
					<div className="flex gap-4">
						<button className="py-2 px-4 bg-gray rounded text-sm flex flex-nowrap">
							<ClipboardList />
							Copy content
						</button>
						{changeText ? (
							<button
								className="py-2 px-4 bg-btn-primary rounded text-white text-sm"
								onClick={() => handleChangeText()}
								type="submit"
							>
								Save Message
							</button>
						) : (
							<button
								className="py-2 px-4 bg-gray rounded text-sm flex flex-nowrap"
								onClick={handleChangeText}
							>
								<EditIcons styled="mx-2" />
								Edit
							</button>
						)}
					</div>
				</div>
			</div>
			<div className="mx-4 items-center text-center border-dashed border-2 rounded-5px py-2 mt-4 h-52">
				<div
					direction="column"
					alignItems="center"
					spacing={2}
					className="py-4"
				>
					<label htmlFor="icon-button-file">
						<input accept="image/*" id="icon-button-file" type="file" />
						<Button
							color="success"
							aria-label="upload picture"
							component="span"
						>
							{/* <ArrowCircleUpOutlinedIcon
								fontSize="large"
								sx={{ color: '#5C5F62' }}
							/> */}
							Circle
						</Button>
					</label>
					<label htmlFor="contained-button-file" className="mt-2">
						<input
							accept="image/*"
							id="contained-button-file"
							multiple
							type="file"
						/>
						<Button
							variant="outlined"
							component="span"
							sx={{
								border: '1px solid #BABFC3',
								color: 'black',
								fontSize: '14px',
								textTransform: 'none',
							}}
						>
							Add file
						</Button>
					</label>
					<p className="my-2 text-sm">or drop file to upload</p>
				</div>
			</div>
			<div className="mx-4 mb-4 mt-1 overflow-hidden text-xs">
				<span>
					JPG GIF PNG PDF | Maximum of 10 files up to 10 MB each, 50 MB in total
				</span>
			</div>
		</div>
	);
};
export default CaseResponse;
