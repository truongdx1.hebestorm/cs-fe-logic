
import FormSendMail from '../DrawerContainer/FormSendMail';
import OrderDetailForward from './OrderDetailForward';
import ShortcutIcon from '@mui/icons-material/Shortcut';
import CloseIcon from '@material-ui/icons/Close';
import React from "react";
import { Box, Drawer } from '@material-ui/core';
const ForwardCaseInfo = () => {
	const [state, setState] = React.useState({ right: false });

	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}

		setState({ ...state, [anchor]: open });
	};
	const list = () => (
		<div className="mobile:w-full mobile:overflow-auto">
			<button className="float-right" onClick={toggleDrawer('right', false)}>
				<CloseIcon />
			</button>
			<Box
				sx={{
					width: '100%',
					flexShrink: 1,
					'& .Mui': {
						width: '50%',
						boxSizing: 'border-box',
						backgroundColor: 'white',
					},
				}}
				role="presentation"
				onKeyDown={toggleDrawer('right', false)}
			>
				<div className="m-6 border rounded-5px">
					<OrderDetailForward />
				</div>
				<div className="m-6">
					<FormSendMail />
				</div>
				<div className="w-full">
				    
				</div>
			</Box>
		</div>
	);
	return (
		<>
			<div className="w-full">
				<React.Fragment key={'right'}>
					<button
						onClick={toggleDrawer('right', true)}
						className="border rounded-md mr-3 hover:translate-y-1 sm:h-10 h-7 w-auto capitalize mobile:h-7"
					>
						<ShortcutIcon
							style={{
								color: 'GrayText',
								width: '18px',
								height: '28px',
								padding: '0px',
								margin: '0px 6px',
							}}
							fontSize="small"
						/>
					</button>
					<Drawer
						sx={{
							'& .MuiDrawer-paper': {
								width: '50%',
								boxSizing: 'border-box',
								backgroundColor: 'white',
							},
						}}
						width="100%"
						anchor={'right'}
						open={state['right']}
						onClose={toggleDrawer('right', false)}
						variant="temporary"
					>
						{list('right')}
					</Drawer>
				</React.Fragment>
			</div>
		</>
	);
};
export default ForwardCaseInfo;
