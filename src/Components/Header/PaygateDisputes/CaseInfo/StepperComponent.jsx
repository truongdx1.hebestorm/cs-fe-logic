import * as React from 'react';
import { Step, StepConnector, StepLabel, Stepper } from '@material-ui/core';
import { styled } from '@material-ui/styles';
// import Stepper from '@mui/material/Stepper';
// import Step from '@mui/material/Step';
// import StepLabel from '@mui/material/StepLabel';
// // import { StepConnector } from '@mui/material';
// import { styled } from '@mui/material/styles';
// import StepConnector, {
// 	stepConnectorClasses,
// } from '@mui/material/StepConnector';
const steps = [
	'Need Checking',
	'Negotiating',
	'Solving',
	'Reviewing',
	'Closed',
];
const QontoConnector = styled(StepConnector)(({ theme }) => ({
	[`&.${stepConnectorClasses.alternativeLabel}`]: {
		top: 10,
		left: 'calc(-50% + 16px)',
		right: 'calc(50% + 16px)',
	},
	[`&.${stepConnectorClasses.active}`]: {
		[`& .${stepConnectorClasses.line}`]: {
			borderColor: '#219653',
		},
	},
	[`&.${stepConnectorClasses.completed}`]: {
		[`& .${stepConnectorClasses.line}`]: {
			borderColor: '#219653',
		},
	},
	[`& .${stepConnectorClasses.line}`]: {
		borderColor:
			theme.palette.mode === 'dark' ? theme.palette.grey[400] : '#bdbdbd',
		borderTopWidth: 3,
		borderRadius: 1,
	},
}));

const StepperComponent = () => {
	return (
		<div className="xl:w-full mobile:w-1/5">
			<Stepper
				activeStep={1}
				alternativeLabel
				sx={{
					width: '100%',
					color: '#219653',
				}}
				connector={<QontoConnector />}
			>
				{steps.map((label, index) => (
					<Step key={label} index={index}>
						{label ? <StepLabel>{label}</StepLabel> : <StepLabel></StepLabel>}
					</Step>
				))}
			</Stepper>
		</div>
	);
};

export default StepperComponent;
