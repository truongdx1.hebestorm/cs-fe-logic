import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import React from 'react';
import { Editor } from '@tinymce/tinymce-react';
import { Popup } from 'reactjs-popup';
import FormSearchTemplate from '../../Issues/LeftContent/FormSearchTemplate';
import AddIcons from '../../../../Icons/AddIcons';
import CustomButton from '../../../ComponentReuse/CustomButton/CustomButton';
import DocumentIcons from '../../../../Icons/DocumentIcons';

const li = [
	'Tracking number active',
	'Order not found',
	'Ask for proof',
	"HoachId's Template",
];
const initialValues = {
	orderNumber: '#212704149-GBS',
	from: '',
	level_1: '',
	level_2: '',
	level_3: '',
	to: '',
	csState: '',
	subject: '',
	desc: '',
};
const validationSchema = Yup.object().shape({
	orderNumber: Yup.string().required('Order Number is required'),
	from: Yup.string().required('From is required'),
	level_1: Yup.string().required('Level 1 is required'),
	level_2: Yup.string().required('Level 2 is required'),
	level_3: Yup.string().required('Level 3 is required'),
	to: Yup.string().required('To is required'),
	csState: Yup.string().required('CS State is required'),
	subject: Yup.string().required('Subject is required'),
	desc: Yup.string().required(''),
});

const FormCreateTicket = () => {
	const onHandleClick = (e) => {
		e.preventDefault();
	};
	const onSubmit = (values) => {
		console.log(values);
	};
	const editorRef = React.useRef(null);
	return (
		<div className="border rounded-5px my-4 font-normal">
			<Formik
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={(values) => onSubmit(values)}
			>
				{(formikProps) => {
					return (
						<Form className="m-4">
							<div className="w-full">
								<label className="text-sm my-5">
									Order Number <span className="text-red-600">*</span>
								</label>
								<Field
									name="orderNumber"
									className="w-full border rounded p-2.5"
								/>
							</div>
							<div className="flex gap-4 my-4">
								<div className="basis-1/2">
									<label className="text-sm my-4">From</label>
									<Field
										name="from"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Alice P</option>
										<option>Alice A</option>
										<option>Alice B</option>
										<option>Alice C</option>
									</Field>
								</div>
								<div className="basis-1/2">
									<label className="my-4 text-sm">
										Level 1 <span className="text-red-600">*</span>
									</label>
									<Field
										name="level_1"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Choose Level 1</option>
										<option>Choose Level 2</option>
										<option>Choose Level 3</option>
									</Field>
								</div>
							</div>
							<div className="flex gap-4 my-4">
								<div className="basis-1/2">
									<label className="my-4 text-sm">To</label>
									<Field
										name="to"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Alice P</option>
										<option>Alice A</option>
										<option>Alice B</option>
										<option>Alice C</option>
									</Field>
								</div>
								<div className="basis-1/2">
									<label className="my-4 text-sm">
										Level 2 <span className="text-red-600">*</span>
									</label>
									<Field
										name="level_2"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Choose level 2</option>
										<option>Choose level 3</option>
										<option>Choose level 4</option>
									</Field>
								</div>
							</div>
							<div className="flex gap-4 my-4">
								<div className="basis-1/2">
									<label className="my-4 text-sm">Cs State</label>
									<Field
										name="csState"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Closed</option>
										<option>Not Open</option>
										<option>Dont Open</option>
									</Field>
								</div>
								<div className="basis-1/2">
									<label className="my-4 text-sm">
										Level 3 <span className="text-red-600">*</span>
									</label>
									<Field
										name="level_3"
										component="select"
										className="w-full rounded p-2.5 border"
									>
										<option>Level 3</option>
										<option>Level 4</option>
										<option>Level 5</option>
										<option>Level 6</option>
									</Field>
								</div>
							</div>
							<div className="my-4">
								<label className="my-4 text-sm">
									Subject <span className="text-red-600">*</span>
								</label>
								<Field name="subject" className="w-full rounded p-2.5 border" />
							</div>
							<div>
								<Editor
									name="desc"
									textareaName="desc"
									onInit={(evt, editor) => (editorRef.current = editor)}
									initialValue="<p>This is the initial content of the editor.</p>"
									init={{
										height: 300,
										menubar: false,
										plugins: [
											'advlist autolink lists link image charmap print preview anchor',
											'searchreplace visualblocks code fullscreen',
											'insertdatetime media table paste code help wordcount',
										],
										toolbar:
											'undo redo  ' +
											'bold italic underline backcolor  | alignleft aligncenter ' +
											'alignright alignjustify | bullist numlist outdent indent | ' +
											'removeformat | help',
										content_style:
											'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
									}}
								/>
							</div>
							<div className="flex md:flex-nowrap mobile:flex-wrap justify-between my-4">
								<Popup
									trigger={(open) => (
										<CustomButton
											icons={<DocumentIcons />}
											text="template"
											background="bg-gray text-sm px-4 py-2"
										/>
									)}
									className=""
									position="top left"
									closeOnDocumentClick
									open={false}
									onClose={() => onHandleClick()}
								>
									{(close) => (
										<FormSearchTemplate
											show={false}
											text={li}
											icons={<AddIcons />}
											close={close}
										/>
									)}
								</Popup>
								<div className="flex md:flex-nowrap mobile:flex-wrap text-sm mobile:my-2 xl:my-0">
									<CustomButton
										text="Cancel"
										background="bg-gray px-4 py-2 text-sm"
									/>
									<CustomButton
										text="Create ticket"
										background="bg-btn-primary px-4 py-2 text-white mr-0 text-sm"
									/>
								</div>
							</div>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
};
export default FormCreateTicket;
