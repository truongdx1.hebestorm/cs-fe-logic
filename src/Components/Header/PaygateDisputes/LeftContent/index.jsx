import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import FormCreateTicket from './FormCreateTicket';
import React from "react";
const LeftContent = ({handleBack}) => {
	return (
		<>
			<div className="items-center">
				<button className="p-2 border rounded mr-4" onClick={handleBack}>
					<ArrowBackIcon />
				</button>
				<span className="capitalize text-base font-semibold">new ticket</span>
			</div>
			<div>
				<FormCreateTicket />
			</div>
		</>
	);
};
export default LeftContent;
