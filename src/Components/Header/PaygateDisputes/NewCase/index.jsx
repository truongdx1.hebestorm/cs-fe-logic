import * as React from 'react';

import LeftContent from '../LeftContent';
import RightContent from '../RightContent';
import { useHistory } from 'react-router-dom';
import { Box } from '@material-ui/core';
export default function InfoCustomerPayment(props) {
	const { text, icons } = props;
	const [open, setOpen] = React.useState(false);
	const [state, setState] = React.useState({
		bottom: open,
	});
	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}
		setOpen(true);
		setState({ ...state, [anchor]: open });
	};
	const handleKeyPress = (event) => {
		event.preventDefault();
		if (event.key === 'ESC') {
			setOpen(false);
		}
	};

	const list = (anchor, index) => (
		<Box
			key={index}
			sx={{ width: '100%' }}
			role="presentation"
			onClick={toggleDrawer(anchor, open)}
			onKeyDown={toggleDrawer(anchor, open)}
		>
			<span>{text}</span>
		</Box>
	);
	const history = useHistory();
	const handleDrawerClose = () => {
		setOpen(false);
		history.push('/paygate-disputes');
	};

	return (
		<>
			{/* <React.Fragment key={'bottom'}>
				<button
					onClick={toggleDrawer('bottom', open)}
					className=" border px-4 py-2.5 rounded bg-btn-primary text-white text-sm"
				>
					{icons}
					{text}
				</button>
				<Drawer
					sx={{
						width: '100%',
						flexShrink: 0,
						'& .MuiDrawer-paper': {
							width: '100%',
							border: 'none',
							backgroundColor: 'white',
						},
					}}
					className="md:h-12/12 2xl:h-11/12"
					variant="persistent"
					anchor="bottom"
					open={open}
				>
					< className="w-full mx-auto my-4">
						{/* <div className="">
							<div className="flex justify-between items-baseline">
								<div className="basis-8/12">
									<button
										onClick={() => handleDrawerClose()}
										className="border p-2 mr-2 rounded"
									>
										
									</button>
								</div>
							</div>
						</div> */}
			{/*<div className="flex my-4 gap-4 md:flex-wrap xl:flex-nowrap hidescroll mx-4">
							<div className="xl:w-1/2 md:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll ">
								<LeftContent handleBack={handleDrawerClose} />
							</div>
							<div className="xl:w-1/2 md:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll">
								<RightContent />
							</div>
						</div>
					</div>
				</Drawer>
			</React.Fragment> */}

			<div className="w-full mx-auto my-4">
				{/* <div className="">
							<div className="flex justify-between items-baseline">
								<div className="basis-8/12">
									<button
										onClick={() => handleDrawerClose()}
										className="border p-2 mr-2 rounded"
									>
										
									</button>
								</div>
							</div>
						</div> */}
				<div className="flex my-4 gap-4 mobile:flex-wrap xl:flex-nowrap hidescroll mx-4">
					<div className="xl:w-1/2 md:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll ">
						<LeftContent handleBack={handleDrawerClose} />
					</div>
					<div className="xl:w-1/2 mobile:w-full xl:overflow-scroll md:h-auto xl:h-screen hidescroll">
						<RightContent />
					</div>
				</div>
			</div>
		</>
	);
}
