import React from "react";
import MenuComponent from '../../ComponentReuse/MenusComponent/MenusComponent';
import TableComponent from './TablePaygateComponents';
import { Route, Switch, useHistory } from 'react-router-dom';
import CaseIdInfo from './CaseInfo/CaseIDInfo';
import ListOptionFilterDay from '../../ComponentReuse/ListOptionFilterDay';
import MoreIcons from '../../../Icons/MoreIcons';
import SearchIcons from '../../../Icons/SearchIcons';
import DownIcons from '../../../Icons/DownIcons';
import { Button } from 'antd';

const PaygateDisputes = () => {
	const listChoosen = [
		'Today',
		'Yesterday',
		'Last 7 days',
		'Last 30 days',
		'Last 90 days',
	];
	const listMenu = [
		'Case Due',
		'Case FillingDate',
		'CS State',
		'CS Agent',
		'Case Reason',
		'Case Type',
		'Supplier',
		'Store',
	];
	const history = useHistory();
	const onRouterNewTicket = () => {
		history.push('/paygate-disputes/new-ticket');
	};
	return (
		<>
			<div className="m-4 relative">
				<ListOptionFilterDay listChoosen={listChoosen} link={onRouterNewTicket}/>
				<div className="border rounded-5px my-2">
					<div className="xl:flex w-full gap-2 m-2 2xl:flex-nowrap md:flex-wrap mobile:flex-wrap items-center">
						<div className="grow w-full shrink flex gap-2 md:my-4 xl:my-0">
							<Button className="border p-1 rounded-md md:inline btn-more-icons">
								<MoreIcons />
							</Button>
							<div className="w-full mobile:flex bg-gray1 border grow rounded-md p-1 overflow-hidden items-center mobile:mr-4 2xl:mr-0 mobile:flex-nowrap">
								
								<SearchIcons/>
								<input
									placeholder={`Search template...`}
									className="border-none w-full inline visited:border-none bg-gray1 focus:outline-none ml-1  "
								/>
							</div>
						</div>
						<div className="md:w-full flex gap-2 2xl:flex-nowrap mobile:flex-wrap mobile:my-2 xl:my-0 items-center mr-4">
							{listMenu.map((item, index) => (
								<MenuComponent
									title={item}
									styled="bg-white text-sm rounded-md"
									icons={<DownIcons/>}
								/>
							))}
							<Button className="border rounded-md p-1 btn-more-icons">
								<MoreIcons />
							</Button>
						</div>
					</div>
					<div className="w-full 2xl:overflow-visible box-border">
						<TableComponent />
					</div>
				</div>
			</div>
			<Switch>
				<Route exact path="/paygate-disputes/case-id" component={CaseIdInfo} />
			</Switch>
		</>
	);
};
export default PaygateDisputes;
