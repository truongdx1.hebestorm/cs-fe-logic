import RelatedIssues from "../../../ContentBox/DrawerLineItem/RelatedIssues";
import GeneralInfo from "../../Issues/RightContent/GeneralInfo";
import OrderDetail from "../../Issues/RightContent/OrderDetail";
import React from "react";

const RightContent = () => {
	return (
		<>
			<div className="w-full">
				<div className="mb-5 border rounded-5px ">
					<div className=" border-b py-2.5">
						<span className="font-semibold m-4 text-emerald-700 text-sm">Issue Info</span>
					</div>
					<div className="mx-4 border-b flex items-baseline text-sm">
						<span className="font-semibold basis-1/3 py-2">Company</span>
						<span className="basis-2/3">HBS</span>
					</div>
					<div className="mx-4 flex items-baseline text-sm">
						<span className="font-semibold basis-1/3 py-2">Agent</span>
						<span className=" basis-2/3">Alice P</span>
					</div>
				</div>
                <div>
                    <GeneralInfo/>
                </div>
                <div>
                    <OrderDetail/>
                </div>
                <div>
                    <RelatedIssues/>
                </div>
			</div>
		</>
	);
};
export default RightContent;
