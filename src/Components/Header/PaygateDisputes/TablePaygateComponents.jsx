import { dataPaygate } from '../../../Data/dataPaygate';
import React, { useState, useEffect, useCallback } from 'react';
import { PaginationMemo } from '../../Pagination';
import EyeIcons from '../../../Icons/EyeIcons';
import { Link } from 'react-router-dom';
import CaseIdInfo from './CaseInfo/CaseIDInfo';
import { Checkbox } from 'antd';
const TableComponent = () => {
	const [paygate, setPaygate] = useState(dataPaygate);
	const [postsPerPage, setPostsPerPage] = useState(20);
	const [totalEntries, setTotalEntries] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	useEffect(() => {
		setPaygate(dataPaygate);
	}, []);
	useEffect(() => {
		setTotalEntries(paygate.length);
	}, [paygate]);
	const handleBack = useCallback(() => {
		if (currentPage > 1) {
			setCurrentPage(currentPage - 1);
		}
	}, [currentPage]);
	const handleNext = () => {
		if (currentPage < Math.ceil(totalEntries / postsPerPage)) {
			setCurrentPage(currentPage + 1);
		}
		console.log('abc');
	};
	const handleChoosePage = useCallback((page) => {
		setCurrentPage(page);
	}, []);
	const handleSetPostsPerPage = useCallback((entries) => {
		setPostsPerPage(entries);
		setCurrentPage(1);
	}, []);
	return (
		<>
			<div className="mobile:overflow-x-scroll 3xl:overflow-hidden">
				<table className="table-auto mobile:w-full truncate xl:text-clip mobile:overflow-x-scroll 2xl:overflow-hidden">
					<thead className="">
						<tr className="bg-gray text-sm none-border border-y">
							<td className="font-semibold none-border xl:items-baseline mobile:items-center none-border py-2.5 px-2 flex gap-2">
								<Checkbox />
								Level
							</td>
							<td className="px-2 font-semibold none-border">Case Type</td>
							<td className="px-2 font-semibold none-border">Case ID</td>
							<td className="px-2 font-semibold none-border">Case Reason</td>
							<td className="px-2 font-semibold none-border">
								Case Filling Date
							</td>
							<td className="px-2 font-semibold none-border">Link</td>
							<td className="px-2 font-semibold none-border">OrderNumber</td>
							<td className="px-2 font-semibold none-border">SUP</td>
							<td className="px-2 font-semibold none-border">TKN</td>
							<td className="px-2 font-semibold none-border">Argent Note</td>
							<td className="px-2 font-semibold none-border">Next Action</td>
							<td className="px-2 font-semibold none-border">Due Case</td>
							<td className="px-2 font-semibold none-border">CS Stage</td>
						</tr>
					</thead>
					<tbody>
						{paygate.map((item, index) => {
							if (
								index >= (currentPage - 1) * postsPerPage &&
								index < currentPage * postsPerPage
							) {
								return (
									<tr
										key={index}
										className={`text-sm font-normal border-tr ${
											index % 2 !== 0 ? 'bg-gray' : 'bg-white'
										}`}
									>
										<td className="md:text-center 2xl:text-left text-sm none-border py-2.5 flex gap-2 px-2">
											<Checkbox />
											{item.level}
										</td>
										<td className="px-2 truncate xl:text-start text-sm none-border">
											{item.caseType}
										</td>
										<td className="px-2  text-sm none-border">
											<Link
												to="/paygate-disputes/case-info"
												className="text-black text-sm "
											>
												{item.caseId}
											</Link>
											{item.status === true ? (
												<span className="px-1 py-0.5 mx-2 text-xs bg-notify rounded ">
													New
												</span>
											) : null}
										</td>
										<td className="px-2 truncate xl:text-start capitalize none-border">
											{item.caseReason}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.caseFillingDate}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											<EyeIcons />
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.orderNumber}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.sup}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.tkn}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.agentNote}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.nextAction}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.dueCase}
										</td>
										<td className="px-2 truncate xl:text-start none-border">
											{item.csStage}
										</td>
									</tr>
								);
							}
						})}
					</tbody>
				</table>
			</div>

			<div className="p-5">
				<PaginationMemo
					handleBack={handleBack}
					handleNext={handleNext}
					handleChoosePage={handleChoosePage}
					handleSetPostsPerPage={handleSetPostsPerPage}
					postsPerPage={postsPerPage}
					totalPosts={totalEntries}
					currentPage={currentPage}
				/>
			</div>
		</>
	);
};
export default TableComponent;
