import * as React from 'react';
import LeftContentInfoComponent from './LeftContentInfoComponent';
import RightContentInfoComponent from './RightContentInfoComponent';
import { useHistory } from 'react-router-dom';
import { Button } from 'antd';
import { Box } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
export default function InfoTemplates(props) {
	const { icons, text } = props;
	const [open, setOpen] = React.useState(false);
	const [state, setState] = React.useState({
		bottom: open,
	});
	const toggleDrawer = (anchor, open) => (event) => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}
		setOpen(true);

		setState({ ...state, [anchor]: open });
	};

	const list = (anchor) => (
		<Box
			sx={{ width: '100%', height: '100vh' }}
			role="presentation"
			onClick={toggleDrawer(anchor, open)}
			onKeyDown={toggleDrawer(anchor, open)}
		>
			<span>{text}</span>
		</Box>
	);
	const history = useHistory();
	const handleDrawerClose = () => {
		setOpen(false);
		history.push('/templates');
	};

	return (
		<div className="">
			<div className="xl:w-7/12 xl:mx-auto mobile:w-full ">
				<div className="my-2 mobile:mx-4 xl:mx-4">
					<Button
						size="large"
						className="border rounded btn-direct"
						onClick={() => handleDrawerClose()}
					>
						<ArrowBackIcon />
					</Button>
				</div>
				<div className="flex gap-4 mobile:flex-wrap xl:flex-nowrap mobile:mx-4">
					<div className="xl:basis-2/3 mobile:w-full md:h-auto">
						<LeftContentInfoComponent />
					</div>
					<div className="xl:basis-1/3 mobile:w-full md:h-auto">
						<RightContentInfoComponent />
					</div>
				</div>
			</div>
		</div>
	);
}
