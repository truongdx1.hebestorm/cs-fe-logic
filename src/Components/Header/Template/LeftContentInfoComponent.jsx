import * as Yup from 'yup';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as React from 'react';
import { Button, Radio } from 'antd';
import { Editor } from '@tinymce/tinymce-react';
const initialValues = {
	title: '',
	shortDescription: '',
	available: '',
	issueKind: '',
	desc: '',
};
const validationSchema = Yup.object().shape({
	title: Yup.string().required('Title is required'),
	shortDescription: Yup.string().required('Short description is required'),
	available: Yup.string(),
	issueKind: Yup.string().required('Issue kind is required'),
	desc: Yup.string(),
});

const LeftContentInfoComponent = () => {
	const [value, setValue] = React.useState(1);

	const onChange = (e) => {
		console.log('radio checked', e.target.value);
		setValue(e.target.value);
	};
	const onSubmit = (values) => {
		console.log(values);
	};
	const editorRef = React.useRef(null);
	const log = () => {
		if (editorRef.current) {
			console.log(editorRef.current.getContent());
		}
	};
	return (
		<div className="h-full my-4 w-full">
			<div className="w-full border rounded-5px">
				<Formik
					initialValues={initialValues}
					validationSchema={validationSchema}
					onSubmit={(values) => {
						console.log(values);
					}}
				>
					{(formikProps) => {
						return (
							<Form className="xl:m-4 font-400 mobile:mx-2">
								<div className="my-4">
									<label htmlFor="title" className="block text-sm mb-1 ">
										Title
									</label>
									<Field
										name="title"
										className="w-full border rounded p-2"
										placeholder="Address confirmation"
									/>
									<ErrorMessage
										name="title"
										className="text-red-600"
									></ErrorMessage>
								</div>
								<div className="my-4">
									<label
										htmlFor="shortDescription"
										className="block text-sm mb-1"
									>
										Short Decription
									</label>
									<Field
										name="shortDescription"
										className="w-full border rounded p-2"
										placeholder="Example: To initialate a refund for all cancellation request"
									/>
									<ErrorMessage
										name="shortDescription"
										className="text-red-600"
									></ErrorMessage>
								</div>
								<div className="items-baseline my-4">
									<label className="inline text-sm mr-8">Avaiable for </label>

									<Radio.Group onChange={onChange} value={value}>
										<Radio value="agentsCompany">Agents in company</Radio>
										<Radio value="allAgents">All agents</Radio>
										<Radio value="onlyMe">Only Me</Radio>
									</Radio.Group>
								</div>
								<div className="my-4">
									<label
										htmlFor="issueKind"
										className="capitalize block text-sm mb-1"
									>
										issue kind
									</label>
									<Field
										name="issueKind"
										className="w-full border rounded p-2"
									/>
									<ErrorMessage name="issueKind" className="text-red-600">
										{(message) => message}
									</ErrorMessage>
								</div>
								<div>
									<label className="capitalize text-sm mb-2" htmlFor="desc">
										Description
									</label>
									<Editor
										name="desc"
										textareaName="desc"
										onInit={(evt, editor) => (editorRef.current = editor)}
										initialValue="<p>This is the initial content of the editor.</p>"
										init={{
											height: 400,
											menubar: false,
											plugins: [
												'advlist autolink lists link image charmap print preview anchor',
												'searchreplace visualblocks code fullscreen',
												'insertdatetime media table paste code help wordcount',
											],
											toolbar:
												'undo redo  ' +
												'bold italic underline backcolor  | alignleft aligncenter ' +
												'alignright alignjustify | bullist numlist outdent indent | ' +
												'removeformat | help',
											content_style:
												'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
										}}
									/>
								</div>
								<div className="flex flex-row-reverse gap-4 my-2">
									<Button
										size="large"
										className="border rounded p-3 bg-blue text-white  btn-submit"
										type="submit"
									>
										Create template
									</Button>
									<Button
										className="border rounded p-3 bg-gray btn-cancel"
										type="reset"
										size="large"
									>
										Cancel
									</Button>
								</div>
							</Form>
						);
					}}
				</Formik>
			</div>
		</div>
	);
};
export default LeftContentInfoComponent;
