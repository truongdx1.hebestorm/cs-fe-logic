import React from 'react';
import { Button, Popover } from 'antd';

export default function MouseOverPopover(props) {
	const { icon, text } = props;
	const [anchorEl, setAnchorEl] = React.useState(null);

	const handlePopoverOpen = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handlePopoverClose = () => {
		setAnchorEl(null);
	};

	const open = Boolean(anchorEl);

	return (
		<div>
			<Popover content={text}><Button className="btn-icon" size='small'>{icon}</Button></Popover>
		</div>
	);
}
