import { Menu } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import React from 'react'; 
const { SubMenu } = Menu;
const RightContentInfoComponent = () => {
	const handleClick = (e) => {
		console.log('click ', e);
	};
	return (
		<div>
			<div className="border rounded-5px mt-4">
				<div className="m-4">
					<span className="text-base font-semibold">Status</span>
					<Menu
						onClick={() => handleClick()}
						style={{
							width: 'full',
							border: '1px solid gray',
							listStyleType: 'none',
							borderRadius: '5px',
							marginTop: '16px',
						}}
						mode="inline"
					>
						<SubMenu
							key="active"
							title="Active"
							style={{
								listStyleType: 'none',
							}}
						>
							<Menu.Item key="1">Option 1</Menu.Item>
							<Menu.Item key="2">Option 2</Menu.Item>
						</SubMenu>
					</Menu>
				</div>
			</div>
			<div className="mt-4 border rounded-5px">
				<div className="m-4">
					<span className="block font-semibold capitalize my-4 text-base">
						ticket templates
					</span>
					<span className="my-4 block">
						Templates allow you to pre-fill new tickets and outgoing emails with
						a single click. You can save time spent on typing out the same
						content again and again, and quickly create tickets or send outbound
						emails.
					</span>
					<span className="block mb-4">
						For example, you can create a template called 'Initiate Refund' by
						filling the body of the ticket and setting the priority 'medium'. So
						the next time you receive a phone call about a refund, you can
						create a medium priority ticket for your customer and initiate the
						refund request with a single click.
					</span>
				</div>
			</div>
		</div>
	);
};
export default RightContentInfoComponent;
