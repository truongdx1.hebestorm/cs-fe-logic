import { dataTemplate } from '../../../Data/dataCustomer';
import React, { useState, useEffect, useCallback } from 'react';
import { PaginationMemo } from '../../Pagination';
import Checkbox from 'antd';
import { useHistory } from 'react-router-dom';
import PopoverCustomize from './Popover';
import CopyIcons from '../../../Icons/CopyIcons';
import EditIcons from '../../../Icons/EditIcons';
import TrashIcons from './../../../Icons/TrashIcons';

const TableComponent = () => {
	const [anchorEl, setAnchorEl] = React.useState(null);

	const handlePopoverOpen = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handlePopoverClose = () => {
		setAnchorEl(null);
	};
	const [postsPerPage, setPostsPerPage] = useState(20);
	const [totalEntries, setTotalEntries] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);

	useEffect(() => {
		setTotalEntries(dataTemplate.length);
	}, [dataTemplate]);
	const handleBack = useCallback(() => {
		if (currentPage > 1) {
			setCurrentPage(currentPage - 1);
		}
	}, [currentPage]);
	const handleNext = useCallback(() => {
		if (currentPage < Math.ceil(totalEntries / postsPerPage)) {
			setCurrentPage(currentPage + 1);
		}
	}, [currentPage]);
	const handleChoosePage = useCallback((page) => {
		setCurrentPage(page);
	}, []);
	const handleSetPostsPerPage = useCallback((entries) => {
		setPostsPerPage(entries);
		setCurrentPage(1);
	}, []);
	const open = Boolean(anchorEl);
	const history = useHistory();
	const clickToInfoTemplate = () => {
		history.push('/templates/template-info');
	};
	return (
		<div className="mobile:overflow-scroll 2xl:overflow-auto">
			<table className="xl:table-auto md:table-fixed  w-full border-none mobile:overflow-auto">
				<thead className="text-start bg-gray">
					<tr className="font-semibold ">
						<td className="px-2 w-1/5 border-none">
							<Checkbox /> 
							<span>Title</span>
						</td>
						<td className="px-2 w-2/5 border-none">Description</td>
						<td className="px-2 border-none">Creator</td>
						<td className="px-2 border-none">Issue Kind</td>
						<td className="px-2 w-36 border-none">Action</td>
					</tr>
				</thead>
				<tbody>
					{dataTemplate.map((item, index) => {
						if (
							index >= (currentPage - 1) * postsPerPage &&
							index < currentPage * postsPerPage
						) {
							return (
								<tr key={index} className="hide-border">
									<td
										className="px-2 border-none truncate"
										onClick={clickToInfoTemplate}
									>
										{/* <InfoTemplate icons={<Checkbox />} text={item.title} /> */}
										<Checkbox />
										<span className="cursor-pointer">{item.title}</span>
									</td>
									<td className="px-2 border-none truncate">{item.desc}</td>
									<td className="px-2 border-none truncate">{item.creator}</td>
									<td className="px-2 border-none truncate">
										{item.issueKind}
									</td>
									<td className="border-none mobile:flex mobile:flex-nowrap">
										<PopoverCustomize
											icon={<CopyIcons styled="" />}
											text="Clone"
										/>
										<PopoverCustomize icon={<EditIcons />} text="Edit" />
										<PopoverCustomize
											icon={<TrashIcons margin="icons-gray" />}
											text="Delete"
										/>
									</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>

			<div className="p-5">
				<PaginationMemo
					handleBack={handleBack}
					handleNext={handleNext}
					handleChoosePage={handleChoosePage}
					handleSetPostsPerPage={handleSetPostsPerPage}
					postsPerPage={postsPerPage}
					totalPosts={totalEntries}
					currentPage={currentPage}
				/>
			</div>
		</div>
	);
};

export default TableComponent;
