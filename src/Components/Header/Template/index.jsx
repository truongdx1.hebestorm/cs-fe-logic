
import React, { useState, useEffect, useCallback } from 'react';
import { dataTemplate } from '../../../Data/dataCustomer';
import TableComponent from './TableComponent';
import MenuComponent from '../../ComponentReuse/MenusComponent/MenusComponent';
import DownIcons from '../../../Icons/DownIcons';
import MoreIcons from '../../../Icons/MoreIcons';
import SearchIcons from '../../../Icons/SearchIcons';
import { Button } from 'antd';
const TemplatesComponent = () => {
	const [rowsPerPage, setRowsPerPage] = React.useState(10);
	const [page, setPage] = React.useState(
		Math.ceil(dataTemplate.length / rowsPerPage)
	);

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};
	return (
		<>
			<div className="border rounded-5px m-4 mt-8">
				<div className="m-4 flex gap-2 md:flex-nowrap mobile:flex-wrap md:my-4 2xl:flex-nowrap justify-between md:items-center">
					<div className="flex grow w-90 mobile:w-full md:w-auto mobile:flex-nowrap mobile:gap-2">
					<MenuComponent icons={<MoreIcons />} />
						<div className="md:w-full mobile:w-full flex bg-secondary border rounded px-2 overflow-hidden items-center">
							<SearchIcons/>
							<input
								placeholder={`Search template...`}
								className="w-full border-none visited:border-none bg-secondary focus:outline-none ml-1 p-1"
							/>
						</div>
					</div>
					<div className="flex gap-2 mobile:my-2 2xl:my-0">
						<MenuComponent title="Issues Kind" icons={<DownIcons />} />
						<MenuComponent title="Create By" icons={<DownIcons />} />
					</div>
				</div>
				<div>
					<TableComponent />
				</div>
			</div>
		</>
	);
};
export default TemplatesComponent;
