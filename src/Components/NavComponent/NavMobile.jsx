import { NavLink } from 'react-router-dom';
import Logo from '../../Icons/Logo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChartColumn } from '@fortawesome/free-solid-svg-icons/faChartColumn';
import { faFaceFrown } from '@fortawesome/free-solid-svg-icons/faFaceFrown';
import { faUserGroup } from '@fortawesome/free-solid-svg-icons/faUserGroup';
import { faPaste } from '@fortawesome/free-solid-svg-icons/faPaste';
import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons/faTriangleExclamation';
import React from 'react';
const NavMobile = () => {
	return (
		<div className="flex-col mobile:w-60 header p-4 ">
			<div className="my-4">
				<NavLink
					to="/"
					className="hover:text-black  text-white flex items-center "
				><Logo className="basis-1/5"/>
					Hebecares
				</NavLink>
			</div>
			<div className="my-4">
				<NavLink
					to="/dashboards"
					activeStyle={{
						backgroundColor: '#FBBC05',
						
						margin: '0',
						borderRadius: '5px',
						color: 'white',
					}}
					className="hover:text-black  text-white py-2 flex items-center"
				>
					<FontAwesomeIcon icon={faChartColumn} className="basis-1/5" />
					Dashboards
				</NavLink>
			</div>
			<div className="my-4">
				<NavLink
					to="/issues"
					activeStyle={{
						backgroundColor: '#FBBC05',
						
						borderRadius: '5px',
						color: 'white',
					}}
					className="hover:text-black flex items-center py-2 text-white "
				>
					<FontAwesomeIcon icon={faTriangleExclamation} className="basis-1/5" />
					Issues
				</NavLink>
			</div>
			<div className="my-4">
				<NavLink
					to="/paygate-disputes"
					activeStyle={{
						backgroundColor: '#FBBC05',
						
						width: '100%',
						borderRadius: '5px',
						color: 'white',
					}}
					className="hover:text-black flex items-center py-2 text-white"
				>
					<FontAwesomeIcon icon={faFaceFrown} className="basis-1/5 inline" />
					Paygate Disputes
				</NavLink>
			</div>
			<div className="my-4">
				<NavLink
					to="/customers"
					activeStyle={{
						backgroundColor: '#FBBC05',
				
						margin: 0,
						borderRadius: '5px',
						color: 'white',
					}}
					className="hover:text-black flex items-center py-2 text-white text-sm"
				>
						<FontAwesomeIcon icon={faUserGroup} className="basis-1/5" />
					Customers
				</NavLink>
			</div>
			<div className="my-4">
				<NavLink
					to="/templates"
					activeStyle={{
						backgroundColor: '#FBBC05',
					
						margin: 0,
						borderRadius: '5px',
						color: 'white',
					}}
					className="hover:text-black flex items-center py-2 text-white text-sm"
				>
					<FontAwesomeIcon icon={faPaste} className="basis-1/5" />
					Templates
				</NavLink>
			</div>
		</div>
	);
};
export default NavMobile;
