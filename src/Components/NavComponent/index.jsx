
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons/faAngleDown';
import { NavLink } from 'react-router-dom';
import Logo from '../../Icons/Logo';
import React, { useEffect, useState } from 'react';
import NavMobile from './NavMobile';
import CloseIcons from "../../Icons/BarIcons"
import { Avatar, Popover } from '@material-ui/core';
const NavBar = () => {
	const [anchorEl, setAnchorEl] = React.useState(null);
	 
	const [anchorMenu, setAnchorMenu] = React.useState(null);
	const open = Boolean(anchorEl);
	const openMenu = Boolean(anchorMenu);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClickMenu = (event) => {
		setAnchorMenu(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};
	const handleCloseMenu = () => {
		setAnchorMenu(null);
	};
	console.log("abc");
	const id = openMenu ? 'simple-popover' : undefined;
	return (
		<div className="flex header sticky top-0 z-50 md:w-full font-semibold md:gap-0 xl:gap-2 items-baseline md:text-xs xl:text-base p-2 mobile:justify-between text-sm">
			<div className="items-baseline mobile:hidden lg:flex lg:flex-wrap 2xl:flex-nowrap  xl:basis-10/12 gap-0  mx-auto">
				<div className="items-baseline ">
					<NavLink to="/" className="hover:text-black md:text-sm text-white">
						<Logo />
						Hebecares
						<FontAwesomeIcon icon={faAngleDown} className="mx-2" />
					</NavLink>
				</div>
				<div className="mx-2 xl:text-base md:text-sm">
					<NavLink
						to="/dashboards"
						activeStyle={{
							backgroundColor: '#FBBC05',
							margin: '0',
							borderRadius: '5px',
							color: 'white',
						}}
						className="hover:opacity-60 px-4 py-2 flex-nowrap  text-white md:text-sm md:block lg:inline 2xl:inline"
					>
						<svg
							className="icons-white mr-1 inline"
							width="18"
							height="18"
							viewBox="0 0 24 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M3 2C3.55228 2 4 2.44772 4 3V19C4 19.5523 4.44772 20 5 20H21C21.5523 20 22 20.4477 22 21C22 21.5523 21.5523 22 21 22H5C3.34315 22 2 20.6569 2 19V3C2 2.44772 2.44772 2 3 2Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M7 8C7.55228 8 8 8.44772 8 9V17C8 17.5523 7.55228 18 7 18C6.44772 18 6 17.5523 6 17V9C6 8.44772 6.44772 8 7 8Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M11 14C11.5523 14 12 14.4477 12 15V17C12 17.5523 11.5523 18 11 18C10.4477 18 10 17.5523 10 17V15C10 14.4477 10.4477 14 11 14Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M15 4C15.5523 4 16 4.44772 16 5V17C16 17.5523 15.5523 18 15 18C14.4477 18 14 17.5523 14 17V5C14 4.44772 14.4477 4 15 4Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M19 10C19.5523 10 20 10.4477 20 11V17C20 17.5523 19.5523 18 19 18C18.4477 18 18 17.5523 18 17V11C18 10.4477 18.4477 10 19 10Z"
								fill="#33383F"
							/>
						</svg>
						Dashboards
					</NavLink>
				</div>
				<div className="mx-2 xl:text-base md:text-sm ">
					<NavLink
						to="/issues"
						activeStyle={{
							backgroundColor: '#FBBC05',
							borderRadius: '5px',
							color: 'white',
						}}
						className="hover:opacity-60 px-4 py-2 mx-2 md:text-sm flex-nowrap  text-white md:block 2xl:inline"
					>
						<svg
							className="icons-white mr-1 inline"
							width="18"
							height="18"
							viewBox="0 0 24 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M12 15C12.5523 15 13 14.5523 13 14V9.00001C13 8.44772 12.5523 8.00001 12 8.00001C11.4477 8.00001 11 8.44772 11 9.00001V14C11 14.5523 11.4477 15 12 15Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M19.6738 17.5049L12.8688 5.56389C12.4852 4.89073 11.5148 4.89073 11.1312 5.56389L4.32621 17.5049C3.9463 18.1715 4.42773 19 5.19503 19H18.805C19.5723 19 20.0537 18.1715 19.6738 17.5049ZM14.6065 4.57364C13.4556 2.55415 10.5444 2.55415 9.39353 4.57364L2.58857 16.5146C1.44883 18.5146 2.89312 21 5.19503 21H18.805C21.1069 21 22.5512 18.5145 21.4114 16.5146L14.6065 4.57364Z"
								fill="#33383F"
							/>
							<path
								d="M13 17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17C11 16.4477 11.4477 16 12 16C12.5523 16 13 16.4477 13 17Z"
								fill="#33383F"
							/>
						</svg>
						Issues
					</NavLink>
				</div>
				<div className="md:basis-1/10 rounded text-center xl:basis-1/10 md:text-sm mx-2">
					<NavLink
						to="/paygate-disputes"
						activeStyle={{
							backgroundColor: '#FBBC05',
							width: '100%',
							borderRadius: '5px',
							color: 'white',
						}}
						className="hover:opacity-60 px-4 py-2 flex-nowrap xl:w-full text-white md:block 2xl:inline  md:text-sm"
					>
						<svg
							className="icons-white mr-1 inline"
							width="18"
							height="18"
							viewBox="0 0 24 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M12 20C16.4183 20 20 16.4183 20 12C20 7.58172 16.4183 4 12 4C7.58172 4 4 7.58172 4 12C4 16.4183 7.58172 20 12 20ZM12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M7.01565 16.9164C7.52174 17.1375 8.11126 16.9065 8.33238 16.4004C8.95054 14.9856 10.3613 14 12 14C13.6387 14 15.0494 14.9856 15.6676 16.4004C15.8887 16.9065 16.4782 17.1375 16.9843 16.9164C17.4904 16.6952 17.7214 16.1057 17.5003 15.5996C16.5753 13.4826 14.4619 12 12 12C9.53804 12 7.42463 13.4826 6.49967 15.5996C6.27855 16.1057 6.50957 16.6952 7.01565 16.9164Z"
								fill="#33383F"
							/>
							<path
								d="M9 9C9 9.55228 8.55228 10 8 10C7.44772 10 7 9.55228 7 9C7 8.44772 7.44772 8 8 8C8.55228 8 9 8.44772 9 9Z"
								fill="#33383F"
							/>
							<path
								d="M17 9C17 9.55228 16.5523 10 16 10C15.4477 10 15 9.55228 15 9C15 8.44772 15.4477 8 16 8C16.5523 8 17 8.44772 17 9Z"
								fill="#33383F"
							/>
						</svg>
						Paygate Disputes
					</NavLink>
				</div>
				<div className="p-1 text-center mx-2">
					<NavLink
						to="/customers"
						activeStyle={{
							backgroundColor: '#FBBC05',
							margin: 0,
							borderRadius: '5px',
							color: 'white',
						}}
						className="hover:opacity-60 px-4 py-2 items-baseline flex-nowrap gap-2  text-white md:block 2xl:inline md:text-sm"
					>
						<svg
							className="text-white inline mr-1 icons-white"
							width="18"
							height="18"
							viewBox="0 0 24 24"
							fill="white"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M14.4997 10.9691C16.473 10.7232 18 9.03992 18 7.00001C18 4.96009 16.473 3.27682 14.4997 3.03091C15.4334 4.08866 16 5.47817 16 7.00001C16 8.52185 15.4334 9.91136 14.4997 10.9691Z"
								fill="#33383F"
							/>
							<path
								d="M20 20C20 20.5523 20.4477 21 21 21C21.5523 21 22 20.5523 22 20V18C22 15.3399 19.9227 13.165 17.3018 13.009C18.3539 13.9407 19.1588 15.1454 19.6056 16.512C19.8565 16.9505 20 17.4585 20 18V20Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M7 15C5.34315 15 4 16.3431 4 18V20C4 20.5523 3.55228 21 3 21C2.44772 21 2 20.5523 2 20V18C2 15.2386 4.23858 13 7 13H13C15.7614 13 18 15.2386 18 18V20C18 20.5523 17.5523 21 17 21C16.4477 21 16 20.5523 16 20V18C16 16.3431 14.6569 15 13 15H7Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M10 5C8.89543 5 8 5.89543 8 7C8 8.10457 8.89543 9 10 9C11.1046 9 12 8.10457 12 7C12 5.89543 11.1046 5 10 5ZM6 7C6 4.79086 7.79086 3 10 3C12.2091 3 14 4.79086 14 7C14 9.20914 12.2091 11 10 11C7.79086 11 6 9.20914 6 7Z"
								fill="#33383F"
							/>
						</svg>
						Customers
					</NavLink>
				</div>
				<div className="text-center hover:opacity-60 hover:border-separate mx-2">
					<NavLink
						to="/templates"
						activeStyle={{
							backgroundColor: '#FBBC05',

							margin: 0,
							borderRadius: '5px',
							color: 'white',
						}}
						className="hover:opacity-60 px-4 py-2 text-base text-white flex-nowrap md:text-sm md:block 2xl:inline "
					>
						<svg
							className="icons-white mr-1 inline"
							width="18"
							height="18"
							viewBox="0 0 24 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M7 11C7 10.4477 7.44772 10 8 10H16C16.5523 10 17 10.4477 17 11C17 11.5523 16.5523 12 16 12H8C7.44772 12 7 11.5523 7 11Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M7 15C7 14.4477 7.44772 14 8 14H12C12.5523 14 13 14.4477 13 15C13 15.5523 12.5523 16 12 16H8C7.44772 16 7 15.5523 7 15Z"
								fill="#33383F"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M21 19C21 20.6569 19.6569 22 18 22H6C4.34315 22 3 20.6569 3 19V5C3 3.34315 4.34315 2 6 2H16L16.0107 2.01071C16.7136 2.07025 17.3761 2.3761 17.8787 2.87868L20.1213 5.12132C20.6239 5.6239 20.9297 6.28645 20.9893 6.98929L21 7V19ZM18 20H6C5.44772 20 5 19.5523 5 19V5C5 4.44772 5.44771 4 6 4H15V6C15 7.10457 15.8954 8 17 8H19V19C19 19.5523 18.5523 20 18 20Z"
								fill="#33383F"
							/>
						</svg>
						Templates
					</NavLink>
				</div>
			</div>

			{/* Nav menu moblie */}
			<div className="mobile:w-1/2 lg:hidden ">
				<div>
					<button
						aria-describedby={id}
						variant="contained"
						onClick={handleClickMenu}
					>
						<CloseIcons />
					</button>
					<Popover
						id={id}
						open={openMenu}
						anchorEl={anchorMenu}
						onClose={handleCloseMenu}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'left',
						}}
					>
						<div className="w-full h-1/2">
							{/* <CloseIcon onClick={handleCloseMenu} /> */}
							<NavMobile />
						</div>
					</Popover>
				</div>
			</div>
			<div className="rounded capitalize flex flex-row-reverse mr-4 md:text-center md:items-start items-baseline mobile:flex-nowrap mobile:basis-1/3 md:basis-2/12 mobile:items-baseline">
				<div
					id="button"
					aria-controls={open ? 'basic-menu' : undefined}
					aria-haspopup="true"
					aria-expanded={open ? 'true' : undefined}
					onClick={handleClick}
					className=" mobile:flex 2xl:items-baseline gap-3 hover:opacity-70 hover:cursor-pointer md:items-center  xl:text-base md:text-sm"
				>
					<div
						direction="col"
						spacing={1}
						className="md:items-center md:ml-2 md:mr-0 mobile:mx-2"
					>
						<Avatar
							sx={{
								bgcolor: '#FFD79D',
								fontWeight: '500',
								color: '#000000',
								fontSize: 12,
							}}
						>
							JB
						</Avatar>
					</div>
					<span className="font-semibold mobile:text-xs xl:text-sm mobile:my-auto md:inline md:mr-4">
						Chi Nguyen
					</span>
				</div>
				{/* <Menu
					id="basic-menu"
					anchorEl={anchorEl}
					open={open}
					onClose={handleClose}
					MenuListProps={{
						'aria-labelledby': 'basic-button',
					}}
				>
					<MenuItem onClick={handleClose}>
						{' '}

						<p className="inline mx-4">Manager Account</p>
					</MenuItem>
					<MenuItem onClick={handleClose}>
					
						<p className="inline mx-4">Setting</p>
					</MenuItem>
					<MenuItem onClick={handleClose}>
						
						<p className="inline mx-4">Logout</p>
					</MenuItem>
				</Menu> */}
			</div>
		</div>
	);
};
export default NavBar;
