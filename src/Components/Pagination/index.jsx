import React, { useCallback, useMemo } from "react";
import { Button } from "antd";
import ArrowForwardIcons from "../../Icons/ArrowForwardIcons";
import ArrowBackwardIcons from "../../Icons/ArrowBackwardIcons";
import MenuComponent from "../ComponentReuse/MenusComponent/MenusComponent";
import DownIcons from "../../Icons/DownIcons";
const Pagination = ({
  handleBack,
  handleNext,
  handleSetPostsPerPage,
  postsPerPage,
  totalPosts,
  currentPage,
}) => {
  const lastPage = Math.ceil(totalPosts / postsPerPage);

  const getArrOfPages = useCallback(
    (lastPage) => {
      const res = [];
      for (let i = 1; i <= lastPage; i++) {
        res.push(i);
      }
      return res;
    },
    [lastPage]
  );
  const arrOfPages = useMemo(() => {
    return getArrOfPages(lastPage);
  }, [lastPage]);
  if (totalPosts < 10) return null;
  return (
    <nav
      aria-label="Page navigation"
      className="flex justify-between  flex-wrap gap-y-2 items-baseline"
    >
      <div className="space-x-2 text-sm flex items-center">
        <span>Row per page</span>
        {/* <select
					name="postPerPage"
					className="border bg-gray rounded-md text-sm  py-1 focus:border-sky-600 visited:border-sky-600 selection:border-sky-600"
					onChange={(e) => {
						handleSetPostsPerPage(+e.target.value);
					}}
					value={postsPerPage}
				>
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="50">50</option>
				</select> */}
        <MenuComponent
          key="postPerPage"
          list={["10", "20", "50"]}
          title={postsPerPage}
          onClick={({ key }) => {
            handleSetPostsPerPage(key);
          }}
          icons={<DownIcons className="mr-0" />}
          className="menu-pagination btn-default"
        />
      </div>
      {/* <div className="h-8">
				Showing <b>{postsPerPage * (currentPage - 1) + 1}</b> to
				<b>
					{" "}
					{postsPerPage * currentPage > totalPosts
						? totalPosts
						: postsPerPage * currentPage}
				</b>{" "}
				of <b>{totalPosts}</b> items
			</div> */}
      <ul className="inline-flex items-baseline">
        <li className="list-none">
          <Button
            className={
              "p-1 text-black transition-colors duration-150 bg-gray border rounded-md btn-filter-days" +
              (currentPage === 1
                ? " cursor-not-allowed"
                : " hover:bg-slate-300 hover:text-white")
            }
            onClick={handleBack}
            icon={<ArrowBackwardIcons />}
          ></Button>
        </li>
        {/* {arrOfPages.map((page) => (
					<li key={page} className="list-none">
						<button
							className={
								"h-8 px-3 transition-colors duration-150 bg-white border border-r-0 border-slate-600" +
								(page === currentPage
									? " bg-slate-300"
									: " text-black hover:bg-slate-300 hover:text-white")
							}
							onClick={() => {
								handleChoosePage(page);
							}}
						>
							{page}
						</button>
					</li>
				))} */}
        <div className="h-8 mx-2 text-sm">
          <span>{postsPerPage * (currentPage - 1) + 1}</span>-
          <span>
            {postsPerPage * currentPage > totalPosts
              ? totalPosts
              : postsPerPage * currentPage}
          </span>
          <span> of {totalPosts}</span>
        </div>
        <li className="list-none">
          <Button
            className={
              "p-1 text-black transition-colors bg-gray  border rounded-md btn-filter-days" +
              (currentPage === lastPage
                ? " cursor-not-allowed"
                : " hover:bg-slate-300 hover:text-white")
            }
            onClick={() => handleNext()}
            icon={<ArrowForwardIcons />}
          ></Button>
        </li>
      </ul>
    </nav>
  );
};

export const PaginationMemo = React.memo(Pagination);
