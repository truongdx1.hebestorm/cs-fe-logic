import React, { useEffect } from 'react';
import tinymce from 'tinymce';
import 'tinymce/themes/silver';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/table';

const TinyEditorComponent = (props) => {
	const [editor, setEditor] = React.useState(null);
	const { toolbar, id , content } = props;
	const default_tolbar = {
		toolbar1:
			'bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | image media link anchor | code outdent indent',
		toolbar2:
			'numlist bullist | forecolor backcolor removeformat pagebreak | table',
		toolbar3: 'fontselect fontsizeselect styleselect',
	};
    const onEditorChange = (editor) => {
        
    }
	var init = {
		plugins: 'link image autoresize media anchor code pagebreak lists table',
		autoresize_bottom_margin: 50,

		toolbar_sticky: true,
		toolbar_sticky_offset: 60,
		menubar: false,
		resize: false,
		statusbar: false,
		setup: (editor) => {
			editor.on('keyup change', () => {
				const content = editor.getContent();
				onEditorChange(content);
			});
		},
	};
	if (toolbar) {
		init = { ...init, ...toolbar };
	} else {
		init = { ...init, ...default_tolbar };
	}
	tinymce.init(init);
	if (content) {
		onSetContent(content);
	}

	useEffect(() => {
		tinymce.remove(editor);
	}, [editor]);

	const onSetContent = (description) => {
		tinymce.activeEditor.setContent(description);
	};

	return <textarea id={id} value={content} onChange={(e) => console.log(e)} />;
};

export default TinyEditorComponent;
