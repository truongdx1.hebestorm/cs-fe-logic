export const dataCustomer = [
	{
		id: 1,
		name: 'Allison Kienlen',
		comment: {
			title: '',
			content: `I would like to know where this is??? It is supporsed to be a Chrismas present and needs to be here by December 24th. Where is it?`,
			end: 'Thanks you,',
			signature: "Renei Seibert",
		},
		createAt: 'Fri,14 Jan 2022 at 1:43 PM',
	},
	{
		id: 2,
		name: 'Jessica N',
		comment: {
			title: "Dear Renee Seibert",
			content: `
            In this situation, the manufactoring distributor would like to offer you a replacement for your order and start the process regularly. 
            Please kindly confirm with us that you will be pleased with this replacement.
            Right after we receive your confirmation , we will inform the manufacturing distributor to start processing your new package and inform you of the unique tracking number once it dispatches.
            We sincerely apologize for the inconvenience and hope you can consider and accept it as out apology for the trouble you have experienced.
            `,
			end: `
            Thank you so much, and we look forward to hearing from you soon.`,
			signature: "",
		},
		createAt: 'Fri , 14 Jan 2022 at 1:43 PM',
	},
	{
		id: 3,
		name: 'Allison Kienlen',
		comment: { title: '', content: 'Thanks you so much...', end: '' },
		createAt: 'Fri , 14 Jan 2022 at 1:43PM',
		signature: "",
	},
];
export const dataIssueTimeline = [
	{
		user: "Allison Kienlen",
		createAt: "16/01/2022 10:11:50 -0800",
		status: "Closed",
		note: "Allison Kienlen created an I100 issue"
	},
	{
		user: "Allison Kienlen",
		createAt: "16/01/2022 10:11:50 -0800",
		status: "Resolved",
		note: "Allison Kienlen created an I200 issue"
	},
	{
		user: "Allison Kienlen",
		createAt: "16/01/2022 10:11:50 -0800",
		status: "Open",
		note: "Allison Kienlen created an I300 issue"
	},
]

export const dataEdit = [{
	status: false,
	code: "#212704149-GBS-F2",
	product: [{
		codeProduct: "Hippie life 0022",
		nameProduct: "Face mask 3.5D / 26cm X 16cm / 1PC",
		codeStock: "SKU: POD_202013958",
		customName: "Custom Info: Name: 'PeterParker'",
		price: 13.99,
		count: 1,
	}]
},
{
	status: false,
	code: "#212704149-GBS-F2",
	product: [{
		codeProduct: "Hippie life 0022",
		nameProduct: "Face mask 3.5D / 26cm X 16cm / 1PC",
		codeStock: "SKU: POD_202013958",
		customName: "Custom Info: Name: 'PeterParker'",
		price: 13.99,
		count: 1,
	}]
}];

export const OrderData = [{
	id: 3421492,
	code:"OI-0086679",
	level: "T5",
	kind:"Damaged/Wrong/Missing",
	title:"#6362334-CERA",
	note:"KH mua 1 xanh và 1 đỏ flying orb",
	createAt: "2022-01-10T14:31:30.669+07:00",
	forwardTo: "Alex"

},
{
	id: 3421492,
	code:"OI-0086679",
	level:"T5",
	kind:"Unmatched suppllier",
	title:"#6362334-CERA",
	note:"",
	createAt: "2022-01-10T14:31:30.669+07:00",
	forwardTo: ""
}];

export const dataTemplate =[{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
{
	title: "Order not found ver 2",
	desc: "A short to mediun length description of the current item can go here. Maxed...",
	creator: "Celina V",
	issueKind: "L202",
},
]