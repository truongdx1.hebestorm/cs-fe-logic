/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

var Layer = require("react-layer");
import React from "react";
// import NumberFormat from 'react-number-format';
import { IntlProvider, addLocaleData } from "react-intl";
import messages from "../messages";
import en from "react-intl/locale-data/en";
import vi from "react-intl/locale-data/vi";
import moment from "moment";
import { Provider } from "react-redux";
import { store } from "../_helpers";
import { Popup} from '../_components'

addLocaleData(en);
addLocaleData(vi);
// import { formatNumber } from "../ultis";

export const layerRender = {
  showPopup,
};

function showPopup(otherProps) {
  // console.log(button)
  var lang = store.getState().locale.lang;
  console.log(otherProps)
  var layer = new Layer(document.body, function renderModal() {
    return (
      <Provider store={store}>
        <IntlProvider locale={lang} messages={messages[lang]}>
            <Popup
              {...otherProps}
              onClose={finish}
            />
        </IntlProvider>
      </Provider>
    );
  });

  layer.render();

  function finish() {
    layer.destroy(); // unmount and remove the React Component tree
  }
  return layer;
}
