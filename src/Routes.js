import { BrowserRouter, Route,Switch, Redirect } from 'react-router-dom';
import React, { Suspense, useState, useEffect, lazy } from 'react';
import { RouteWithLayout } from './_components';
// import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

const MainLayout = React.lazy(() => import('./layouts/Main'));
const MinimalLayout = React.lazy(() => import('./layouts/Minimal'));
// const ManageLayout = React.lazy(() => import('./layouts/Manage'));
// const UpgradeLayout = React.lazy(() => import('./layouts/Upgrade'));

// const BaseLayout = React.lazy(() => import('./layouts/Base'));

// const HomeForm = React.lazy(() => import('./views/Home'));
// const Profile = React.lazy(() => import('./views/Profile'))

// const Issues = React.lazy(() => import ('./views/Issues/List'))
// const IssueForm = React.lazy(() => import ('./views/Issues/Form'))
// const IssueCreateForm = React.lazy(() => import ('./views/Issues/FormCreate'))
// const PaygateDisputes = React.lazy(() => import ('./views/PaygateDisputes/List'))
// const PaygateDisputeForm = React.lazy(() => import ('./views/PaygateDisputes/Form'))
// const Customers = React.lazy(() => import ('./views/Customers/List'))
// const CustomerForm = React.lazy(() => import ('./views/Customers/Form'))
// const Templates = React.lazy(() => import ('./views/Templates/List'))
// const TemplateForm = React.lazy(() => import ('./views/Templates/Form'))
// const InternalIssueForm = React.lazy(() => import ('./views/InternalIssues/Form'))
import Dashboards from './Components/Header/Dashboards';
import Issues from './Components/Header/Issues';
import IssuesInfo from './Components/Header/Issues/IssuesInfo';
import PaygateDisputes from './Components/Header/PaygateDisputes/PaygateDisputes';
import Customers from './Components/Header/Customer';
import NavBar from './Components/NavComponent';
import TemplatesComponent from './Components/Header/Template';
import InfoCustomerPayment from './Components/Header/Customer/InfoCustomerPayment';
import CaseIdInfo from './Components/Header/PaygateDisputes/CaseInfo/CaseIDInfo';
import NewCaseIdInfo from './Components/Header/PaygateDisputes/NewCase';
import InfoTemplates from './Components/Header/Template/InfoTemplate';
import TableCustomerIssue from './Components/Header/Issues/TableIssue/TableCustomerIssue';
import { NotFound2 } from './views/NotFound'

const Main = React.lazy(() => import("./Components/NavComponent/index"));


const routerArr = [
  // {
  //   component: HomeForm,
  //   layout: MainLayout,
  //   path: '/home',
  // },
  // {
  //   component: Profile,
  //   layout: MainLayout,
  //   path: '/account',
  // },
  {
    component: Dashboards,
    layout:MainLayout,
    path:"dashboard"
  },
  {
    component: Issues,
    layout: MainLayout,
    path: '/issues',
  },
  {
    component: IssuesInfo,
    layout: MainLayout,
    path: '/issues/issue-info',
  },
  {
    component: PaygateDisputes,
    layout: MainLayout,
    path: '/paygate-disputes',
  },
  {
    component:NewCaseIdInfo,
    layout:MainLayout,
    path:"/paygate-disputes/case-info"
  },

  {
    component: CaseIdInfo,
    layout: MainLayout,
    path: '/paygate-disputes/:id',
  },
  {
    component: Customers,
    layout: MainLayout,
    path: '/customers',
  },
  {
    component: InfoCustomerPayment,
    layout: MainLayout,
    path: '/customers/:id',
  },
  {
    component: Customers,
    layout: MainLayout,
    path: '/customers',
  },
  {
    component: TemplatesComponent,
    layout: MainLayout,
    path: '/templates',
  },
  {
    component: InfoTemplates,
    layout: MainLayout,
    path: '/templates/:id',
  },
]


const Routes = ({ ...rest }) => {
  const [redirectPath, setRedirectPath] = useState('/');
  const [arrRoutes, setArrRoutes] = useState([]);

  // useEffect(() => {
  //   var arrAccept = []
  //   routerArr.map((rou, index) => {
  //     arrAccept.push(rou)
  //   })
  //   if (arrAccept.length > 0) {
  //     setRedirectPath(arrAccept[0].path)
  //   }
  //   setArrRoutes(arrAccept)
  //   console.log(arrAccept)
  // }, [rest.user]);

  return (
    <>
			<BrowserRouter>
				<NavBar />
				<Switch>
					<Route exact path="/issues" component={Issues} />
					<Route exact path="/dashboards" component={Dashboards} />
					<Route exact path="/paygate-disputes" component={PaygateDisputes} />
					<Route exact path="/customers" component={Customers} />
					<Route exact path="/templates" component={TemplatesComponent} />
					<Route exact path="/customers/info-payments">
						<InfoCustomerPayment />
					</Route>
					<Route exact path="/paygate-disputes/case-info" component={CaseIdInfo} />
					<Route exact path="/paygate-disputes/new-ticket" component={NewCaseIdInfo} />
					<Route exact path="/templates/template-info" component={InfoTemplates}/>
					<Route path="/issues/issue-info" component={IssuesInfo}/>
					<Route
							exact
								path="/issues/issue-infomation"
								component={TableCustomerIssue}
							/>
				</Switch>
			</BrowserRouter>

    {/* <Switch>
      <Redirect
        exact
        from="/"
        to={redirectPath}
      />
      {arrRoutes.map((rou, index) => {
        return (
          <RouteWithLayout
            key={index}
            // component={<AsyncComponent moduleProvider={rou.component} />}
            component={rou.component}
            exact
            layout={rou.layout}
            onRouteChange={rest.onRouteChange}
            path={rou.path}
          />
        )
      })} */}
      {/* <RouteWithLayout
        component={NotFound2}
        exact
        layout={MinimalLayout}
        onRouteChange={rest.onRouteChange}
        path="/not-found"
      /> */}
      {/* {arrRoutes.length > 0 && <Redirect to="/not-found" />} */}
    {/* </Switch> */}
    </>
    
  );
};

export default Routes;
