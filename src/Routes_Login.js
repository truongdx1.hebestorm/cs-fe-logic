import { Switch, Redirect } from 'react-router-dom';
import React, { Suspense, useState, useEffect, lazy } from 'react';

import { RouteWithLayout } from './_components';
// import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import { set } from 'date-fns/esm';
import { SignUp } from './views/SignUp/SignUp';

const routerArr = [

  {
    component: SignUp,
    layout: null,
    path: '/admin/signup',
  }

]

const Routes_login = ({ ...rest }) => {
  const [redirectPath, setRedirectPath] = useState('/');
  const [arrRoutes, setArrRoutes] = useState([]);
  useEffect(() => {
  }, [rest.user]);
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to={redirectPath}
      />
      <Redirect
        exact
        from="/admin"
        to={redirectPath}
      />
      {routerArr.map((rou, index) => {
        return (
          <RouteWithLayout
            key={index}
            // component={<AsyncComponent moduleProvider={rou.component} />}
            component={rou.component}
            exact
            path={rou.path}
          />
        )

      })}
      <Redirect to={redirectPath} />
    </Switch>
  );
};

export default Routes_login;
