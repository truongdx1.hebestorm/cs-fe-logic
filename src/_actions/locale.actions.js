import { localeConstants } from '../_constants'

export const localeSet = lang => ({
  type: localeConstants.LOCALE_SET,
  lang
});

export const setLocale = lang => dispatch => {
  localStorage.feroshLng = lang;
  dispatch(localeSet(lang));
};
