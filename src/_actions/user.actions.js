/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
import { userConstants } from '../_constants'
import { userService } from '../_services'
import { alertActions } from './'
import { history } from '../_helpers'
import toastr from '../common/toastr'
import Cookies from 'universal-cookie'
const cookies = new Cookies()
import config from 'config'
const ref = window.location.protocol + '//' + window.location.host + window.location.pathname

const url_login = `${config.accountUrl}/login?ref=${ref}`

export const userActions = {
  login,
  logout,
  register,
  getAll,
  delete: _delete,
  listUser,
  forgotPassword,
  recoveryPassword
}

function login(query, callback) {
  return dispatch => {
    dispatch(request(query))
    userService.login(query)
      .then(
        user => {
          dispatch(success(user,'user'))
          toastr.success('Login successfully')
          const params = new URLSearchParams(window.location.search);
          const ref = params.get('ref')
          if(ref){
            window.location.href = ref
          }
        },
        error => {
          console.log(error,'error')

          dispatch(failure(error.toString()))
          if(error){
            toastr.error(error.msg)
          }else{
            toastr.error(error.toString())
          }
          if(callback){
            callback()
          }
          // dispatch(alertActions.error(error.toString()));
        }
      )
  }

  function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
  function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function forgotPassword(query, callback){
  userService.forgotPassword(query)
}

function recoveryPassword(query, callback){
  userService.recoveryPassword(query)
}

function register(query, callback) {
  return dispatch => {
    dispatch(request(query))
    userService.register(query)
      .then(
        user => {
          dispatch(success(user))
          history.push('/admin/home')
          toastr.success('Register 123 successfully')
        },
        error => {
          dispatch(failure(error.toString()))
          toastr.error(error.toString())
          callback()
          // dispatch(alertActions.error(error.toString()));
        }
      )
  }

  function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
  function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
  function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function logout(type) {
  localStorage.removeItem('user_hebecore')
  localStorage.removeItem('current_app')
  localStorage.removeItem('current_company')
  localStorage.removeItem('nameCol')
  cookies.remove('user_hebecore', { path: '/', domain: '.salegate.io' })
  window.location.assign(url_login)
  return { type: userConstants.LOGOUT }
}

function listUser(queryObject) {
  return dispatch => {
    dispatch({
      type: 'QUERY_OBJECT',
      queryObject
    })
    userService.getListUser(queryObject)
      .then(res => {
        if (res.data.success) {
          var res = res.data
          dispatch({
            type: userConstants.LIST_USER,
            res
          });
        }
      },
        err => {
          toastr.error(err)
        })
  }
}

// function register(user) {
//   return dispatch => {
//     dispatch(request(user))

//     userService.register(user)
//       .then(
//         user => {
//           dispatch(success())
//           dispatch(alertActions.success('Registration successful'))
//         },
//         error => {
//           dispatch(failure(error.toString()))
//           dispatch(alertActions.error(error.toString()))
//         }
//       )
//   }

//   function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
//   function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
//   function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
// }

function getAll() {
  return dispatch => {
    dispatch(request())

    userService.getAll()
      .then(
        users => dispatch(success(users)),
        error => dispatch(failure(error.toString()))
      )
  }

  function request() { return { type: userConstants.GETALL_REQUEST } }
  function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
  function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id))

    userService.delete(id)
      .then(
        user => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      )
  }

  function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
  function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
  function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}
