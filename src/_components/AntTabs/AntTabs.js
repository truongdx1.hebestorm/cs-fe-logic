import React from 'react'
import { Tabs, Tab } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';

const TabStyles = withStyles({
  root: {
  },
  indicator: {
    backgroundColor: '#129535',
  },
})(Tabs);

const TabItem = withStyles((theme) => ({
  root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      outline: 'none !important',
      fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
      ].join(','),
      '&:hover': {
          color: '#129535',
          opacity: 1,
      },
      '&$selected': {
          color: '#129535',
          fontWeight: theme.typography.fontWeightMedium,
      },
      '&:focus': {
          color: '#129535',
      },
  },
  selected: {
    color: '#129535',
    fontWeight: 'bold'
  },
}))((props) => <Tab disableRipple {...props} />);

const AntTabs = ({ items, handleChangeTab, currentTab, centered }) => {
  return (
    <TabStyles 
      value={currentTab || (items ? items[0].value : '')}
      onChange={handleChangeTab}
      aria-label="Ant Example"
      centered={centered}
    >
      {items && items.map((item) => (
        <TabItem key={item.value} value={item.value} label={item.label} />
      ))}
    </TabStyles>
  )
}

export default AntTabs