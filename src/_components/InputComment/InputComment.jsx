/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSort, faSmile, faAt, faHashtag, faPaperclip, faExclamationCircle } from '@fortawesome/free-solid-svg-icons'
import { cssConstants } from '../../_constants'
import validate from 'validate.js';

const useStyles = theme => ({
    ...cssConstants,
    container: {
        display: 'flex',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
        alignItems: 'center',
        flexGrow: 1,
        appearance: 'none',
        zIndex: 30,
        width: '100%',
        minHeight: '38px',
        '&.error':{
            border: '1px solid #bf0711 !important',
        }
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, main: {
        // display: 'flex',
        // flexWrap: 'wrap',
        position: 'relative',
        minHeight: '35px',
    }, right: {
        marginLeft: '-1px'
    }, label: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        minHeight: '35px',
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
    }, span_label: {
        color: '#999999'
    }, span_value: {
        color: '#000',
        margin: '0 5px',
        flexGrow: 1
    }, error: {
        fontSize: 14,
        color: 'var(--p-text-critical,#bf0711)',
        fill: 'var(--p-icon-critical,#bf0711)'
    }
});

class InputComment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array_value: ['1', '2'],
            formState: {
                values: {
                },
                change: {},
                touched: {},
                errors: {},
                isSubmiting: false,
            },
            options: []
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);


    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event


    }



    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
    }

    validateForm(formState) {
        var schema = {
            text: {
                presence: { allowEmpty: false, message: '^Required' },
            }
        }
        const errors = validate(formState.values, schema);
        formState['isValid'] = errors ? false : true
        formState['errors'] = errors || {}
        return formState
    }

    hasError = (field) => {
        var formState = Object.assign({}, this.state.formState);
        return (formState.isSubmiting || formState.touched[field]) && formState.errors[field] ? true : false
    }


    render() {
        const { classes,
            placeholder,
            className, onSubmit } = this.props;
        const { formState } = this.state
        return (
            <div>
            <div className={clsx('MuiFormControl-marginDense', classes.main, className)}>
                <div className={clsx(classes.container,this.hasError('text')?'error':'')}
                    onBlur={() => {
                    }}
                    ref={this.wrapperRef}
                >
                    <TextField
                        fullWidth
                        InputProps={{
                            classes: {
                                input: classes.input,
                                underline: classes.underline
                            }
                        }}
                        name={'text'}
                        value={formState.values.text || ''}
                        placeholder={placeholder}
                        onChange={this.handleChangeValue}
                        error={this.hasError('text')}
                    />

                    {/* <button className='m-2'>
                        <FontAwesomeIcon icon={faSmile} style={{ fontSize:18}} />
                    </button>
                    <button className='m-2'>
                        <FontAwesomeIcon icon={faAt} style={{ fontSize:18}} />
                    </button>
                    <button className='m-2'>
                        <FontAwesomeIcon icon={faHashtag} style={{ fontSize:18}} />
                    </button>
                    <button className='m-2'>
                        <FontAwesomeIcon icon={faPaperclip} style={{ fontSize:18}} />
                    </button> */}
                    <button className={clsx(classes.button_normal, 'ml-2 m-1 p-2 pl-3 pr-3', (!formState.values.text || formState.values.text.trim().length==0) ? 'disable' : 'primary')}
                        disabled={(!formState.values.text || formState.values.text.trim().length==0) ? true : false}
                        onClick={() => {
                            var formState = Object.assign({}, this.state.formState);
                            formState['isLoading'] = true
                            formState['isSubmiting'] = true
                            formState = this.validateForm(formState)
                            this.setState({ formState })
                            if (formState['isValid']) {
                                onSubmit(formState.values.text).then(res => {
                                    if (res) {
                                        var formState = Object.assign({}, this.state.formState)
                                        formState['values']['text'] = ''
                                        this.setState({ formState })
                                    }
                                })
                            }

                        }}
                    >
                        Post
                    </button>
                    {/* <FontAwesomeIcon icon={faTimesCircle}
                                        size={'1x'} style={{ fontSize: 16, color: '#637381', cursor: 'pointer' }} /> */}
                </div>

                
            </div>
            {this.hasError('text') && <div className={clsx("d-flex align-items-center", classes.error)}>
                    <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
                    <span>{formState.errors.text[0]}</span>
                </div>}
            </div>
        )
    }
}

InputComment.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(InputComment)));
export { connectedList as InputComment };
