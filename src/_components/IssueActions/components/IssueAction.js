import React, { useEffect, useState } from 'react'
import {
  Box, TextField, FormControlLabel, Checkbox, FormGroup, List, ListItem, ListItemText,
  CircularProgress, Divider
} from '@material-ui/core'
import { Button, Avatar } from 'rsuite'

import { cssConstants } from '../../../_constants/css.constants'
import { makeStyles } from '@material-ui/styles'
import { DatePicker } from 'rsuite'
import AsyncSelect from "react-select/async";
import { request } from '../../../_services/request'
import { userService } from '../../../_services/user.service'
import config from 'config'
import CreatableSelect from 'react-select/creatable'
import { customStyles } from '../../../utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationCircle, faFileAlt, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import clsx from 'clsx'
import Select from 'react-select'
import Dropzone from 'react-dropzone'
import download from '../../../assets/images/download.svg'
import {
  IconArrowUp, IconFile, IconTrash, IconCheck, IconX
} from '@tabler/icons'
import { capitalizeStr, makeId } from "../../../utils";
import { offerIssues } from "../../../_constants";
import { TinyEditorComponent } from '../../../_components'

const useStyles = makeStyles({
  dropzone: {
    margin: '10px 0',
    border: '2px dashed #ccc',
    outline: 'none',
    color: '#666'
  }, dropzone_input: {
    outline: 'none',
    visibility: 'hidden'
  }, fileContainer: {
    borderTop: '1px solid #ccc',
    borderBottom: '1px solid #ccc'
  },
  ...cssConstants
})

export const CheckDesign = (props) => {
  return (
    <div>Hey, make sure you know what you're doing before hitting the button!</div>
  )
}

export const ConfirmSentOffer = (props) => {
  const { submitData, onHandleChangeText, hasError } = props
  const classes = useStyles()
  return (
    <div style={{ fontSize: 14 }}>
      <div>
        <div className='mb-2'>
          <Box fontWeight={'bold'}>
            Link contact offer <span style={{ color: 'red' }}> * </span>
          </Box>
        </div>
        <TextField
          fullWidth
          className={classes.textField}
          InputProps={{
            className: classes.textFieldInput,
            classes: {
              placeholder: classes.placeholder,
            }
          }}
          margin="dense"
          name="link_contact_offer"
          variant="outlined"
          value={submitData.values.link_contact_offer || ''}
          onChange={onHandleChangeText}
          error={hasError('link_contact_offer')}
        />
        {hasError('link_contact_offer') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData.errors.link_contact_offer[0]}</span>
        </div>}
      </div>
    </div>
  )
}

export const AddAssignee = (props) => {
  const { submitData, onHandleChangeText, hasError, code } = props
  const classes = useStyles()
  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  };

  const handleInputChange = (newValue) => {
    return newValue;
  };
  return (
    <div style={{ fontSize: 14 }}>
      <div>
        <div className='mb-2'>
          <Box fontWeight={'bold'}>
            Assignee <span style={{ color: 'red' }}> * </span>
          </Box>
        </div>
        <AsyncSelect
          isMulti={code == "add_followers" ? true : false}
          className="MuiFormControl-marginDense"
          cacheOptions
          loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
            return new Promise(resolve => {
              request.get(`${config.apiLoginUrl}/api/v1/users`, {}).then(res => {
                if (res.data.success) {
                  resolve(res.data.data.result)
                } else {
                  resolve([])
                }
              }, err => {
                resolve([])
              })
            })
          })}
          defaultOptions
          onInputChange={handleInputChange}
          isSearchable
          // isClearable
          name={"assignee_id"}
          onChange={(value) => {
            var e = {
              target: {
                name: "assignee_id",
                value
              },
            };
            onHandleChangeText(e)
          }}
          placeholder={"Assignment"}
          getOptionLabel={({ name }) => name}
          getOptionValue={({ uid }) => uid}
          valueKey={"uid"}
          value={submitData?.values["assignee_id"] || null}
          styles={customStyles[hasError('assignee_id')]}
        />
        {hasError('assignee_id') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData?.errors.assignee_id[0]}</span>
        </div>}
      </div>
    </div>
  )
}

export const AddSolution = (props) => {
  const { submitData, onHandleChangeText, hasError, code } = props
  const classes = useStyles()
  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  };

  const handleInputChange = (newValue) => {
    return newValue;
  };
  return (
    <div style={{ fontSize: 14 }}>
      {code == "update_customer_decision" ?
        <>
          <div className='mb-2'>
            <Box fontWeight={'bold'}>
              Customer decision <span style={{ color: 'red' }}> * </span>
            </Box>
            <Select
              className={"MuiFormControl-marginDense"}
              isDisabled={submitData.isLoading || false}
              options={[
                { id: 'agree', name: "Agree" },
                { id: 'disagree', name: "Disagree" },
                { id: 'no_response', name: "No response" }]}
              styles={{}}
              onChange={(value) => {
                var e = {
                  target: {
                    name: "customer_decision",
                    value
                  },
                };
                onHandleChangeText(e)
              }}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ id }) => id}
              value={submitData?.values?.customer_decision || null}
              styles={customStyles[hasError('customer_decision')]}
            />
            {hasError('customer_decision') && <div className={clsx("d-flex align-items-center", classes.error)}>
              <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
              <span>{submitData?.errors.customer_decision[0]}</span>
            </div>}
          </div>
          <div className='mb-2'>
            <Box fontWeight={'bold'}>
              Next action <span style={{ color: 'red' }}> * </span>
            </Box>
            <AsyncSelect
              className="MuiFormControl-marginDense"
              cacheOptions
              loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.issueApiUrl}/api/solutions`, {}).then(res => {
                    if (res.data.success) {
                      resolve(res.data.data.result)
                    } else {
                      resolve([])
                    }
                  }, err => {
                    resolve([])
                  })
                })
              })}
              defaultOptions
              onInputChange={handleInputChange}
              isSearchable
              // isClearable
              name={"solution_id"}
              onChange={(value) => {
                var e = {
                  target: {
                    name: "solution_id",
                    value
                  },
                };
                onHandleChangeText(e)
              }}
              placeholder={"Next action"}
              getOptionLabel={({ name, code }) => `${capitalizeStr(code)}. ${name}`}
              getOptionValue={({ id }) => id}
              valueKey={"id"}
              value={submitData?.values["solution_id"] || null}
              styles={customStyles[hasError('solution_id')]}
            />
            {hasError('solution_id') && <div className={clsx("d-flex align-items-center", classes.error)}>
              <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
              <span>{submitData?.errors.solution_id[0]}</span>
            </div>}
          </div>
        </> :
        <div className='mb-2'>
          <Box fontWeight={'bold'}>
            Solution <span style={{ color: 'red' }}> * </span>
          </Box>
          <AsyncSelect
            className="MuiFormControl-marginDense"
            cacheOptions
            loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
              return new Promise(resolve => {
                request.get(`${config.issueApiUrl}/api/solutions`, {}).then(res => {
                  if (res.data.success) {
                    resolve(res.data.data.result)
                  } else {
                    resolve([])
                  }
                }, err => {
                  resolve([])
                })
              })
            })}
            defaultOptions
            onInputChange={handleInputChange}
            isSearchable
            // isClearable
            name={"solution_id"}
            onChange={(value) => {
              var e = {
                target: {
                  name: "solution_id",
                  value
                },
              };
              onHandleChangeText(e)
            }}
            placeholder={"Solution"}
            getOptionLabel={({ name, code }) => `${capitalizeStr(code)}. ${name}`}
            getOptionValue={({ id }) => id}
            valueKey={"id"}
            value={submitData?.values["solution_id"] || null}
            styles={customStyles[hasError('solution_id')]}
          />
          {hasError('solution_id') && <div className={clsx("d-flex align-items-center", classes.error)}>
            <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
            <span>{submitData?.errors.solution_id[0]}</span>
          </div>}
        </div>}
      <div className='mb-2'>
        <Box fontWeight={'bold'}>
          Assignee <span style={{ color: 'red' }}> * </span>
        </Box>
        <AsyncSelect
          className="MuiFormControl-marginDense"
          cacheOptions
          loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
            return new Promise(resolve => {
              request.get(`${config.apiLoginUrl}/api/v1/users`, {}).then(res => {
                if (res.data.success) {
                  resolve(res.data.data.result)
                } else {
                  resolve([])
                }
              }, err => {
                resolve([])
              })
            })
          })}
          defaultOptions
          onInputChange={handleInputChange}
          isSearchable
          // isClearable
          name={"assignee_id"}
          onChange={(value) => {
            var e = {
              target: {
                name: "assignee_id",
                value
              },
            };
            onHandleChangeText(e)
          }}
          placeholder={"Assignment"}
          getOptionLabel={({ name }) => name}
          getOptionValue={({ uid }) => uid}
          valueKey={"uid"}
          value={submitData?.values["assignee_id"] || null}
          styles={customStyles[hasError('assignee_id')]}
        />
        {hasError('assignee_id') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData?.errors.assignee_id[0]}</span>
        </div>}
      </div>
      <div className='mb-2'>
        <Box fontWeight={'bold'}>
          Next Action note
        </Box>
        <TextField
          name="next_action_note"
          fullWidth
          InputProps={{
            classes: {
              input: classes.input
            },
          }}
          margin="dense"
          multiline
          rows={3}
          rowsMax={5}
          value={submitData.values.next_action_note || ''}
          variant="outlined"
          onChange={onHandleChangeText}
        />
        {hasError('next_action_note') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData?.errors.next_action_note[0]}</span>
        </div>}
      </div>
      <FormGroup>
        <FormControlLabel control={<Checkbox
          color="primary"
          name={"trigger_resolve"}
          checked={submitData.values.trigger_resolve}
          onChange={onHandleChangeText}
        />}
          label="Select to submit and resolve this issue." />
      </FormGroup>
      <div className='mb-2'

      >
        <Box fontWeight={'bold'}>
          Atachments
        </Box>
        <div
          className={(submitData.values?.extra?.files?.length || 0) == 0 ? '' : 'invisible'}

        >
          <Dropzone onDrop={props.ctx.onDropFile}
            maxFiles={1} multiple={false}
            accept={[".xlsx", ".xls"]}
            ref={props.ctx.dropzoneRef}>
            {({ getRootProps, getInputProps }) => (
              <section>
                <div {...getRootProps()} className={(submitData.values?.extra?.files?.length || 0) == 0 ? classes.dropzone : classes.heightNull} style={{ outline: 'none' }}>
                  <input {...getInputProps()} className={classes.dropzone_input} />
                  {(submitData.values?.extra?.files?.length || 0) == 0 && <>
                    <div className={clsx("text-center addlogo w-auto p-3 mb-3 d-flex flex-column justify-content-center align-items-center")}>
                      <img
                        className='mb-2'
                        width='30'
                        src={download}
                      />
                      <Button
                        className={clsx(classes.button_normal, 'mb-2')}
                        size='small'
                        variant='contained'
                        color='primary'>
                        Add file
                      </Button>
                      <p className="font18 mb-1">or drop files to upload</p>
                    </div>
                  </>}
                  <aside>
                  </aside>
                </div>
              </section>
            )}

          </Dropzone>
        </div>
        <List style={{ minWidth: '60%' }}>
          {submitData.values?.extra?.files?.map(file => (
            <>
              <ListItem key={file.id}>
                <ListItemText secondary={
                  <div className="d-flex align-items-center justify-content-between">
                    <div>{file.filename}</div>
                    <div className="d-flex align-items-center justify-content-end">
                      {file.status == 'uploading' && <CircularProgress style={{ width: 10, height: 10 }} />}
                      {file.status == 'uploaded' && <IconCheck size={16} stroke={2} color="green" />}
                      {file.status == 'failed' && <IconX size={16} stroke={2} color="red" />}
                      <IconTrash
                        className="ml-2"
                        size={16} stroke={2}
                        cursor={"pointer"}
                        onClick={(e) => {
                          e.stopPropagation()
                          const data = Object.assign({}, submitData)
                          const files = data.values.extra.files.filter(e => e.id != file.id)
                          data.values.extra['files'] = files
                          setState({ submitData: data })
                        }}
                      />
                    </div>
                  </div>
                } />
              </ListItem>
              <Divider />
            </>
          ))}
        </List>
      </div>
      {["A3", "A5"].includes((submitData?.values?.solution_id?.code || '').toUpperCase()) ? <>
        <div className='mb-2 d-flex'>
          <div className="col-lg-6 pl-0">
            <Box fontWeight={'bold'}>
              Refund Percent(%)
            </Box>
            <TextField
              name="refund_percent"
              type="number"
              fullWidth
              InputProps={{
                classes: {
                  input: classes.input
                },
              }}
              margin="dense"
              value={submitData.values.refund_percent || ''}
              variant="outlined"
              onChange={onHandleChangeText}
            />
            {hasError('refund_percent') && <div className={clsx("d-flex align-items-center", classes.error)}>
              <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
              <span>{submitData?.errors.refund_percent[0]}</span>
            </div>}
          </div>
          <div className="col-lg-6 pr-0">
            <Box fontWeight={'bold'}>
              Unit
            </Box>
            <Select
              className={"MuiFormControl-marginDense"}
              isDisabled={submitData.isLoading || false}
              options={[
                { id: 'clone', name: "item's value" },
                { id: 'creative', name: "order's value" }]}
              styles={{}}
              placeholder="Refund Unit (order's value / item's value)"
              onChange={(value) => {
                var e = {
                  target: {
                    name: "unit",
                    value
                  },
                };
                onHandleChangeText(e)
              }}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ id }) => id}
              value={submitData?.values?.unit || null}
              styles={customStyles[hasError('unit')]}
            />
            {hasError('unit') && <div className={clsx("d-flex align-items-center", classes.error)}>
              <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
              <span>{submitData?.errors.unit[0]}</span>
            </div>}
          </div>
        </div>
        <div className='mb-2'>
          <Box fontWeight={'bold'}>
            Refund Amound(USD)
          </Box>
          <TextField
            name="refund_amount"
            type="number"
            fullWidth
            InputProps={{
              classes: {
                input: classes.input
              },
            }}
            margin="dense"
            value={submitData.values.refund_amount || ''}
            variant="outlined"
            onChange={onHandleChangeText}
          />
          {hasError('refund_amount') && <div className={clsx("d-flex align-items-center", classes.error)}>
            <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
            <span>{submitData?.errors.refund_amount[0]}</span>
          </div>}
        </div>
      </> : null}
    </div>
  )
}

export const AddOffer = (props) => {
  const { submitData, onHandleChangeText, hasError, code } = props
  const classes = useStyles()
  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  };

  const handleInputChange = (newValue) => {
    return newValue;
  };
  return (
    <div style={{ fontSize: 14 }}>

      <div className='mb-2'>
        <Box fontWeight={'bold'}>
          Offer <span style={{ color: 'red' }}> * </span>
        </Box>
        <AsyncSelect
          className="MuiFormControl-marginDense"
          cacheOptions
          loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
            return new Promise(resolve => {
              const result = offerIssues.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
              resolve(result)
            })
          })}
          defaultOptions
          onInputChange={handleInputChange}
          isSearchable
          name={"offer"}
          onChange={(value) => {
            var e = {
              target: {
                name: "offer",
                value
              },
            };
            onHandleChangeText(e)
          }}
          placeholder={"Offer"}
          getOptionLabel={({ name, code }) => `${name}`}
          getOptionValue={({ id }) => id}
          valueKey={"id"}
          value={submitData?.values["offer"] || null}
          styles={customStyles[hasError('offer')]}
        />
        {hasError('offer') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData?.errors.offer[0]}</span>
        </div>}
      </div>

      <div className='mb-2'>
        <Box fontWeight={'bold'}>
          Offer note
        </Box>
        <TextField
          name="offer_note"
          fullWidth
          InputProps={{
            classes: {
              input: classes.input
            },
          }}
          margin="dense"
          multiline
          rows={3}
          rowsMax={5}
          value={submitData.values.offer_note || ''}
          variant="outlined"
          onChange={onHandleChangeText}
        />
        {hasError('offer_note') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData?.errors.offer_note[0]}</span>
        </div>}
      </div>

    </div>
  )
}

export const AddLink = (props) => {
  const { submitData, onHandleChangeText, hasError, code } = props
  const classes = useStyles()
  return (
    <div style={{ fontSize: 14 }}>
      <div>
        <div className='mb-2'>
          <Box fontWeight={'bold'}>
            Design Link <span style={{ color: 'red' }}> * </span>
          </Box>
        </div>
        <TextField
          fullWidth
          className={classes.textField}
          InputProps={{
            className: classes.textFieldInput,
            classes: {
              placeholder: classes.placeholder,
            }
          }}
          margin="dense"
          name="link"
          variant="outlined"
          value={submitData.values.link || ''}
          onChange={onHandleChangeText}
          error={hasError('link')}
        />
        {hasError('link') && <div className={clsx("d-flex align-items-center", classes.error)}>
          <FontAwesomeIcon icon={faExclamationCircle} className='mr-2' />
          <span>{submitData.errors.link[0]}</span>
        </div>}
      </div>
    </div>
  )
}

export const SendOffer = (props) => {
  const { submitData, onHandleChangeText, hasError, code } = props

  useEffect(() => {
    onHandleChangeText({
      target: {
        name: 'send_to_customer',
        value: true
      }
    })
  }, [])

  console.log(submitData.values, 'data')

  return (
    <div className="issue-description">
      <TinyEditorComponent 
        className={submitData.errors?.content ? 'error' : ''}
        style={{ width: '100%' }}
        // ref={this.noteRef}
        content={submitData.values.content}
        id={makeId(20)}
        onEditorChange={(content) => {
          const event = {
            target: {
              name: 'content',
              value: content
            }
          }
          onHandleChangeText(event)
        }}
      />
      <FormControlLabel
        control={
          <Checkbox
            checked={!!submitData.values.send_to_customer}
            onChange={(e) => onHandleChangeText({
              target: {
                name: 'send_to_customer',
                value: e.target.checked
              }
            })}
            name="send_to_customer"
            color="primary"
          />
        }
        label="Send to customer"
      />
      {submitData.errors?.note && <small style={{ color: 'red'}}>{submitData.errors.note[0]}</small>}
    </div>
  )
}