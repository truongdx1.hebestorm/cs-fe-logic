import React, { useState } from 'react'
import { Box, TextField } from '@material-ui/core'
import { cssConstants } from '../../../_constants/css.constants'
import { makeStyles } from '@material-ui/styles'
import { DatePicker } from 'rsuite'
import AsyncSelect from "react-select/async";
import { request } from '../../../_services/request'
import { userService } from '../../../_services/user.service'
import config from 'config'
import CreatableSelect from 'react-select/creatable'
import { customStyles } from '../../../utils'

const SOLUTIONS = [
  { value: 'appeal_paypal_to_review', label: 'Appeal paypal to review' },
  { value: 'convince_customer_to_close_case', label: 'Convince customer to close case' },
  { value: 'no_action_case_under_review', label: 'No action case under review' },
  { value: 'request_refund', label: 'Request refund' }
]

const useStyles = makeStyles({
  input: {
    
  },
  ...cssConstants
})

export const ConfirmCheckPaypal = (props) => {
  const { submitData, handleChange } = props
  const classes = useStyles()
  // const [productTypeOptions, setProductTypeOptions] = useState([])

  const loadProductTypes = (inputValue, callback) => {
    const url = config.apiCoreUrl + '/api/product_types/active'
    const query = {}

    if (inputValue) query['name'] = inputValue

    request.get(url, query).then(res => {
      const data = res.data.data
      const productTypes = data.result.map((pt, index) => ({
        value: pt.name,
        label: pt.name
      }))
      callback(productTypes)
    })
  }
 
  const handleProductTypeChange = (newValue, actionMeta) => {
    handleChange({
      target: {
        name: 'product_types',
        value: newValue.map(v => v.value)
      }
    })
  }

  return (
    <div>
      <Box fontWeight='bold' fontSize={13}>
        Paypal Case Link
      </Box>
      <TextField
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          },
        }}
        margin="dense"
        name="paypal_case_link"
        value={submitData.values.paypal_case_link || ''}
        variant="outlined"
        onChange={handleChange}
        // error={hasError('links')}
      />
      <Box fontWeight='bold' fontSize={13}>
        Product Sku
      </Box>
      <TextField
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          },
        }}
        margin="dense"
        name="product_sku"
        value={submitData.values.product_sku || ''}
        variant="outlined"
        onChange={handleChange}
        // error={hasError('links')}
      />
      <Box fontWeight='bold' fontSize={13}>
        Product Type
      </Box>
      <AsyncSelect
        cacheOptions
        loadOptions={loadProductTypes}
        defaultOptions
        isMulti
        styles={{...customStyles}}
        menuPortalTarget={document.body}
        onChange={handleProductTypeChange}
        value={submitData.values.product_types?.map(x => ({ value: x, label: x }))}
      />
      <Box fontWeight='bold' fontSize={13} marginTop='5px'>
        Check Date
      </Box>
      <DatePicker format="DD-MM-YYYY HH:mm:ss" block
        showMeridian
        ranges={[
          {
            label: 'Now',
            value: new Date()
          }
        ]}
        name="paypal_check_date"
        value={submitData.values.paypal_check_date}
        onChange={(value) => handleChange({
          target: {
            name: 'paypal_check_date',
            value
          }
        })}
      />
    </div>
  )
}

export const ConfirmSendEmail = (props) => {
  const { submitData, handleChange } = props
  const classes = useStyles()

  return (
    <div>
      <Box fontWeight='bold' fontSize={13}>
        Link contact client
      </Box>
      <TextField
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          },
        }}
        margin="dense"
        name="link_contact_client"
        value={submitData.values.link_contact_client || ''}
        variant="outlined"
        onChange={handleChange}
        // error={hasError('links')}
      />

      <Box fontWeight='bold' fontSize={13} marginTop='10px'>
        Check Date
      </Box>
      <DatePicker format="DD-MM-YYYY HH:mm:ss" block
        showMeridian
        ranges={[
          {
            label: 'Now',
            value: new Date()
          }
        ]}
        name="paypal_check_date"
        value={submitData.values.paypal_check_date}
        onChange={(value) => handleChange({
          target: {
            name: 'paypal_check_date',
            value
          }
        })}
      />
    </div>
  )
}

export const AddSolution = (props) => {
  const { submitData, handleChange } = props
  const classes = useStyles()

  const loadUsers = (inputValue, callback) => {
    const query = {}

    if (inputValue) query.name = inputValue

    userService.fetchUsers(query).then(res => {
      const users = res.data.data.result
      const userOptions = users.map(user => ({ value: user.uid, label: user.name }))

      callback(userOptions)
    })
  }

  const handleAssigneeChange = (newValue, actionMeta) => {
    handleChange({
      target: {
        name: 'assignee_id',
        value: newValue.value
      }
    })
  }

  const handleSolutionChange = (newValue, actionMeta) => {
    handleChange({
      target: {
        name: 'solution',
        value: newValue.value
      }
    })
  }

  return (
    <div>
      <Box fontWeight='bold' fontSize={13}>
        Processing Date
      </Box>
      <DatePicker format="DD-MM-YYYY HH:mm:ss" block
        showMeridian
        ranges={[
          {
            label: 'Now',
            value: new Date()
          }
        ]}
        value={submitData.values.processing_date}
        onChange={(value) => handleChange({
          target: {
            name: 'processing_date',
            value
          }
        })}
      />

      <Box fontWeight='bold' fontSize={13} marginTop='10px'>
        Solution
      </Box>
      <CreatableSelect
        isClearable
        onChange={handleSolutionChange}
        options={SOLUTIONS}
        styles={{...customStyles}}
        menuPortalTarget={document.body}
      />
      { submitData.values.solution == 'appeal_paypal_to_review' &&
        <>
          <Box fontWeight='bold' fontSize={13} marginTop='10px'>
            Shipping Label
          </Box>
          <TextField
            fullWidth
            InputProps={{
              classes: {
                input: classes.input
              },
            }}
            margin="dense"
            name="shipping_label"
            value={submitData.values.shipping_label || ''}
            variant="outlined"
            onChange={handleChange}
          />
        </>
      }

      { submitData.values.solution == 'request_refund' &&
        <>
          <Box fontWeight='bold' fontSize={13} marginTop='10px'>
            Request Refund
          </Box>
          <TextField
            fullWidth
            InputProps={{
              classes: {
                input: classes.input
              },
            }}
            margin="dense"
            name="request_refund_amount"
            value={submitData.values.request_refund_amount || ''}
            variant="outlined"
            onChange={handleChange}
          />
        </>
      }

      <Box fontWeight='bold' fontSize={13} marginTop='10px'>
        Assignee
      </Box>
      <AsyncSelect
        cacheOptions
        loadOptions={loadUsers}
        defaultOptions
        style={{ zIndex: 10 }}
        onChange={handleAssigneeChange}
        styles={{...customStyles}}
        menuPortalTarget={document.body}
      />
    </div>
  )
}

export const PaypalDisputeResolve = (props) => {
  const { submitData, handleChange } = props
  const classes = useStyles()

  return (
    <div>
      Do you want to resolve this issue?
    </div>
  )
}

export const PaypalConfirmRefund = (props) => {
  const { submitData, handleChange } = props
  const classes = useStyles()

  return (
    <div>
      <Box fontWeight='bold' fontSize={13}>
        Refunded at
      </Box>
      <DatePicker format="DD-MM-YYYY HH:mm:ss" block
        showMeridian
        ranges={[
          {
            label: 'Now',
            value: new Date()
          }
        ]}
        value={submitData.values.refunded_at}
        onChange={(value) => handleChange({
          target: {
            name: 'refunded_at',
            value
          }
        })}
      />

      <Box fontWeight='bold' fontSize={13} marginTop='10px'>
        Note
      </Box>
      <TextField
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          },
        }}
        margin="dense"
        name="refunded_note"
        value={submitData.values.refunded_note || ''}
        variant="outlined"
        onChange={handleChange}
        maxRows={10}
        multiline
        // error={hasError('links')}
      />

      <Box fontWeight='bold' fontSize={13} marginTop='10px'>
        Refund Amount($)
      </Box>
      <TextField
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          },
        }}
        margin="dense"
        name="refunded_amount"
        value={submitData.values.refunded_amount || ''}
        variant="outlined"
        onChange={handleChange}
        // error={hasError('links')}
      />
    </div>
  )
}