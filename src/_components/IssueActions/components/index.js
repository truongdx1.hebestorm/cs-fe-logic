import React from 'react'
import ResolveAction from './ResolveAction'
import { ConfirmCheckPaypal, ConfirmSendEmail,
  AddSolution as PaypalDisputeAddSolution,
  PaypalDisputeResolve, PaypalConfirmRefund, AddSolution
} from './PaypalDisputeAction'
import { AddAssignee, AddLink, AddOffer, CheckDesign, ConfirmSentOffer, SendOffer } from './IssueAction'

const ActionDefault = (props) => {
  return (
    <div>This is action Default</div>
  )
}

export const schemaViews = {
  confirm_sent_offer: {
    link_contact_offer: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  assign: {
    assignee_id: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  add_followers: {
    assignee_id: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  update_customer_decision: {
    customer_decision: {
      presence: { allowEmpty: false, message: '^Required' },
    },
    solution_id: {
      presence: { allowEmpty: false, message: '^Required' },
    }, assignee_id: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  update_solution: {
    assignee_id: {
      presence: { allowEmpty: false, message: '^Required' },
    },
    solution_id: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  }, add_offer: {
    offer: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  }, resend_order: {
    offer: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  add_custom_design_link: {
    link: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  add_design_link: {
    link: {
      presence: { allowEmpty: false, message: '^Required' },
    }
  },
  send_offer: {
    content: {
      presence: { allowEmpty: false, messge: '^Required' }
    }
  }
}

const componentActions = {
  resolve: ResolveAction,
  confirm_check_paypal: ConfirmCheckPaypal,
  confirm_send_email: ConfirmSendEmail,
  paypal_dispute_add_solution: PaypalDisputeAddSolution,
  paypal_dispute_resolve: PaypalDisputeResolve,
  paypal_confirm_refund: PaypalConfirmRefund,
  assign: AddAssignee,
  add_followers: AddAssignee,
  check_design: CheckDesign,
  confirm_sent_offer: ConfirmSentOffer,
  update_customer_decision: AddSolution,
  update_solution: AddSolution,
  add_offer: AddOffer,
  resend_order: AddOffer,
  add_custom_design_link: AddLink,
  add_design_link: AddLink,
  send_offer: SendOffer
}

export const mappingComponent = (actionCode) => {
  const component = componentActions[actionCode]
  if (!component){
    return ActionDefault
  }

  return component
}

export default ActionDefault