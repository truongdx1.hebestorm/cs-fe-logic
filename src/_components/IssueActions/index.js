import React from 'react'
import issue_request from '../../_services/issue.service'
import { mappingComponent } from './components'
import { Grid } from '@material-ui/core'
import { Button } from 'rsuite'
import { FormModal } from '../../common'

import { schemaViews } from './components'
import { layerRender } from '../../Layer'
import toastr from '../../common/toastr'
import { request } from "../../_services/request";
import config from "config";
import { capitalizeStr } from "../../utils";

export function IssueActionPop(props) {
  // const classes = useStyles();
  const { action, issue, context, onSubmitDone, schema, defaultData } = props

  let Component = action.actionComponent

  if (!Component) {
    Component = mappingComponent(action.code)
  }

  layerRender.showPopup({
    title: capitalizeStr(action.code.replaceAll("_", " ")),
    submitData: defaultData || {},
    visible_content: ["add_followers", "assign"].includes(action.code) ? true : false,
    button: `Done`,
    colorAction: true,
    widthDialog: '40vw',
    schema: schemaViews[action.code] || {},
    customView: (submitData, hasError, onHandleChangeText, setState, onClose, otherProps) =>
      <Component
        {...otherProps}
        submitData={submitData}
        onHandleChangeText={onHandleChangeText}
        hasError={hasError}
        issue={issue}
        code={action.code}
      />,
    onAction: (submitData) => {
      return new Promise((resolve) => {
        if (!submitData.isValid) {
          resolve(false)
          return
        }
        var formSubmit = {}
        var arrKeys = Object.keys(submitData.values || {})
        arrKeys.map(key => {
          if (submitData.values[key]) {
            if (typeof submitData.values[key] == 'object' && submitData.values[key] instanceof Object) {
              if (["assignee_id", "customer_decision"].includes(key)) {
                if (action.code == "add_followers") {
                  formSubmit["follower_ids"] = submitData.values.assignee_id.map(x => x.uid)
                } else if (action.code == "assign") {
                  formSubmit["assignee_id"] = submitData.values.assignee_id.uid
                } else {
                  formSubmit[key] = submitData.values[key].uid
                }
              } else if (key == "offer") {
                formSubmit[key] = submitData.values[key].name
              } else if (key == "extra") {
                formSubmit[key] = submitData.values[key]
              } else {
                formSubmit[key] = submitData.values[key].id
              }
            } else if (["refund_percent", "unit", "refund_amount"].includes(key)) {
              if (["A3", "A5"].includes((submitData?.values?.solution?.code || '').toUpperCase())) {
                if (key == "refund_percent") {
                  formSubmit[key] = `${submitData.values[key]}% ${submitData.values["unit"].name}`
                } else if (key == "refund_amount") {
                  formSubmit[key] = submitData.values[key]
                }
              }
            } else if (key == "file") {
              formSubmit["attachments"] = [submitData.values[key]]
            } else {
              formSubmit[key] = submitData.values[key]
            }
          }
        })
        request.post_json(`${config.issueApiUrl}/api/actions/commit`, {
          action_code: action.code,
          action_data: formSubmit,
          object_id: context.objectId,
          object_type: "issues",
        }).then(res => {
          if (res.data.success) {
            toastr.success(`${capitalizeStr(action.code.replaceAll("_", " "))} successful`)
            onSubmitDone()
            resolve(true)
          } else {
            resolve(false)
            toastr.error(res.data.msg)
          }
        }, err => {
          resolve(false)
          toastr.error(err)
        })
      })
    }
  })
}

const getIssueActionFormData = (args) => {
  const { action, issue, context, onSubmitDone } = args

  let Component = action.actionComponent

  if (!Component){
    Component = mappingComponent(action.code)
  }

  return {
    title: action.label,
    customView: (submitData, handleChange) => <Component
      submitData={submitData} handleChange={handleChange}
      issue={issue}
    />,
    action: {
      title: 'Submit',
      onAction: (submitData) => {
        const data = submitData.values
        return issue_request.commitAction({
          code: action.code,
          data: submitData.values,
          objectId: context.objectId,
          objectType: context.objectType,
          objectIds: context.objectIds
        })
      },
      onActionDone: onSubmitDone
    }
  }
}

export const IssueActionButton = (props) => {
  const { steps, issue, onActionClick, objectType, onSubmitDone } = props
 
  const handleClick = (e, action) => {
    const data = getIssueActionFormData({
      action, issue,
      context: {
        objectId: issue.id,
        objectType: objectType || 'issues'
      },
      onSubmitDone: onSubmitDone || (() => {})
    })

    FormModal.instance.current.showForm(data)

    if (onActionClick){
      onActionClick(data)
    }
  }

  return (
    <>
    <FormModal />
    <Grid container style={{ paddingBottom: '10px'}}>
      <Grid item>
        {steps.map((action, index) =>
          <Button style={{ color: '#fff', backgroundColor: '#4caf50' }} key={index} onClick={(e) => handleClick(e, action)}>
            { action.label }
          </Button>
        )}
      </Grid>
    </Grid>
    </>
  )
}