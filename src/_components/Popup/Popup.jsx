import React, { Component } from 'react'
import PropTypes from 'prop-types'
import toastr from '../../common/toastr'
import { layerRender } from "../../Layer"
import { injectIntl } from "react-intl";
import { connect } from 'react-redux'
import {
    TextField,
    Box,
    Button
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import validate from 'validate.js';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
// import { scheduleActions } from '../_actions'
import clsx from 'clsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight, faArrowLeft, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { cssConstants } from '../../_constants'
import { PopUpBulkPrices } from '../../_components'

import Cookies from "universal-cookie";
import config from 'config'
const cookies = new Cookies()
import axios from 'axios'
import { makeId } from '../../utils'

const propTypes = {
    // classes: PropTypes.object.isRequired
}

const defaultProps = {
    // company: []
}



const useStyles = theme => ({
    ...cssConstants,
    modal: {
        // width: '50%',
        margin: '0 10px',
        maxHeight: '90vh',
        // minHeight: '90vh',
    },
    progress: {
        width: '15px !important',
        height: '15px !important',
        color: '#8c9196'
    }, button: {
        position: 'relative',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        textTransform: 'none',
        minHeight: '1.6rem',
        minWidth: '1.6rem',
        margin: '0 0 0 10px',
        padding: '.6rem 1rem',
        background: 'linear-gradient(180deg,#fff,#f9fafb)',
        border: '.1rem solid var(--p-border,#c4cdd5)',
        boxShadow: '0 1px 0 0 rgba(22,29,37,.05)',
        borderRadius: '3px',
        lineHeight: 1,
        color: '#212b36',
        textAlign: 'center',
        cursor: 'pointer',
        userSelect: 'none',
        textDecoration: 'none',
        transitionProperty: 'background,border,box-shadow',
        transitionDuration: 'var(--p-override-none,.2s)',
        transitionTimingFunction: 'var(--p-override-none,cubic-bezier(.64,0,.35,1))',
        '&:hover': {
            background: 'linear-gradient(180deg,#fff,#eee)',
        }
    },
    button_secondary: {
        background: 'linear-gradient(180deg,#e6391a,#d53417)',
        borderColor: '#b02b13',
        boxShadow: 'inset 0 1px 0 0 #e73d1f, 0 1px 0 0 rgba(22,29,37,.05), 0 0 0 0 transparent',
        color: '#fff',
        '&:hover': {
            background: 'linear-gradient(180deg,#d53417,#d53417)',
        }
    }, footer: {
        borderTop: '1px solid #ccc',
        padding: '15px 10px',
        display: 'flex'
    }, spacer: {
        flexGrow: 1
    }, button_primary: {
        background: 'linear-gradient(180deg,#6371c7,#5563c1)',
        borderColor: '#3f4eae',
        boxShadow: 'inset 0 1px 0 0 #6774c8, 0 1px 0 0 rgba(22,29,37,.05), 0 0 0 0 transparent',
        color: '#fff',
        '&:hover': {
            background: 'linear-gradient(180deg,#5563c1,#5563c1)',
        }
    }, error: {
        fontSize: 14,
        color: 'var(--p-text-critical,#bf0711)',
        fill: 'var(--p-icon-critical,#bf0711)'
    }, button_icon: {
        outline: 'none',
        position: 'relative',
        zIndex: 10,
        display: 'inline-block',
        minWidth: '2.6rem',
        margin: 0,
        padding: '.7rem .8rem',
        background: 'var(--p-action-secondary,linear-gradient(180deg,#fff,#f9fafb))',
        border: '.1rem solid var(--p-border,#c4cdd5)',
        borderRadius: '3px',
        lineHeight: 1,
        color: 'var(--p-text,#212b36)',
        textAlign: 'center',
        cursor: 'pointer',
        userSelect: 'none',
        textDecoration: 'none',
        transitionProperty: 'color,background,border,box-shadow',
        transitionDuration: '.2s',
        transitionTimingFunction: 'cubic-bezier(.64,0,.35,1)',
        '&:hover': {
            background: 'var(--p-action-secondary,linear-gradient(180deg,#fff,#ccc))',
        }, '&.disable': {
            background: '#f4f6f8 !important',
            color: '#919eab',
            cursor: 'default',
            boxShadow: 'none',
        }
    }, content_pop: {
        overflowY: 'auto',
        flexDirection: 'column',
        height: '100%',
        display: 'flex',
        fontSize: 14

    }, content_visible: {
        overflowY: 'visible !important',
    }
});


class Popup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            submitData: {
                isValid: false,
                values: this.props.submitData ? this.props.submitData : {},
                change: {},
                touched: {},
                errors: {},
                warning: {},
                isSubmiting: false,
            },
            isLoading: false,
            tabKey: 0,
            pageImage: {
                values: {}
            },
            formCondition: {
                isValid: false,
                values: {},
                change: {},
                touched: {},
                errors: {},
                isSubmiting: false,
            }
        }
        this.wrapperRef = React.createRef()
        this.handleClick = this.handleClick.bind(this);
        this.onHandleChangeText = this.onHandleChangeText.bind(this)
        this.hasError = this.hasError.bind(this)

        this.dropzoneRef = React.createRef()
        this.dropzoneMobileRef = React.createRef()

        this.onDrop = (files) => {
            var submitData = Object.assign({}, this.state.submitData)
            var arrKeys = Object.keys(submitData.values || {})
            if (arrKeys.includes('images')) {
                var files1 = Object.assign([], submitData['values']?.images?.data)
                files.forEach(file => files1.push({ file: file, status: 0, path: null, isUpLoad: true }))
                submitData['values']['images']['data'] = files1
                submitData['change']['images'] = submitData['values']['images']
            } else if (arrKeys.includes('image')) {
                var files1 = []
                files.forEach(file => files1.push({ file: file, status: 0, path: null, isUpLoad: true }))
                submitData['values']['image'] = files1.length > 0 ? files1[0] : null
                submitData['change']['image'] = files1.length > 0 ? files1[0] : null
            }
            this.setState({ submitData })
        }

        this.onDropMobile = (files) => {
            var submitData = Object.assign({}, this.state.submitData)
            var arrKeys = Object.keys(submitData.values || {})
            if (arrKeys.includes('images_mobile')) {
                var files1 = Object.assign([], submitData['values']?.images?.data)
                files.forEach(file => files1.push({ file: file, status: 0, path: null, isUpLoad: true }))
                submitData['values']['images_mobile']['data'] = files1
                submitData['change']['images_mobile'] = submitData['values']['images_mobile']
            } else if (arrKeys.includes('image_mobile')) {
                var files1 = []
                files.forEach(file => files1.push({ file: file, status: 0, path: null, isUpLoad: true }))
                submitData['values']['image_mobile'] = files1.length > 0 ? files1[0] : null
                submitData['change']['image_mobile'] = files1.length > 0 ? files1[0] : null
            }
            this.setState({ submitData })
        }

        this.schema = {
            reason_cancel: {
                presence: { allowEmpty: false, message: 'Bạn chưa điền lý do' },
            }
        };

    }

    componentDidMount() {
        if (this.props.schema) {
            this.schema = this.props.schema
        }
        var formCached = this.getFormCached(this.state.submitData)
        this.setState({ formCached })
        if (this.props.onLoadOption2) {
            this.setState({ isLoading: true }, () => {
                this.props.onLoadOption2().then(res => {
                    var submitData = Object.assign({}, this.state.submitData)
                    submitData['values']['images'] = res
                    this.setState({ submitData, images_2: res, isLoading: false })
                })
            })
        }
        if (this.props.onLoadOption) {
            this.setState({ isLoading: true }, () => {
                this.props.onLoadOption().then(res => {
                    if (this.props.onLoadOption2) {
                        this.setState({ images: res })
                    } else {
                        var submitData = Object.assign({}, this.state.submitData)
                        submitData['values']['images'] = res
                        this.setState({ submitData, isLoading: false })
                    }
                })
            })
        }

        if (this.props.onLoadData) {
            this.setState({ isLoading: true }, () => {
                this.props.onLoadData().then(res => {
                    if (res) {
                        console.log(res, 'res')
                        var submitData = Object.assign({}, this.state.submitData)
                        submitData['values'] = res
                        this.setState({ submitData, isLoading: false })
                    } else {
                        this.setState({ isEmpty: true })
                    }

                })
            })
        }

        if (this.props.onLoadVariants) {
            this.setState({ isLoading: true }, () => {
                this.props.onLoadVariants().then(res => {
                    var submitData = Object.assign({}, this.state.submitData)
                    submitData['values']['variants'] = res
                    this.setState({ submitData, isLoading: false })
                })
            })
        }

        if (this.props.onLoadData3) {
            this.setState({ isLoading: true }, () => {
                this.props.onLoadData3().then(res => {
                    var submitData = Object.assign({}, this.state.submitData)
                    submitData['values']['orders'] = res
                    this.setState({ submitData, isLoading: false })
                })
            })
        }

        // document.addEventListener('click', this.handleClick);
    }



    componentWillUnmount() {
        // important
        // document.removeEventListener('click', this.handleClick);

    }

    getFormCached = (formState) => {
        var formCached = {
            values: {},
            errors: {},
            change: {},
            touched: {},
            isSubmiting: false,
            isLoading: false,
        }
        var arrKeys = Object.keys(formState.values)
        arrKeys.map((key, index) => {
            if (typeof formState.values[key] == 'array' || formState.values[key] instanceof Array) {
                formCached.values[key] = []
                formState.values[key].map((r, j) => {
                    if (r && (typeof r == 'object' || r instanceof Object)) {
                        var item = {}
                        var keys = Object.keys(r)
                        keys.map((k, i) => {
                            if (typeof r[k] == 'array' || r[k] instanceof Array) {
                                var item2 = []
                                r[k].map((m, jj) => {
                                    if (m && (typeof m == 'object' || m instanceof Object)) {
                                        var item3 = {}
                                        var keys2 = Object.keys(m)
                                        keys2.map((l, ff) => {
                                            item3[l] = m[l]
                                        })
                                        item2.push(item3)
                                    } else {
                                        item2.push(m)
                                    }
                                })
                                item[k] = item2
                            } else {
                                item[k] = r[k]
                            }
                        })
                        formCached.values[key].push(item)
                    } else {
                        formCached.values[key].push(r)
                    }
                })
            } else if (formState.values[key] && (typeof formState.values[key] == 'object' || formState.values[key] instanceof Object)) {
                formCached.values[key] = {}
                var keys = Object.keys(formState.values[key])
                keys.map((k, i) => {
                    formCached.values[key][k] = formState.values[key][k]
                })
            } else {
                formCached.values[key] = formState.values[key]
            }
        })
        return formCached
    }

    handleClick = (event) => {
        const { target } = event
        if (!this.wrapperRef.current.contains(target)) {
            //   this.props._layer.destroy();
            // this.props._layerInvi.destroy();
            // this.props._layer.destroy();
            // ReactDOM.unmountComponentAtNode(node)
        }
    }


    validateForm(submitData) {
        const errors = validate(submitData.values, this.schema);
        submitData['isValid'] = errors ? false : true
        submitData['errors'] = errors || {}
        return submitData
    }

    hasError = (field) => {
        var submitData = Object.assign({}, this.state.submitData);
        return (submitData.isSubmiting || submitData.touched[field]) && submitData.errors[field] ? true : false
    }

    hasWarning = (field) => {
        var submitData = Object.assign({}, this.state.submitData);
        return (submitData.isSubmiting || submitData.touched[field]) && submitData.warning[field] ? true : false
    }

    onHandleChangeText(e) {
        var submitData = Object.assign({}, this.state.submitData);
        submitData['values'][e.target.name] = e.target.type === 'checkbox' ? (e.target.checked ? 1 : 0) : e.target.value
        submitData['change'][e.target.name] = e.target.type === 'checkbox' ? (e.target.checked ? 1 : 0) : e.target.value
        this.setState({ submitData })
    }

    openDialogMedia = () => {
        // Note that the ref is set async,
        // so it might be null at some point 
        if (this.dropzoneRef.current) {
            this.dropzoneRef.current.open()
        }
    };

    openDialogMediaMobile = () => {
        // Note that the ref is set async,
        // so it might be null at some point 
        if (this.dropzoneMobileRef.current) {
            this.dropzoneMobileRef.current.open()
        }
    };

    isUpLoad = () => {
        var upload = false
        var submitData = Object.assign({}, this.state.submitData)
        if (submitData['values']['images'] && submitData['values']['images']['data']) {
            submitData['values']['images']['data'].map((image, index) => {
                if (image.isUpLoad && !upload) {
                    upload = true
                }
            })
        }
        return upload
    }

    onDropFile = (accepted, rejected) => {
        if (Object.keys(rejected).length !== 0) {
            return
        }
        const extra = Object.assign({}, this.state.submitData.values.extra)
        const files = accepted.map(e => ({ id: makeId(20), filename: e.name, file: e, status: 'uploading', type: e.type.split('/')[0] == 'image' ? 'image' : 'file' }))

        if (extra.files) {
            files.forEach(file => {
                extra.files.push(file)
            })
        } else {
            extra['files'] = files
        }
        const submitData = Object.assign({}, this.state.submitData)
        submitData.values['extra'] = extra
        this.setState({ submitData }, () => {
            if (cookies.get('user_hebecore')) {
                files.map(file => {
                    var formData = new FormData();
                    formData.append('file', file.file);
                    formData.append("namespace", "issues/files")
                    axios({
                        method: "POST",
                        url: `${config.apiUrl}/api/upload_file`,
                        headers: {
                            'x-access-token': cookies.get('user_hebecore').token
                        },
                        data: formData
                    }).then(res => {
                        if (res.data.success) {
                            const submitData = Object.assign({}, this.state.submitData)
                            const extra = Object.assign({}, submitData.values.extra)
                            const files = Object.assign([], extra?.files)
                            var arr = files.map(e => e.id == file.id ? { ...res.data.data, id: e.id, status: 'uploaded', type: e.type } : e)
                            submitData.values.extra = { files: arr }
                            this.setState({ submitData })
                        } else {
                            const submitData = Object.assign({}, this.state.submitData)
                            const extra = Object.assign({}, submitData.values.extra)
                            const files = Object.assign([], extra?.files)
                            var arr = files.map(e => e.id == file.id ? { ...e, status: 'failed' } : e)
                            submitData.values.extra = { files: arr }
                            this.setState({ submitData })
                        }
                    }, err => {
                        const submitData = Object.assign({}, this.state.submitData)
                        const extra = Object.assign({}, submitData.values.extra)
                        const files = Object.assign([], extra?.files)
                        var arr = files.map(e => e.id == file.id ? { ...e, status: 'failed' } : e)
                        submitData.values.extra = { files: arr }
                        this.setState({ submitData })
                    })
                })
            }
        })
    };





    render() {
        const {
            classes, title, content, button, onClose, customView,
            variantConfirm, colorConfirm, colorAction, onConfirm,
            onAction, titleAction, hideCancel, no_pd, widthDialog, nextPreBtn, addImage,
            activeAction, visible_content, onOtherAction, titleOtherAction, check_errors, bgContent, onQuickAction, quick_button
        } = this.props;
        const { formatMessage } = this.props.intl;
        const { submitData, isLoading, isEmpty } = this.state;
        return (
            <div data-layerid="2" ref={this.wrapperRef}>
                <div className="ModalLayer">
                    <div className="ModalBuffer">
                        <div className="ModalBuffer-topBuffer">
                        </div><div className="ModalBuffer-content">
                            <div className={clsx("ModalPaneWithBuffer-pane", classes.modal)} >
                                <div className={clsx("Dialog d-flex flex-column")} style={{ width: widthDialog ? widthDialog : '580px', height: '100%' }}>
                                    <header className="Dialog-header">
                                        <div className="Dialog-headerTitle">{title}</div>
                                        <div role="button" tabIndex="0" className="CloseButton Dialog-closeButton">
                                            <svg className="Icon--small Icon XIcon CloseButton-xIcon" focusable="false" viewBox="0 0 32 32"
                                                onClick={() => {
                                                    onClose()
                                                }}>
                                                <path d="M18.1,16L27,7.1c0.6-0.6,0.6-1.5,0-2.1s-1.5-0.6-2.1,0L16,13.9l-8.9-9C6.5,4.3,5.6,4.3,5,4.9S4.4,6.4,5,7l8.9,8.9L5,24.8c-0.6,0.6-0.6,1.5,0,2.1c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4l8.9-8.9l8.9,8.9c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4c0.6-0.6,0.6-1.5,0-2.1L18.1,16z"></path>
                                            </svg>
                                        </div>
                                    </header>
                                    {
                                        isEmpty ?
                                            <div className={'d-flex flex-column justify-content-center align-items-center pt-4 pb-4'}>
                                                <FontAwesomeIcon icon={faInfoCircle} size='4x' style={{ color: '#6d7175' }} />
                                                <span style={{ fontSize: 16, fontWeight: 'bold' }} className={'mt-3'}>There’s no data</span>
                                                <span>Check the URL and try again.</span>
                                            </div> :
                                            ((!submitData.isSubmiting && isLoading) ?
                                                <div className='d-flex align-items-center justify-content-center' style={{ minHeight: '10rem' }}>
                                                    <CircularProgress />
                                                </div>
                                                :
                                                <div className={clsx("ConfirmationDialog-dialogContents flex-grow-1", classes.content_pop, (no_pd ? 'p-0' : ''), visible_content ? classes.content_visible : '', bgContent ? bgContent : '')}>
                                                    <div className={''}>
                                                        {content}
                                                    </div>
                                                    {customView ? customView(submitData,
                                                        this.hasError,
                                                        this.onHandleChangeText,
                                                        (data) => { this.setState(data) },
                                                        onClose, {
                                                        onDrop: this.onDrop,
                                                        onDropMobile: this.onDropMobile,
                                                        refDrop: this.dropzoneRef,
                                                        refDropMobile: this.dropzoneMobileRef,
                                                        openDialogMedia: this.openDialogMedia,
                                                        openDialogMediaMobile: this.openDialogMediaMobile,
                                                        state: this.state,
                                                        hasWarning: this.hasWarning,
                                                        ctx: this
                                                    }) : ''}
                                                </div>)}
                                    <div className={classes.footer}>
                                        {nextPreBtn && <div className={'d-flex'}>
                                            <button className={clsx(classes.button_icon, (!submitData['values']['images']?.prev_page_url || this.isUpLoad()) ? 'disable' : '')}
                                                onClick={() => {
                                                    this.setState({ isLoading: true })
                                                    if (submitData['values']['images']?.prev_page_url) {
                                                        if (this.props.onLoadOption2 && this.state.tabKey == 0) {
                                                            this.props.onLoadOption2(submitData['values']['images']?.prev_page_url)
                                                                .then(res => {
                                                                    console.log(res)
                                                                    var submitData = Object.assign({}, this.state.submitData)
                                                                    submitData['values']['images'] = res
                                                                    this.setState({ submitData, isLoading: false })
                                                                })
                                                        } else {
                                                            this.props.onLoadOption(submitData['values']['images']?.prev_page_url)
                                                                .then(res => {
                                                                    var submitData = Object.assign({}, this.state.submitData)
                                                                    submitData['values']['images'] = res
                                                                    this.setState({ submitData, isLoading: false })
                                                                })
                                                        }
                                                    }
                                                }}
                                                disabled={!submitData['values']['images']?.prev_page_url ? true : false}>
                                                <FontAwesomeIcon icon={faArrowLeft}
                                                    size={'1x'} style={{ fontSize: 14 }} />
                                            </button>
                                            <button className={clsx(classes.button_icon, (!submitData['values']['images']?.next_page_url || this.isUpLoad()) ? 'disable' : '')}
                                                onClick={() => {
                                                    this.setState({ isLoading: true })
                                                    if (submitData['values']['images']?.next_page_url) {
                                                        if (this.props.onLoadOption2 && this.state.tabKey == 0) {
                                                            this.props.onLoadOption2(submitData['values']['images']?.next_page_url)
                                                                .then(res => {
                                                                    console.log(res)
                                                                    var submitData = Object.assign({}, this.state.submitData)
                                                                    submitData['values']['images'] = res
                                                                    this.setState({ submitData, isLoading: false })
                                                                })
                                                        } else {
                                                            this.props.onLoadOption(submitData['values']['images']?.next_page_url)
                                                                .then(res => {
                                                                    console.log(res)
                                                                    var submitData = Object.assign({}, this.state.submitData)
                                                                    submitData['values']['images'] = res
                                                                    this.setState({ submitData, isLoading: false })
                                                                })
                                                        }

                                                    }
                                                }}
                                                disabled={!submitData['values']['images']?.next_page_url ? true : false}>
                                                <FontAwesomeIcon icon={faArrowRight}
                                                    size={'1x'} style={{ fontSize: 14 }} />
                                            </button>
                                        </div>}
                                        <div className={classes.spacer} />
                                        {!hideCancel &&

                                            <Button
                                                disabled={(this.state.submitData.isLoading || this.state.submitData.isLoadingOther) || false}
                                                className={classes.button}
                                                onClick={() => {
                                                    onClose()
                                                }}
                                                size="small"
                                            >
                                                <FormattedMessage id='cancel' />
                                            </Button>
                                        }
                                        {addImage && <Button
                                            disabled={this.state.submitData.isLoading}
                                            className={classes.button}
                                            onClick={() => { this.openDialogMedia() }}
                                            size="small"
                                            disabled={this.isUpLoad()}
                                        >
                                            {this.isUpLoad() ? <CircularProgress style={{ width: 20, height: 20 }} /> : 'Add Image'}
                                        </Button>}
                                        {onConfirm && <Button
                                            disabled={this.state.submitData.isLoading || this.isUpLoad()}
                                            className={classes.button}
                                            variant={variantConfirm ? variantConfirm : "contained"}
                                            color={colorConfirm ? colorConfirm : "secondary"}
                                            size="small"
                                            onClick={() => {
                                                var submitData = Object.assign({}, this.state.submitData);
                                                var errors = null;
                                                if (!hideReason) {
                                                    errors = validate(submitData.values, this.schema);
                                                } else if (this.props.schedules && this.props.schedules.length > 0) {
                                                    errors = validate(submitData.values, this.schema2);
                                                }

                                                submitData['isValid'] = errors ? false : true
                                                submitData['errors'] = errors || {}
                                                submitData['isSubmiting'] = true
                                                submitData['isLoading'] = submitData['isValid'] ? true : false
                                                this.setState({ submitData })
                                                if (submitData.isValid) {
                                                    this.props.onConfirm(submitData.values, onClose).then(res => {
                                                        submitData.isLoading = false
                                                        this.setState({ submitData }, () => {
                                                            if (res) {
                                                                onClose()
                                                            }
                                                        })

                                                    })
                                                }
                                            }}>
                                            {submitData.isLoading ? <CircularProgress className={classes.progress} /> : button}
                                        </Button>}
                                        {onOtherAction &&
                                            <button
                                                disabled={(this.state.submitData.isLoading || this.state.submitData.isLoadingOther) || false}
                                                className={clsx(classes.button_normal, 'ml-2',
                                                    (this.state.submitData.isLoading || this.state.submitData.isLoadingOther) ? 'disable1' : '')}
                                                // variant={variantConfirm ? variantConfirm : "contained"}
                                                size="small"
                                                onClick={() => {
                                                    var submitData = Object.assign({}, this.state.submitData);
                                                    submitData['isLoadingOther'] = true
                                                    submitData['isSubmiting'] = true
                                                    if (this.props.schema) {
                                                        submitData = this.validateForm(submitData)
                                                    } else {
                                                        submitData['isValid'] = true
                                                    }
                                                    this.setState({ submitData })
                                                    this.props.onOtherAction(submitData,
                                                        this.state,
                                                        (data) => { this.setState(data) }
                                                    ).then(res => {
                                                        submitData.isLoadingOther = false
                                                        this.setState({ submitData }, () => {
                                                            if (res) {
                                                                onClose()
                                                            }
                                                        })
                                                    })

                                                }}>
                                                {submitData.isLoadingOther ? <CircularProgress className={classes.progress} /> : (titleOtherAction ? titleOtherAction : button)}
                                            </button>}
                                        {onAction &&
                                            <button
                                                disabled={this.state.submitData.isLoading || this.state.submitData.isLoadingOther || this.isUpLoad() || (activeAction ? activeAction(this.state) : false) || (check_errors ? Object.keys(this.state.submitData.errors)?.length > 0 : false)}
                                                className={clsx(classes.button_normal, 'ml-2',
                                                    (this.state.submitData.isLoading ||
                                                        this.state.submitData.isLoadingOther ||
                                                        this.isUpLoad() ||
                                                        (activeAction ? activeAction(this.state) : false) ||
                                                        (check_errors ? Object.keys(this.state.submitData.errors)?.length > 0 : false)) ? 'disable1' : (colorAction ? 'primary' : 'secondary2'))}
                                                // variant={variantConfirm ? variantConfirm : "contained"}
                                                size="small"
                                                onClick={() => {
                                                    var submitData = Object.assign({}, this.state.submitData);
                                                    submitData['isLoading'] = true
                                                    submitData['isSubmiting'] = true
                                                    if (this.props.schema) {
                                                        submitData = this.validateForm(submitData)
                                                    } else {
                                                        submitData['isValid'] = true
                                                    }
                                                    this.setState({ submitData })
                                                    this.props.onAction(submitData,
                                                        this.state,
                                                        (data) => { this.setState(data) }
                                                    ).then(res => {
                                                        submitData.isLoading = false
                                                        this.setState({ submitData }, () => {
                                                            if (res) {
                                                                onClose()
                                                            }
                                                        })
                                                    })

                                                }}>
                                                {submitData.isLoading ? <CircularProgress className={classes.progress} /> : (titleAction ? titleAction : button)}
                                            </button>}

                                        {onQuickAction &&
                                            <button
                                                disabled={this.state.submitData.isLoading}
                                                className={clsx(classes.button_normal, 'ml-2',
                                                    (this.state.submitData.isLoading) ? 'disable1' : 'secondary2')}
                                                // variant={variantConfirm ? variantConfirm : "contained"}
                                                size="small"
                                                onClick={() => {
                                                    var submitData = Object.assign({}, this.state.submitData);
                                                    submitData['isLoading'] = true
                                                    submitData['isSubmiting'] = true
                                                    if (this.props.schema) {
                                                        submitData = this.validateForm(submitData)
                                                    } else {
                                                        submitData['isValid'] = true
                                                    }
                                                    this.setState({ submitData })
                                                    this.props.onQuickAction(submitData,
                                                        this.state,
                                                        (data) => { this.setState(data) }
                                                    ).then(res => {
                                                        submitData.isLoading = false
                                                        this.setState({ submitData }, () => {
                                                            if (res) {
                                                                onClose()
                                                            }
                                                        })
                                                    })

                                                }}>
                                                {submitData.isLoading ? <CircularProgress className={classes.progress} /> : (quick_button || '')}
                                            </button>}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ModalBuffer-bottomBuffer"></div>
                    </div>
                    <div className="LayerContext-layerContainer">
                    </div>
                </div>
                {this.state.isBulk && <PopUpBulkPrices
                    submitData={this.state.formCondition.values}
                    onClose={() => {
                        this.setState({ isBulk: false })
                    }}
                    onReloadData={() => {
                        onClose()
                        this.props.onReload()
                        // var submitData = Object.assign({}, this.state.submitData)
                        // var pos = submitData['values']['variants'].findIndex(x => x.id == data.id)
                        // if(data.success){
                        //     submitData['values']['variants'][pos]['apply_status']=1
                        // }else{
                        //     submitData['values']['variants'][pos]['apply_status']=2
                        // }
                        // this.setState({ submitData })
                    }}
                />}
            </div>
        );
    }

}

// PopUpPJ.propTypes = propTypes
// PopUpPJ.defaultProps = defaultProps

Popup.propTypes = propTypes
Popup.defaultProps = defaultProps

function mapStateToProps(state) {
    const { authentication, schedules } = state;
    const { user } = authentication;
    return {
        user: user,
        schedules
    }
}
const connectedApp = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(Popup)));

export { connectedApp as Popup }