/* eslint-disable react/prop-types */
import React from 'react'
import { Route } from 'react-router-dom'
import config from 'config'
import Cookies from 'universal-cookie'
const cookies = new Cookies()
let ref = window.location.protocol + '//' + window.location.host + window.location.pathname
export const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (
        // (cookies.get('user_hebecore') && cookies.get('user_hebecore').token) ?
        <Component
          {...props}
          {...rest}
          onRouteChange={rest.onRouteChange}
        />
        // : (window.location.assign(`${config.accountUrl}/login?ref=${ref}`))
      )}
    />
  )
}
