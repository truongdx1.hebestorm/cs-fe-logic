import React, { Suspense } from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

const RouteWithLayout = props => {
  const { layout: Layout, component: Component, ...rest } = props;
  return (
   
    <Route
      {...rest}
      render={(matchProps) => {
        if (Layout) {
          return (
            <Layout path={rest.path}>
              <Suspense fallback={'Loading'}>
                <Component
                  {...matchProps}
                  {...rest}
                />
              </Suspense>
            </Layout>
          )
        } else {
          return (
            <Suspense fallback={'Loading'}>
              <Component
                {...matchProps}
                {...rest}
              />
            </Suspense>
          )
        }
      }}
    />
  );
};

RouteWithLayout.propTypes = {
  component: PropTypes.any.isRequired,
  layout: PropTypes.any,
  path: PropTypes.string
};

export default RouteWithLayout;
