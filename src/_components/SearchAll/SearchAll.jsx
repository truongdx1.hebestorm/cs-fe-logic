/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
    Typography, TextField, Button, FormControlLabel,
    Popper, Box,
    ClickAwayListener,
    MenuItem, MenuList, Paper, Grow, Tooltip, CircularProgress
} from '@material-ui/core';
import clsx from 'clsx';
import chroma from 'chroma-js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faCaretDown, faStar, faSort, faTimes, faTimesCircle, faPlus, faUserTie } from '@fortawesome/free-solid-svg-icons'
import SelectBase from '../SelectBase'
// import { productService } from '../../_services'
import config from 'config'
import { history } from '../../_helpers'
import { cssConstants } from '../../_constants'

const useStyles = theme => ({
    ...cssConstants,
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '3px 0',
        position: 'relative',
        fontSize: 14
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, itemSelect: {
        padding: '8px 10px',
        cursor: 'pointer',
        '&:hover': {
            background: chroma('#3f4eae').alpha(0.2).css()
        }
    }, popper: {
        width: '100%',
        zIndex: '200',
        // maxHeight: '200px',
        // boxShadow: 'var(--p-popover-shadow, 0 0 0 1px rgba(39,44,48,0.05), 0 2px 7px 1px rgba(39,44,48,0.16))'
    }, loadding_container: {
        display: 'flex',
        padding: '10px',
        alignItems: 'center',
        justifyContent: 'center'
    }, sub_title: {
        fontSize: 14,
        color: '#637381',
    }, item_menu: {
        cursor: 'pointer',
        '&:hover': {
            background: '#edeeef'
        }
    }, menu_list: {
        maxHeight: '300px',
        overflow: 'auto'
    }, inputSearch: {
        float: 'left',
        backgroundColor: '#fff'
    }, pager: {
        backgroundColor: '#000',
        color: 'var(--p-text-subdued,#637381)'
    }, selection: {
        backgroundColor: 'transparent !important',
        color: 'var(--p-text-subdued,#637381)',
        border: 'none !important',
        outline: 'none !important'
    }, image_item: {
        width: '50',
        height: '50'
    }, img: {
        width: '50px',
        height: '50px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 'var(--p-border-radius-base,3px)',
        border: '.1rem solid var(--p-border-subdued,#dfe3e8)',
        background: 'var(--p-surface,#f9fafb)',
    }, menu_item_search: {
        '&:hover': {
            background: '#3d3f40 !important'
        }
    }
});


class SearchAll extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            formState: {
                values: {
                    query: '',
                    type_search: 0
                }
            },
            itemSelect: [],
            items: []
        }
        this.wrapperRef = React.createRef()
        this.poperRef = React.createRef();
        this.isLoading = false
        this.start = -1

    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        this.schedule()
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
        if (formState.values.query && (formState.values.query || '').trim() != '') {
            this.start = 1
        }
    }

    schedule = () => {
        setTimeout(() => {
            if (this.start == 0) {
                var formState = Object.assign({}, this.state.formState)
                this.getData(null, formState.values)
            }
            this.start -= 1
            this.schedule()
        }, 300)
    }

    getData = (url, queryObject) => {
        // this.isLoading = true
        // this.forceUpdate()
        // productService.searchAll(url, queryObject)
        //     .then(res => {
        //         if (res.status == 200) {
        //             this.setState({ data: res.data.result })
        //         }
        //         this.isLoading = false
        //         this.forceUpdate()
        //     },
        //         error => {
        //             this.isLoading = false
        //             this.forceUpdate()

        //         }
        //     )
    }

    clearData = () => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'] = {
            query: '',
            type_search: 0
        }
        this.setState({ open: false, formState, data: null })
    }

    getSearch = (url, queryObject) => {
        // this.setState({ isLoadMore: true }, () => {
        //     productService.searchAll(url, queryObject)
        //         .then(res => {
        //             if (res.status == 200) {
        //                 var data = Object.assign({}, this.state.data)
        //                 data['data'] = [...data['data'], ...res.data.result.data]
        //                 data['next_page_url'] = res.data.result.next_page_url
        //                 this.setState({ data, isLoadMore: false })
        //             } else {
        //                 this.setState({ isLoadMore: false })
        //             }
        //         },
        //             error => {
        //                 this.setState({ isLoadMore: false })

        //             }
        //         )
        // })

    }


    render() {
        const { classes, className, placeholder } = this.props;
        const { array_value, formState, open, isLoading, data, isLoadMore } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense d-flex align-items-center', classes.container, className, classes.inputSearch)}
                ref={this.wrapperRef}
                onBlur={() => {
                    // this.clearData()
                }}
            >
                <FontAwesomeIcon icon={faSearch} className='ml-2'
                    size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                <TextField
                    ref={this.poperRef}
                    onFocus={() => {
                        this.setState({ open: true })
                    }}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name="query"
                    // value={item.name || ''}
                    // variant="outlined"
                    placeholder="Search"
                    onChange={this.handleChangeValue}
                    onKeyDown={(e) => {

                    }}
                    onKeyPress={(e) => {

                    }}
                // style={{ backgroundColor: '#eee' }}
                />
                {this.state.open && <Popper
                    // placement={comp.placement}
                    className={classes.popper}
                    open={this.state.open || false}
                    // open={true}
                    anchorEl={this.wrapperRef.current}
                    role={undefined}
                    transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin: 'center top',
                                marginTop: 5,

                            }}
                        >
                            <Paper className={classes.pager}>
                                <ClickAwayListener onClickAway={() => {
                                    // if(this.state.open){
                                    //     this.clearData()
                                    // }
                                }}>
                                    {!formState.values.query ? <div className={'d-flex p-5 text-center justify-content-center'}><span>
                                        Search your store.</span></div> :
                                        <div className={'p-2'} style={{ position: 'relative' }}>
                                            <div className='d-flex p-2 align-items-center'>
                                                <div className='col-lg-6 d-flex justify-content-center'>
                                                    <Box fontSize={14} fontWeight={'bold'} className={'text-uppercase'}>
                                                        Store result
                                                    </Box>

                                                </div>
                                                <div className='col-lg-6 d-flex justify-content-center'>
                                                    <b>Include: </b>
                                                    <select
                                                        name={'type_search'}
                                                        className={classes.selection}
                                                        value={formState.values.type_search || 0}
                                                        onChange={this.handleChangeValue}
                                                    >
                                                        <option value={0} >{'everything'}</option>
                                                        <option value={1} >{'only products'}</option>
                                                        <option value={2} >{'only orders'}</option>
                                                        <option value={3} >{'only customers'}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {this.isLoading ? <div className={classes.loadding_container}>
                                                <CircularProgress
                                                    size={30}
                                                />
                                            </div> :
                                                <div >
                                                    <MenuList autoFocusItem={false} id="menu-list-grow"
                                                        className={clsx(classes.menu_list, 'w-100')}
                                                    >
                                                        {data && data.data.map((item, i) =>
                                                            <MenuItem
                                                                classes={{
                                                                    root: classes.menu_item_search
                                                                }}

                                                                key={`${item.id} ${item.type}`}
                                                                onClick={() => {
                                                                    switch (item.type) {
                                                                        case 1:
                                                                            history.push(`/admin/products/${item.id}`)
                                                                            break
                                                                        case 2:
                                                                            history.push(`/admin/orders/${item.id}`)
                                                                            break
                                                                        case 3:
                                                                            history.push(`/admin/customers/${item.id}`)
                                                                            break
                                                                        default:
                                                                            break
                                                                    }
                                                                    this.clearData()
                                                                }} className='w-100'>
                                                                <div className="d-flex align-items-center">
                                                                    <div className={clsx('d-flex align-items-center justify-content-center mr-2', classes.image_item)}>
                                                                        {item.type == 3 ?
                                                                            <FontAwesomeIcon icon={faUserTie}
                                                                                size={'1x'} style={{ fontSize: 16, color: '#fff' }} /> : (
                                                                                item.type == 2 ?
                                                                                    <svg viewBox="0 0 20 20" width='20' style={{ fontSize: 16, color: '#fff' }} fill='#fff' focusable="false" aria-hidden="true">
                                                                                        <path d="M11 1a1 1 0 1 0-2 0v7.586L7.707 7.293a1 1 0 0 0-1.414 1.414l3 3a1 1 0 0 0 1.414 0l3-3a1 1 0 0 0-1.414-1.414L11 8.586V1z"></path>
                                                                                        <path d="M3 14V3h4V1H2.5A1.5 1.5 0 0 0 1 2.5v15A1.5 1.5 0 0 0 2.5 19h15a1.5 1.5 0 0 0 1.5-1.5v-15A1.5 1.5 0 0 0 17.5 1H13v2h4v11h-3.5c-.775 0-1.388.662-1.926 1.244l-.11.12A1.994 1.994 0 0 1 10 16a1.994 1.994 0 0 1-1.463-.637l-.111-.12C7.888 14.664 7.275 14 6.5 14H3z"></path>
                                                                                    </svg> : <div className={classes.img}>
                                                                                        <img
                                                                                            src={`${config.cdnUrl}` + item.image}
                                                                                            alt="Tshirt / S / White"
                                                                                            style={{ width: '100%', height: '100%' }} />
                                                                                    </div>
                                                                            )}
                                                                    </div>
                                                                    <div>
                                                                        <div>
                                                                            <Box fontSize={15} fontWeight={'bold'}>
                                                                                {item.title}
                                                                            </Box>
                                                                        </div>
                                                                        <div>
                                                                            <span style={{ fontSize: 13 }}>{(item.subtitle || '').substring(0, 100)}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </MenuItem>)}
                                                        {data && data.next_page_url && <MenuItem
                                                            classes={{
                                                                root: classes.menu_item_search
                                                            }}
                                                            key={`add`}
                                                            onClick={() => {
                                                                this.getSearch(data.next_page_url, formState.values)
                                                            }}
                                                            className='w-100'>
                                                            <div className="d-flex align-items-center">

                                                                <div>
                                                                    <div>
                                                                        <span className={classes.a_href} style={{ fontSize: 13 }}>
                                                                            {'Show more 10 result'}
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </MenuItem>}

                                                    </MenuList>

                                                </div>}
                                            {isLoadMore && <div style={{ width: '98%', height: '98%', position: 'absolute', top: '2%', background: 'rgb(247 243 243 / 20%)' }} className={'d-flex justify-content-center align-items-center p-2'}>
                                                <CircularProgress
                                                    size={20}
                                                />
                                            </div>}
                                        </div>

                                    }

                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>}
            </div >
        )
    }
}

SearchAll.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(SearchAll)));
export { connectedList as SearchAll };
