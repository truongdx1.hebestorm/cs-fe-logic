/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
    Typography, TextField, Button, FormControlLabel,
    Popper,
    ClickAwayListener,
    MenuItem, MenuList, Paper, Grow, Tooltip, CircularProgress
} from '@material-ui/core';
import clsx from 'clsx';
import chroma from 'chroma-js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faCaretDown, faStar, faSort, faTimes, faTimesCircle, faPlus } from '@fortawesome/free-solid-svg-icons'

const useStyles = theme => ({
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '3px 0',
        position: 'relative',
        fontSize:14
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, itemSelect: {
        padding: '8px 10px',
        cursor: 'pointer',
        '&:hover': {
            background: chroma('#3f4eae').alpha(0.2).css()
        }
    }, popper: {
        width: '100%',
        zIndex: '200',
        // maxHeight: '200px',
        // boxShadow: 'var(--p-popover-shadow, 0 0 0 1px rgba(39,44,48,0.05), 0 2px 7px 1px rgba(39,44,48,0.16))'
    }, loadding_container: {
        display: 'flex',
        padding: '10px',
        alignItems: 'center',
        justifyContent: 'center'
    }, sub_title: {
        fontSize: 14,
        color: '#637381',
    }, item_menu: {
        cursor: 'pointer',
        '&:hover': {
            background: '#edeeef'
        }
    }, menu_list:{
        maxHeight:'300px',
        overflow: 'auto'
    }
});


class SearchPopover extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            formState: {
                values: {
                }
            },
            itemSelect: [],
            items: []
        }
        this.wrapperRef = React.createRef()
        this.poperRef = React.createRef();
        this.isLoading = false

    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        if (this.props.onLoadOption) {
            this.isLoading = true
            this.forceUpdate()
            this.props.onLoadOption().then(res => {
                this.isLoading = false
                this.forceUpdate()
                var items = res
                this.setState({ items })
            })
        }
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        var value = event.target.value
        this.setState({ formState, isLoading: true }, () => {
            this.loadData(value)
        })
    }

    loadData = (value) => {
        if (!this.isLoading) {
            this.isLoading = true
            this.forceUpdate()
            this.props.onLoadOption(value).then(res => {
                this.isLoading = false
                this.forceUpdate()
                console.log(res,'123')
                this.setState({ items: res }, () => {
                    if (value != this.state.formState['values']['text']) {
                        this.loadData(this.state.formState['values']['text'])
                    }
                })
            })
        }
    }


    render() {
        const { classes, user, item, onChange, className, placeholder } = this.props;
        const { array_value, formState, open, items, isLoading } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense d-flex align-items-center', classes.container, className)}
                onBlur={() => {
                    this.setState({ open: false })
                }}
                ref={this.wrapperRef}
            >
                <FontAwesomeIcon icon={faSearch} className='ml-2'
                    size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                <TextField
                    ref={this.poperRef}

                    onFocus={() => {
                        this.setState({ open: true })
                    }}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name="text"
                    // value={item.name || ''}
                    // variant="outlined"
                    placeholder={placeholder || 'Size'}
                    onChange={this.handleChangeValue}
                    onKeyDown={(e) => {

                    }}
                    onKeyPress={(e) => {

                    }}
                // style={{ backgroundColor: '#eee' }}
                />
                <Popper
                    // placement={comp.placement}
                    className={classes.popper}
                    open={this.state.open||false}
                    anchorEl={this.wrapperRef.current}
                    role={undefined}
                    transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin: 'center top',
                                marginTop: 5,

                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={() => {
                                    // this.setState({ open: false })
                                }}>
                                    {this.isLoading ? <div className={classes.loadding_container}>
                                        <CircularProgress
                                            size={30}
                                        />
                                    </div> :
                                        <MenuList autoFocusItem={false} id="menu-list-grow"
                                            className={classes.menu_list}
                                            onKeyDown={(event) => {
                                            }}>
                                            {this.props.createFunction && <MenuItem onClick={() => {
                                                if(this.props.createFunction)
                                                    this.props.createFunction()
                                                this.setState({ open: false })
                                            }}>
                                                <div className="p-2 d-flex align-items-center">
                                                    <FontAwesomeIcon icon={faPlus}  className='mr-3' style={{color:'#c4cdd5'}}/>
                                                    <div><span>{this.props.create_text}</span></div>
                                                </div>
                                            </MenuItem>}
                                            {items && items.map((cus, i) =>
                                                <MenuItem onClick={() => {
                                                    this.setState({ open: false })
                                                    onChange(cus)
                                                }} key={i}>
                                                    <div className="p-2">
                                                        <div><span>{cus.first_name + ' ' + cus.last_name}</span></div>
                                                        <div><span className={classes.sub_title}>{cus.email}</span></div>
                                                    </div>
                                                </MenuItem>
                                            )}

                                        </MenuList>}

                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </div >
        )
    }
}

SearchPopover.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(SearchPopover)));
export { connectedList as SearchPopover };
