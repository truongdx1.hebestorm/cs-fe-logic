/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-set-state */
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import {
    Card,
    CardContent,
    IconButton,
    CardHeader,
    TextField, Button, Checkbox, Box, TableCell, Table, TableBody,
    TableHead, TableRow, RadioGroup, Tooltip,
    FormControlLabel, InputAdornment, Typography, Divider
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import clsx from 'clsx';

import moment from 'moment';
import toastr from '../../common/toastr'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faArrowLeft, faTimes, faAngleRight } from '@fortawesome/free-solid-svg-icons'
// import { productService, collectionService } from '../../_services'
import config from 'config'
import CircularProgress from '@material-ui/core/CircularProgress';
import { cssConstants } from '../../_constants'



const useStyles = theme => ({
    ...cssConstants,
    modal: {
        width: '50%',
        margin: '0 10px',
        maxHeight: '90vh',
        // minHeight: '90vh',
    }, title: {
        paddingTop: 15,
        paddingBottom: 15,
        display: 'flex',
        alignItems: 'center',
    }, content: {
        // maxHeight: 'calc(100vh - 10rem)',
        overflowY: 'auto',
        paddingLeft: '0 !important',
        paddingBottom: '0 !important',
        paddingRight: '0 !important',
        flexDirection: 'column',
        height: '100%',
        // minHeight: '70vh',
        // flexWrap: 'wrap',
        display: 'flex'
    }, container: {
        display: 'flex',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
        alignItems: 'center',
        flexGrow: 1,
        minHeight: '35px',
        margin: '0 15px 15px'
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, input: {
        marginLeft: '.4rem',
    }, cell_entry: {
        // whiteSpace: 'nowrap',
        width: '100%',
        padding: '8px'
    }, tableRow: {
        '&:hover': {
            background: '#dfe3e8'
        },
        padding: '5px 10px'

    }, img: {
        width: '70px',
        height: '70px',
        display: 'flex',
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 'var(--p-border-radius-base,3px)',
        border: '.1rem solid var(--p-border-subdued,#dfe3e8)',
        background: 'var(--p-surface,#f9fafb)',
    }, nameContainer: {
        // marginLeft: 20,
        '& >a': {
            cursor: 'pointer',
            '&:hover': {
                textDecoration: 'underline'
            }
        }
    }, table_container: {
        flexGrow: 1, height: '100%',
        overflowY: 'auto',
    }, footer: {
        borderTop: '1px solid #ccc',
        padding: '15px 10px',
    }, button: {
        fontWeight: 'bold',
        fontSize: 13,
        marginLeft: '10px'
    }, cell_check: {
        padding: '0 10px !important'
    }, loadding_container: {
        display: 'flex',
        padding: '30px',
        alignItems: 'center',
        justifyContent: 'center'
    }, sub_title: {
        fontSize: 14,
        color: '#637381',
    }, li_item: {
        cursor: 'pointer',
        '&:hover': {
            background: '#f7f7f7'
        }
    }, search: {
        maxHeight: '40px'
    }, span_option_1: {
        color: '#29bc94'
    }, span_option_2: {
        color: '#763eaf'
    }, span_option_3: {
        color: '#ff9517'
    }, img_item: {
        width: '80px',
        height: '80px',
        overflow: 'hidden',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 'var(--p-border-radius-base,3px)',
        border: '.1rem solid var(--p-border-subdued,#dfe3e8)',
        background: 'var(--p-surface,#f9fafb)',
    }, btn_link: {
        outline: 'none !important',
        background: 'transparent',
        whiteSpace: 'nowrap',
        color: 'var(--p-interactive, #007ace)',
        '&:hover': {
            textDecoration: 'underline !important'
        },
        overflow: 'hidden',
        '&.over': {
            maxWidth: '12rem',
        },
        minHeight: '2.2rem',
        minWidth: '1.6rem',
        padding: '.5rem 1rem',
    }, top_check: {
        margin: '0 !important',
        '& >span': {
            fontWeight: '600',
        }
    }
});


class PopUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            formState: {
                values: this.props.formData ? this.props.formData : {}
            },
            products: null,
            selectedIds: this.props.products ? this.props.products : [],
            breadcrums: this.props.data_config ? [
                { name: 'config', label: 'Select products', item_name: 'name' }
            ] : [{ name: 'products', label: 'Select products' }],
            selectedParents: {},
            conditions: {},
            disableIds: [],
            selectedItems: [],
            responseData: {}
        }
        this.textSearchRef = React.createRef()

    }

    componentDidMount() {
        const { formState } = this.state
        const { ArrSelect, key_id, data_config, type, loadmore } = this.props

        if (type == 2) {
            if (!data_config) {
                this.setState({ data: [], isLoading: true, disableIds: ArrSelect ? ArrSelect() : [] }, () => {
                    this.props.onLoadOption(formState.values.text).then(res => {
                        if (loadmore) {
                            this.setState({ data: res?.data || [], isLoading: false, responseData: res })

                        } else {
                            this.setState({ data: res, isLoading: false })
                        }
                    })
                })
            } else {
                this.setState({ disableIds: ArrSelect ? ArrSelect() : [] }, () => {
                })
            }


        } else if (type != 2) {
            this.props.onLoadOption(formState.values.text).then(res => {
                if (loadmore) {
                    this.setState({ data: res?.data || [], isLoading: false, responseData: res })

                } else {
                    this.setState({ data: res, isLoading: false })
                }
            })
            if (ArrSelect) {
                var arr = []
                ArrSelect.map((item) => {
                    arr.push(item)
                })
                this.setState({ selectedIds: arr })
            }
        }


    }

    onOpenModal = (item) => {
        this.setState({ openModalDetail: true, dataDetail: item })
    }

    handleChangeValue = (event) => {
        const { type, loadmore } = this.props
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        var value = event.target.value
        if (type == 2 && this.props.data_config) {
            var breadcrums = Object.assign([], this.state.breadcrums)
            var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
            if (type_content == 'products') {
                var parrams = { title: value, ...this.state.conditions }
                this.setState({ formState }, () => {
                    this.onClickRow('products', parrams)
                })
            } else {
                this.setState({ formState })
            }
        } else {
            this.setState({ formState }, () => {
                this.props.onLoadOption(value).then(res => {
                    if (loadmore) {
                        this.setState({ data: res?.data || [], isLoading: false, responseData: res })

                    } else {
                        this.setState({ data: res, isLoading: false })
                    }
                })
            })
        }

    }


    handleSelectOne = (event, pd) => {
        const { key_id } = this.props
        var selectedIds = Object.assign([], this.state.selectedIds)
        const selectedIndex = selectedIds.findIndex(x => x[key_id] == pd[key_id]);
        let newselectedIds = [];
        if (selectedIndex === -1) {
            newselectedIds = newselectedIds.concat(selectedIds, pd);
        } else if (selectedIndex === 0) {
            newselectedIds = newselectedIds.concat(selectedIds.slice(1));
        } else if (selectedIndex === selectedIds.length - 1) {
            newselectedIds = newselectedIds.concat(selectedIds.slice(0, -1));
        } else if (selectedIndex > 0) {
            newselectedIds = newselectedIds.concat(
                selectedIds.slice(0, selectedIndex),
                selectedIds.slice(selectedIndex + 1)
            );
        }
        this.setState({ selectedIds: newselectedIds })
    };

    handleRadioOne = (event, pd) => {
        const { key_id } = this.props
        var selectedIds = Object.assign([], this.state.selectedIds)
        const selectedIndex = selectedIds.findIndex(x => x[key_id] == pd[key_id]);
        let newselectedIds = [];
        if (selectedIndex === -1) {
            newselectedIds = [pd]
        } else if (selectedIndex === 0) {
            newselectedIds = newselectedIds.concat(selectedIds.slice(1));
        } else if (selectedIndex === selectedIds.length - 1) {
            newselectedIds = newselectedIds.concat(selectedIds.slice(0, -1));
        } else if (selectedIndex > 0) {
            newselectedIds = newselectedIds.concat(
                selectedIds.slice(0, selectedIndex),
                selectedIds.slice(selectedIndex + 1)
            );
        }
        this.setState({ selectedIds: newselectedIds })
    };

    onChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.type === 'checkbox' ? (event.target.checked ? 1 : 0) : event.target.value
        this.setState({ formState })
    }

    getList = () => {
        var breadcrums = Object.assign([], this.state.breadcrums)
        const { data_config } = this.props
        const { data, selectedItems } = this.state
        var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
        switch (type_content) {
            case 'config':
                return data_config
            case 'select_products':
                return selectedItems
            default:
                return data
        }
    }

    onClickRow(type_items, queryObject) {
        // switch (type_items) {
        //     case 'products':
        //         if (queryObject && !queryObject['title']) {
        //             this.setState({ conditions: queryObject })
        //         }
        //         productService.getProductsHaveItems(queryObject)
        //             .then(res => {
        //                 if (res.status == 200) {
        //                     this.setState({ data: res.data.products, isLoading: false })
        //                 } else {
        //                     this.setState({ data: [], isLoading: false })
        //                 }
        //             }, error => {
        //                 this.setState({ data: [], isLoading: false })
        //             })
        //         break
        //     case 'collections':
        //         collectionService.getCollections(null, {})
        //             .then(res => {
        //                 if (res.status == 200) {
        //                     this.setState({ data: res.data.collections.data, isLoading: false })
        //                 } else {
        //                     this.setState({ data: [], isLoading: false })
        //                 }
        //             }, error => {
        //                 this.setState({ data: [], isLoading: false })
        //             })
        //         break
        //     case 'product_types':
        //         productService.getTypes({}).then(res => {
        //             if (res.status == 200) {
        //                 this.setState({ data: res.data.types.data, isLoading: false })
        //             } else {
        //                 this.setState({ data: [], isLoading: false })
        //             }
        //         }, error => {
        //             this.setState({ data: [], isLoading: false })
        //         })
        //         break
        //     case 'vendors':
        //         productService.getVendors({}).then(res => {
        //             if (res.status == 200) {
        //                 this.setState({ data: res.data.vendors.data, isLoading: false })
        //             } else {
        //                 this.setState({ data: [], isLoading: false })
        //             }
        //         }, error => {
        //             this.setState({ data: [], isLoading: false })
        //         })
        //         break
        //     default:
        //         break
        // }

    }

    handleSelectParent = (event, pd) => {
        const { key_id } = this.props
        const { disableIds } = this.state
        var selectedIds = Object.assign([], this.state.selectedIds)
        var selectedParents = Object.assign({}, this.state.selectedParents)
        var selectedItems = Object.assign([], this.state.selectedItems)
        var position = selectedItems.findIndex(x => x.product_id == pd.product_id)
        if (event.target.checked) {
            pd.items.map((product_item, pos) => {
                const selectedIndex = selectedIds.findIndex(x => x[key_id] == product_item[key_id]);
                if (selectedIndex == -1 && !disableIds.includes(product_item[key_id])) {
                    product_item['title'] = pd.title
                    product_item['image'] = pd.image
                    product_item['quantity'] = 1
                    product_item['added'] = true
                    product_item['discount_value'] = 0
                    product_item['discount_type'] = 1
                    selectedIds.push(product_item)
                }
            })
            selectedParents[pd.product_id] = this.getMaxItems(pd.items || [])
            if (position == -1) {
                selectedItems.push(pd)
            }
        } else {
            pd.items.map((product_item, pos) => {
                const selectedIndex = selectedIds.findIndex(x => x[key_id] == product_item[key_id]);
                if (selectedIndex != -1) {
                    selectedIds.splice(selectedIndex, 1)
                }
            })
            selectedParents[pd.product_id] = 0
            selectedItems.splice(position, 1)
        }
        this.setState({ selectedIds, selectedParents, selectedItems })
    };

    getMaxItems = (items) => {
        const { disableIds } = this.state
        const { key_id } = this.props
        var total = items.length
        items.map((item) => {
            if (disableIds.includes(item[key_id]) && this.props.data_config) {
                total -= 1
            }
        })
        return total
    }

    handleSelectAll = event => {
        const { key_id } = this.props
        const { data } = this.state
        let sellectProducts;
        var selectedIds = Object.assign([], this.state.selectedIds)
        if (event.target.checked) {
            sellectProducts = data.filter(e => e[key_id]);
        } else {
            sellectProducts = [];
        }
        this.setState({ selectedIds: sellectProducts })
    };

    getOptionName = (option) => {
        var arr_title = []
        option?.map((op) => {
            arr_title.push(op?.value?.name || "")
        })
        var title = arr_title.join(" • ")
        return title;
    }


    render() {
        const { classes, user, title, key_id, label_name, placeholder, imageInvisible, title_name, type_pop, button, selectOne, radioOne,
            type, minHeight, textSelect, sellectAll, textAll, type_theme_collections, ArrSelect, sellectType, loadmore } = this.props;
        const { formState, selectedIds, data, isLoading, breadcrums, item_name, selectedParents, disableIds, responseData, isLoadmore } = this.state
        return (
            <div data- layerid="2" style={{ display: 'block' }}>
                <div className="ModalLayer">
                    <div className="ModalBuffer">
                        <div className="ModalBuffer-topBuffer">
                        </div>
                        <div className="ModalBuffer-content">
                            <div className={clsx("ModalPaneWithBuffer-pane", classes.modal)} >
                                <div className={clsx("Dialog d-flex flex-column", minHeight)} style={{ width: '100%', height: '100%' }}>
                                    <header className={clsx("Dialog-header", classes.title)}>
                                        {type != 2 && <div className={clsx("Dialog-headerTitle")}>{title}</div>}
                                        {type == 2 && <div className='d-flex flex-grow-1 align-items-center'>
                                            {breadcrums && breadcrums.length > 1 &&
                                                < button className={'mr-2'} style={{ background: 'transparent', padding: 5 }}
                                                    onClick={() => {
                                                        var breadcrums = Object.assign([], this.state.breadcrums)
                                                        var formState = Object.assign({}, this.state.formState)
                                                        formState['values']['text'] = ''
                                                        var type_last = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
                                                        if (type_last == 'select_products') {
                                                            breadcrums = [breadcrums[0]]
                                                        } else {
                                                            breadcrums.splice(breadcrums.length - 1, 1)
                                                        }
                                                        if (breadcrums.length <= 1) {
                                                            this.setState({ breadcrums, isLoading: false, formState })
                                                        } else {
                                                            var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
                                                            if (type_content == 'collections') {
                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                    this.onClickRow('collections')
                                                                })
                                                            } else if (type_content == 'product_types') {
                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                    this.onClickRow('product_types')
                                                                })
                                                            } else if (type_content == 'vendors') {
                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                    this.onClickRow('vendors')
                                                                })
                                                            }
                                                        }
                                                    }}
                                                >
                                                    <FontAwesomeIcon icon={faArrowLeft} size='1x' />
                                                </button>
                                            }
                                            <div>
                                                <Box fontWeight='bold' fontSize={16}>{(breadcrums && breadcrums.length <= 1) || !breadcrums ?
                                                    title : breadcrums[breadcrums.length - 1]['label']
                                                }</Box>
                                            </div>
                                        </div>}


                                        <div role="button" tabIndex="0" className="CloseButton">
                                            <svg className="Icon--small Icon XIcon CloseButton-xIcon" focusable="false" viewBox="0 0 32 32"
                                                onClick={this.props.onClose}>
                                                <path d="M18.1,16L27,7.1c0.6-0.6,0.6-1.5,0-2.1s-1.5-0.6-2.1,0L16,13.9l-8.9-9C6.5,4.3,5.6,4.3,5,4.9S4.4,6.4,5,7l8.9,8.9L5,24.8c-0.6,0.6-0.6,1.5,0,2.1c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4l8.9-8.9l8.9,8.9c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4c0.6-0.6,0.6-1.5,0-2.1L18.1,16z"></path>
                                            </svg>
                                        </div>
                                    </header>
                                    {type != 2 && <div className={clsx("ConfirmationDialog-dialogContents flex-grow-1", classes.content)}>
                                        {type_pop == 2 && <div className='flex-fill pl-3 pr-3 mb-2'>
                                            <div>
                                                <span style={{ fontWeight: 500, fontSize: 16 }}>
                                                    {title_name}
                                                </span>
                                                <TextField
                                                    disabled={isLoading}
                                                    fullWidth
                                                    InputProps={{
                                                        classes: {
                                                            input: classes.input
                                                        }
                                                    }}
                                                    placeholder=''
                                                    margin="dense"
                                                    name="zone_name"
                                                    value={formState.values.zone_name || ''}
                                                    variant="outlined"
                                                    onChange={this.onChangeValue}
                                                // style={{ backgroundColor: '#eee' }}
                                                />
                                                <span className={classes.sub_title}>Customers won’t see this.</span>
                                            </div>
                                        </div>}
                                        {type_pop == 2 && <Divider className={'mb-3'} />}
                                        <div className={clsx(classes.container)}>
                                            <FontAwesomeIcon icon={faSearch}
                                                size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                                            <TextField
                                                autoFocus
                                                fullWidth
                                                InputProps={{
                                                    classes: {
                                                        input: classes.input,
                                                        underline: classes.underline
                                                    }
                                                }}
                                                name="text"
                                                value={formState.values.text || ''}
                                                // variant="outlined"
                                                placeholder={placeholder}
                                                onChange={
                                                    this.handleChangeValue
                                                }

                                            />
                                        </div >
                                        <Divider />
                                        <div className={classes.table_container}>
                                            {data ?
                                                <>
                                                    <Table>
                                                        {sellectAll && <TableHead>
                                                            <TableRow className={classes.tableRow}>
                                                                <TableCell colSpan={3} className={classes.cell_check} padding="checkbox">
                                                                    <FormControlLabel
                                                                        className={classes.top_check}
                                                                        color="primary"
                                                                        control={<Checkbox
                                                                            // className={}
                                                                            checked={(data && selectedIds && selectedIds.length == data.length)}
                                                                            color="primary"
                                                                            indeterminate={
                                                                                selectedIds && data &&
                                                                                selectedIds.length > 0 &&
                                                                                selectedIds.length < data.length
                                                                            }
                                                                            onChange={this.handleSelectAll}
                                                                        />
                                                                        }
                                                                        label={textAll ? textAll : ((selectedIds ? selectedIds.length : 0) + " selected")}
                                                                    />
                                                                </TableCell>
                                                            </TableRow>
                                                        </TableHead>}
                                                        <TableBody>
                                                            {data.map((pd, index) =>
                                                                <TableRow className={classes.tableRow} key={pd[key_id]}>
                                                                    <TableCell className={classes.cell_check} style={{ width: '8%' }} padding="checkbox" >
                                                                        {!type_theme_collections ? <Checkbox
                                                                            style={{ visibility: (selectOne && selectedIds && selectedIds.length > 0 && selectedIds.findIndex(x => x[key_id] == pd[key_id]) == -1) ? 'hidden' : '' }}
                                                                            checked={selectedIds.findIndex(x => x[key_id] == pd[key_id]) != -1}
                                                                            color="primary"
                                                                            // indeterminate={
                                                                            //     selectedIds.length > 0 &&
                                                                            //     selectedIds.length < products.length
                                                                            // }
                                                                            onChange={(event) => {
                                                                                if (radioOne) {
                                                                                    this.handleRadioOne(event, pd)
                                                                                } else {
                                                                                    this.handleSelectOne(event, pd)
                                                                                }
                                                                            }}
                                                                        /> :
                                                                            <Checkbox
                                                                                style={{
                                                                                    visibility:
                                                                                        (ArrSelect.filter(x => x[key_id] == pd[key_id])?.length > 0) ? 'hidden' : ''
                                                                                }}

                                                                                checked={selectedIds.findIndex(x => x[key_id] == pd[key_id]) != -1}

                                                                                color="primary"
                                                                                // indeterminate={
                                                                                //     selectedIds.length > 0 &&
                                                                                //     selectedIds.length < products.length
                                                                                // }
                                                                                onChange={(event) => {
                                                                                    if (radioOne) {
                                                                                        this.handleRadioOne(event, pd)
                                                                                    } else {
                                                                                        this.handleSelectOne(event, pd)
                                                                                    }
                                                                                }}
                                                                            />
                                                                        }

                                                                    </TableCell>
                                                                    <TableCell className={clsx(classes.cell_entry)} style={{ width: '85%' }}>
                                                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                                                            {!imageInvisible && <div className={classes.img}>
                                                                                {!pd.image ? <svg viewBox="0 0 20 20" className="_1nZTW xvNMs" width='20'>
                                                                                    <path d="M2.5 1A1.5 1.5 0 0 0 1 2.5v15A1.5 1.5 0 0 0 2.5 19h15a1.5 1.5 0 0 0 1.5-1.5v-15A1.5 1.5 0 0 0 17.5 1h-15zm5 3.5c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zM16.499 17H3.497c-.41 0-.64-.46-.4-.79l3.553-4.051c.19-.21.52-.21.72-.01L9 14l3.06-4.781a.5.5 0 0 1 .84.02l4.039 7.011c.18.34-.06.75-.44.75z"></path>
                                                                                </svg> : <img style={{ width: '100%' }} src={`${config.cdnUrl}` + pd.image} />}
                                                                            </div>}
                                                                            <div className={clsx(classes.nameContainer, 'pl-2')} style={{ flex: 1 }}>
                                                                                <Typography variant="body1" component='a'>{pd[label_name]}</Typography>
                                                                            </div>
                                                                        </div>
                                                                    </TableCell >
                                                                </TableRow>
                                                            )}

                                                        </TableBody>
                                                    </Table>
                                                    {(loadmore && responseData?.next_page_url) ?
                                                        <div className={clsx('d-flex p-4 align-items-center w-100 text-center')} style={{
                                                            position: "relative",
                                                            padding: "10px 15px",
                                                            fontSize: "14px",
                                                            background: "#fff",
                                                            cursor: "pointer",
                                                            display: 'flex',
                                                            alignItems: "center",
                                                            justifyContent: "center"
                                                        }}>
                                                            {isLoadmore ? < CircularProgress
                                                                size={20}
                                                            /> : <span className={classes.a_href}
                                                                onClick={() => {
                                                                    this.setState({ isLoadmore: true }, () => {
                                                                        this.props?.onLoadMore(formState.values.text, responseData?.next_page_url).then(res => {

                                                                            var data = Object.assign([], this.state.data)
                                                                            data = res ? [...data, ...res?.data] : data
                                                                            this.setState({ data: data, isLoadmore: false, responseData: res })
                                                                        })
                                                                    })
                                                                }}

                                                            >Show more products</span>}
                                                        </div> : null}
                                                </> : <div className={classes.loadding_container}>
                                                    <CircularProgress />
                                                </div>
                                            }
                                        </div>
                                    </div>}
                                    {type == 2 && <div className={clsx("ConfirmationDialog-dialogContents flex-grow-1", classes.content)}>
                                        <div className={clsx(classes.container, classes.search)}>
                                            <FontAwesomeIcon icon={faSearch}
                                                size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                                            <TextField
                                                autoFocus
                                                fullWidth
                                                InputProps={{
                                                    classes: {
                                                        input: classes.input,
                                                        underline: classes.underline
                                                    }
                                                }}
                                                name="text"
                                                value={formState.values.text || ''}
                                                // variant="outlined"
                                                placeholder={placeholder}
                                                onChange={
                                                    this.handleChangeValue
                                                }

                                            />
                                        </div >
                                        <Divider />
                                        <div className={clsx(classes.table_container, 'flex-grow-1')}>
                                            {!isLoading ? <div>
                                                <ul className={'list-unstyled'}>
                                                    {this.getList() && this.getList().map((item, i) => {
                                                        var breadcrums = Object.assign([], this.state.breadcrums)
                                                        var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
                                                        if (type_content == 'products' || type_content == 'select_products') {
                                                            var type_select = type_content == 'products' ? false : true
                                                            return (
                                                                <div key={i}>
                                                                    <li className={classes.li_item} key={i}>
                                                                        <div className='d-flex pl-3 pr-3 pt-2 pb-2 align-items-center'>
                                                                            {!type_select && <Checkbox
                                                                                className={this.getMaxItems(item.items || []) == 0 ? 'hidden' : ''}
                                                                                checked={selectedParents[item.product_id] == this.getMaxItems(item.items || [])}
                                                                                color="primary"
                                                                                size='small'
                                                                                indeterminate={
                                                                                    selectedParents[item.product_id] &&
                                                                                    selectedParents[item.product_id] > 0 &&
                                                                                    selectedParents[item.product_id] < this.getMaxItems(item.items || [])
                                                                                }
                                                                                onChange={(event) => {
                                                                                    this.handleSelectParent(event, item)
                                                                                }}
                                                                            />}
                                                                            <div className={classes.img_item}>
                                                                                {!item.image ? <svg viewBox="0 0 20 20" className="_1nZTW xvNMs" width='20'>
                                                                                    <path d="M2.5 1A1.5 1.5 0 0 0 1 2.5v15A1.5 1.5 0 0 0 2.5 19h15a1.5 1.5 0 0 0 1.5-1.5v-15A1.5 1.5 0 0 0 17.5 1h-15zm5 3.5c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zM16.499 17H3.497c-.41 0-.64-.46-.4-.79l3.553-4.051c.19-.21.52-.21.72-.01L9 14l3.06-4.781a.5.5 0 0 1 .84.02l4.039 7.011c.18.34-.06.75-.44.75z"></path>
                                                                                </svg> : <img style={{ width: '100%' }} src={`${config.cdnUrl}` + item.image} />}
                                                                            </div>
                                                                            <div className={'ml-2 d-flex align-items-center'} style={{ flex: 1 }}>
                                                                                <span className={(!type_select && this.getMaxItems(item.items || []) == 0) ? classes.sub_title : ''}>{item.title}</span>
                                                                                {type_select && <button className={clsx(classes.button_normal, 'icon')}
                                                                                    onClick={() => {
                                                                                        var event = {
                                                                                            target: {
                                                                                                checked: false
                                                                                            }
                                                                                        }
                                                                                        this.handleSelectParent(event, item)
                                                                                    }}
                                                                                >
                                                                                    <FontAwesomeIcon icon={faTimes} />
                                                                                </button>}
                                                                            </div>
                                                                        </div>
                                                                        <Divider />
                                                                    </li>
                                                                    {
                                                                        <div >
                                                                            {item.items && item.items.map((variant, pos) => {
                                                                                if (!type_select || (type_select && selectedIds.findIndex(x => x[key_id] == variant[key_id]) != -1)) {
                                                                                    return (
                                                                                        <li className={clsx(classes.li_item, 'pl-4')} key={`variant${variant.product_item_id}`}>
                                                                                            <div className='d-flex pl-4 pr-4 pt-2 pb-2 align-items-center'>
                                                                                                {!type_select && <Checkbox
                                                                                                    className={disableIds.includes(variant[key_id]) ? 'invisible' : ''}
                                                                                                    checked={selectedIds.findIndex(x => x[key_id] == variant[key_id]) != -1}
                                                                                                    color="primary"
                                                                                                    size='small'
                                                                                                    onChange={(event) => {
                                                                                                        var selectedParents = Object.assign({}, this.state.selectedParents)
                                                                                                        var selectedItems = Object.assign([], this.state.selectedItems)
                                                                                                        if (event.target.checked) {
                                                                                                            if (!selectedParents[item.product_id] || selectedParents[item.product_id] == 0) {
                                                                                                                selectedItems.push(item)
                                                                                                            }
                                                                                                            selectedParents[item.product_id] = (selectedParents[item.product_id] || 0) + 1
                                                                                                        } else {
                                                                                                            selectedParents[item.product_id] = (selectedParents[item.product_id] || 0) - 1
                                                                                                            if (!selectedParents[item.product_id] || selectedParents[item.product_id] <= 0) {
                                                                                                                var position = selectedItems.findIndex(x => x.product_id == item.product_id)
                                                                                                                selectedItems.splice(position, 1)
                                                                                                            }
                                                                                                        }
                                                                                                        this.setState({ selectedParents, selectedItems })
                                                                                                        var child = Object.assign({}, variant)
                                                                                                        child['title'] = item.title
                                                                                                        child['image'] = item.image
                                                                                                        child['quantity'] = 1
                                                                                                        child['added'] = true
                                                                                                        child['discount_value'] = 0
                                                                                                        child['discount_type'] = 1
                                                                                                        this.handleSelectOne(event, child)
                                                                                                    }}
                                                                                                />}
                                                                                                <div className={'d-flex flex-grow-1 align-items-center'}>
                                                                                                    <div className='d-flex flex-grow-1'>
                                                                                                        <span className={(!type_select && disableIds.includes(variant[key_id]) && this.props.data_config)
                                                                                                            ? clsx(classes.sub_title, 'mr-1') : clsx(classes.span_option_1, 'mr-1')}>
                                                                                                            {variant.variant_name}
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    {!type_select ? <div className={''}>
                                                                                                        {(disableIds.includes(variant[key_id]) && this.props.data_config) ? <span className={classes.sub_title}>{`Item already added`}</span> :
                                                                                                            <span>{`$${variant.price}`}</span>
                                                                                                        }
                                                                                                    </div> : <div className={''}><button className={clsx(classes.button_normal, 'icon')}
                                                                                                        onClick={() => {
                                                                                                            var event = {
                                                                                                                target: {
                                                                                                                    checked: false
                                                                                                                }
                                                                                                            }
                                                                                                            var selectedParents = Object.assign({}, this.state.selectedParents)
                                                                                                            var selectedItems = Object.assign([], this.state.selectedItems)
                                                                                                            selectedParents[item.product_id] = (selectedParents[item.product_id] || 0) - 1
                                                                                                            if (!selectedParents[item.product_id] || selectedParents[item.product_id] <= 0) {
                                                                                                                var position = selectedItems.findIndex(x => x.product_id == item.product_id)
                                                                                                                selectedItems.splice(position, 1)
                                                                                                            }
                                                                                                            this.setState({ selectedParents, selectedItems }, () => {
                                                                                                                this.handleSelectOne(event, variant)
                                                                                                            })
                                                                                                        }}
                                                                                                    >
                                                                                                        <FontAwesomeIcon icon={faTimes} />
                                                                                                    </button>
                                                                                                    </div>}
                                                                                                </div>
                                                                                            </div>
                                                                                            <Divider />
                                                                                        </li>)
                                                                                }
                                                                            }
                                                                            )}
                                                                        </div>
                                                                    }
                                                                </div>
                                                            )
                                                        } else {
                                                            return (
                                                                <div key={i}>
                                                                    <li className={classes.li_item} key={i}
                                                                        onClick={() => {
                                                                            var breadcrums = Object.assign([], this.state.breadcrums)
                                                                            var formState = Object.assign([], this.state.formState)
                                                                            formState['values']['text'] = ''
                                                                            var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
                                                                            if (type_content == 'config') {
                                                                                switch (item.items) {
                                                                                    case 'products':
                                                                                        breadcrums.push({ name: 'products', label: item.name })
                                                                                        this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                            this.onClickRow('products')
                                                                                        })
                                                                                        break
                                                                                    case 'collections':
                                                                                        breadcrums.push({ name: 'collections', label: item.name, item_name: 'title' })
                                                                                        this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                            this.onClickRow('collections')
                                                                                        })
                                                                                        break
                                                                                    case 'product_types':
                                                                                        breadcrums.push({ name: 'product_types', label: item.name, item_name: 'type' })
                                                                                        this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                            this.onClickRow('product_types')
                                                                                        })
                                                                                        break
                                                                                    case 'vendors':
                                                                                        breadcrums.push({ name: 'vendors', label: item.name, item_name: 'vendor' })
                                                                                        this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                            this.onClickRow('vendors')
                                                                                        })
                                                                                        break
                                                                                }
                                                                            } else if (type_content == 'collections') {
                                                                                breadcrums.push({ name: 'products', label: item.title })
                                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                    this.onClickRow('products', { collection_id: item.collection_id })
                                                                                })
                                                                            } else if (type_content == 'product_types') {
                                                                                breadcrums.push({ name: 'products', label: item.type })
                                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                    this.onClickRow('products', { product_type: item.type_id })
                                                                                })
                                                                            } else if (type_content == 'vendors') {
                                                                                breadcrums.push({ name: 'products', label: item.vendor })
                                                                                this.setState({ isLoading: true, breadcrums, formState }, () => {
                                                                                    this.onClickRow('products', { vendor_id: item.vendor_id })
                                                                                })
                                                                            }
                                                                        }}
                                                                    >
                                                                        <div className='d-flex pl-4 pr-4 pt-3 pb-3 align-items-center'>
                                                                            <div className='flex-grow-1'>
                                                                                <span>{item[breadcrums[breadcrums.length - 1]['item_name']]}</span>
                                                                            </div>
                                                                            <FontAwesomeIcon icon={faAngleRight} size='1x' />
                                                                        </div>
                                                                        <Divider />
                                                                    </li>
                                                                </div>
                                                            )
                                                        }
                                                    }
                                                    )}
                                                    {(loadmore && responseData?.next_page_url) ?
                                                        <div className={clsx('d-flex p-4 align-items-center w-100 text-center')} style={{
                                                            position: "relative",
                                                            padding: "10px 15px",
                                                            fontSize: "14px",
                                                            background: "#fff",
                                                            cursor: "pointer",
                                                            display: 'flex',
                                                            alignItems: "center",
                                                            justifyContent: "center"
                                                        }}>
                                                            {isLoadmore ? < CircularProgress
                                                                size={20}
                                                            /> : <span className={classes.a_href}
                                                                onClick={() => {
                                                                    this.setState({ isLoadmore: true }, () => {
                                                                        this.props.onLoadMore(formState.values.text, responseData?.next_page_url).then(res => {
                                                                            var data = Object.assign([], this.state.data)
                                                                            data = [...data, ...res.data]
                                                                            this.setState({ data: data, isLoadmore: false, responseData: res })
                                                                        })
                                                                    })
                                                                }}

                                                            >Show more products</span>}
                                                        </div> : null}
                                                </ul>
                                            </div> : <div className={classes.loadding_container}>
                                                <CircularProgress />
                                            </div>}
                                        </div>
                                    </div>}


                                    <div className={clsx("ConfirmationDialog-dialogButtonRow", classes.footer)}>
                                        {textSelect && <button
                                            size="small"
                                            disabled={selectedIds.length == 0 ? true : false}
                                            className={clsx(selectedIds.length == 0 ? classes.button_normal : classes.btn_link, 'disable')}
                                            variant="contained"
                                            onClick={() => {
                                                var breadcrums = Object.assign([], this.state.breadcrums)
                                                var formState = Object.assign([], this.state.formState)
                                                breadcrums.push({ name: 'select_products', label: 'Select products' })
                                                this.setState({ breadcrums, formState })
                                            }}
                                        // startIcon={<NotInterestedIcon />}
                                        >
                                            {selectedIds.length == 0 ? `No ${textSelect} selected` : `${selectedIds.length} ${textSelect} selected`}
                                        </button>}
                                        <div className={'flex-grow-1'} />
                                        <button
                                            size="small"
                                            // disabled={formState.isLoading}
                                            className={classes.button_normal}
                                            variant="contained"
                                            onClick={this.props.onClose}
                                        // startIcon={<NotInterestedIcon />}
                                        >
                                            Cancel
                                        </button>
                                        <button
                                            disabled={(!selectedIds || selectedIds.length == 0) ? true : false}
                                            className={clsx(classes.button_normal, 'primary ml-2', (!selectedIds || selectedIds.length == 0) ? 'disable' : '')}
                                            variant="contained"
                                            color='primary'
                                            size="small"
                                            onClick={() => {
                                                if (type_pop == 2) {
                                                    var formData = {
                                                        countries: selectedIds,
                                                        zone_name: formState.values.zone_name
                                                    }
                                                    this.props.onChange(formData)
                                                } else {
                                                    this.props.onChange(selectedIds)
                                                }
                                                this.props.onClose()
                                            }}
                                        >
                                            {button ? button : 'Done'}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ModalBuffer-bottomBuffer">
                        </div>
                    </div>
                    <div className="LayerContext-layerContainer">
                    </div>
                </div>
            </div >
        )

    }

}

function mapStateToProps(state) {
    const {
        authentication
    } = state;
    const { user } = authentication;
    return {
        user
    };
}

const connectedAccount = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(PopUp)));
export { connectedAccount as PopUp };

