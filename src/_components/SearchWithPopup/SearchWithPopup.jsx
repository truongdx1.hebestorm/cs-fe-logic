/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { PopUp } from './PopUp'

const useStyles = theme => ({
    container: {
        display: 'flex',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
        alignItems: 'center',
        flexGrow: 1,
        borderTopRightRadius: 'var(--p-border-radius-base,0)!important',
        borderBottomRightRadius: 'var(--p-border-radius-base,0)!important'
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, main: {
        display: 'flex',
        flexWrap: 'wrap'
    }, button_brower: {
        position: 'relative',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '35px',
        minWidth: '40px',
        margin: 0,
        padding: '0px 1rem',
        background: 'linear-gradient(180deg,#fff,#f9fafb)',
        border: '.1rem solid var(--p-border,#c4cdd5)',
        boxShadow: '0 1px 0 0 rgba(22,29,37,.05)',
        borderRadius: '3px',
        lineHeight: 1,
        color: '#212b36',
        textAlign: 'center',
        cursor: 'pointer',
        userSelect: 'none',
        textDecoration: 'none',
        transitionProperty: 'background,border,box-shadow',
        transitionDuration: 'var(--p-override-none,.2s)',
        transitionTimingFunction: 'var(--p-override-none,cubic-bezier(.64,0,.35,1))',
        borderTopLeftRadius: 'var(--p-border-radius-base,0)!important',
        borderBottomLeftRadius: 'var(--p-border-radius-base,0)!important',
        outline: 'none !important',
        '&:hover':{
            background: 'linear-gradient(180deg,#fff,#eee)',
        }
    }, right: {
        marginLeft: '-1px'
    }
});

class SearchWithPopup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            formState: {
                values: {
                }
            }
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        // document.addEventListener('click', this.handleClick);
    }

    componentWillUnmount() {
        // try {
        //     document.removeEventListener('click', this.handleClick);
        // } catch {
        // }
    }

    handleClick = (event) => {
        const { target } = event
        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }



    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState, open:true })
    }

    render() {
        const { classes, user, values, onChange, placeholder, onLoadOption, key_id, label_name, imageInvisible, ArrSelect,
            title, type, config, minHeight,textSelect,sellectAll,onLoadMore, loadmore } = this.props;
        const { array_value, formState, open} = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense', classes.main)}>
                {open && <PopUp
                    onLoadOption={onLoadOption}
                    open={open}
                    onClose={()=>{
                        this.setState({ open: false})
                    }}
                    title={title}
                    onChange={onChange}
                    key_id={key_id}
                    label_name={label_name}
                    placeholder={placeholder}
                    imageInvisible={imageInvisible}
                    ArrSelect={ArrSelect||null}
                    formData={{text:formState.values.text||''}}
                    type={type}
                    data_config={config}
                    minHeight={minHeight}
                    textSelect={textSelect}
                    sellectAll={sellectAll}
                    onLoadMore = {onLoadMore}
                    loadmore={loadmore}
                />}
                <div className={clsx(classes.container)}
                    onBlur={() => {
                    }}
                    ref={this.wrapperRef}
                >
                    <FontAwesomeIcon icon={faSearch}
                        size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                    <TextField
                        fullWidth
                        InputProps={{
                            classes: {
                                input: classes.input,
                                underline: classes.underline
                            }
                        }}
                        name="text"
                        value={''}
                        placeholder={placeholder}
                        onChange={this.handleChangeValue}
                    />
                </div >
                <div className={classes.right}>
                    <button className={classes.button_brower}
                        onClick={()=>{
                            this.setState({open: true})
                        }}
                    >
                        Browse
                    </button>
                </div>
            </div>
        )
    }
}

SearchWithPopup.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(SearchWithPopup)));
export { connectedList as SearchWithPopup };
