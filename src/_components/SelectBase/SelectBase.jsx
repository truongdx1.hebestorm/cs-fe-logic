/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSort } from '@fortawesome/free-solid-svg-icons'

const useStyles = theme => ({
    container: {
        display: 'flex',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
        alignItems: 'center',
        flexGrow: 1,
        minHeight: '35px',
        appearance: 'none',
        position: 'absolute',
        zIndex: 30,
        width: '100%',
        opacity: '0.001'
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, main: {
        // display: 'flex',
        // flexWrap: 'wrap',
        position: 'relative',
        minHeight: '35px',
    }, right: {
        marginLeft: '-1px'
    }, label: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        minHeight: '35px',
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '0px 8px',
    }, span_label: {
        color: '#999999'
    }, span_value: {
        color: '#000',
        margin: '0 5px',
        flexGrow: 1
    }
});

class SelectBase extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array_value: ['1', '2'],
            formState: {
                values: {
                }
            },
            options: props?.options ? props?.options : []
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        if(this.props.default_options){
            this.setState({
                options : this.props.default_options
            })
        }
        document.addEventListener('click', this.handleClick);
        if (this.props.onLoadOption) {
            this.props.onLoadOption().then(res => {
                this.setState({ options: res })
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.options != this.props.options) {
            this.setState({ options: this.props.options })
        }
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        // if (!this.wrapperRef.current.contains(target)) {
        //     this.setState({ open: false })
        //     this.outForm = true
        // }
    }



    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
    }

    getName = (value) => {
        const { key_id, label_name, optionsAuto } = this.props
        var { options } = this.state
        if (value && optionsAuto) {
            var index = optionsAuto?.findIndex(x => x[key_id] == value)
            if (index != -1) {
                return optionsAuto[index][label_name]
            }
        } else if (value && options) {
            var index = options?.findIndex(x => x[key_id] == value)
            if (index != -1) {
                return options[index][label_name]
            }
        }
        return ''
    }

    render() {
        const { classes, user, onChange, placeholder, type, label_name, key_id, value, name, className,
            disableSelected, optionsAuto, typeValue,no_full
        } = this.props;
        const { array_value, formState, options } = this.state
        return (
            <div className={clsx(no_full?'MuiFormControl-marginDense':'MuiFormControl-marginDense w-100', classes.main, className)}>
                <select className={clsx(classes.container)} name={name} value={value || ''}
                    onChange={(event) => {
                        if (typeValue == 'object') {
                            var postion = options?.findIndex(x => x[key_id] == event?.target?.value)
                            var e = {
                                target: {
                                    value: postion > -1 ? options[postion] : null,
                                    name: name
                                }
                            }
                            onChange(e)

                        } else {
                            onChange(event)

                        }
                    }
                    }
                    disabled={disableSelected || false}
                >
                    {/* {disableSelected &&<option value={disableSelected[key_id]} disabled>{disableSelected[label_name]}</option>} */}
                    {options && options.map((item, index) =>
                        <option value={item[key_id]} key={index} disabled={(item[key_id] == -1 || item?.disabled) ? true : false}>{item[label_name]}</option>
                    )}
                    {optionsAuto && optionsAuto.map((item2, index2) =>
                        <option value={item2[key_id]} key={index2} disabled={(item2[key_id] == -1 || item2?.disabled) ? true : false}>{item2[label_name]}</option>
                    )}
                </select>
                {type == 1 ? <div className={clsx(classes.label, className)}>
                    <span className={classes.span_value}>{this.getName(value)}</span>
                    <FontAwesomeIcon icon={faSort}
                        size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                </div> : <div className={clsx(classes.label, className)}>
                    <span className={classes.span_label}>Sort:</span>
                    <span className={classes.span_value}>{this.getName(value)}</span>
                    <FontAwesomeIcon icon={faSort}
                        size={'1x'} style={{ fontSize: 16, color: '#637381' }} />
                </div>}
            </div>
        )
    }
}

SelectBase.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(SelectBase)));
export { connectedList as SelectBase };
