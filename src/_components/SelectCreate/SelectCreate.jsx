/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';
import chroma from 'chroma-js';


const useStyles = theme => ({
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '3px 0',
        position: 'relative'
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, itemSelect: {
        padding: '8px 10px',
        cursor: 'pointer',
        '&:hover': {
            background: chroma('#3f4eae').alpha(0.2).css()
        }
    }
});


class SelectCreate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            formState: {
                values: {
                }
            },
            itemSelect: [],
            items: []
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        this.props.onLoadOption().then(res => {
            var itemSelect = Object.assign([], this.state.itemSelect)
            itemSelect = res
            var items = res
            this.setState({ itemSelect, items })
        })
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        const { itemSelect } = this.state
        const { item, type } = this.props
        var items = Object.assign([], this.state.items)
        items = itemSelect.filter(x => x.name.toLowerCase().includes(event.target.value.toLowerCase()))
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState, items })
        if (item.id && type != 'variant') {
            this.props.onChange({ name: event.target.value, id: item.id })
        } else {
            this.props.onChange({ name: event.target.value, id: this.makeId(20) })
        }
        if (!this.state.open) {
            this.setState({ open: true })
        }
    }


    render() {
        const { classes, user, item, onChange, className, placeholder, textRef } = this.props;
        const { array_value, formState, open, items } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense', classes.container, className)}
                ref={this.wrapperRef}
            >
                <TextField
                    onFocus={() => {
                        this.setState({ open: true })
                    }}
                    onBlur={() => {
                        // if (this.state.open) {
                        //     this.setState({ open: false })
                        // }
                    }}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name="text"
                    value={item.name || ''}
                    // variant="outlined"
                    placeholder={placeholder || 'Size'}
                    onChange={this.handleChangeValue}
                    onKeyDown={(event) => {
                        if (event.key === 'Enter') {
                            event.preventDefault();
                            this.setState({ open: false })
                        }
                    }}
                // style={{ backgroundColor: '#eee' }}
                />
                {items && items.length > 0 && <div
                    className="input-dropdown-container"
                    style={{
                        display: open ? '' : 'none',
                        width: '100%',
                        position: 'absolute',
                        background: '#fff',
                        borderRadius: 2,
                        boxShadow: '0 0 0 1px hsla(0,0%,0%,0.1), 0 4px 11px hsla(0,0%,0%,0.1)',
                        top: "110%",
                        // padding: 5,
                        zIndex: 20, maxHeight: '12rem',
                        overflowY: 'scroll'
                    }}>
                    <ul style={{ listStyle: "none", marginBottom: 0 }}>
                        {items.map((item, index) =>
                            <li key={index} onClick={() => {
                                onChange(item)
                                this.setState({ open: false })
                            }}>
                                <div className={classes.itemSelect}>
                                    <a>
                                        {item.name}
                                    </a>
                                </div>
                            </li>)}
                    </ul>

                </div>}
            </div >
        )
    }
}

SelectCreate.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(SelectCreate)));
export { connectedList as SelectCreate };
