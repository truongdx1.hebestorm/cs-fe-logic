/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button, CircularProgress } from '@material-ui/core';
import clsx from 'clsx';
import chroma from 'chroma-js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTimes, faEllipsisV, faTrashAlt, faChevronLeft, faHome, faSearch, faFile, faTag } from '@fortawesome/free-solid-svg-icons'
import { cssConstants } from '../../_constants'

// import { productService, collectionService, pageService } from '../../_services'

const useStyles = theme => ({
    ...cssConstants,
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: '3px 0',
        position: 'relative',
        height: 40
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, itemLi: {
        padding: '8px 10px',
        cursor: 'pointer',
        '&:hover': {
            background: chroma('#ccc').alpha(0.2).css()
        }
    }
});


class SelectMultiLevel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            formState: {
                values: this.props.data ? this.props.data : {}
            },
            items: [],
            isLoading: false,
            breadcrums: this.props.data_config ? [
                { name: 'config' }
            ] : [{ name: 'items' }],
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        // this.props.onLoadOption().then(res => {
        //     var itemSelect = Object.assign([], this.state.itemSelect)
        //     itemSelect = res
        //     var items = res
        //     this.setState({ itemSelect, items })
        // })
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event
        if (!this.wrapperRef.current.contains(target) && (target && !target.className.includes('item_config_menu'))) {
            this.setState({ open: false })
        }
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    handleChangeValue = (event) => {
        const { onChangeValue } = this.props
        var formState = Object.assign({}, this.state.formState)
        formState['values']['path'] = event.target.value
        formState['values']['keyword'] = null
        formState['values']['title'] = null
        onChangeValue(formState['values'])
        this.setState({ formState })
    }

    getList = () => {
        var breadcrums = Object.assign([], this.state.breadcrums)
        const { data_config } = this.props
        const { data, selectedItems } = this.state
        var type_content = !breadcrums || breadcrums.length == 0 ? 'config' : breadcrums[breadcrums.length - 1]['name']
        switch (type_content) {
            case 'config':
                return data_config
            default:
                return data
        }
    }

    onClickRow = (type_items) => {
        // const { user } = this.props;
        // switch (type_items) {
        //     case 'products':
        //         productService.getProducts(null, {})
        //             .then(res => {
        //                 if (res.status == 200) {
        //                     // this.setState({ data: res.data.products, isLoading: false })
        //                     this.setState({
        //                         data: this.getArrayData([{ title: 'All Product', id: -1 }, ...res.data.products.data], 'products'),
        //                         isLoading: false
        //                     })
        //                 } else {
        //                     this.setState({ data: [], isLoading: false })
        //                 }
        //             }, error => {
        //                 this.setState({ data: [], isLoading: false })
        //             })
        //         break
        //     case 'collections':
        //         collectionService.getCollections(null, {})
        //             .then(res => {
        //                 if (res.status == 200) {
        //                     console.log(res.data.collections.data, 'res.data.collections.data')
        //                     this.setState({
        //                         data: this.getArrayData([{ title: 'All Collection', id: -1 }, ...res.data.collections.data], 'collections'),
        //                         isLoading: false
        //                     })
        //                 } else {
        //                     this.setState({ data: [], isLoading: false })
        //                 }
        //             }, error => {
        //                 this.setState({ data: [], isLoading: false })
        //             })
        //         break
        //     case 'pages':
        //         pageService.getPages(null, {}).then(res => {
        //             if (res.status == 200) {
        //                 this.setState({
        //                     data: this.getArrayData(res.data.pages.data, 'pages'),
        //                     isLoading: false
        //                 })
        //             } else {
        //                 this.setState({ data: [], isLoading: false })
        //             }
        //         }, error => {
        //             this.setState({ data: [], isLoading: false })
        //         })
        //         break
        //     default:
        //         break
        // }

    }

    getArrayData = (data, keywords) => {
        var arr = []
        data.map((item, i) => {
            arr.push({
                title: item.title,
                path: (item?.id == -1) ? `/${keywords}` : `/${keywords}/${item.slug}`,
                keyword: keywords
            })
        })
        return arr
    }

    getIcon = () => {
        var formState = Object.assign({}, this.state.formState)
        switch (formState['values']['keyword']) {
            case 'home':
                return (<FontAwesomeIcon icon={faHome} style={{ fontSize: 20, color: '#999999' }} className='mr-2' />)
            case 'search':
                return (<FontAwesomeIcon icon={faSearch} style={{ fontSize: 20, color: '#999999' }} className='mr-2' />)
            case 'collections':
                return (<svg id="next-collections" width='20' height='20' viewBox="0 0 20 20" className='mr-2'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" color='#999999' fill='#999999'>
                        <path fill="currentColor" d="M7 15h12v4H7v-4z"></path>
                        <path d="M19 20c.553 0 1-.447 1-1V6c0-.297-.132-.578-.36-.77l-6-5c-.37-.307-.91-.307-1.28 0L10 2.198 7.64.23c-.37-.307-.91-.307-1.28 0l-6 5C.13 5.423 0 5.704 0 6v13c0 .553.447 1 1 1h18zM8 18v-2h10v2H8zm-6 0v-2h4v2H2zM7 2.302L8.438 3.5l-2.08 1.73C6.133 5.423 6 5.704 6 6v8H2V6.47L7 2.3zm6 0l5 4.167V14H8V6.47L13 2.3zM13 7c.553 0 1-.447 1-1s-.447-1-1-1c-.553 0-1 .447-1 1s.447 1 1 1z"></path>
                    </svg>
                </svg>)
            case 'pages':
                return (<FontAwesomeIcon icon={faFile} style={{ fontSize: 20, color: '#999999' }} className='mr-2' />)
            default:
                return '';
        }
    }




    render() {
        const { classes, user, item, onChange, className, placeholder, onChangeValue } = this.props;
        const { breadcrums, formState, open, items, isLoading } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense', classes.container, className)}
                ref={this.wrapperRef}

            >
                <div
                    className="input-dropdown-container"
                    style={{
                        display: open ? '' : 'none',
                        width: '100%',
                        position: 'absolute',
                        background: '#fff',
                        borderRadius: 2,
                        boxShadow: '0 0 0 1px hsla(0,0%,0%,0.1), 0 4px 11px hsla(0,0%,0%,0.1)',
                        bottom: "110%",
                        top: "auto",
                        // padding: 5,
                        zIndex: 100, maxHeight: '12rem',
                        overflowY: 'scroll'
                    }}>

                    <ul style={{ listStyle: "none", marginBottom: 0, alignContent: 'space-around', position: 'relative' }}>
                        {breadcrums.length > 1 && <li className={'item_config_menu'}
                            onClick={() => {
                                var breadcrums = Object.assign([], this.state.breadcrums)
                                breadcrums = [{ name: 'config' }]
                                this.setState({ isLoading: false, data: [], breadcrums })
                            }}
                        >
                            <div className={clsx(classes.itemLi, 'd-flex align-items-center p-2 item_config_menu')}>
                                <FontAwesomeIcon icon={faChevronLeft} style={{ fontSize: 10 }} className='mr-2 item_config_menu' />
                                <div className={clsx(classes.itemSelect, 'item_config_menu')}>
                                    <a className={'item_config_menu'}>
                                        {'Back'}
                                    </a>
                                </div>
                            </div>
                        </li>}
                        {isLoading &&
                            <div className='d-flex align-items-center justify-content-center' style={{ minHeight: '10rem' }}>
                                <CircularProgress />
                            </div>}
                        {!isLoading && this.getList() && this.getList().map((item, index) =>
                            <li className={'item_config_menu'}
                                key={index} onClick={() => {
                                    if (!item.path) {
                                        var breadcrums = Object.assign([], this.state.breadcrums)
                                        breadcrums.push({ name: item.keyword })
                                        this.setState({ isLoading: true, breadcrums }, () => {
                                            this.onClickRow(item.keyword)
                                        })
                                    } else {
                                        var breadcrums = Object.assign([], this.state.breadcrums)
                                        breadcrums = [{ name: 'config' }]
                                        var formState = Object.assign({}, this.state.formState)
                                        formState['values']['title'] = item.title
                                        formState['values']['path'] = item.path
                                        formState['values']['keyword'] = item.keyword
                                        onChangeValue(formState['values'])
                                        this.setState({ formState, breadcrums, open: false })
                                    }
                                }}>
                                <div className={clsx(classes.itemLi, 'd-flex align-items-center p-2', 'item_config_menu')}>
                                    {item.icon ||
                                        (item.keyword == 'pages' ? <FontAwesomeIcon icon={faFile} style={{ fontSize: 20, color: '#999999' }} className='mr-2' /> :
                                            (item.keyword == 'products' ? <FontAwesomeIcon icon={faTag} style={{ fontSize: 20, color: '#999999' }} className='mr-2' /> :
                                                <svg id="next-collections" width='20' height='20' viewBox="0 0 20 20" className='mr-2'>
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" color='#999999' fill='#999999'>
                                                        <path fill="currentColor" d="M7 15h12v4H7v-4z"></path>
                                                        <path d="M19 20c.553 0 1-.447 1-1V6c0-.297-.132-.578-.36-.77l-6-5c-.37-.307-.91-.307-1.28 0L10 2.198 7.64.23c-.37-.307-.91-.307-1.28 0l-6 5C.13 5.423 0 5.704 0 6v13c0 .553.447 1 1 1h18zM8 18v-2h10v2H8zm-6 0v-2h4v2H2zM7 2.302L8.438 3.5l-2.08 1.73C6.133 5.423 6 5.704 6 6v8H2V6.47L7 2.3zm6 0l5 4.167V14H8V6.47L13 2.3zM13 7c.553 0 1-.447 1-1s-.447-1-1-1c-.553 0-1 .447-1 1s.447 1 1 1z"></path>
                                                    </svg>
                                                </svg>))}
                                    <div className={clsx(classes.itemSelect, 'item_config_menu')}>
                                        <a className={'item_config_menu'}>
                                            {item.title}
                                        </a>
                                    </div>
                                </div>
                            </li>)}
                    </ul>
                </div>
                {!formState.values.keyword && <TextField
                    onFocus={() => {
                        this.setState({ open: true })
                    }}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name="path"
                    value={formState.values.path || ''}
                    // variant="outlined"
                    placeholder={placeholder || ''}
                    onChange={this.handleChangeValue}
                // style={{ backgroundColor: '#eee' }}
                />}
                {formState.values.keyword && <div className={'d-flex align-items-center h-100 p-2'}
                    onClick={() => {
                        this.setState(prevState => ({ open: !prevState.open }))
                    }}
                >
                    {this.getIcon()}
                    <div className={clsx(classes.itemSelect, 'flex-fill')}>
                        <a>
                            {formState.values.title}
                        </a>
                    </div>
                    <button className={clsx(classes.button_normal, 'icon')}
                        onClick={() => {
                            var formState = Object.assign({}, this.state.formState)
                            formState['values']['path'] = null
                            formState['values']['keyword'] = null
                            this.setState({ formState })
                        }}
                    >
                        <FontAwesomeIcon icon={faTimes} />
                    </button>
                </div>}
            </div >
        )
    }
}

SelectMultiLevel.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(SelectMultiLevel)));
export { connectedList as SelectMultiLevel };
