/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';


const useStyles = theme => ({
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: 3,
        display: 'flex',
        alignItems: 'center'
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.1rem',
        color:'#666',
        fontSize: 13
    }
});

class Slug extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array_value: ['1', '2'],
            formState: {
                values: {
                }
            }
        }
        this.wrapperRef = React.createRef()

        this.textSlug = React.createRef()

    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
            this.outForm = true
        }
    }



    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    render() {
        const { classes, user, values, onChange, name, prefix } = this.props;
        const { array_value, formState } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense pl-1', classes.container)}
                ref={this.wrapperRef}
                onClick={() => {
                    this.textSlug?.current?.focus();
                }}
            >
                <span style={{ color: '#333333' }}>{prefix}</span>
                <TextField
                    inputRef={this.textSlug}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name={name}
                    value={values || ''}
                    // variant="outlined"
                    placeholder={''}
                    onChange={onChange}
                // style={{ backgroundColor: '#eee' }}
                />
            </div >
        )
    }
}

Slug.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(Slug)));
export { connectedList as Slug };
