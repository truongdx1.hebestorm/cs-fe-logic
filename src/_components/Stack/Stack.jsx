/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button } from '@material-ui/core';
import clsx from 'clsx';
import chroma from 'chroma-js';


const useStyles = theme => ({
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: 3,
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {

        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, itemSelect: {
        padding: '8px 10px',
        cursor: 'pointer',
        '&:hover': {
            background: chroma('#3f4eae').alpha(0.2).css()
        }
    }
});

class Stack extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array_value: ['1', '2'],
            formState: {
                values: {
                }
            },
            itemSelect: [],
            items: []
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        if (this.props.onLoadOption) {
            this.props.onLoadOption().then(res => {
                var itemSelect = Object.assign([], this.state.itemSelect)
                itemSelect = res
                var items = res
                this.setState({ itemSelect, items })
            })
        }

    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event

        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
            this.outForm = true
        }
    }



    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    getItems = (array_value) => {
        const { itemSelect } = this.state
        var items = []
        itemSelect.map((va) => {
            if (array_value.findIndex(x => x.name == va.name) == -1) {
                items.push(va)
            }
        })
        this.setState({ items })
    }

    render() {
        const { classes, user, values, onChange, className, placeholder, type } = this.props;
        const { array_value, formState, open, items, itemSelect } = this.state
        return (
            <div className={clsx('MuiFormControl-marginDense', classes.container, className)}
                onBlur={() => {
                    var formState = Object.assign({}, this.state.formState)
                    var value = formState['values']['text']
                    var array_value = Object.assign([], values)
                    if(type=='string' && array_value.findIndex(x => x == value) == -1 && (value != '' && value)){
                        array_value.push(e.target.value)
                        var formState = Object.assign({}, this.state.formState)
                        formState['values'][e.target.name] = ''
                        this.setState({ formState })
                        onChange(array_value)
                        this.getItems(array_value)
                    }else if (array_value.findIndex(x => x.name == value) == -1 && (value != '' && value)) {
                        var pos = itemSelect.findIndex(x => x.name == value)
                        if (pos != -1) {
                            array_value.push(itemSelect[pos])
                        } else {
                            array_value.push({ name: value, id: this.makeId(20) })
                        }
                        var formState = Object.assign({}, this.state.formState)
                        formState['values']['text'] = ''
                        this.setState({ formState })
                        onChange(array_value)
                        this.getItems(array_value)
                    }
                }}
                ref={this.wrapperRef}
            >
                {values && values.length > 0 &&
                    <div className={classes.group_tag}>
                        {values.map((vl, index) =>
                            <div className={classes.tag} key={index}>
                                <span className={classes.span_tag}>
                                    <span title="a" className={classes.content_tag}>{type=='string'?vl:vl.name}</span>
                                    <button type="button" aria-label="Remove a" className={classes.button}
                                        onClick={() => {
                                            var array_value = Object.assign([], values)
                                            array_value.splice(index, 1)
                                            onChange(array_value)
                                            this.getItems(array_value)
                                        }}
                                    >
                                        <span className={classes.span_icon}>
                                            <svg viewBox="0 0 20 20" className={classes.svg} focusable="false" aria-hidden="true">
                                                <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z"></path>
                                            </svg>
                                        </span>
                                    </button>
                                </span>
                            </div>)}
                    </div>}
                <TextField
                    onFocus={() => {
                        this.setState({ open: true })
                    }}
                    fullWidth
                    InputProps={{
                        classes: {
                            input: classes.input,
                            underline: classes.underline
                        }
                    }}
                    name="text"
                    value={formState.values.text || ''}
                    // variant="outlined"
                    placeholder={placeholder}
                    onChange={this.handleChangeValue}
                    onKeyDown={(e) => {
                        if ((e.key === "Delete" || e.key === "Backspace" || e.key === 8) && (e.target.value == '' || !e.target.value)) {
                            var array_value = Object.assign([], values)
                            array_value.splice(array_value.length - 1, 1)
                            onChange(array_value)
                            this.getItems(array_value)
                        }
                    }}
                    onKeyPress={(e) => {
                        if (e.key === 'Enter') {
                            var array_value = Object.assign([], values)
                            if(type=='string' && array_value.findIndex(x => x == e.target.value) == -1 && (e.target.value != '' && e.target.value)){
                                array_value.push(e.target.value)
                                var formState = Object.assign({}, this.state.formState)
                                formState['values'][e.target.name] = ''
                                this.setState({ formState })
                                onChange(array_value)
                                this.getItems(array_value)
                            }else if (array_value.findIndex(x => x.name == e.target.value) == -1 && (e.target.value != '' && e.target.value)) {
                                var pos = itemSelect.findIndex(x => x.name == e.target.value)
                                if (pos != -1) {
                                    array_value.push(itemSelect[pos])
                                } else {
                                    array_value.push({ name: e.target.value, id: this.makeId(20) })
                                }
                                var formState = Object.assign({}, this.state.formState)
                                formState['values'][e.target.name] = ''
                                this.setState({ formState })
                                onChange(array_value)
                                this.getItems(array_value)
                            }
                        }
                    }}
                // style={{ backgroundColor: '#eee' }}
                />
                {items && items.length > 0 && <div
                    className="input-dropdown-container"
                    style={{
                        display: open ? '' : 'none',
                        minWidth: '10rem',
                        position: 'absolute',
                        background: '#fff',
                        borderRadius: 2,
                        boxShadow: '0 0 0 1px hsla(0,0%,0%,0.1), 0 4px 11px hsla(0,0%,0%,0.1)',
                        top: "110%",
                        // padding: 5,
                        zIndex: 20, maxHeight: '12rem',
                        overflowY: 'scroll'
                    }}>
                    <ul style={{ listStyle: "none", marginBottom: 0 }}>
                        {items.map((item, index) =>
                            <li key={index} onClick={() => {
                                var array_value = Object.assign([], values)
                                if (array_value.findIndex(x => x.name == item.name) == -1 && (item.name != '' && item.name)) {
                                    array_value.push(item)
                                    onChange(array_value)
                                    this.getItems(array_value)
                                }
                            }}>
                                <div className={classes.itemSelect}>
                                    <a>
                                        {item.name}
                                    </a>
                                </div>
                            </li>)}
                    </ul>

                </div>}
            </div >
        )
    }
}

Stack.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(Stack)));
export { connectedList as Stack };
