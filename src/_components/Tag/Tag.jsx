/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, TextField, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faTimes } from '@fortawesome/free-solid-svg-icons'

const useStyles = theme => ({
    container: {
        width: '100%',
        border: '1px solid #ccc',
        borderRadius: 3,
        padding: 3,
    },
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    }, group_tag: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    }, tag: {
        marginTop: '.2rem',
        marginLeft: '.4rem',
        maxWidth: '100%'
    }, span_tag: {
        fontSize: 14,
        display: 'inline-flex',
        maxWidth: '100%',
        alignItems: 'center',
        minHeight: '1.8rem',
        padding: '0 0 0 .4rem',
        backgroundColor: '#dfe3e8',
        borderRadius: '3px',
        color: '#212b36',
    }, content_tag: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        verticalAlign: 'middle',
    }, button: {
        appearance: 'none',
        margin: 0,
        padding: 0,
        background: 'none',
        border: 'none',
        fontSize: 'inherit',
        lineHeight: 'inherit',
        cursor: 'pointer',
        display: 'block',
        height: '1.8rem',
        width: '1.8rem',
        marginLeft: '.4rem',
        borderRadius: '0 3px 3px 0',
        '&:hover': {
            background: '#AAAAAA',
        }
    }, span_icon: {
        display: 'block',
        height: '1rem',
        width: '1rem',
        maxHeight: '100%',
        maxWidth: '100%',
        margin: 'auto',
    }, svg: {
        position: 'relative',
        display: 'block',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
        fill: '#637381'
    }, input: {
        marginLeft: '.4rem',
    }, tag_main: {
        display: 'flex',
        flexWrap: 'wrap'
    }, title: {
        fontSize: 13,
        fontWeight: 'bold',
        color: '#637381',
        borderBottom: 'solid 1px #cfd7df'
    }, li: {
        '&:hover': {
            background: '#dfe3e8'
        }
    }, a_title: {
        textDecoration: 'none',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        marginRight: '.8rem',
        color: '#454f5b',
    }, li_tag: {
        borderBottom: 'solid 1px #cfd7df',
        width: '100%',
    }, close: {
        cursor: 'pointer',
        fontSize: '16px',
        color: '#454f5b',
        '&:hover': {
            color: '#000',
        }
    }
});

const itemSelect = [{ id: 1, name: 'Size' }, { id: 2, name: 'Color' }, { id: 3, name: 'Material' }, { id: 4, name: 'Style' }, { id: 5, name: 'Title' }]
var items = itemSelect

class Tag extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array_value: ['1', '2'],
            formState: {
                values: {
                }
            }
        }
        this.wrapperRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        if (this.props.onLoadOption) {
            this.props.onLoadOption().then(res => {
                var formState = Object.assign({}, this.state.formState)
                formState['values']['items'] = res
                this.setState({ formState })
            })
        }
    }

    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }

    handleClick = (event) => {
        const { target } = event
        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }

    handleChangeValue = (event) => {
        var formState = Object.assign({}, this.state.formState)
        formState['values'][event.target.name] = event.target.value
        this.setState({ formState })
        this.props.onLoadOption(event.target.value).then(res => {
            var formState = Object.assign({}, this.state.formState)
            formState['values']['items'] = res
            this.setState({ formState })
        })
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }


    render() {
        const { classes, user, values, onChange, typeView, placeholder } = this.props;
        const { array_value, formState, open } = this.state
        return (
            <div className={classes.tag_main} ref={this.wrapperRef}>
                <div className={clsx('MuiFormControl-marginDense', classes.container, 'col-lg-12')}
                    onBlur={() => {

                    }}

                >
                    <TextField
                        onFocus={() => {
                            this.setState({ open: true })
                        }}
                        fullWidth
                        InputProps={{
                            classes: {
                                input: classes.input,
                                underline: classes.underline
                            }
                        }}
                        name="text"
                        value={formState.values.text || ''}
                        // variant="outlined"
                        placeholder={placeholder}
                        onChange={this.handleChangeValue}
                        // onKeyDown={(e) => {
                        //     if ((e.key === "Delete" || e.key === "Backspace" || e.key === 8) && (e.target.value == '' || !e.target.value)) {
                        //         var array_value = Object.assign([], values)
                        //         array_value.splice(array_value.length - 1, 1)
                        //         onChange(array_value)
                        //     }
                        // }}
                        onKeyPress={(e) => {
                            if (typeView == 1 && e.key === 'Enter') {
                                var array_value = Object.assign([], values)
                                let pos = array_value.findIndex(x => x.name == e.target.value)
                                if (pos == -1 && (e.target.value != '' && e.target.value)) {
                                    array_value.push({ id: this.makeId(20), name: e.target.value })
                                    var formState = Object.assign({}, this.state.formState)
                                    formState['values']['text'] = ''
                                    this.setState({ formState, open: false })
                                    onChange(array_value)
                                }
                            } else if (e.key === 'Enter') {
                                var formState = Object.assign({}, this.state.formState)
                                formState['values']['text'] = ''
                                this.setState({ formState, open: false })
                            }
                        }}
                    // style={{ backgroundColor: '#eee' }}
                    />
                    <div
                        className="input-dropdown-container"
                        style={{
                            display: open ? '' : 'none',
                            width: '100%',
                            position: 'absolute',
                            background: '#fff',
                            borderRadius: 2,
                            boxShadow: '0 0 0 1px hsla(0,0%,0%,0.1), 0 4px 11px hsla(0,0%,0%,0.1)',
                            top: "110%",
                            // padding: 5,
                            zIndex: 2,
                        }}>
                        {typeView == 1 && (formState.values.items && formState.values.items.length > 0) && <div className={clsx('p-2 text-uppercase', classes.title)}>Frequently used tags</div>}
                        {(!formState.values.items || formState.values.items.length == 0) &&
                            (!formState.values.text || formState.values.text == '')
                            && <div className={clsx('p-2 text-uppercase', classes.title)}>No options</div>}
                        <ul style={{ listStyle: "none", marginBottom: 0, maxHeight: '10rem', overflowY: 'scroll' }}>
                            {typeView == 1 && formState.values.text && formState.values.text != '' && <li className={classes.li} onClick={() => {
                                var formState = Object.assign({}, this.state.formState)
                                var value = formState['values']['text']
                                var array_value = Object.assign([], values)
                                let pos = array_value.findIndex(x => x.name == value)
                                if (typeView == 1 && pos == -1 && (value != '' && value)) {
                                    array_value.push({ id: this.makeId(20), name: value })
                                    var formState = Object.assign({}, this.state.formState)
                                    formState['values']['text'] = ''
                                    this.setState({ formState, open: false })
                                    onChange(array_value)
                                }
                            }}>
                                <div className={'p-2 d-flex align-items-center'} style={{ cursor: 'pointer' }}>
                                    <FontAwesomeIcon icon={faPlusCircle} className={'mr-2'} style={{ fontSize: '16px' }} />
                                    <span><b>Add </b>"{formState.values.text}"</span>
                                </div>
                            </li>}
                            {formState.values.items && formState.values.items.map((item, index) =>
                                <li className={classes.li} key={index} onClick={() => {
                                }}>
                                    <div className={'p-2 d-flex align-items-center'}>
                                        <FormControlLabel
                                            className={'m-0 align-items-start flex-fill'}
                                            control={
                                                <Checkbox
                                                    size="small"
                                                    className={'p-0 mr-1'}
                                                    name="is_variant"
                                                    checked={values.findIndex(x => x.id == item.id) != -1}
                                                    color="primary"
                                                    onChange={(event) => {
                                                        var array_value = Object.assign([], values)
                                                        if (event.target.checked) {
                                                            array_value.push(item)
                                                            onChange(array_value)
                                                        } else {
                                                            var array_value = Object.assign([], values)
                                                            let pos = array_value.findIndex(x => x.id == item.id)
                                                            if (pos != -1) {
                                                                array_value.splice(pos, 1)
                                                                onChange(array_value)
                                                            }
                                                        }
                                                    }}
                                                />
                                            }
                                            label={item.name}
                                        />
                                    </div>
                                </li>)}
                        </ul>
                    </div>
                </div >

                {values && values.length > 0 && typeView == 1 &&
                    <div className={clsx(classes.group_tag, 'col-lg-12', 'row')}>
                        {values.map((vl, index) =>
                            <div className={classes.tag} key={index}>
                                <span className={classes.span_tag}>
                                    <span title="a" className={classes.content_tag}>{vl.name}</span>
                                    <button type="button" aria-label="Remove a" className={classes.button}
                                        onClick={() => {
                                            var array_value = Object.assign([], values)
                                            array_value.splice(index, 1)
                                            onChange(array_value)
                                        }}
                                    >
                                        <span className={classes.span_icon}>
                                            <svg viewBox="0 0 20 20" className={classes.svg} focusable="false" aria-hidden="true">
                                                <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z"></path>
                                            </svg>
                                        </span>
                                    </button>
                                </span>
                            </div>)}
                    </div>}
                {values && values.length > 0 && typeView == 2 &&
                    <div style={{ width: '100%' }} >
                        <ul className='list-unstyled'>
                            {values.map((vl, index) =>
                                <li className={clsx('d-flex pt-2 pb-2', classes.li_tag)} key={vl.id}>
                                    <a className={clsx(classes.a_title, 'flex-fill')}>{vl.name}</a>
                                    <div className='d-flex flex-shrink-0 pr-1 pl-1' onClick={() => {
                                        var array_value = Object.assign([], values)
                                        array_value.splice(index, 1)
                                        onChange(array_value)
                                    }}>
                                        <FontAwesomeIcon className={clsx(classes.close)} icon={faTimes} />
                                    </div>
                                </li>)}
                        </ul>
                    </div>}
            </div>
        )
    }
}

Tag.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(Tag)));
export { connectedList as Tag };
