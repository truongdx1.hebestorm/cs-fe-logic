/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
    Typography, TextField, Button, Checkbox, FormControlLabel,
    Card, Grid, Box, CardContent, Divider, IconButton,
    Popper,
    ClickAwayListener,
    MenuItem, MenuList, Paper, Grow, Tooltip,
    Radio, RadioGroup, FormControl, FormLabel, TextareaAutosize, CircularProgress
} from '@material-ui/core';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faCaretDown, faCaretRight, faTimes } from '@fortawesome/free-solid-svg-icons'
import { cssConstants } from '../../_constants'
import { layerRender } from '../../Layer'
// import { orderService } from '../../_services'
import toastr from '../../common/toastr'

import moment from 'moment'
const useStyles = theme => ({
    ...cssConstants,
    whiteSpace: {
        whiteSpace: 'nowrap'
    }, popper: {
        zIndex: 30
    }, ul_items: {
        borderBottom: '.1rem solid var(--p-divider,#dfe3e8)'
    }, ul_edit: {
        listStylePosition: 'inside',
        listStyleType: 'disc',
    }
});


class TimeLineComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            detail: null
        }
        this.wrapperRef = React.createRef()
        this.topActions = React.createRef();
    }
    componentDidMount() {
        document.addEventListener('click', this.handleClick);
        const { data } = this.props
        if (data?.type == 3) {
            this.setState({
                detail: JSON.parse(data?.response || '{}')
            })
        }

    }
    componentWillUnmount() {
        try {
            document.removeEventListener('click', this.handleClick);
        } catch {
        }
    }
    handleClick = (event) => {
        const { target } = event
        if (!this.wrapperRef.current.contains(target)) {
            this.setState({ open: false })
        }
    }
    handleChangeValue = (event) => {

    }

    contentPayment = (data, detail) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Authorization key</div>
                    <div><span>{detail?.summary || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.Ack || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${detail?.AMT || 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{detail?.payment_method || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{data?.payment_status || ''}</span></div>
                </div> */}
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{detail?.resource_type || ''}</span></div>
                </div> */}
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(data?.order_created_at).utcOffset('GMT-00:00').format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{detail?.TIMESTAMP || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Ack</div>
                            <div><span>{detail?.ACK || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Correlation</div>
                            <div><span>{detail?.CORRELATIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Version</div>
                            <div><span>{detail?.VERSION || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Build</div>
                            <div><span>{detail?.BUILD || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Token</div>
                            <div><span>{data?.token || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transaction</div>
                            <div><span>{detail?.TRANSACTIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transaction type</div>
                            <div><span>{detail?.TRANSACTIONTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment type</div>
                            <div><span>{detail?.PAYMENTTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment date</div>
                            <div><span>{detail?.ORDERTIME || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross amount</div>
                            <div><span>{detail?.AMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee amount</div>
                            <div><span>{detail?.FEEAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Tax amount</div>
                            <div><span>{detail?.TAXAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Tax amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment status</div>
                            <div><span>{detail?.PAYERSTATUS || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Pending reason</div>
                            <div><span>{detail?.PENDINGREASON || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Reason code</div>
                            <div><span>{detail?.REASONCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protection eligibility</div>
                            <div><span>{detail?.PROTECTIONELIGIBILITY || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protection eligibility type</div>
                            <div><span>{detail?.PROTECTIONELIGIBILITYTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Pay pal account</div>
                            <div><span>{detail?.RECEIVEREMAIL || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Secure merchant account</div>
                            <div><span>{detail?.SECUREMERCHANTACCOUNT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Success page redirect requested</div>
                            <div><span>{detail?.SUCCESSPAGEREDIRECTREQUESTED || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Token</div>
                            <div><span>{data?.token || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Paymentinfo</div>
                            <div><span>{detail?.PAYMENTINFO || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transactionid</div>
                            <div><span>{detail?.TRANSACTIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transactiontype</div>
                            <div><span>{detail?.TRANSACTIONTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Paymenttype</div>
                            <div><span>{detail?.PAYMENTTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Paymentdate</div>
                            <div><span>{detail?.ORDERTIME || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Grossamount</div>
                            <div><span>{detail?.AMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Feeamount</div>
                            <div><span>{detail?.FEEAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Taxamount</div>
                            <div><span>{detail?.TAXAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Paymentstatus</div>
                            <div><span>{detail?.PAYMENTSTATUS || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Reasoncode</div>
                            <div><span>{detail?.REASONCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protectioneligibility</div>
                            <div><span>{detail?.PROTECTIONELIGIBILITY || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protectioneligibilitytype</div>
                            <div><span>{detail?.PROTECTIONELIGIBILITYTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Sellerdetails</div>
                            <div><span>{detail?.SELLERDETAILS || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Paypalaccountid</div>
                            <div><span>{detail?.RECEIVEREMAIL || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Securemerchantaccountid</div>
                            <div><span>{detail?.SECUREMERCHANTACCOUNTID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Successpageredirectrequested</div>
                            <div><span>{detail?.SUCCESSPAGEREDIRECTREQUESTED || ''}</span></div>
                        </div>
                    </>
                }
            </div>
        )
    }

    contentPayment2 = (data, detail) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        return (
            <div className='mt-3'>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Authorization key</div>
                    <div><span>{detail?.summary || ''}</span></div>
                </div> */}
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.Ack || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${(detail?.resource && detail?.resource.amount) ? detail?.resource.amount.total : 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{data?.payment_method || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{data?.payment_status || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{detail?.resource_type || ''}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(data?.order_created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{detail?.create_time || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Ack</div>
                            <div><span>{detail?.ACK || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Version</div>
                            <div><span>{detail?.event_version || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Token</div>
                            <div><span>{data?.token || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transaction</div>
                            <div><span>{detail?.resource ? detail?.resource.id : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transaction type</div>
                            <div><span>{detail?.TRANSACTIONTYPE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment type</div>
                            <div><span>{detail?.resource ? detail?.resource.payment_mode : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment date</div>
                            <div><span>{detail?.resource ? detail?.resource.create_time : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross amount</div>
                            <div><span>{(detail?.resource && detail?.resource.amount) ? detail?.resource.amount.total : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross amount currency</div>
                            <div><span>{(detail?.resource && detail?.resource.amount) ? detail?.resource.amount.currency : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee amount</div>
                            <div><span>{(detail?.resource && detail?.resource.transaction_fee) ? detail?.resource.transaction_fee.value : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee amount currency</div>
                            <div><span>{(detail?.resource && detail?.resource.transaction_fee) ? detail?.resource.transaction_fee.currency : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment status</div>
                            <div><span>{detail?.resource ? detail?.resource.state : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protection eligibility</div>
                            <div><span>{detail?.resource ? detail?.resource.protection_eligibility : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Protection eligibility type</div>
                            <div><span>{detail?.resource ? detail?.resource.protection_eligibility_type : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Pay pal account</div>
                            <div><span>{data?.paypal_id || ''}</span></div>
                        </div>

                    </>}
            </div>
        )
    }

    contentPayment3 = (data, detail) => {
        const { classes } = this.props;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Authorization key</div>
                    <div><span>{detail?.summary || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.message || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${detail?.amount || 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{detail?.gateway || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{detail?.status || ''}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{detail?.resource_type || ''}</span></div>
                </div> */}
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(detail?.created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>
            </div>
        )
    }

    contentPayment4 = (data, detail) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        return (
            <div className='mt-3'>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{((detail?.amount || 0) / 100).toFixed(2)}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{data?.payment_method || ''}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(data?.order_created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{moment.unix(detail?.created || 0).format("YYYY-MM-DD HH:mm:ss")}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Ack</div>
                            <div><span>{detail?.status || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Token</div>
                            <div><span>{data?.token || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Transaction</div>
                            <div><span>{detail?.balance_transaction || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Amount</div>
                            <div><span>{((detail?.amount || 0) / 100).toFixed(2)}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Amount captured</div>
                            <div><span>{((detail?.amount_captured || 0) / 100).toFixed(2)}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Currency</div>
                            <div><span>{detail?.currency || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Risk level</div>
                            <div><span>{detail?.outcome ? (detail?.outcome.risk_level || '') : ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Risk score</div>
                            <div><span>{detail?.outcome ? (detail?.outcome.risk_score || '') : ''}</span></div>
                        </div>

                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment status</div>
                            <div><span>{detail?.status || ''}</span></div>
                        </div>

                    </>}
            </div>
        )
    }

    contentPayment5 = (data, detail) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        return (
            <div className='mt-3'>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{((detail?.amount || 0))}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{data?.payment_method || ''}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{detail?.transaction_date || 0}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{detail?.transaction_date || 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Reference No</div>
                            <div><span>{detail?.reference_no || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>TransactionID</div>
                            <div><span>{detail?.transaction_id || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Amount</div>
                            <div><span>{((detail?.amount || 0))}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Currency</div>
                            <div><span>{detail?.currency || ''}</span></div>
                        </div>

                        <div className='mb-2'>
                            <div className='font-weight-bold'>Payment status</div>
                            <div><span>{detail?.payment_status || ''}</span></div>
                        </div>

                    </>}
            </div>
        )
    }

    contentFulfillment = (detail) => {
        const { classes } = this.props;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Items</div>
                    <ul className={clsx(classes.ul_items, 'list-unstyled mt-1')}>
                        {detail?.items.map((item, k) =>
                            <li key={k}>
                                <div className='d-flex align-items-center'>
                                    {item.quantity}{' x '}{item.title}
                                    {` (${item.variant_name || ''})`}
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
                <div className='mb-1'>
                    <div className='font-weight-bold'>Service</div>
                    <div><span>{detail?.service || ''}</span></div>
                </div>
            </div>
        )
    }

    contentEdit = (detail) => {
        const { classes } = this.props;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                {detail?.add && detail?.add.length > 0 && <div className='mb-2'>
                    <div className='font-weight-bold'>Added</div>
                    <ul className={clsx(classes.ul_edit, 'mt-1')}>
                        {detail?.add.map((item, k) =>
                            <li key={k}>
                                <span>
                                    {item.quantity}{' x '}
                                    <a href={`/admin/products/${item.product_id}`}>{item.title}</a>
                                    {` (${item.variant_name || ''})`}
                                </span>
                            </li>
                        )}
                    </ul>
                </div>}
                {detail?.remove && detail?.remove.length > 0 && <div className='mb-2'>
                    <div className='font-weight-bold'>Removed</div>
                    <ul className={clsx(classes.ul_edit, 'mt-1')}>
                        {detail?.remove.map((item, k) =>
                            <li key={k}>
                                <span>
                                    {item.quantity}{' x '}{item.title}
                                    {` (${item.variant_name || ''})`}
                                </span>
                            </li>
                        )}
                    </ul>
                </div>}
            </div>
        )
    }

    contentEdit2 = (detail) => {
        const { classes } = this.props;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                {detail?.note && <div className='mb-2'>
                    <div className={clsx('font-weight-bold', classes.ul_items)}>Note</div>
                    <div className={'mt-2'}>
                        <span>
                            {detail?.note}
                        </span>
                    </div>
                </div>}
            </div>
        )
    }

    contentRefund = (data, response) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        var detail = response.response || {};
        var items = response.item;

        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded by</div>
                    <div><span>{data?.email || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.Ack || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${detail?.GROSSREFUNDAMT || 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded items</div>
                    <ul className={clsx(classes.ul_items, 'list-unstyled mt-1')}>
                        {items && items.map((rfitem, k) =>
                            <li key={k}>
                                <div className='d-flex align-items-center'>
                                    {rfitem.quantity}{' x '}
                                    <a href={`/admin/products/${rfitem.product_id}`} className='ml-1 mr-1'>{rfitem.title}</a>
                                    {` - ${rfitem.variant_name || ''}`}
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{'CREDIT CARD'}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{data?.payment_status || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{'refund'}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(data?.order_created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{detail?.TIMESTAMP || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Ack</div>
                            <div><span>{detail?.ACK || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Correlation</div>
                            <div><span>{detail?.CORRELATIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Version</div>
                            <div><span>{detail?.VERSION || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Build</div>
                            <div><span>{detail?.BUILD || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Refund transaction</div>
                            <div><span>{detail?.REFUNDTRANSACTIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Net refund amount</div>
                            <div><span>{detail?.NETREFUNDAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Net refund amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee refund amount</div>
                            <div><span>{detail?.FEEREFUNDAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee refund amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross refund amount</div>
                            <div><span>{detail?.GROSSREFUNDAMT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross refund amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Total refunded amount</div>
                            <div><span>{detail?.TOTALREFUNDEDAMOUNT || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Total refunded amount currency</div>
                            <div><span>{detail?.CURRENCYCODE || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Refund status</div>
                            <div><span>{detail?.REFUNDSTATUS || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Pending reason</div>
                            <div><span>{detail?.PENDINGREASON || ''}</span></div>
                        </div>
                    </>
                }
            </div>
        )
    }

    contentRefund2 = (data, response) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        var detail = response.response || {};
        var items = response.item;

        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded by</div>
                    <div><span>{data?.email || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.state || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${(detail && detail?.amount) ? detail?.amount.total : 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded items</div>
                    <ul className={clsx(classes.ul_items, 'list-unstyled mt-1')}>
                        {items && items.map((rfitem, k) =>
                            <li key={k}>
                                <div className='d-flex align-items-center'>
                                    {rfitem.quantity}{' x '}
                                    <a href={`/admin/products/${rfitem.product_id}`} className='ml-1 mr-1'>{rfitem.title}</a>
                                    {` - ${rfitem.variant_name || ''}`}
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{'PAYPAL'}</span></div>
                </div>
                {/* <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{data?.payment_status || ''}</span></div>
                </div> */}
                <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{'refund'}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(data?.order_created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>

                <button className={clsx(classes.button_normal, 'drop', 'font-weight-bold')}
                    onClick={() => {
                        this.setState(prevState => ({ open_gateway: !prevState.open_gateway }))
                    }}
                >
                    <span className='mr-1'>{`Information from the gateway`}</span>
                    <FontAwesomeIcon icon={open_gateway ? faCaretDown : faCaretRight} />
                </button>
                {open_gateway &&
                    <>
                        <div className='mb-2 mt-2'>
                            <div className='font-weight-bold'>Timestamp</div>
                            <div><span>{detail?.create_time || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Ack</div>
                            <div><span>{detail?.state || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Correlation</div>
                            <div><span>{detail?.CORRELATIONID || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Refund transaction</div>
                            <div><span>{detail?.id || ''}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Net refund amount</div>
                            <div><span>{(detail && detail?.refund_from_received_amount) ? detail?.refund_from_received_amount?.value : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Net refund amount currency</div>
                            <div><span>{(detail && detail?.refund_from_received_amount) ? detail?.refund_from_received_amount?.currency : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee refund amount</div>
                            <div><span>{(detail && detail?.refund_from_transaction_fee) ? detail?.refund_from_transaction_fee?.value : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Fee refund amount currency</div>
                            <div><span>{(detail && detail?.refund_from_transaction_fee) ? detail?.refund_from_transaction_fee?.currency : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross refund amount</div>
                            <div><span>{(detail && detail?.amount) ? detail?.amount.total : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Gross refund amount currency</div>
                            <div><span>{(detail && detail?.amount) ? detail?.amount.currency : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Total refunded amount</div>
                            <div><span>{(detail && detail?.total_refunded_amount) ? detail?.total_refunded_amount.value : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Total refunded amount currency</div>
                            <div><span>{(detail && detail?.total_refunded_amount) ? detail?.total_refunded_amount.currency : 0}</span></div>
                        </div>
                        <div className='mb-2'>
                            <div className='font-weight-bold'>Refund status</div>
                            <div><span>{detail?.state || ''}</span></div>
                        </div>
                    </>
                }
            </div>
        )
    }



    contentRefund3 = (data, response) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        var detail = response.response || {};
        var items = response.item;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded by</div>
                    <div><span>{data?.email || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.message || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${(detail && detail?.amount) ? detail?.amount : 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded items</div>
                    <ul className={clsx(classes.ul_items, 'list-unstyled mt-1')}>
                        {items && items.map((rfitem, k) =>
                            <li key={k}>
                                <div className='d-flex align-items-center'>
                                    {rfitem.quantity}{' x '}
                                    <a href={`/admin/products/${rfitem.product_id}`} className='ml-1 mr-1'>{rfitem.title}</a>
                                    {` - ${rfitem.variant_name || ''}`}
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{detail?.gateway || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{detail?.status || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{'refund'}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(detail?.created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>
            </div>
        )
    }

    contentRefund4 = (data, response) => {
        const { classes } = this.props;
        const { open_gateway } = this.state;
        var detail = response.response || {};
        var items = response.item;
        return (
            <div className='mt-3' style={{ fontSize: 14 }}>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Order</div>
                    <div><span className={classes.a_href}>{data?.order_code || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded by</div>
                    <div><span>{data?.email || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Message</div>
                    <div><span>{detail?.message || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Amount</div>
                    <div><span>{`$${(detail && detail?.amount) ? detail?.amount : 0}`}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Refunded items</div>
                    <ul className={clsx(classes.ul_items, 'list-unstyled mt-1')}>
                        {items && items.map((rfitem, k) =>
                            <li key={k}>
                                <div className='d-flex align-items-center'>
                                    {rfitem.quantity}{' x '}
                                    <a href={`/admin/products/${rfitem.product_id}`} className='ml-1 mr-1'>{rfitem.title}</a>
                                    {` - ${rfitem.variant_name || ''}`}
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Gateway</div>
                    <div><span>{detail?.gateway || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Status</div>
                    <div><span>{detail?.status || ''}</span></div>
                </div>
                <div className='mb-2'>
                    <div className='font-weight-bold'>Type</div>
                    <div><span>{'refund'}</span></div>
                </div>
                <div className='mb-3'>
                    <div className='font-weight-bold'>Created</div>
                    <div><span>{moment(detail?.created_at).format('MMMM DD,YYYY, hh:mm a')}</span></div>
                </div>
            </div>
        )
    }


    resendEmail = (data) => {
        const { dispatch } = this.props
        var detail = JSON.parse(data?.detail || '{}')
        var title = ''
        var content = ''
        switch (detail?.type_email) {
            case 13:
                title = 'Resend shipping confirmation?'
                content = `The shipping confirmation email will be sent to ${detail?.email}`
                break;
            case 5:
                title = 'Resend order cancelled?'
                content = `The order cancelled email will be sent to ${detail?.email}`
                break;
            case 0:
                title = 'Resend order confirm?'
                content = `The order confirmation email will be sent to ${detail?.email}`
                break;
            default:
                break;
        }
        
        layerRender.showPopup({
            title: title,
            button: `Send`,
            content:
                <div>
                    <div>
                        <span>{content}</span>
                    </div>
                </div>
            ,
            colorAction: true,
            onAction: () => {
                return new Promise((resolve) => {
                    // orderService.send_mail(detail?.order_id, { type_email: detail?.type_email })
                    //     .then(res => {
                    //         if (res.status == 200) {
                    //             toastr.success('Send email successful')
                    //             resolve(true)
                    //         } else {
                    //             resolve(false)
                    //             toastr.error('Send email error')
                    //         }
                    //     }, error => {
                    //         resolve(false)
                    //         toastr.error('Send email error')
                    //     })
                })
            }
        })

    }

    getContentRefund = (type, data) => {
        switch (type) {
            case 1:
                return this.contentRefund2(data, JSON.parse(data?.detail || '{}'))
            case 2:
                return this.contentRefund(data, JSON.parse(data?.detail || '{}'))
            case 3:
                return this.contentRefund3(data, JSON.parse(data?.detail || '{}'))
            case 4:
                return this.contentRefund4(data, JSON.parse(data?.detail || '{}'))
            default:
                return null
        }
    }

    getContentPayment = (type, data) => {
        const { payment } = this.props
        if (!payment) {
            return null
        }
        let dataLog = Object.assign({}, data);
        if ([1, 2, 3, 4].includes(type)) {
            dataLog['payment_id'] = payment['payment_id']
            dataLog['payer_id'] = payment['payer_id']
            dataLog['token'] = payment['token']
            dataLog['payment_status'] = payment['payment_status']
            dataLog['payment_id'] = payment['payment_id']
            dataLog['response'] = payment['response']
        }
        switch (type) {
            case 1:
                return this.contentPayment2(dataLog, JSON.parse(dataLog?.response || '{}'))
            case 2:
                return this.contentPayment(dataLog, JSON.parse(dataLog?.response || '{}'))
            case 3:
                return this.contentPayment4(dataLog, JSON.parse(dataLog?.response || '{}'))
            case 4:
                return this.contentPayment5(dataLog, JSON.parse(dataLog?.response || '{}'))
            default:
                return this.contentPayment3(data, JSON.parse(data?.detail || '{}'))
        }
    }





    render() {
        const { classes, type, data, onDelete, payment } = this.props;
        const { array_value, formState, open, detail } = this.state
        return (
            <div ref={this.wrapperRef} style={{ fontSize: 14 }}>
                {type == 4 && <div className='pb-3 d-flex'>
                    <div className='flex-fill mr-2'>
                        <div>
                            <span>
                                {data?.content}
                            </span>
                        </div>
                        <div className='mt-3'>
                            <button className={clsx(classes.button_normal, 'small')}
                                onClick={() => {
                                    this.resendEmail(data)
                                }}
                            >
                                Resend email
                            </button>
                        </div>
                    </div>
                    <div className={classes.whiteSpace}>
                        <span>
                            {moment(data?.created_at).utcOffset('GMT-00:00').format('HH:mm')}
                        </span>
                    </div>
                </div>}

                {[6, 5, 3].includes(type) && <div className='pb-3 d-flex'>
                    <div className='flex-fill mr-2'>
                        <div>
                            {(type == 6 && data?.payment_method_id == 3) ? <span className='mr-1'>{data?.content}</span> :
                                <button className={clsx(classes.button_normal, 'drop')}
                                    onClick={() => {
                                        this.setState(prevState => ({ open: !prevState.open, open_gateway: false }))
                                    }}
                                >
                                    <span className='mr-1'>{data?.content}</span>
                                    <FontAwesomeIcon icon={open ? faCaretDown : faCaretRight} />
                                </button>}
                            {open && (type == 3 ? (data?.payment_id ?
                                (this.getContentPayment(data?.payment_method_id, data)) :
                                this.contentPayment3(data, JSON.parse(data?.detail))
                            )
                                : ((type == 6 && JSON.parse(data?.detail)?.response) ?
                                    (
                                        this.getContentRefund(data?.payment_method_id, data)
                                    )
                                    :
                                    (data?.action.includes('edit order') ?
                                        this.contentEdit(JSON.parse(data?.detail || '{}')) :
                                        (data?.action.includes('edit note') ? this.contentEdit2(JSON.parse(data?.detail || '{}')) :
                                            this.contentFulfillment(JSON.parse(data?.detail || '{}'))
                                        )
                                    ))
                            )}

                        </div>
                    </div>
                    <div className={classes.whiteSpace}>
                        <span>
                            {moment(data?.created_at).utcOffset('GMT-00:00').format('HH:mm')}
                        </span>
                    </div>
                </div>}
                {type == 2 && <div className='pb-3 d-flex'>
                    <div className='flex-fill mr-2'>
                        <div>
                            <span>
                                {data?.content}
                            </span>
                        </div>
                    </div>
                    <div className={classes.whiteSpace}>
                        <span>
                            {moment(data?.created_at).utcOffset('GMT-00:00').format('HH:mm')}
                        </span>
                    </div>
                </div>}
                {type == 1 && <div className='pb-3 d-flex'>
                    <Card className='w-100'>
                        <CardContent>
                            <div className='d-flex '>
                                <div className='flex-fill'>
                                    <div className='d-flex'>
                                        <b className='mr-2'>{data?.title}</b>
                                        <span className={classes.sub_title}>{moment(data?.created_at).utcOffset('GMT-00:00').fromNow()}</span>
                                    </div>
                                    <div>
                                        <span>{data?.content}</span>
                                    </div>
                                </div>
                                <button className={clsx(classes.button_normal, 'transparent')}
                                    onClick={() => {
                                        onDelete()
                                        // this.setState(prevState => ({ actions: !prevState.actions }))
                                    }}
                                    ref={this.topActions}
                                ><FontAwesomeIcon icon={faTimes}
                                    size={'1x'} style={{ fontSize: 16, color: '#637381', marginLeft: 10 }} />

                                </button>
                                {/* <Popper
                                            // placement={comp.placement}
                                            className={classes.popper}
                                            open={this.state.actions || false}
                                            anchorEl={this.topActions.current}
                                            role={undefined}
                                            transition disablePortal>
                                            {({ TransitionProps, placement }) => (
                                                <Grow
                                                    {...TransitionProps}
                                                    style={{
                                                        transformOrigin: 'center top',
                                                        marginTop: 5,

                                                    }}
                                                >
                                                    <Paper>
                                                        <ClickAwayListener onClickAway={() => {
                                                            this.setState({ actions: false })
                                                        }}>
                                                            <MenuList autoFocusItem={false} id="menu-list-grow"
                                                                onKeyDown={(event) => {
                                                                }}>
                                                                <MenuItem onClick={() => {
                                                                    this.setState({ actions: false })
                                                                }}>
                                                                    Edit comment
                                                                            </MenuItem>
                                                                
                                                                <MenuItem onClick={() => {
                                                                    this.setState({ actions: false })
                                                                }}>
                                                                    Delete comment
                                                                            </MenuItem>

                                                            </MenuList>

                                                        </ClickAwayListener>
                                                    </Paper>
                                                </Grow>
                                            )}
                                        </Popper>
                                    */}
                            </div>
                        </CardContent>
                    </Card>
                </div>}
            </div>
        )
    }
}

TimeLineComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(TimeLineComponent)));
export { connectedList as TimeLineComponent };
