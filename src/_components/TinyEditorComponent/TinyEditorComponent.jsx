import React, { Component } from "react";
import tinymce from "tinymce";
import "tinymce/themes/silver";
import "tinymce/plugins/wordcount";
import "tinymce/plugins/table";
import config from 'config'
import axios from 'axios'

class TinyEditorComponent extends Component {
    constructor() {
        super();
        this.state = { editor: null };
    }

    componentDidMount() {
        const {toolbar} = this.props;
        const default_tolbar = {
            toolbar1: 'bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | image media link anchor | code outdent indent',
            toolbar2: 'numlist bullist | forecolor backcolor removeformat pagebreak | table',
            toolbar3: 'fontselect fontsizeselect styleselect',
        }
        var init = {
            selector: `#${this.props.id}`,
            plugins: "link image autoresize media anchor code pagebreak lists table",
            autoresize_bottom_margin: 50,
            images_upload_url: `${config.apiUrl}/images`,
            toolbar_sticky: true,
            toolbar_sticky_offset: 60,
            images_upload_handler: function (blobInfo, success, failure) {
                setTimeout(function () {
                    console.blobInfo
                    var formData = new FormData();
                    formData.append('image', blobInfo.blob(), blobInfo.filename());
                    let user = JSON.parse(localStorage.getItem('user_hebecore'))
                    axios({
                        method: 'POST',
                        url: `${config.apiUrl}/images`,
                        headers: { 'Authorization': "" + user.token, },
                        data: formData
                    }).then(res => {
                        if (res.status == 200) {
                            success(`${config.cdnUrl_large}${res.data.url}`);
                        } else {
                            failure()
                        }
                    }).catch(err => {
                        failure()
                    })
                }, 2000);
            },
            menubar: false,
            classes: 'error-xxxx',
            resize: true,
            statusbar: false,
            setup: (editor) => {
                editor.on("keyup change", () => {
                    const content = editor.getContent();
                    this.props.onEditorChange(content);
                });
            },
        }
        if (toolbar) {
            init = { ...init, ...toolbar}
        }else{
            init = { ...init, ...default_tolbar }
        }
        tinymce.init(init);
        if (this.props.content) {
            this.onSetContent(this.props.content)
        }
    }

    componentWillUnmount() {
        tinymce.remove(this.state.editor);
    }

    onSetContent = (description) => {
        tinymce.activeEditor.setContent(description);
    }

    render() {
        return (
            <textarea
                id={this.props.id}
                value={this.props.content}
                onChange={(e) => console.log(e)}
                style={this.props.style || {}}
                className={this.props.className || ''}
                placeholder={this.props.placeholder || ''}
            />
        );
    }
}

export default TinyEditorComponent;