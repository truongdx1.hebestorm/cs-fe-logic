/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Grid, IconButton, Tooltip,
  Typography, Breadcrumbs, Link
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import FilterListIcon from '@material-ui/icons/FilterList';
import { SearchInput } from '..';
import { connect } from 'react-redux'
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { history } from '../../_helpers'
import CircularProgress from '@material-ui/core/CircularProgress';
import PrintIcon from '@material-ui/icons/Print';
import MenuIcon from '@material-ui/icons/Menu';
import GetAppIcon from '@material-ui/icons/GetApp';
import PublishIcon from '@material-ui/icons/Publish';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import clsx from 'clsx';
import { cssConstants } from '../../_constants'



const useStyles = makeStyles(theme => ({
  ...cssConstants,
  root: {

  },
  row: {
    // height: '42px',
    display: 'flex',
    // alignItems: 'center',
    // marginTop: theme.spacing(2),

  },
  column1: {
    // flexDirection: "column",
    display: 'flex',
    float: 'right'

  },

  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1),
  },
  tiltePageLink: {
    fontSize: 18,
    fontWeight: 'bold',
    // marginBottom: theme.spacing(2),
    marginTop: 10,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    lineHeight: '30px'
    // marginTop: theme.spacing(2)
  },
  tiltePage: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#3f3f3f',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    lineHeight: '30px'
    // marginTop: theme.spacing(2)
  }, titleGroup: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#888888',
    textTransform: 'uppercase',
    // marginTop: theme.spacing(2)
  }
  , button: {
    margin: theme.spacing(1),
    fontWeight: 'bold',
    whiteSpace: 'nowrap'
    // marginLeft: 'auto',
    // float: 'right'
  },
  progress: {
    width: '18px !important',
    height: '18px !important',
    color: 'white'
  }, sub_container: {
    display: 'flex'
  }
  , sub: {
    marginRight: '2rem',
    display: 'flex',
    alignItems: 'center',
    color: '#212b36',
    cursor: 'pointer',
    '&:hover': {
      color: '#000',
    },
    '& >span': {
      margin: '0 0px 0 5px'
    }
  }, a_back: {
    color: '#637381 !important',
    '&:hover': {
      color: '#000 !important'
    },
    cursor: 'pointer'
  }
}));



const ToolBar = props => {
  const { title, formState, showForm, showSearch, showSearchInput, showPayment, searchName, name, hideEdit, statusView, subTitle, ...rest } = props;
  const { formatMessage } = rest.intl ? rest.intl : { formatMessage: null }
  const classes = useStyles();
  const handleClickBreadCrums = (bread, index) => {
    const { breadcrums, dispatch } = props;
    var data = []
    if (breadcrums.data != null) {
      data = Object.assign([], breadcrums.data.data)
      data.splice(index);
    }
    dispatch({ type: 'SET_BREADCRUM', data })
    history.push(bread.url)
  }
  return (
    <div>
      {props.type > 0 &&
      <div className={classes.row} style={{ marginBottom: 10, justifyContent: 'space-between' }}>
        {props.type == 2 && <a onClick={rest.actionBack} className={classes.a_back}>
          <ArrowBackIosIcon fontSize='small' /> {title}
        </a>}
        {props.type == 1000 &&
          <div>
            <ArrowBackIcon />
            <ArrowForwardIcon />
          </div>}
      </div>
      }
      <div className={'d-flex justify-content-between'}>
        <div>
          {rest.breadcrums.data != null &&
            <div className='d-flex align-items-center'>
              <Breadcrumbs
                aria-label="breadcrumb"
                separator={<NavigateNextIcon fontSize="small" />}
                style={{ marginBottom: 0, marginLeft: 3, }}
              >
                {rest.breadcrums.data.data.map((bread, index) =>
                  (index != (rest.breadcrums.data.data.length - 1))
                    ?
                    <Link
                      color="inherit"
                      key={index}
                      onClick={() => { handleClickBreadCrums(bread, index) }}
                      // href={bread.url}
                      style={{ cursor: 'pointer' }}
                    >
                      <Typography
                        className={classes.tiltePageLink}
                        gutterBottom
                      >
                        {bread.name}
                      </Typography>
                    </Link>
                    :
                    <Typography
                      className={classes.tiltePage}
                      gutterBottom
                      key={index}
                    >
                      {bread.name}
                    </Typography>
                )}
              </Breadcrumbs>
              {statusView && statusView()}
            </div>}
          {props.type == 2 &&
            <div className={classes.sub_container}>
              {subTitle ? <span>{subTitle}</span> : ''}
              {rest.sub_actions && rest.sub_actions.map((ac, index) => {
                if (ac.href && ac.visible) {
                  return (
                    <a href={ac.href} className={classes.sub} key={index} target={'_blank'}>
                      {ac.icon} <FormattedMessage id={ac.text} />
                    </a>
                  )
                } else if (ac.visible) {
                  return (
                    <a onClick={ac.action} className={classes.sub} key={index}>
                      {ac.icon} <FormattedMessage id={ac.text} />
                    </a>
                  )
                }
              }
              )}
            </div>}
        </div>
        <div
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            display: 'flex'
          }}
        >
          {rest.actions.length > 0 && <div className={classes.column1}>{
            rest.actions.reverse().map((ac, index) => {
              if (ac.visible) {
                return (
                  <Button
                    key={index}
                    className={clsx(classes.button_normal, ac.color, 'hb-button')}
                    color={ac.color}
                    onClick={ac.action}
                    size={ac.size}
                    variant={ac.variant}
                    startIcon={ac.icon}
                    endIcon={ac.endIcon}
                    ref={ac.ref}
                  >
                    {formatMessage({ id: ac.text })}
                  </Button>
                )
              }
            }
            )
          }
          </div>}

        </div>
      </div>
    </div>
  );
};

ToolBar.propTypes = {
  breadcrumbs: PropTypes.any,
  className: PropTypes.string,
  dispatch: PropTypes.any,
  formState: PropTypes.object,
  hideEdit: PropTypes.bool,
  isSubmitting: PropTypes.bool,
  name: PropTypes.string,
  searchName: PropTypes.string,
  showForm: PropTypes.bool,
  showPayment: PropTypes.bool,
  showSearch: PropTypes.bool,
  title: PropTypes.any,
  actions: PropTypes.array,
};

ToolBar.defaultProps = {
  actions: [],
}

function mapStateToProps(state) {
  return {
    breadcrums: state.breadcrums
  }
}

export default connect(mapStateToProps)(ToolBar);
