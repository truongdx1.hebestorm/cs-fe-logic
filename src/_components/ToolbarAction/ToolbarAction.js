import React from 'react'
import { Button } from 'rsuite'
import { Button as ButtonMui } from '@material-ui/core'
import { cssConstants } from '../../_constants'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
import { IconCheck } from '@tabler/icons'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  buttonDate: {
    border: "1px solid #ccc",
    marginRight: 5
  },
  buttonSelected: {
    backgroundColor: "#129535",
    marginRight: 5,
    color: "#fff",
    fontWeight: "bold"
  }
}));

const ActionBar = ({ leftActions, rightActions, selectedAction, leftResetAction }) => {
  const classes = useStyles()
  return (
    <div className='d-flex justify-content-between' style={{ marginTop: 5 }}>
      <div className="d-flex align-items-center">
        {leftActions && leftActions.filter(e => e.visible).map((ac, index) => (
          <Button
            key={index}
            onClick={ac.action}
            className={selectedAction == ac.id ? classes.buttonSelected : classes.buttonDate}
          >
            {selectedAction == ac.id && <IconCheck size={16} stroke={3} color='#fff' className='mr-1' />}<FormattedMessage id={ac.text} />
          </Button> 
        ))}
        {leftResetAction && leftResetAction.visible &&
          <a 
            href='#' 
            className='ml-2'
            onClick={(e) => {
              e.preventDefault()
              e.stopPropagation()
              leftResetAction.action()
            }}
          >{leftResetAction.title}</a>}
      </div>
      <div className="d-flex justify-content-end align-items-center">
        {rightActions && rightActions.filter(e => e.visible).map((ac, index) => (
          <ButtonMui
            key={index}
            className={clsx(classes.button_normal, ac.color || 'primary', 'hb-button')}
            color={ac.color || 'primary'}
            onClick={ac.action}
            size={ac.size || 'small'}
            variant={ac.variant || 'contained'}
            startIcon={ac.icon}
            endIcon={ac.endIcon}
          >
            <FormattedMessage id={ac.text} />
          </ButtonMui>
        ))}
      </div>
    </div>
  )
}

export default ActionBar;