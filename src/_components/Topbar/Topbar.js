/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { history } from '../../_helpers'
import { connect } from 'react-redux'
import logo from '../../assets/images/logos/logo_1.png'

import { FormattedMessage } from 'react-intl';
import toastr from '../../common/toastr';
import { userConstants } from '../../_constants';
import {
  Button, MenuItem, ClickAwayListener, Grow, AppBar, ListItem,
  Paper, Popper, MenuList, Typography, List, Menu, ListItemText, Tooltip, TextField
} from '@material-ui/core';
import LoadingBar from 'react-redux-loading-bar'
import Backdrop from '@material-ui/core/Backdrop';
import { cssConstants } from '../../_constants'



const useStyles = makeStyles(theme => ({
  ...cssConstants,
  root: {
    boxShadow: '0 2px 4px rgba(0,0,0,.1)'
  },
  flexGrow: {
    flexGrow: 1
  },
  icon: {
    // marginRight: theme.spacing(1),
    color: '#fff'
  },
  iconPop: {
    marginRight: theme.spacing(1),
    color: '#2c6835'
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 13,
    // marginRight: theme.spacing(1),
    textTransform: 'initial',
    color: '#fff',
    // maxWidth: '100px',
    display: '-webkit-box',
    lineClamp: '1',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    boxOrient: 'vertical'
  },
  textMenuItem: {
    fontSize: 14
  },
  toolBar: {
    minHeight: 70,
    backgroundColor: '#ffffff',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
  },
  textLogo: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 15, textAlign: 'center'
  },
  dropdownMenu: {
    backgroundColor: '#2F6737',
    border: 'none',
    marginTop: 5
  }, tabMenu: {
    maxWidth: '160px',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  }, rightMenuBtn: {
    outline: 'none !important'
  }, inputSearch: {
    float: 'left',
    backgroundColor: '#fff'
  }, center_top_bar: {
    alignItems: 'center',
    display: 'flex',
    padding: '5px 8rem',
    flexGrow: '1'
  }, title_top_bar: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#637381',
  }, button: {
    margin: '0 0 0 10px',
    fontWeight: 'bold',
    whiteSpace: 'nowrap'
    // marginLeft: 'auto',
    // float: 'right'
  }, bar: {
    width: '100vw',
    background: '#f9fafb',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
    padding: '0 0',
    height: '100%',
    display: 'inline-flex',
  }, logo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '220px',
    border: '1px solid #ccc',
    borderBottom: 0,
    borderTop: 0,
    borderLeft: 'none',
  }
}));

const allPages = [];



const Topbar = props => {
  const { className, onSidebarOpen, disableSave, ...rest } = props;

  const classes = useStyles();

  const anchorRef = useRef(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [pages, setPages] = useState([]);
  const [widthScreen, setWidthScreen] = useState(window.innerWidth);


  console.log();

  const handleZoom = () => {
    setWidthScreen(window.innerWidth)
  }
  window.addEventListener('resize', handleZoom);


  useEffect(() => {
  }, [props.user, widthScreen])



  const handleCloseDept = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem('user_hebecore')
    history.push('/')
  }

  const handleHover = (event) => {
    setHover(event)
  }

  const onClickMenu = (page, index) => {
    if (index == 0) {
      return
    } else {
      handleHover(-1)
    }
    history.push(page.href)
  }


  const handleToggle = (value) => {
    setOpen(value);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleAccount = () => {
    history.push('/admin/account/' + rest.user.data.public_id, { clear: true })
    setOpen(false);
  };

  return (
    <AppBar
      className={clsx(classes.root, className)}
    >
      <div
        className={classes.bar}
      >
        <div className={classes.logo}>
          <img
            alt="Logo"
            height={43}
            src={logo}
          />
          <Typography
            className={classes.textLogo}
            component="span"
          >
            HBpro
          </Typography>
        </div>
        <div className={classes.center_top_bar}>
          <Typography
            component="span"
            className={classes.title_top_bar}
          >
            {rest.text_unsaved ? rest.text_unsaved : 'Unsaved'}
          </Typography>
          <div style={{ visibility: 'hidden', flexGrow: '1' }}>
            <TextField
              variant="outlined"
              // className={classes.inputSearch}
              fullWidth
              InputProps={{
                className: classes.inputSearch,
                classes: {
                  placeholder: classes.placeholder,
                }
              }}
              margin="dense"
              placeholder="Search"
            />
          </div>
          <Button
            className={clsx(classes.button_normal, 'default','mr-1')}
            size={'small'}
            variant={'contained'}
            color="default"
            onClick={props.onDiscard}
          >
            Discard
          </Button>
          <Button
            className={clsx(classes.button_normal, 'primary', disableSave ? 'disable' : '')}
            disabled={disableSave ? true : false}
            size={'small'}
            variant={'contained'}
            color="primary"
            onClick={props.onSave}
          >
            Save
          </Button>
        </div>
      </div>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;

  return {
    user
  }
}



export default connect(mapStateToProps)(Topbar);
