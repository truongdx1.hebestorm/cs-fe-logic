import moment from 'moment'
export const ranges = [
  {
    label: "Today",
    value: [new Date(), new Date()],
  },
  {
    label: "Yesterday",
    value: [new Date(moment().subtract(1, "days")), new Date(moment().subtract(1, "days"))],
  },
  {
    label: "Last 7 Days",
    value: [new Date(moment().subtract(6, "days")), new Date(moment())],
  },
  {
    label: "Last 30 Days",
    value: [new Date(moment().subtract(29, "days")), new Date(moment())],
  },
  {
    label: "This Month",
    value: [new Date(moment().startOf("month")), new Date(moment().endOf("month"))],
  },
  {
    label: "Last Month",
    value: [new Date(moment().subtract(1, "month").startOf("month")), new Date(moment().subtract(1, "month").endOf("month"))],
  }
];

export const offerIssues = [
  {id: 1, name: 'None'},
  {id: 2, name: "Hold order and wait on Customer's feedback"},
  {id: 3, name: "Refund"},
  {id: 4, name: "Resend"},
  {id: 5, name: "Resend + Refund"},
  {id: 6, name: "Cancel"},
  {id: 7, name: "Cancel + Refund"},
  {id: 8, name: "Change info"},
  {id: 9, name: "Change info + Refund"},
  {id: 10, name: "Check information"},
  {id: 11, name: "Contact PO"},
  {id: 12, name: "No Action"},
  {id: 13, name: "Offer resend/refund"},
]