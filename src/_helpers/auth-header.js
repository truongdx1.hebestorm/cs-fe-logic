import Cookies from 'universal-cookie'
const cookies = new Cookies();

export function authHeader() {
  // return authorization header with jwt token
  const user = cookies.get('user_hebecore')
  if (user) {
    return {
      'x-access-token': '' + user.token,
      'Accept': 'application/x-www-form-urlencoded',
      'Content-Type': 'application/x-www-form-urlencoded',
      "Access-Control-Allow-Origin": "*",
      "source": "cs"
    }
  } else {
    return {}
  }
}

export const authHeaderJson = () => {
  const user = cookies.get('user_hebecore')
  if (user) {
    return {
      'x-access-token': '' + user.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      "Access-Control-Allow-Origin": "*",
      "source": "cs"
    }
  } else {
    return {}
  }  
}
