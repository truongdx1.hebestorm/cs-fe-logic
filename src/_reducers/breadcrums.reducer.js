export function breadcrums(state = {}, action) {
  switch (action.type) {
    case 'SET_BREADCRUM': {
      return {
        data: action
      }
    }
    default:
      return state
  }
}