import { combineReducers } from 'redux';
import { authentication } from './authentication.reducer';
import { alert } from './alert.reducer';
import { locale } from './locale.reducer'
import { users } from './users.reducer'
import { query } from './query.reducer'
import { openSnackbar } from './snackbar.reducer'
import { breadcrums } from './breadcrums.reducer'

import { loadingBarReducer } from 'react-redux-loading-bar'

const rootReducer = combineReducers({
  authentication,
  locale,
  alert,
  users,
  query,
  breadcrums,
  openSnackbar,
  loadingBar: loadingBarReducer,
});

export default rootReducer
