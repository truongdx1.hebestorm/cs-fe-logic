
export function query(state = {}, action) {
  switch (action.type) {
    case 'QUERY_OBJECT': {
      return {
        data: action.queryObject
      }
    }
    default:
      return state
  }
}
