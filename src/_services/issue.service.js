import { authHeader, authHeaderJson } from '../_helpers'
import axios from 'axios'
import toastr from '../common/toastr'
import { history, store } from '../_helpers'
import { userActions } from '../_actions'
import config from 'config'
import { request as request_form } from './request'

export default {
    getIssue, commitAction, updateIssue, fetchLogs, fetchRelatedResources
}

const request = axios.create({
  baseURL: config.apiUrl,
  headers: authHeaderJson()
})

function getIssue(id){
  return request.get(`/api/issues/${id}`).then((res) => res.data.data)
}

function updateIssue(id, data){
  return request.patch(`/api/issues/${id}`, {
    data: data
  }).then(res => res.data.data)
}

function commitAction(args){
  const requestData = {
    action_code: args.code,
    action_data: args.data,
    object_type: args.objectType,
    object_id: args.objectId,
    object_ids: args.objectIds
  }
  return request.post(`/api/actions/commit`, requestData).then(res => res)
}

function fetchLogs(issue, filters = {}){
  const requestData = {
    object_id: issue.id,
    object_type: filters.objectType
  }

  if (filters.actionCode){
    requestData.action_code = filters.actionCode
  }

  return request_form.get(`${config.apiUrl}/api/logs`, requestData).then(res => res.data.data)
}

function fetchRelatedResources(issue){
  return request.get(`/api/issues/${issue.id}/related_resources`).then(res => res.data.data)
}