import { authHeader, authHeaderJson } from '../_helpers'
import axios from 'axios'
import toastr from '../common/toastr'
import { history, store } from '../_helpers'
import { userActions } from '../_actions'
import Cookies from 'universal-cookie'

const cookies = new Cookies()

export const request = {
    post, get, update, _delete, post_form,update_form,get_blob,
    post_json, update_json,
}

function post(url, queryObject) {
    // console.log(queryObject)
    if(!queryObject)
        queryObject={}
    const searchParams = Object.keys(queryObject).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(queryObject[key]);
    }).join('&');
    return axios({
        method: 'POST',
        url: url,
        data: searchParams,
        headers: authHeader(),
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                cookies.remove('user_hebecore', { domain: '.salegate.io' })
                cookies.remove('user_hebecore')
            }
        } catch (error) {
        }

        throw err
    })
}

function post_json(url, queryObject) {
  // console.log(queryObject)
  if(!queryObject)
      queryObject={}
  return axios({
      method: 'POST',
      url: url,
      data: queryObject,
      headers: authHeaderJson(),
  }).then(res => res).catch((err) => {
      try {
          if (err.response != null && err.response.status == 401) {
              cookies.remove('user_hebecore', { domain: '.salegate.io' })
              cookies.remove('user_hebecore')
          }
      } catch (error) {
      }

      throw err
  })
}

function post_form(url, queryObject) {
    if(!queryObject)
        queryObject={}
    var formData = new FormData();
    var arrKeys = Object.keys(queryObject)
    arrKeys.map((key)=>{
        if(['image','file'].includes(key)){
            if(queryObject[key] && queryObject[key].file){
                formData.append(key,queryObject[key].file);
            }
        }else{
            formData.append(key, queryObject[key]);
        }
    })
    return axios({
        method: 'POST',
        url: url,
        headers: authHeader(),
        data: formData
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                cookies.remove('user_hebecore', { domain: '.salegate.io' })
                cookies.remove('user_hebecore')
            }
        } catch (error) {
        }
        throw err
    })
}

function get(url, queryObject) {
    var searchParams = ''
    var parramExits = new URL(url).searchParams.toString()
    if(queryObject){
        if(Object.keys(queryObject).length>0){
            searchParams= (parramExits?'&':'?') + Object.keys(queryObject).map((key) => {
                return encodeURIComponent(key) + '=' + encodeURIComponent(queryObject[key]);
            }).join('&');
        }
    }
    return axios({
        method: 'GET',
        url: url + searchParams,
        headers: authHeader(),
    }).then(res => res).catch((err) => {
        // try {
        //     if (err.response != null && err.response.status == 401) {
        //         cookies.remove('user_hebecore', { domain: '.salegate.io' })
        //         cookies.remove('user_hebecore')
        //     }
        // } catch (error) {
        // }
        throw err
    })
}

function get_blob(url, queryObject) {
    var searchParams = ''
    var parramExits = new URL(url).searchParams.toString()
    if(queryObject){
        if(Object.keys(queryObject).length>0){
            searchParams= (parramExits?'&':'?') + Object.keys(queryObject).map((key) => {
                return encodeURIComponent(key) + '=' + encodeURIComponent(queryObject[key]);
            }).join('&');
        }
    }
    return axios({
        method: 'GET',
        url: url + searchParams,
        headers: authHeader(),
        responseType: 'blob'
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                store.dispatch(userActions.logout(1))
                history.push('/')
            }
        } catch (error) {
        }
        throw err
    })
}

function update(url, queryObject) {
    if(!queryObject)
        queryObject={}
    const searchParams = Object.keys(queryObject).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(queryObject[key]);
    }).join('&');
    return axios({
        method: 'PUT',
        url: url,
        headers: authHeader(),
        data: searchParams
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                cookies.remove('user_hebecore', { domain: '.salegate.io' })
                cookies.remove('user_hebecore')
            }
        } catch (error) {
        }

        throw err
    })
}

function update_json(url, queryObject) {
  // console.log(queryObject)
  if(!queryObject)
      queryObject={}
  return axios({
      method: 'PUT',
      url: url,
      data: queryObject,
      headers: authHeaderJson(),
  }).then(res => res).catch((err) => {
      try {
          if (err.response != null && err.response.status == 401) {
              cookies.remove('user_hebecore', { domain: '.salegate.io' })
              cookies.remove('user_hebecore')
          }
      } catch (error) {
      }

      throw err
  })
}

function update_form(url, queryObject) {
    if(!queryObject)
        queryObject={}
    var formData = new FormData();
    var arrKeys = Object.keys(queryObject)
    arrKeys.map((key)=>{
        if(key == 'image'){
            formData.append('image', queryObject[key].file);
        }else{
            formData.append(key, queryObject[key]);
        }
    })
    return axios({
        method: 'PUT',
        url: url,
        headers: authHeader(),
        data: formData
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                cookies.remove('user_hebecore', { domain: '.salegate.io' })
                cookies.remove('user_hebecore')
            }
        } catch (error) {
        }
        throw err
    })
}

function _delete(url, queryObject) {
    if(!queryObject)
        queryObject={}
    const searchParams = Object.keys(queryObject).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(queryObject[key]);
    }).join('&');
    return axios({
        method: 'DELETE',
        url: url,
        headers: authHeader(),
        data: searchParams
    }).then(res => res).catch((err) => {
        try {
            if (err.response != null && err.response.status == 401) {
                cookies.remove('user_hebecore', { domain: '.salegate.io' })
                cookies.remove('user_hebecore')
            }
        } catch (error) {
        }
        throw err
    })
}