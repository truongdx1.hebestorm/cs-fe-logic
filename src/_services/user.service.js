import config from 'config'
import { authHeader, userAPI } from '../_helpers'
import axios from 'axios'
import toastr from '../common/toastr';
import { request } from './request';
import Cookies from 'universal-cookie'

export const userService = {
  login,
  quicklogin,
  logout,
  register,
  getListUser,
  createUser,
  getUserDetail,
  editUser,
  getDataUser,
  updateUser,
  createUserDetail,
  updatePassword,
  signup,
  getUserInStore,
  submitAccount,
  forgotPassword,
  recoveryPassword,
  me,
  inviteSupplier,
  inviteUserByAdmin,
  fetchUsers
}

function me(){
  const cookies = new Cookies()

  return axios.get(`${config.apiUrl}/api/v1/user_profile`, { headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*',
    'x-access-token': cookies.get('user_hebecore').token
  }})
}

function login(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    },
    body: searchParams
  }
  const cookies = new Cookies()
  // console.log(requestOptions);
  localStorage.removeItem('user_hebecore')
  cookies.remove('user_hebecore', {domain: '.salegate.io'})
  return fetch(`${config.apiUrl}/api/v1/login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      localStorage.setItem('user_hebecore', JSON.stringify(user))
      cookies.set('user_hebecore', {token: user.data.token, public_id: user.data.public_id}, { domain: '.salegate.io' })
      return user
    })
}

function submitAccount(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');

  return axios.post(`${config.apiUrl}/api/v1/password`, searchParams, { headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*'
  }}).then(user => {
      const cookies = new Cookies()
      localStorage.setItem('user_hebecore', JSON.stringify(user.data))
      cookies.set('user_hebecore', {token: user.data.data.token, public_id: user.data.data.public_id}, { domain: '.salegate.io' })
    }).then(user => {
      window.open(config.supplierUrl, '_blank')
    }).catch(error => toastr.error(error.response.data.msg))
}

function forgotPassword(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');

  return axios.post(`${config.apiUrl}/api/v1/forgot_password`, searchParams, { headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*'
  }}).then(response => {
      toastr.success(response.data.msg)
    }).catch(error => {
      toastr.error(error.response.data.msg)
    })
}

function recoveryPassword(query){
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');

  return axios.post(`${config.apiUrl}/api/v1/recovery_password`, searchParams, { headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*'
  }}).then(response => {
      alert("Change password successful!")
      window.location.href = '/login'
      return response
    }).catch(error => {
      if(error.response){
        toastr.error(error.response.data.msg)
      }
    })
}

function register(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    },
    body: searchParams
  }

  // console.log(requestOptions);
  return fetch(`${config.apiUrl}/auth/quickregister`, requestOptions)
    .then(handleResponse)
    .then(user => {
      localStorage.setItem('user_hebecore', JSON.stringify(user))
      return user
    })
}

function quicklogin(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    },
    body: searchParams
  }

  // console.log(requestOptions);
  return fetch(`${config.apiUrl}/auth/quicklogin`, requestOptions)
    .then(handleResponse)
    .then(user => {
      localStorage.setItem('user_hebecore', JSON.stringify(user))
      return user
    })
}

function signup(query) {
  const searchParams = Object.keys(query).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(query[key]);
  }).join('&');
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    },
    body: searchParams
  }

  // console.log(requestOptions);
  return fetch(`${config.apiUrl}/auth/signup`, requestOptions)
    .then(handleResponse)
    .then(user => {
      return user
    })
}

function logout(type) {
  // remove user from local storage to log user out
  if(type==1 && localStorage.getItem('user_hebecore')){
    let user = JSON.parse(localStorage.getItem('user_hebecore'))
    delete user['token']
    localStorage.setItem('user_hebecore', JSON.stringify(user))
  }else{
    localStorage.removeItem('user_hebecore')
  }
}

function getListUser(queryObject) {
  return request.get(userAPI.list, queryObject)
  // const searchParams = '?' + Object.keys(queryObject).map((key) => {
  //   return key + '=' + queryObject[key];
  // }).join('&');
  // return axios({
  //   method: 'GET',
  //   url: userAPI.list + searchParams,
  //   headers: authHeader(),
  // }).then(res => res).catch((err) => {
  //   toastr.error(err)
  //   throw err
  // })
}
function getUserDetail() {
  return request.get(userAPI.detail, {})
}



function createUser(param) {
  return request.get(userAPI.create, param)
}

function editUser(param, public_id) {
  return request.update(userAPI.edit(public_id), param)
}

function getDataUser(queryObject) {
  return request.get(userAPI.user, queryObject)
}
function updateUser(queryObject) {
  return request.update(userAPI.update, queryObject)
}

function createUserDetail(queryObject) {
  return request.post(userAPI.createDetail, queryObject)
}

function handleResponse(response) {
  // console.log(response)
  return response.text().then(text => {
    const data = text && JSON.parse(text)
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        // logout()
        // location.reload(true)
      }
      const error = (data) || response.statusText
      return Promise.reject(error)
    }

    return data
  })
}

function updatePassword(queryObject) {
  return request.update(userAPI.changePass, queryObject)
}


function getUserInStore(queryObject) {
  return request.get(userAPI.users_in_store, queryObject)
}

function inviteSupplier(queryObject) {
  return request.post(`${config.apiUrl}/api/v1/invite_supplier`, queryObject)
}

function inviteUserByAdmin(queryObject){
  return request.post(`${config.apiUrl}/api/v1/admin/invite_user`, queryObject)
}

function fetchUsers(queryObject = {}){
  return request.get(`${config.apiLoginUrl}/api/v1/users`, queryObject)
}