import React, { Component } from 'react'
import { CopyToClipboard as CopyToClipboardLib } from 'react-copy-to-clipboard'
import { IconClipboardList } from '@tabler/icons'
import { connect } from 'react-redux'
import { Button } from 'rsuite'

class CopyToClipboard extends Component {
  
  copySuccess = () => {
    const { dispatch } = this.props;
    const openSnackbar = {
      open: true
    }
    dispatch({ type: 'OPEN_SNACKBAR', openSnackbar })
  }

  render() {
    return (
      <CopyToClipboardLib text={this.props.text}>
        {this.props.component == 'button' ? 
          <Button style={this.props.buttonStyle} onClick={() => this.copySuccess()}>
            <IconClipboardList size={20} stroke={2} /> {this.props.titleButton}
          </Button> :
          (this.props.icon ? 
            <this.props.icon size={20} stroke={2} cursor="pointer" onClick={() => this.copySuccess()} /> : 
            <IconClipboardList size={20} stroke={2} cursor={"pointer"} onClick={() => this.copySuccess()} />
          )
        }
      </CopyToClipboardLib>
    )
  }

}

const connectedList = connect(null)(CopyToClipboard)
export { connectedList as CopyToClipboard }