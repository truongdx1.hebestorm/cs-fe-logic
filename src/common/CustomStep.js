import React from "react";
import { Stepper, Step, StepLabel, StepConnector } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import CheckIcon from "@material-ui/icons/Check";

const CustomConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    "& $line": {
      backgroundColor: "#449242",
    },
  },
  completed: {
    "& $line": {
      backgroundColor: "#449242",
    },
  },
  line: {
    height: 2,
    border: 0,
    backgroundColor: "#ccc",
    borderRadius: 1,
  },
})(StepConnector);

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: "transparent",
    padding: 0,
  },
}));

const CustomStep = ({ steps, activeStep }) => {
  const classes = useStyles();
  return (
    <Stepper
      className={classes.root}
      alternativeLabel
      activeStep={steps?.findIndex((e) => e.value == activeStep)}
      connector={<CustomConnector />}
    >
      {steps.map((step) => (
        <Step key={step.value}>
          <StepLabel style={{ marginTop: 10 }}>{step.label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

export default CustomStep;
