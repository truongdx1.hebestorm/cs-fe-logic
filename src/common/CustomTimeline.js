import React, { useState } from 'react'
import { Checkbox, FormControlLabel, Divider } from '@material-ui/core'
import { Timeline, Input, Button } from 'rsuite'

const CustomTimeline = ({ 
  title, titleCheckbox, items, inputStyle, buttonStyle, buttonText, placeholder, onReply,
  renderItem, firstItemDot, itemClassName, containerClass, handleSubmit, submitText, disabledButton
}) => {
  const [show, setShow] = useState(true)
  const [content, setContent] = useState(null)

  const handleSubmitText = (e) => {
    if (submitText){
      submitText(content, () => {
        setContent(null)
      })
    }
    if (handleSubmit){
      handleSubmit(content)
      setContent(null)
    }

    if (onReply){
      onReply(content).then(() => setContent(null))
    }
  }

  return (
    <div className='timeline'>
      {title &&
      <>
        <div className='p-3'>
          <div className='d-flex align-items-center justify-content-between'>
            <h4>{title}</h4>
            <FormControlLabel 
              control={
                <Checkbox 
                  checked={show} 
                  onChange={(event) => {
                    const checked = event.target.checked
                    setShow(checked)
                  }}
                />
              }
              label={titleCheckbox || "Show history"}
            />
          </div>
        </div>
        <Divider style={{ background: '#DCDCDC' }} />
      </>
      }
      {show &&
        <div className='p-3'>
          <Timeline endless className={`custom-timeline ${containerClass || ''}`}>
            <Timeline.Item dot={firstItemDot || null}>
              <div className={`d-flex justify-content-between ${itemClassName || ''}`}>
                <Input
                  placeholder={placeholder || "Leave a comment..."}
                  value={content || ''}
                  onChange={(value, e) => setContent(value)}
                  // style={inputStyle || {}}
                />
                <Button 
                  className='ml-1' 
                  style={buttonStyle || { background: '#449242', color: '#fff' }} 
                  onClick={handleSubmitText}
                  disabled={disabledButton || !content}
                >
                  {buttonText}
                </Button>
              </div>
            </Timeline.Item>
            {items && items.map((item, index) => (
              <Timeline.Item key={index}>
                <div className='ml-1'>
                  {renderItem(item)}
                </div>
              </Timeline.Item>
            ))}
          </Timeline>
        </div>
      }
    </div>
  )
}

export default CustomTimeline;