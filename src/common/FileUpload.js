import React from "react";
import Dropzone from "react-dropzone";
import CircularProgress from '@material-ui/core/CircularProgress';

// for pdf files
class FileUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      warningMsg: ""
    }
  }

  onDrop = (accepted, rejected) => {
    if (Object.keys(rejected).length !== 0) {
      const message = "Please submit valid file type";
      this.setState({ warningMsg: message }, () => {
        setTimeout(() => this.setState({ warningMsg: "" }), 3000)
      });
    } else {
      this.props.addFile(accepted);
      this.setState({ warningMsg: "" });
    }
  };

  render() {
    const { files, uploading } = this.props;
    const render =
      Object.keys(files).length !== 0 ? (
        files.map(file => <p key={file.name}>{file.name}</p>)
      ) : (
        <p>{this.props.placeholder}</p>
      );

    return (
      <div>
        <p style={{ color: 'red' }}>{this.state.warningMsg}</p>

        <Dropzone
          multiple={false}
          accept={this.props.acceptExtension}
          onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
        >
          {({getRootProps, getInputProps, isDragAccept, isDragReject, acceptedFiles, rejectedFiles}) => (
            <section>
              <div 
                {...getRootProps()} 
                style={{
                  width: '100%',
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "10vh",
                  border: '1px solid #ccc',
                  borderRadius: 5
                }}
              >
                {uploading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : 
                <>
                  <input {...getInputProps()}  />
                  {isDragReject ? <p style={{ color: 'red' }}>{'Please submit a valid file'}</p> : render}
                </>
                }
              </div>
            </section>
          )}
        </Dropzone>
      </div>
    );
  }
}

export default FileUpload;
