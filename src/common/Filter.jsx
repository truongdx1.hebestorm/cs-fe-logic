import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import {
  FormControlLabel,
  Checkbox,
  TextField,
  Typography,
  Drawer, Button
} from "@material-ui/core";
import { connect } from "react-redux";
import { DateRangePicker, IconButton } from "rsuite";
import AsyncSelect from "react-select/async";
import { IconX } from '@tabler/icons'
import { ranges } from '../_constants'
import moment from 'moment'
import clsx from 'clsx'
import { MultiInputPicker } from './MultiInputPicker'
import { cssConstants } from '../_constants'

const useStyles = (theme) => ({
  ...cssConstants,
  titleTable: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#6CA754",
    padding: theme.spacing(1),
    color: "#fff",
  },
  seperator: {
    height: 10,
  },
  field: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    backgroundColor: "transparent",
    padding: "5px 0",
    borderRadius: 5,
    overflow: "visible"
  },
  textField: {
    width: "90%",
  },
  checkBoxContainer: {
    padding: "0 10px",
    backgroundColor: "#fff",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
  },
  buttonClear: {
    backgroundColor: "#eeeeee",
    fontWeight: "bold",
  },
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    fontWeight: "bold",
    backgroundColor: "#6CA754",
    color: "#ffffff",
  },
  labelRoot: {
    color: "rgba(0, 0, 0, 0.38)",
    fontSize: 13,
  },
});

const customStyles = {
  control: (provided, state) => ({
    ...provided,
    border: "1px solid #999999",
    backgroundColor: "transparent !important",
    margin: '0 14px',
    textAlign: "center",
  }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: "none",
  }),
  placeholder: (provided, state) => ({
    ...provided,
    color: "rgba(0, 0, 0, 0.38)",
    fontSize: 14,
    paddingLeft: 4,
  }),
  input: (provided, state) => ({
    ...provided,
    paddingLeft: 4,
  }),
  singleValue: (provided, state) => ({
    ...provided,
    paddingLeft: 4,
    fontSize: 14,
    color: "black",
  }),
  menu: (provided, state) => ({
    ...provided,
    maxWidth: '90%',
    marginLeft: 15,
    marginTop: 2,
    zIndex: 100,
  }),
  menuList: (provided, state) => ({
    ...provided,
    maxHeight: 200,
    textAlign: 'left'
  }),
};

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {},
    };
  }

  handleInputChange = (inputValue) => {
    return inputValue;
  }

  loadOptions = (inputValue, loadingData) => {
    var result = loadingData(inputValue);
    return result;
  }

  onChangeDateRange = (name, value) => {
    var arr = []
    if (value.length > 0) {
      arr.push(moment(new Date(value[0])).format('YYYY-MM-DD'))
      arr.push(moment(new Date(value[1])).format('YYYY-MM-DD'))
    }
    const event = {
      target: {
        name,
        value: arr
      }
    }
    this.props.onChange(event)
  }

  onChangeType = (event, option) => {
    const arr = this.props.formState.values[option.name] || []
    if (event.target.checked) {
      arr.push(option.value)
    } else {
      var index = arr.indexOf(option.value);
      arr.splice(index, 1)
    }
    const e = {
      target: {
        name: option.name,
        value: arr
      }
    }
    this.props.onChange(e)
  }

  sideList = () => {
    const { 
      classes, onChange, formState, filters,
    } = this.props;
    const { values } = this.state;
    return (
      <div style={{ position: 'relative', height: '100vh', width: '20vw' }}>
        <div className="d-flex justify-content-between p-1 align-items-center p-3" style={{ borderBottom: '1px solid #ccc' }}>
          <h4>Filter</h4>
          <IconButton 
            style={{ backgroundColor: '#fff' }} 
            icon={<IconX size={20} stroke={1.5} />} 
            onClick={this.props.onClose('right', false)} 
          />
        </div>
        <div style={{ height: 'calc(100vh - 130px)', overflow: 'auto' }}>
        {filters && filters.map((options, index) => (
          <div className={classes.field} key={index}>
            <Typography
              style={{
                textAlign: "left",
                padding: "5px 15px",
              }}
            >
              <span style={{ fontWeight: 600, fontSize: 14 }}>{options.label}</span>
            </Typography>
            {options.childs.map((option, i) => {
              if (option.type == "text") {
                return (
                  <TextField
                    key={i}
                    disabled={false}
                    margin="dense"
                    id={option.id}
                    label={option.label}
                    placeholder={option.placeholder}
                    name={option.name}
                    value={formState.values[option.name] || ""}
                    onChange={onChange}
                    style={{ width: "92%", fontSize: "90%", marginTop: 0 }}
                    InputProps={{
                      style: {
                        fontSize: 13,
                      },
                    }}
                    fullWidth
                    variant="outlined"
                    InputLabelProps={{
                      classes: {
                        root: classes.labelRoot,
                      },
                    }}
                  />
                );
              } else if (option.type == "daterange") {
                return (
                  <div key={i}>
                    {values[option.name] && values[option.name].length > 0 && option.label && (
                      <a
                        style={{
                          position: "absolute",
                          zIndex: 10,
                          background: "white",
                          fontSize: 10,
                          marginLeft: 10,
                          padding: "0 15px 0 5px",
                          color: "gray",
                        }}
                      >
                        {option.label}
                      </a>
                    )}
                    <DateRangePicker
                      appearance="subtle"
                      style={{
                        width: "92%",
                        border: "1px solid #ccc",
                        borderRadius: 5,
                      }}
                      onChange={(value) => {
                        this.onChangeDateRange(option.name, value)
                      }}
                      value={formState.values[option.name] || []}
                      onSelect={(value, _this) => {
                        var button = document.getElementsByClassName("rs-picker-toolbar-right-btn-ok");
                        if (button.length > 0) {
                          button[0].click();
                        }
                      }}
                      format="DD-MM-YYYY"
                      ranges={ranges}
                      placement={option.placement == "right" ? "bottomEnd" : "bottomStart"}
                      placeholder={option.placeholder}
                      cleanable={option?.required ? false : true}
                    />
                  </div>
                );
              } else if (option.type == "checkbox") {
                return (
                  <div
                    style={{ textAlign: "left", padding: "0 15px" }}
                    key={i}
                  >
                    <FormControlLabel
                      style={{ width: "90%", marginBottom: "0" }}
                      control={
                        <Checkbox
                          color="primary"
                          name={option.name}
                          onChange={(event) => this.onChangeType(event, option)}
                          checked={formState.values[option.name] ? formState.values[option.name].includes(option.value) ? true : false : false}
                        />
                      }
                      label={
                        <div style={{ color: option.color }}>
                          {option.label}
                        </div>
                      }
                    />
                  </div>
                );
              } else if (option.type == "select" && !option.options) {
                return (
                  <AsyncSelect
                    key={i}
                    cacheOptions
                    loadOptions={(inputValue) =>
                      this.loadOptions(inputValue, option.loadOptions)
                    }
                    defaultOptions
                    onInputChange={this.handleInputChange}
                    isSearchable
                    isClearable
                    name={option.name}
                    onChange={(value) => {
                      var e = {
                        target: {
                          name: option.name,
                          value
                        },
                      };
                      this.props.onChange(e)
                    }}
                    placeholder={option.label}
                    getOptionLabel={(item) => option.getOptionLabel ? option.getOptionLabel(item) : (option.labelKey ? item[option.labelKey] : item.name)}
                    getOptionValue={(item) => option.getOptionValue ? option.getOptionValue(item) : (option.valueKey ? item[option.valueKey] : item.id)}
                    valueKey={option.valueKey || "id"}
                    value={formState.values[option.name] || null}
                    styles={customStyles}
                  />
                );
              } else if (option.type == "select" && option.options) {
                return (
                  <AsyncSelect
                    key={i}
                    className="MuiFormControl-marginDense"
                    cacheOptions
                    loadOptions={(inputValue) =>
                      this.loadOptions(inputValue, option.loadOptions)
                    }
                    defaultOptions={option.options}
                    onInputChange={this.handleInputChange}
                    isSearchable
                    isClearable
                    name={option.name}
                    onChange={(value) => {
                      var e = {
                        target: {
                          name: option.name,
                          value
                        },
                      };
                      this.props.onChange(e)
                    }}
                    placeholder={option.label}
                    getOptionLabel={(item) => option.getOptionLabel ? option.getOptionLabel(item) : (option.labelKey ? item[option.labelKey] : item.name)}
                    getOptionValue={(item) => option.getOptionValue ? option.getOptionValue(item) : (option.valueKey ? item[option.valueKey] : item.id)}
                    valueKey={option.valueKey || "id"}
                    value={formState.values[option.name] || null}
                    styles={customStyles}
                  />
                );
              } else if (option.type == "multiselect" && !option.options) {
                return (
                  <AsyncSelect
                    key={i}
                    isMulti
                    cacheOptions
                    closeMenuOnSelect={false}
                    loadOptions={(inputValue) =>
                      this.loadOptions(inputValue, option.loadOptions)
                    }
                    defaultOptions
                    onInputChange={this.handleInputChange}
                    isSearchable
                    isClearable
                    name={option.name}
                    onChange={(value) => {
                      var e = {
                        target: {
                          name: option.name,
                          value
                        },
                      };
                      this.props.onChange(e)
                    }}
                    placeholder={option.label}
                    getOptionLabel={(item) => option.getOptionLabel ? option.getOptionLabel(item) : (option.labelKey ? item[option.labelKey] : item.name)}
                    getOptionValue={(item) => option.getOptionValue ? option.getOptionValue(item) : (option.valueKey ? item[option.valueKey] : item.id)}
                    valueKey={option.valueKey || "id"}
                    value={formState.values[option.name] || null}
                    styles={customStyles}
                  />
                );
              } else if (option.type == "multiselect" && option.options) {
                return (
                  <AsyncSelect
                    key={i}
                    isMulti
                    closeMenuOnSelect={false}
                    className="MuiFormControl-marginDense"
                    cacheOptions
                    loadOptions={(inputValue) =>
                      this.loadOptions(inputValue, option.loadOptions)
                    }
                    defaultOptions={option.options}
                    onInputChange={this.handleInputChange}
                    isSearchable
                    isClearable
                    name={option.name}
                    onChange={(value) => {
                      var e = {
                        target: {
                          name: option.name,
                          value
                        },
                      };
                      this.props.onChange(e)
                    }}
                    placeholder={option.label}
                    getOptionLabel={(item) => option.getOptionLabel ? option.getOptionLabel(item) : (option.labelKey ? item[option.labelKey] : item.name)}
                    getOptionValue={(item) => option.getOptionValue ? option.getOptionValue(item) : (option.valueKey ? item[option.valueKey] : item.id)}
                    valueKey={option.valueKey || "id"}
                    value={formState.values[option.name] || null}
                    styles={customStyles}
                  />
                );
              } else if (option.type == "multi_checkbox_picker") {
                return (
                  <MultiInputPicker
                    key={i}
                    onDelete={click => this.handleDeleteMultiPicker = click}
                    valueKey={option.valueKey || 'id'}
                    icon={option.icon}
                    style={{ marginTop: 8 }}
                    getOptionLabel={(item) => option.getOptionLabel ? option.getOptionLabel(item) : (option.labelKey ? item[option.labelKey] : item.name)}
                    onChange={(items) => {
                      // if (items.length > 0) {
                        const event = {
                          target: {
                            name: option.name,
                            value: items
                          }
                        }
                        this.props.onChange(event)
                      // }
                    }}
                    initialValue={formState.values[option.name]}
                    loadOptions={(inputValue) => option.loadOptions(inputValue)}
                    noOptionsMessage="No results found"
                    renderAriaLabel={(selectedItems) => option.renderAriaLabel(selectedItems)}
                    defaultAriaLabel={option.defaultAriaLabel}
                  />
                )
              }
            })}
          </div>
        ))}
        </div>
        <div className="d-flex justify-content-center align-item-center p-2" style={{ position: 'absolute', bottom: 0, height: 60, borderTop: '1px solid #ccc', width: '100%' }}>
          <Button
            className={clsx(classes.button_normal, 'default', 'hb-button')}
            color="default"
            onClick={() => this.props.resetFilter()}
            variant="contained"
          >
            Clear all filters
          </Button>
          <Button
            className={clsx(classes.button_normal, 'hb-button')}
            color="primary"
            onClick={() => this.props.onSearch()}
            variant="contained"
          >
            Apply
          </Button>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Drawer
          anchor="right"
          onClose={this.props.onClose('right', false)}
          open={this.props.right}
        >
          {this.sideList()}
        </Drawer>
      </div>
    )
  }
}

const propTypes = {};

const defaultProps = {};

Filter.propTypes = propTypes;
Filter.defaultProps = defaultProps;

const connectedFilter = injectIntl(connect(null)(withStyles(useStyles)(Filter)));
export { connectedFilter as Filter };
