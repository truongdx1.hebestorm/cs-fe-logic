import React, { Component } from 'react';
import PropTypes from 'prop-types'
import validate from "validate.js";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import { Button } from '@material-ui/core'
import { Modal, Divider } from 'rsuite'
import CircularProgress from '@material-ui/core/CircularProgress';
import { cssConstants } from  '../_constants'
import clsx from 'clsx'
import Cookies from 'universal-cookie';

const useStyles = (theme) => ({
  ...cssConstants
});

const cookies = new Cookies()

export const singleton = { current: null }
export default class FormModal extends Component {

  constructor(props) {
    super(props)

    this.state = {
      show: false,
      submitData: {
        isValid: true,
        values: {},
        errors: {},
        isSubmiting: false,
      }
    }

    this.config = {}
  }

  componentDidMount() {
    singleton.current = this
  }

  validateForm(schema) {
    var submitData = Object.assign({}, this.state.submitData);
    const errors = validate(submitData.values, schema);
    submitData["isValid"] = errors ? false : true;
    submitData["errors"] = errors || {};
    return submitData
  }

  handleChange = (e) => {
    const submitData = Object.assign({}, this.state.submitData);
    submitData['values'][e.target.name] = e.target.value
    this.setState({ submitData })
  }

  showForm = (config) => {
    this.config = config || {}
    const submitData = Object.assign({}, this.state.submitData)
    if (config.data) {
      submitData['values'] = config.data
      submitData['errors'] = {}
      submitData['isSubmiting'] = false
      submitData['isValid'] = true
    }
    this.setState({
      show: true,
      submitData
    })
  }

  onDropFile = (accepted, rejected, namespace) => {
    if (Object.keys(rejected).length !== 0) {
      
    } else {
      var formData = new FormData();
      formData.append("file", accepted[0]);
      formData.append("namespace", namespace)
      const userObj = cookies.get('user_hebecore')
      axios({
        method: "POST",
        url: `${config.apiCoreUrl}/api/upload_file`,
        headers: {
          'x-access-token': userObj.token
        },
        data: formData
      }).then(res => {
        if (res.data.success) {
          const submitData = Object.assign({}, this.state.submitData)
          submitData.values['files']
        }
      })
    }
  };

  render() {
    const { show, submitData } = this.state
    const { classes } = this.props;
    return (
      <Modal 
        show={show} 
        onHide={() => this.setState({ show: false })} 
        size={this.config.size || 'sm'} 
        full={this.config.fullscreen || false}
        centered={this.config.centered || false }
        backdrop='static'
      >
        <Modal.Header closeButton>
          <Modal.Title>{this.config.title || 'Modal'}</Modal.Title>
        </Modal.Header>
        <Divider style={{ margin: '20px -20px' }} />
        <Modal.Body style={{ marginTop: 0, paddingBottom: 0 }}>
          {this.config.customView ? this.config.customView(submitData, this.handleChange) : (this.config.content || "")}
        </Modal.Body>
        <Divider style={{ margin: '20px -20px' }} />
        <Modal.Footer>
          <Button
            className={clsx(classes.button_normal, 'default', 'hb-button')}
            color="default"
            disabled={submitData.isSubmiting}
            onClick={() => this.setState({ show: false })}
          >
            {submitData.isLoading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : 'Close'}
          </Button>
          {this.config.multipleActions && this.config.actions.map((action, index) => (
            <Button
              className={clsx(classes.button_normal, action.color || 'primary', 'hb-button')}
              key={index}
              style={{ marginLeft: 5 }}
              disabled={submitData.isSubmiting}
              onClick={() => {
                var submitData = Object.assign({}, this.state.submitData);
                submitData["isSubmiting"] = true;
                if (this.config.schema) {
                  const submitData = this.validateForm(this.config.schema)
                  if (!submitData.isValid) {
                    submitData['isSubmiting'] = false
                    this.setState({ submitData })
                    return
                  }
                }
                action.onAction(submitData).then(res => {
                  if (res) {
                    submitData.isSubmiting = false;
                    this.setState({ submitData, show: false })
                  }

                  if (this.config.action.onActionDone){
                    this.config.action.onActionDone(submitData, res)
                  }
                });
              }}
            >
              {submitData.isLoading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : (action.titleAction || 'Confirm')}
            </Button>
          ))}
          {!this.config.multipleActions && this.config.action &&
            <Button
              className={clsx(classes.button_normal, this.config.action.color || 'primary', 'hb-button')}
              disabled={submitData.isSubmiting}
              style={{ marginLeft: 5 }}
              onClick={() => {
                var submitData = Object.assign({}, this.state.submitData);
                submitData["isSubmiting"] = true;
                if (this.config.action.schema) {
                  const submitData = this.validateForm(this.config.action.schema)
                  if (!submitData.isValid) {
                    submitData['isSubmiting'] = false
                    this.setState({ submitData })
                    return
                  }
                }

                this.config.action.onAction(submitData).then(res => {
                  if (res) {
                    submitData.isSubmiting = false;
                    this.setState({ submitData, show: false })
                  }

                  if (this.config.action.onActionDone){
                    this.config.action.onActionDone(submitData, res)
                  }
                });
              }}
            >
              {submitData.isLoading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : (this.config.action.titleAction || 'Confirm')}
            </Button>
          }
        </Modal.Footer>
      </Modal>
    )
  }
}

FormModal.instance = singleton
const connectedList = injectIntl(connect(null)(withStyles(useStyles)(FormModal)));
export { connectedList as FormModal };