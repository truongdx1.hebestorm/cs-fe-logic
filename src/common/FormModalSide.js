import React, { Component } from 'react';
import PropTypes from 'prop-types'
import validate from "validate.js";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import { Button, Drawer, IconButton } from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress';
import { cssConstants } from '../_constants'
import clsx from 'clsx'
import CloseIcon from '@material-ui/icons/Close';

const useStyles = (theme) => ({
  ...cssConstants
});

export const singleton = { current: null }
export default class FormModalSide extends Component {

  constructor(props) {
    super(props)

    this.state = {
      open: false,
      submitData: {
        isValid: true,
        values: {},
        errors: {},
        isSubmiting: false,
        touched: {}
      }
    }

    this.config = {}
  }

  componentDidMount() {
    singleton.current = this
  }

  validateForm(schema) {
    var submitData = Object.assign({}, this.state.submitData);
    const errors = validate(submitData.values, schema);
    submitData["isValid"] = errors ? false : true;
    submitData["errors"] = errors || {};
    submitData["isSubmiting"] = true
    return submitData
  }

  handleChange = (e) => {
    const submitData = Object.assign({}, this.state.submitData);
    submitData['values'][e.target.name] = e.target.value
    this.setState({ submitData })
  }

  showForm = (config) => {
    this.config = config || {}
    var submitData = {
      isValid: true,
      values: {},
      errors: {},
      isSubmiting: false,
      touched: {}
    }
    if (config.data) {
      submitData['values'] = config.data
    }
    // console.log(submitData['values']['options'])
    this.setState({
      open: true,
      submitData,
      modeView: config?.modeView
    })
  }

  hasError = (field) => {
    var submitData = Object.assign({}, this.state.submitData);
    return (submitData.isSubmiting || submitData.touched[field]) && submitData.errors[field] ? true : false
  }

  sideList = (submitData, handleChange, classes, otherProps) => (
    <div style={{ position: 'relative', height: '100vh', width: this.config.width || '50vw' }}>
      <div className="d-flex justify-content-between p-2 align-items-center pl-3" style={{ borderBottom: '1px solid #ccc' }}>
        <h4>{this.config.title}</h4>
        <div className='d-flex align-item-center'>
          {this.config.multipleActions && this.config.actions.map((action, index) => (
            <Button
              className={clsx(classes.button_normal, action.color || 'primary', 'hb-button')}
              key={index}
              style={{ marginLeft: 5 }}
              disabled={submitData.isSubmiting}
              variant="contained"
              onClick={() => {
                var submitData = Object.assign({}, this.state.submitData);
                submitData["isSubmiting"] = true;
                if (this.config.schema) {
                  const submitData = this.validateForm(this.config.schema)
                  if (!submitData.isValid) {
                    submitData['isSubmiting'] = false
                    this.setState({ submitData })
                    return
                  }
                }
                action.onAction(submitData).then(res => {
                  if (res) {
                    submitData.isSubmiting = false;
                    this.setState({ submitData, show: false })
                  }
                });
              }}
            >
              {submitData.isLoading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : (action.titleAction || 'Confirm')}
            </Button>
          ))}
          {!this.config.multipleActions && this.config.action &&
            <Button
              className={clsx(classes.button_normal, this.config.action.color || 'primary', 'hb-button')}
              disabled={submitData.isLoading}
              style={{ marginLeft: 5 }}
              variant="contained"
              onClick={() => {
                var submitData = Object.assign({}, this.state.submitData);
                if (this.config.action.schema) {
                  submitData = this.validateForm(this.config.action.schema)
                }
                if (this.config.action.fieldConfig) {
                  this.config.action.fieldConfig.map(field => {
                    var val = field.validate(this, submitData.values[field.name])
                    if (val != true) {
                      submitData.isValid = false
                      submitData.errors[field.name] = [val]
                    }
                  })
                }
                if (!submitData.isValid) {
                  this.setState({ submitData })
                  return
                }

                this.config.action.onAction(submitData, { ctx: this }).then(res => {
                  if (res) {
                    submitData.isSubmiting = false;
                    submitData.isLoading = false
                    this.setState({ submitData, open: false })
                  }
                });
              }}
            >
              {submitData.isLoading ? <CircularProgress style={{ width: 15, height: 15, color: '#fff' }} /> : (this.config.action.titleAction || 'Confirm')}
            </Button>
          }
          <Button
            className={clsx(classes.button_normal, 'default', 'hb-button')}
            color="default"
            onClick={() => this.setState({ open: false })}
            variant="contained"
          >
            Close
          </Button>
        </div>
      </div>
      <div style={{ height: 'calc(100vh - 80px)', overflow: 'auto', backgroundColor: this.config.bgContent || '#fff' }}>
        {this.config.customView ? this.config.customView(submitData, handleChange, otherProps) : ""}
      </div>
    </div>
  )

  render() {
    const { open, submitData } = this.state
    const { classes } = this.props;
    return (
      <Drawer
        anchor={this.config.side}
        onClose={() => this.setState({ open: false })}
        open={open}
      >
        {this.sideList(submitData, this.handleChange, classes, { ctx: this, hasError: this.hasError, modeView: this.state.modeView })}
      </Drawer>
    )
  }
}

FormModalSide.instance = singleton
const connectedList = injectIntl(connect(null)(withStyles(useStyles)(FormModalSide)));
export { connectedList as FormModalSide };