import React, { Component } from 'react';
import PropTypes from 'prop-types'
import validate from "validate.js";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import { Button } from '@material-ui/core'
import { Modal, Divider } from 'rsuite'
import CircularProgress from '@material-ui/core/CircularProgress';
import { cssConstants } from  '../_constants'
import clsx from 'clsx'

const useStyles = (theme) => ({
  ...cssConstants
});

export const singleton = { current: null }

export default class ModalViewImage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      show: false,
      img: null
    }
    this.config = {}
  }

  componentDidMount() {
    singleton.current = this
  }

  showForm = (config) => {
    this.config = config || {}
    var img = config.data || null
    this.setState({
      show: true,
      img
    })
  }

  render() {
    const { show } = this.state
    return (
      <Modal 
        show={show} 
        onHide={() => this.setState({ show: false })} 
        size={'lg'}
        centered
        backdrop='static'
        className='modal-image-preview'
      >
        <Modal.Body style={{ marginTop: 0, paddingBottom: 0 }}>
          <img src={this.state.img} style={{ height: '80vh', width: 'auto' }} />
        </Modal.Body>
      </Modal>
    )
  }
}

ModalViewImage.instance = singleton
const connectedList = injectIntl(connect(null)(withStyles(useStyles)(ModalViewImage)));
export { connectedList as ModalViewImage };