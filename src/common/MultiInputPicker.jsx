/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { IconBuildingStore, IconX, IconChevronDown } from '@tabler/icons';
import { Checkbox } from 'rsuite'

const useStyles = (theme) => ({
  container: {
    width: "100%",
    border: "1px solid #ccc",
    borderRadius: 6,
    padding: 3,
  },
  underline: {
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  input: {
    marginLeft: ".4rem",
  },
  tag_main: {
    display: "flex",
    flexWrap: "wrap",
  },
  notchedOutline: {
    borderRadius: 6
  }
});

class MultiInputPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      items: [],
      selectedItems: [],
      value: null
    };
    this.wrapperRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClick);
    if (this.props.loadOptions) {
      const { valueKey } = this.props
      this.props.loadOptions().then(items => {
        var selectedItems = []
        if (this.props.initialValue) {
          var initialValue = this.props.initialValue
          if (initialValue.length > 0) {
            if (initialValue[0] instanceof Object || typeof initialValue[0] == 'object') {
              selectedItems = items.filter(e => initialValue.map(x => x[valueKey]).includes(e[valueKey || 'id']))
            } else {
              selectedItems = items.filter(e => initialValue.includes(e[valueKey || 'id']))
            }
          }
        }
        this.setState({ items, selectedItems });
      });
    }
    if (this.props.onDelete) {
      this.props.onDelete(this.handleClearOptions);
    }
  }

  componentWillUnmount() {
    try {
      document.removeEventListener("click", this.handleClick);
    } catch {}
  }

  handleClick = (event) => {
    const { target } = event;
    if (!this.wrapperRef.current.contains(target)) {
      this.setState({ open: false });
    }
  };

  handleChangeValue = (event) => {
    this.setState({ value: event.target.value }, () => {
      this.props.loadOptions(this.state.value).then(items => {
        this.setState({ items })
      })
    });
  };

  makeId = (length) => {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  handleSelectItem = (item) => {
    const { valueKey } = this.props;
    const selectedItems = Object.assign([], this.state.selectedItems)
    const index = selectedItems.findIndex(e => e[valueKey || 'id'] == item[valueKey || 'id'])
    if (index !== -1) {
      selectedItems.splice(index, 1)
    } else {
      selectedItems.push(item)
    }
    this.setState({ selectedItems })
    if (this.props.onChange) {
      this.props.onChange(selectedItems)
    }
  }

  handleClearOptions = () => {
    this.setState({ selectedItems: [] })
    if (this.props.onChange) {
      this.props.onChange([])
    }
  }

  render() {
    const {
      classes,
      valueKey,
      getOptionLabel,
      noOptionsMessage,
      renderAriaLabel,
      defaultAriaLabel,
      noSearch
    } = this.props;
    const { value, items, open, selectedItems } = this.state;
    return (
      <div className={classes.tag_main} ref={this.wrapperRef} key={this.props.key || 1}>
        <div className="col-lg-12 pl-0 pr-0">
          <div style={this.props.style || {}} className="d-flex align-items-center justify-content-between pick_wrapper" onClick={() => this.setState({ open: !this.state.open })}>
            <div className="d-flex align-items-center">
              {this.props.icon}
              <span style={{ padding: '0 10px' }}>{selectedItems.length > 0 ? (renderAriaLabel ? renderAriaLabel(selectedItems) : `${selectedItems.length} items selected`) : (defaultAriaLabel || 'Select')}</span>
            </div>
            <div className="d-flex align-items-center">
            {selectedItems.length > 0 && 
              <IconX 
                size={16} 
                stroke={1} 
                cursor={'pointer'}
                onClick={(e) => {
                  e.stopPropagation()
                  this.handleClearOptions()
                }}
              />
            }
            <IconChevronDown size={16} stroke={1} />
            </div>
          </div>

          <div className="rs-anim-fade rs-anim-in rs-picker-picker-check-menu rs-picker-menu placement-bottom-start" style={{ minWidth: 250, top: 50, display: open ? "" : "none" }}>
            {!noSearch && <div role="searchbox" className="rs-picker-search-bar">
              <input 
                className="rs-picker-search-bar-input" 
                placeholder="Search" 
                value={value || ""}
                onChange={this.handleChangeValue} 
              />
            </div>}
            {items.length == 0 && <div className="rs-picker-none">{noOptionsMessage || 'No results found'}</div>}
            {items.length > 0 && 
              <div role="listbox" className="rs-picker-check-menu rs-picker-check-menu-items" style={{ maxHeight: 320 }}>
                {items.map(item => (
                  <div 
                    role="option" 
                    aria-selected="false" 
                    aria-disabled="false" 
                    tabIndex="-1"
                    key={item[valueKey || 'id']}
                  >
                    {/* <div className="rs-check-item"> */}
                      <Checkbox
                        className="rs-check-item"
                        checked={selectedItems.map(e => e[valueKey || 'id']).includes(item[valueKey || 'id'])}
                        onChange={() => this.handleSelectItem(item)}
                      >
                        {getOptionLabel ? getOptionLabel(item) : item.name}
                      </Checkbox>
                    {/* </div> */}
                  </div>
                ))}
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

MultiInputPicker.propTypes = {
  classes: PropTypes.object.isRequired,
};

const connectedList = injectIntl(
  connect(null)(withStyles(useStyles)(MultiInputPicker))
);
export { connectedList as MultiInputPicker };
