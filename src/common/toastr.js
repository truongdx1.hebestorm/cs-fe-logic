import toastr from 'toastr'
// import '../../node_modules/toastr/build/toastr.min.css'
import '../../node_modules/toastr/build/toastr.min.css'

toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  // "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 3000,
  "extendedTimeOut": 1000
}

const Toastr = {
  success (data) {
    if (data.message) {
      toastr.success(data.message)
    } else {
      toastr.success(data)
    }
  },
  error (data) {
    if (data && data.response && data.response.data.message) {
      if (data.response.data.status_code === 401) {
        if (localStorage.getItem('user_hebecore')) {
          localStorage.removeItem('user_hebecore')
          window.location.href = '/login'
        } else {
          toastr.error(data.response.data.message)
        }
      } else if (data.response.data.status_code === 422) {
        const errors = []
        Object.keys(data.response.data.errors).map(key => (
          data.response.data.errors[key].map(error => (
            errors.push(error)
          ))
        ))
        toastr.error(errors.join('<br>'))
      } else {
        toastr.error(data.response.data.message)
      }
    } else if (data && data.message) {
      toastr.error(data.message)
    } else {
      toastr.error(data)
    }
  }, info (data) {
    if (data.message) {
      toastr.info(data.message)
    } else {
      toastr.info(data)
    }
  },
}

export default Toastr
