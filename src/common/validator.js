import { isMobilePhone, isEmail, isNumeric } from 'validator'
import React from 'react'
import { FormattedMessage } from 'react-intl'

export const required = (value, props) => {
  if (!value || (props.isCheckable && !props.checked)) {
    return <small className="form-text text-danger"><FormattedMessage id="validator.required"/></small>
  }
}

export const number = (value) => {
  if (value && !isNumeric(value)) {
    return <small className="form-text text-danger"><FormattedMessage id="validator.number"/></small>
  }
}

export const phone = (value) => {
  if (value && !isMobilePhone(value)) {
    return <small className="form-text text-danger"><FormattedMessage id="validator.phone"/></small>
  }
}

export const email = (value) => {
  if (!isEmail(value)) {
    return <small className="form-text text-danger"><FormattedMessage id="validator.email"/></small>
  }
}

export const minLength = (value) => {
  if (value.trim().length < 6) {
    return <small className="form-text text-danger">Password must be at least 6 characters long</small>
  }
}
