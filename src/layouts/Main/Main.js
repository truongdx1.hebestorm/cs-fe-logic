/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useMediaQuery, Snackbar } from '@material-ui/core';
// import { menuService } from '../../_services'
import { Sidebar, Topbar } from './components';
import { IconCheck } from '@tabler/icons'
import { connect } from 'react-redux'
import NavBar from '../../Components/NavComponent';
import "../../index.css"

// const useStyles = makeStyles(theme => ({
//   root: {
//     height: '100%'
//   },
//   shiftContent: {
//     // padding:'0 100px'
//   },
//   content: {
//     // marginTop: 60,
//     // marginLeft: 220,
//     // height: '100%',
//     position: 'absolute',
//     top: 60,
//     width: '100vw',
//     height: 'calc(100vh - 60px)',
//     backgroundColor: '#f3f6f8'
//   },
//   snackSuccess: {
//     backgroundColor: '#449242',
//     minWidth: 'auto'
//   }
// }));

const Main = props => {
  const { children } = props;

  // const classes = useStyles();
  // const theme = useTheme();
  // const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
  //   defaultMatches: true
  // });

  const [openSidebar, setOpenSidebar] = useState(true);
  const [openDrawer, setOpenDrawer] = useState(true);


  // const handleSidebarOpen = () => {
  //   var element = document.getElementsByClassName('NavParent')
  //   if (openDrawer) {
  //     element[0].classList.remove('sidebar-max')
  //     element[0].classList.add('sidebar-minimized')
  //   } else {
  //     element[0].classList.remove('sidebar-minimized')
  //     element[0].classList.add('sidebar-max')
  //   }
  //   setOpenDrawer(!openDrawer);
  // };

  // const handleSidebarClose = () => {
  //   setOpenSidebar(false);
  // };

  // const shouldOpenSidebar = openSidebar;
  return (
    <div
      // className={classes.root}
    >
      
      {/* <TopBar/> */}
      {/* <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={props.openSnackbar && props.openSnackbar.data?.openSnackbar?.open}
        autoHideDuration={1000}
        onClose={() => {
          const { dispatch } = props;
          dispatch({ type: 'OPEN_SNACKBAR', openSnackbar: null })
        }}
        message={<div><IconCheck size={20} stroke={2} /> <span className='ml-2'>Copied</span></div>}
        ContentProps={{
          className: classes.snackSuccess
        }}
      />
       <Sidebar
        {...props}
        miniDrawer={openDrawer}
        onClose={handleSidebarClose}
        open
        variant={isDesktop ? 'persistent' : 'temporary'}
      /> 
      <main className={classes.content}>
        {children}
         <Footer /> 
      </main> */}
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node
};

function mapStateToProps(state) {
  return {
    openSnackbar: state.openSnackbar
  }
}

export default connect(mapStateToProps)(Main);;
