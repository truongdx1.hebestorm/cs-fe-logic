/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Drawer, Link } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import PaymentIcon from '@material-ui/icons/Payment';
import SettingsIcon from '@material-ui/icons/Settings';
import EventNoteIcon from '@material-ui/icons/EventNote';
import PersonIcon from '@material-ui/icons/Person';
import PetsIcon from '@material-ui/icons/Pets';
import CategoryIcon from '@material-ui/icons/Category';
import PermDataSettingIcon from '@material-ui/icons/PermDataSetting';
import { SidebarNav } from './components';
import { FormattedMessage } from 'react-intl';
import { orderService } from '../../../../_services';
import CollectionsIcon from '@material-ui/icons/Collections';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTag, faCog, faPlusCircle, faEye, faStore, faUserTie, faHome, faChartBar, faLink } from '@fortawesome/free-solid-svg-icons'
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 220,
    [theme.breakpoints.up('lg')]: {
      marginTop: 60,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    width: 220,
    backgroundColor: '#f4f6f8',
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid #ccc',
    padding: 5,
    position: 'fixed',
    height: 'calc(100%)',
    // color: '#fff',
    zIndex: 20,
    borderBottom: 0,
    borderTop: 0,
    borderLeft: 'none',
    borderRadius: 0,
    overflowX: 'hidden',
    overflowY: 'overlay',
    // boxShadow: '0 0 10px #666',
    fontSize: 14,
    transition: 'all 250ms linear',
    paddingBottom: 70
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }, ul: {
    listStyle: 'none'
  }, item: {
  }, a_href: {
    color: '#212b36',
    fontWeight: '600',
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    padding: '5px 10px',
    '& >span': {
      marginLeft: 5
    },
    '&:hover': {
      backgroundColor: '#eee',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    }, '&.active': {
      fontWeight: '600 !important',
      backgroundColor: 'rgba(92,106,196,.12)',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    }, '&.child': {
      fontWeight: '300',
    }, '&.active_parent': {
      fontWeight: '500 !important',
      // backgroundColor: 'rgba(92,106,196,.12)',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    },
    '&:link': {
      textDecoration: 'none'
    }
  }, menuText: {
    color: '#212b36'
  }, menuIcon: {
    margin: '0 5px',
    fill: '#919eab',
    color: '#919eab',
    width:20
  }, active: {

  }, icon_aws: {
    width: 20,
  }, sub_title: {
    color: '#637381',
    fontWeight: 600,
    textTransform: 'uppercase'
  }
}));
let user = null
const Sidebar = props => {
  const { open, miniDrawer, variant, onClose, className, ...rest } = props;
  const [badge, setBagde] = useState(0)

  useEffect(() => {
    user = JSON.parse(localStorage.getItem('user_hebecore'))
    // orderService.countOrder(1, { condition: 1, payment_status: 1 }).then(res => {
    //   if (res.status == 200) {
    //     setBagde(res.data.orders)
    //   }
    // })
  }, [])


  const classes = useStyles();

  const pages = [
    
    // {
    //   title: "Home",
    //   href: '/admin/home',
    //   icon: <FontAwesomeIcon icon={faHome}
    //     size={'1x'} style={{ fontSize: 16 }} />,
    //   connect_child: true,
    //   items: [
    //     {
    //       title: <FormattedMessage id="dashboards" />,
    //       href: '/admin/home',
    //     },
    //     {
    //       title: <FormattedMessage id="marketer_report" />,
    //       href: '/admin/reports/seller'
    //     }
    //   ]
    // },
    
  ];

  
  const checkChildActive = (item) => {
    var check = false
    if (item.items) {
      for (var i = 0; i < item.items.length; i++) {
        if (rest.path.includes(item.items[i].href)) {
          check = true
          break
        } else if (item.items[i]?.items?.length > 0) {
          for (var j = 0; j < item.items[i]?.items?.length; j++) {
            if (rest.path.includes(item.items[i].items[j].href)) {
              check = true
              break
            }
          }
        }
      }
    }


    return check
  }
  return (
    <div
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div
        {...rest}
        className={clsx(classes.root, className) + ' navFix'}
      >

        <div style={{
          display: 'flex',
          overflow: 'auto',
          flex: '1 1 auto',
          flexDirection: 'column',
          alignItems: 'stretch',
          maxWidth: '100%',
          overflowY: 'scroll',
        }}>
          <ul className={clsx(classes.ul, 'mb-3 mt-3')}>
            {pages.map((item, index) =>
              <li key={index}>
                <div className={classes.item}>
                  <a className={clsx(classes.a_href, 'pr-2 pl-2', (rest.path.includes(item.href) || checkChildActive(item)) ? (item.connect_child ? 'active_parent' : 'active') : '')} href={item.href}>
                    <div className={clsx(classes.menuIcon, 'menu_icon')}>
                      {item.icon}
                    </div>
                    <span>{item.title}</span>
                    <span className="flex-fill"></span>
                    {item.badge && <span className="badge badge-pill badge-secondary">{badge.toLocaleString()}</span>}
                  </a>
                </div>
                {checkChildActive(item) && item.items && item.items.map((child, i) =>
                  <div className={classes.item} key={i}>
                    <a className={clsx(classes.a_href, 'pr-2 pl-2', 'child', (rest.path.includes(child.href) || checkChildActive(child)) ? 'active' : '')} href={child.href} target={child.target || '_self'}>
                      <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                        {item.icon}
                      </div>
                      <span>{child.title}</span>
                    </a>
                    {checkChildActive(child) && child.items && child.items.map((child2, j) =>
                      <div className={classes.item} key={j}>
                        <a className={clsx(classes.a_href, 'pr-2 pl-2', 'child', rest.path.includes(child2.href) ? 'active' : '')} href={child2.href}>
                          <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                            {item.icon}
                          </div>
                          <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                            {item.icon}
                          </div>
                          <span>{child2.title}</span>
                        </a>
                      </div>
                    )}
                  </div>
                )}
              </li>
            )}
          </ul>
          {/* <div style={{ flex: '1 0 auto' }}>
            <ul className='list-unstyled'>
              <li className={'mb-1'}>
                <div className={clsx('d-flex align-items-center p-1 pr-2 pl-2')}>
                  <span className={clsx(classes.sub_title, 'flex-fill font-weight-bold')}>SALES CHANNELS</span>
                  <div className={clsx(classes.menuIcon, 'menu_icon')}>
                    <FontAwesomeIcon icon={faPlusCircle}
                      size={'1x'} style={{ fontSize: 16, fill: '#919eab' }} />
                  </div>
                </div>
              </li>
              {pages_channel.map((item, index) => {
                return (
                  <li key={index}>
                    <div className={clsx(classes.item, 'd-flex align-items-center pr-2')}>
                      <a className={clsx(classes.a_href, 'pr-2 pl-1 flex-fill', (rest.path.includes(item.href) || checkChildActive(item)) ? (item.connect_child ? 'active_parent' : 'active') : '')} href={item.href}>
                        <div className={clsx(classes.menuIcon, 'menu_icon')}>
                          {item.icon}
                        </div>
                        <span className={'flex-fill'}>{item.title}</span>
                      </a>
                      {item.sub_icon ? <div className={clsx(classes.menuIcon, 'menu_icon')}>
                        <a href={`http://${user ? (user.store.domain ? user.store.domain : user.store.sub_domain) : ''}?tt=1`} target='_blank'>
                          <FontAwesomeIcon icon={faEye}
                            size={'1x'} style={{ fontSize: 16, fill: '#919eab' }} />
                        </a>
                      </div> : null}
                    </div>
                    {checkChildActive(item) && item.items && item.items.map((child, i) => {
                      if (user?.store?.company_id && child?.href.includes('/admin/pages')) {
                        return null
                      }
                      return (
                        <div className={clsx(classes.item, 'd-flex')} key={i}>
                          <a className={clsx(classes.a_href, 'mr-2 pl-2 child flex-fill', rest.path.includes(child.href) ? 'active' : '')} href={child.href}>
                            <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                              {item.icon}
                            </div>
                            <span>{child.title}</span>
                          </a>
                          <div className={clsx(classes.menuIcon, 'menu_icon invisible')}>
                            <FontAwesomeIcon icon={faEye}
                              size={'1x'} style={{ fontSize: 16, fill: '#919eab' }} />
                          </div>
                        </div>
                      )
                    }
                    )}
                  </li>)

              }
              )}
            </ul>
          </div>
          <div className={''}>
            <div className={classes.item}>
              <a className={clsx(classes.a_href, rest.path.includes('/admin/settings') ? 'active' : '')} href={'/admin/settings'}>
                <div className={clsx(classes.menuIcon, 'menu_icon')}>
                  <FontAwesomeIcon icon={faCog}
                    size={'1x'} style={{ fontSize: 16 }} />
                </div>
                <span>{'Settings'}</span>
              </a>
            </div>
          </div> */}
        </div>
        {/* <UpgradePlan /> */}
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  miniDrawer: PropTypes.bool,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
};

export default Sidebar;
