/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
/* eslint-disable no-unused-vars */
import React, { forwardRef, useEffect, useState } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { List, ListItem, Button, Collapse } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {},
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
    width: '100%',
    margin: '5px 0'
  },
  button: {
    color: '#fff',
    padding: '10px 8px',
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    minWidth: 0,
    width: '100%',
    fontWeight: theme.typography.fontWeightMedium
  },
  icon: {
    color: '#fff',
    width: 24,
    height: 24,
    alignItems: 'center',
  },
  active: {
    backgroundColor: '#6fce7e',
    color: '#fff',
    fontWeight: theme.typography.fontWeightMedium,
    '& $icon': {
      color: '#fff',
    }
  },
  icArrow: {
    position: 'absolute',
    right: 0,
  }
}));

const CustomRouterLink = forwardRef((props, ref) => (
  <div ref={ref}>
    <RouterLink {...props} />
  </div>
));

const SidebarNav = props => {
  const { pages, className, openDrawer, ...rest } = props;
  const classes = useStyles();
  const [nameC, setNameC] = useState('');
  useEffect(() => {
    if (!openDrawer) {
      setNameC('')
    }
  });

  const handleClick = (name) => {
    if (name == nameC) {
      setNameC('');
    } else {
      setNameC(name);
    }

  };




  return (
    <List
      // {...rest}
      className={clsx(classes.root, className)}
    >
      
    </List>
  );
};

SidebarNav.propTypes = {
  className: PropTypes.string,
  openDrawer: PropTypes.bool,
  pages: PropTypes.array.isRequired,
};

export default SidebarNav;
