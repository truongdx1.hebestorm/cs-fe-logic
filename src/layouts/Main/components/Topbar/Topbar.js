/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import InputIcon from '@material-ui/icons/Input';
import logo from '../../../../assets/images/logos/logo_sg.svg'
import { history } from '../../../../_helpers'
import { connect } from 'react-redux'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { FormattedMessage } from 'react-intl';
import { FormModal } from '../../../../common';
import DashboardIcon from '@material-ui/icons/Dashboard';
import { cssConstants } from '../../../../_constants';
import { userActions } from '../../../../_actions';
import AppsIcon from '@material-ui/icons/Apps';
import {
  Button, MenuItem, ClickAwayListener, Grow, AppBar, ListItem, TextField, IconButton,
  Paper, Popper, MenuList, Typography, List, Menu, ListItemText, Tooltip
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import './style.scss';
import LoadingBar from 'react-redux-loading-bar'
import config from 'config'
import Cookies from 'universal-cookie'
import {
  IconAlertTriangle, IconMoodCry, IconUsers, IconTemplate
} from '@tabler/icons'

const cookies = new Cookies();

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  root: {
    boxShadow: 'none',
    zIndex: 1000
  },
  flexGrow: {
    flexGrow: 1
  },
  icon: {
    // marginRight: theme.spacing(1),
    color: '#fff'
  },
  iconPop: {
    marginRight: theme.spacing(1),
    color: '#2c6835'
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 13,
    // marginRight: theme.spacing(1),
    textTransform: 'initial',
    color: '#fff',
    // maxWidth: '100px',
    display: '-webkit-box',
    lineClamp: '1',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    boxOrient: 'vertical'
  },
  textMenuItem: {
    fontSize: 14
  },
  toolBar: {
    minHeight: 50,
    backgroundColor: '#ffffff',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
  },
  textLogo: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 15, textAlign: 'center'
  },
  dropdownMenu: {
    backgroundColor: '#2F6737',
    border: 'none',
    marginTop: 5
  }, 
  tabMenu: {
    // maxWidth: '160px',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    marginTop: 5
  },
  rightMenuBtn: {
    outline: 'none !important',
    maxWidth: 120,
    float: 'right',
    marginRight: 10,
    width: 'auto',
    borderRadius: 27,
    backgroundColor: '#06bf37',
    padding: '5px 8px',
    transition: 'color 0.2s linear, background-color 0.3s linear',
    '&:hover': {
      background: '#2b6b3b',
      transition: 'color 0.2s linear, background-color 0.3s linear'
    }
  },
  active_sub: {
    background: '#316639',
    borderRadius: 5
  }
}));

const allPages = [
  // {
  //   title: 'Dashboard',
  //   href: '/home',
  //   nameCol: 'dashboard',
  //   icon: <DashboardIcon />
  // },
  {
    title: 'Issues',
    href: '/issues',
    nameCol: 'issues',
    icon: <IconAlertTriangle />
  },
  {
    title: 'Paygate Disputes',
    href: '/paygate_disputes',
    nameCol: 'paygate_disputes',
    icon: <IconMoodCry />
  },
  {
    title: 'Customers',
    href: '/customers',
    nameCol: 'customers',
    icon: <IconUsers />
  },
  {
    title: 'Templates',
    href: '/templates',
    nameCol: 'templates',
    icon: <IconTemplate />
  },
];

const Topbar = props => {
  const { className, onSidebarOpen, ...rest } = props;

  const classes = useStyles();
  const [hover, setHover] = useState(-1);
  const [hoverChild, setHoverChild] = useState(-1);
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);
  const appsAnchorRef = useRef(null)
  const [pages, setPages] = useState([]);
  const [widthScreen, setWidthScreen] = useState(window.innerWidth);
  const groups = JSON.parse(localStorage.getItem('user_hebecore'))?.data?.groups
  const [openApps, setOpenApps] = useState(false)
  const handleZoom = () => {
    setWidthScreen(window.innerWidth)
  }
  window.addEventListener('resize', handleZoom);
  console.log(props.user , "1111");

  useEffect(() => {
      const user_page = []
      allPages.forEach(page => {
        // if (props.user != null && (props.user.data.admin || props.user.data.rule.includes('group_admin') || props.user.data.rule.some(rule => page.groups && page.groups.includes(rule)))) {
          user_page.push(page)
        // }
      })
      var widthScreen = window.innerWidth
      var unit = 160
      var maxUnit = parseInt(((widthScreen-100) * 70 / 100) / unit)
      if (user_page.length > maxUnit) {
        const page = user_page.slice(0, maxUnit)
        const itemMore = []
        user_page.slice(maxUnit).forEach(e => {
          const child = []
          if (e.items) {
            e.items.forEach(ee => child.push({
              title: ee.title,
              href: ee.href,
              groups: ee.groups
            }))
          }
          itemMore.push({
            title: e.title,
            href: e.href,
            child,
            groups: e.groups,
            nameCol: e.nameCol
          })
        })
        // page.push({
        //   title: <FormattedMessage id="more" />,
        //   href: '/more',
        //   icon: <ExpandMoreIcon />,
        //   groups: props.user.data.rule,
        //   nameCol: 'more',
        //   items: itemMore
        // })
        setPages(page)
      } else {
        setPages(user_page)
      }
    // }
  }, [props.user,widthScreen])

  const handleLogout = () => {
    const { dispatch } = props;
    dispatch(userActions.logout(1))
  }

  const handleHover = (event) => {
    setHover(event)
  }

  const onClickMenu = (page, index, nameCol) => {
    if (index == 0) {
      return
    } else {
      handleHover(-1)
    }
    localStorage.setItem('nameCol', nameCol);
    history.push(page.href)
  }

  const menuActive = (nameCol) => {
    let nameColStorage = localStorage.getItem('nameCol')
    return nameColStorage == nameCol
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const handleCloseApps = (event) => {
    if (appsAnchorRef.current && appsAnchorRef.current.contains(event.target)) {
      return;
    }
    setOpenApps(false);
  };

  const handleAccount = () => {
    history.push('/account', { clear: true })
    setOpen(false);
  };

  const handleRedirect = (group) => {
    if(group.code == 'seller'){
      window.open(config.sellerUrl, "_blank")
    }

    if(group.code == 'supplier'){
      window.open(config.supplierUrl, "_blank")
    }
  }

  return (
    <AppBar
      className={clsx(classes.root, className)}
    >
      <div
        style={{
          width: '100vw',
          background: '#129535',
          // border: '2px solid #fff',
          borderLeft: 0,
          borderRight: 0,
          borderTop: 0,
          padding: 2,
          height: '100%',
          display: 'inline-flex'
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '6vw' }}>
          <RouterLink to="/">
            <img
              alt="Logo"
              height={45}
              src={logo}
            />
          </RouterLink>
        </div>
        <div className="menu-bar" style={{ width: '78vw' }}>
          <ul>
            {pages.map((page, index) => {
              var items = null
              if (page.items != null) {
                items = page.items
                // items = page.items.filter(item => (rules.includes('group_admin') || rules.some(v => item.groups && item.groups.includes(v))))
              }
              return (
                <li key={index} className={menuActive(page.nameCol) ? classes.active_sub : ''}>
                  <a onClick={() => onClickMenu(page, page.items == null ? 1 : 0, page.nameCol)} className={classes.tabMenu} style={{textDecoration: 'none'}}>
                    {page.icon}
                    <span>{page.title}</span>
                  </a>
                  {items != null && items.length > 0 &&
                    <div className={items.length > 10 && page.nameCol != 'more' ? "sub-menu-1 frame" : "sub-menu-1"} style={{ height: items.length > 10 && page.nameCol != 'more' ? 500 : 'auto' }}>
                      <ul>
                        {items.map((item, k) =>
                          <li
                            onMouseEnter={() => setHover(k)}
                            onMouseLeave={() => setHover(-1)}
                            key={k}
                            className={item.child && item.child.length > 0 ? "hover-me" : ""}
                          >
                            <a onClick={() => onClickMenu(item, item.child && item.child.length > 0 ? 0 : 1, page.nameCol)} style={{ textDecoration: 'none', background: hoverChild == k ? '#316639' : (hover == k ? '#129535' : 'transparent') }}>
                              <span>{item.title}</span>
                            </a>
                            <div className={item.child && item.child.length > 10 ? "sub-menu-2 frame" : "sub-menu-2"} style={{ height: item.child && item.child.length > 10 ? 500 : 'auto', bottom: items.indexOf(item) > 10 ? 0 : 'none', width: item.nameCol == 'report' ? 400 : 'inherit' }}>
                              <ul
                                onMouseEnter={() => setHoverChild(k)}
                                onMouseLeave={() => setHoverChild(-1)}
                              >
                                {item.child && item.child.map((ch, ii) => (
                                  <li key={ii+'ii'}>
                                    <a href={ch.href} style={{ textDecoration: 'none' }}>
                                      {ch.title}
                                    </a>
                                  </li>
                                ))}
                              </ul>
                            </div>
                          </li>
                        )}
                      </ul>
                    </div>
                  }
                </li>
              )
            })}
          </ul>
        </div>
        <div style={{ display: 'flex', alignItems: 'center', width: '16vw' }}>
          <div style={{ width: '8vw' }}>
            <Button
              aria-controls={openApps ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => setOpenApps(true)}
              ref={appsAnchorRef}
              fullWidth
            >
              <AppsIcon className={classes.icon} />
              <Tooltip title={'Apps'}>
                <Typography
                  className={classes.textHeader}
                  component="span"
                  style={{ textTransform: 'uppercase' }}
                >
                  Apps
                </Typography>
              </Tooltip>
            </Button>
            <Popper
              anchorEl={appsAnchorRef.current}
              disablePortal
              onMouseEnter={() => setOpenApps(true)}
              // onMouseLeave={() => setOpen(false)}
              open={openApps}
              role={undefined}
              transition
            >
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleCloseApps}>
                      <MenuList
                        autoFocusItem={openApps}
                        id="menu-list-grow"
                      // onMouseLeave={() => setOpen(false)}
                      >
                        {
                          groups && groups.map(group => {
                            return (
                              <MenuItem onClick={() => handleRedirect(group)}>
                                <Typography
                                  className={classes.textMenuItem}
                                  component="span"
                                >
                                  {group.name}
                                </Typography>
                              </MenuItem>
                            )
                          })
                        }
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
          <div style={{ width: '8vw' }}>
            <Button
              className={classes.rightMenuBtn}
              aria-controls={open ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => setOpen(true)}
              ref={anchorRef}
              fullWidth
            >
              <AccountCircleIcon className={classes.icon} />
              <Tooltip title={props.user != null ? (props.user.data.name || props.user.data.user_name) : ''}>
                <Typography
                  className={classes.textHeader}
                  component="span"
                  style={{ textTransform: 'uppercase' }}
                >
                  {props.user != null ? (props.user.data.name || props.user.data.user_name) : ''}
                </Typography>
              </Tooltip>
            </Button>
            <Popper
              anchorEl={anchorRef.current}
              disablePortal
              onMouseEnter={() => setOpen(true)}
              open={open}
              role={undefined}
              transition
            >
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleClose}>
                      <MenuList
                        autoFocusItem={open}
                        id="menu-list-grow"
                      // onMouseLeave={() => setOpen(false)}
                      >
                        <MenuItem onClick={handleAccount}>
                          <AccountCircleIcon className={classes.iconPop} />
                          <Typography
                            className={classes.textMenuItem}
                            component="span"
                          >
                            <FormattedMessage id="account" />
                          </Typography>
                        </MenuItem>
                        <MenuItem onClick={handleLogout} >
                          <InputIcon className={classes.iconPop} />
                          <Typography
                            className={classes.textMenuItem}
                            component="span"
                          >
                            <FormattedMessage id="logout" />
                          </Typography>
                        </MenuItem>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        </div>
      </div>
      <LoadingBar showFastActions style={{ backgroundColor: '#33CC33' }} />
      <FormModal />
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;

  return {
    user
  }
}



export default connect(mapStateToProps)(Topbar);
