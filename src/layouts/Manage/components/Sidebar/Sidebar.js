/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Drawer, Link, Box } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import PaymentIcon from '@material-ui/icons/Payment';
import SettingsIcon from '@material-ui/icons/Settings';
import EventNoteIcon from '@material-ui/icons/EventNote';
import PersonIcon from '@material-ui/icons/Person';
import PetsIcon from '@material-ui/icons/Pets';
import CategoryIcon from '@material-ui/icons/Category';
import PermDataSettingIcon from '@material-ui/icons/PermDataSetting';
import { SidebarNav } from './components';
import { FormattedMessage } from 'react-intl';
// import { menuService } from '../../../../_services';
import CollectionsIcon from '@material-ui/icons/Collections';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faTag, faPager, faCreditCard, faCog,
  faStore, faUserTie, faHome, faUserCircle, faRegistered, faInfoCircle, faLock
} from '@fortawesome/free-solid-svg-icons'

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 220,
    [theme.breakpoints.up('lg')]: {
      marginTop: 60,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    width: 220,
    backgroundColor: '#f4f6f8',
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid #ccc',
    padding: 5,
    position: 'fixed',
    height: 'calc(100%)',
    // color: '#fff',
    zIndex: 20,
    borderBottom: 0,
    borderTop: 0,
    borderLeft: 'none',
    borderRadius: 0,
    overflowX: 'hidden',
    overflowY: 'overlay',
    // boxShadow: '0 0 10px #666',
    fontSize: 14,
    transition: 'all 250ms linear',
    paddingBottom: 70
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }, ul: {
    listStyle: 'none'
  }, item: {
  }, a_href: {
    color: '#212b36',
    fontWeight: '600',
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    padding: '5px 10px',
    '& >span': {
      marginLeft: 5
    },
    '&:hover': {
      backgroundColor: '#eee',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    }, '&.active': {
      fontWeight: '600 !important',
      backgroundColor: 'rgba(92,106,196,.12)',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    }, '&.child': {
      fontWeight: '300',
    }, '&.active_parent': {
      fontWeight: '500 !important',
      // backgroundColor: 'rgba(92,106,196,.12)',
      color: '#202e78',
      '& .menu_icon': {
        color: '#5c6ac4 !important',
        fill: '#5c6ac4 !important',
      }
    },
    '&:link': {
      textDecoration: 'none'
    }
  }, menuText: {
    color: '#212b36'
  }, menuIcon: {
    margin: '0 5px',
    fill: '#919eab',
    color: '#919eab'
  }, active: {

  }, icon_aws: {
    width: 20,
  }, sub_title: {
    color: '#637381',
    fontWeight: 600,
    textTransform: 'uppercase'
  }
}));
let user = JSON.parse(localStorage.getItem('user_hebecore'))
const Sidebar = props => {
  const { open, miniDrawer, variant, onClose, className, ...rest } = props;
  // const [dataMenu, setDataMenu] = useState([])
  useEffect(() => {
    user = JSON.parse(localStorage.getItem('user_hebecore'))
  }, [])


  const classes = useStyles();

  const pages = [
    {
      title: "Security",
      href: `/admin/company/security`,
      icon: <FontAwesomeIcon icon={faLock}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
    {
      title: "Stores",
      href: `/admin/company/stores`,
      icon: <FontAwesomeIcon icon={faStore}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
    {
      title: "Seller",
      href: `/admin/company/seller`,
      icon: <FontAwesomeIcon icon={faUserCircle}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
    {
      title: 'Payment',
      href: `/admin/company/payment`,
      icon: <FontAwesomeIcon icon={faCreditCard}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
    {
      title: 'Pages',
      href: `/admin/company/pages`,
      icon: <FontAwesomeIcon icon={faPager}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },{
      title: "Request",
      href: `/admin/company/register`,
      icon: <FontAwesomeIcon icon={faRegistered}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
    {
      title: "Shipping and return policies",
      href: `/admin/company/info`,
      icon: <FontAwesomeIcon icon={faInfoCircle}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    }
  ];

  const seller_pages = [
    {
      title: "Stores",
      href: `/admin/company/stores`,
      icon: <FontAwesomeIcon icon={faStore}
        size={'1x'} style={{ fontSize: 16 }} />,
      connect_child: false,
    },
  ];


  const checkChildActive = (item) => {
    var check = false
    // console.log(rest.path, item.href)

    if (item.items) {
      for (var i = 0; i < item.items.length; i++) {
        if (rest.path.includes(item.items[i].href)) {
          check = true
          break
        }
      }
    }

    return check
  }
  return (
    <div
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div
        {...rest}
        className={clsx(classes.root, className) + ' navFix'}
      >
        {/* <Profile /> */}
        {/* <Divider className={classes.divider} /> */}
        {/* <SidebarNav
          className={classes.nav}
          openDrawer={miniDrawer}
          pages={pages}
        /> */}
        <div style={{
          display: 'flex',
          overflow: 'auto',
          flex: '1 1 auto',
          flexDirection: 'column',
          alignItems: 'stretch',
          maxWidth: '100%',
          overflowY: 'scroll',
        }}>
          <div className='m-3'>
            <div>
              <Box fontSize={18} fontWeight='bold'>Company</Box>
            </div>
            <div>
            </div>
          </div>
          <ul className={clsx(classes.ul, 'mb-3 mt-3')}>
            {(user && user.role) ? pages.map((item, index) => {
              if (user.role != 1 && item.href == '/admin/company/register') {
              } else {
                return (
                  <li key={index}>
                    <div className={classes.item}>
                      <a className={clsx(classes.a_href, 'pr-2 pl-2', (rest.path.includes(item.href) || checkChildActive(item)) ? (item.connect_child ? 'active_parent' : 'active') : '')} href={item.href}>
                        <div className={clsx(classes.menuIcon, 'menu_icon')}>
                          {item.icon}
                        </div>
                        <span>{item.title}</span>
                      </a>
                    </div>
                    {checkChildActive(item) && item.items && item.items.map((child, i) =>
                      <div className={classes.item} key={i}>
                        <a className={clsx(classes.a_href, 'pr-2 pl-2', 'child', rest.path.includes(child.href) ? 'active' : '')} href={child.href}>
                          <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                            {item.icon}
                          </div>
                          <span>{child.title}</span>
                        </a>
                      </div>
                    )}
                  </li>)
              }

            }
            ) :
              seller_pages.map((item, index) =>
                <li key={index}>
                  <div className={classes.item}>
                    <a className={clsx(classes.a_href, 'pr-2 pl-2', (rest.path.includes(item.href) || checkChildActive(item)) ? (item.connect_child ? 'active_parent' : 'active') : '')} href={item.href}>
                      <div className={clsx(classes.menuIcon, 'menu_icon')}>
                        {item.icon}
                      </div>
                      <span>{item.title}</span>
                    </a>
                  </div>
                  {checkChildActive(item) && item.items && item.items.map((child, i) =>
                    <div className={classes.item} key={i}>
                      <a className={clsx(classes.a_href, 'pr-2 pl-2', 'child', rest.path.includes(child.href) ? 'active' : '')} href={child.href}>
                        <div className={clsx(classes.menuIcon, 'menu_icon')} style={{ visibility: 'hidden' }}>
                          {item.icon}
                        </div>
                        <span>{child.title}</span>
                      </a>
                    </div>
                  )}
                </li>
              )}
          </ul>

          <div style={{ flex: '1 0 auto' }}>
          </div>

          <div className={''}>
            <div className={classes.item}>
              <a className={clsx(classes.a_href, rest.path.includes('/admin/settings') ? 'active' : '')} href={'/admin/settings'}>
                <div className={clsx(classes.menuIcon, 'menu_icon')}>
                  <FontAwesomeIcon icon={faCog}
                    size={'1x'} style={{ fontSize: 16 }} />
                </div>
                <span>{'Settings'}</span>
              </a>
            </div>
          </div>
        </div>
        {/* <UpgradePlan /> */}
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  miniDrawer: PropTypes.bool,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
};

export default Sidebar;
