import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar } from '@material-ui/core';
import logo from '../../../../assets/images/logos/logo_2.png'

const useStyles = makeStyles(() => ({
  root: {
    boxShadow: 'none'
  }
}));

const Topbar = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      color="primary"
      position="fixed"
    >
      <Toolbar
        style={{
          minHeight: 80,
          backgroundColor: '#2c6835',
          border: '2px solid #fff',
          borderLeft: 0,
          borderRight: 0,
          borderTop: 0,
        }}
        variant="regular"
      >
        <RouterLink to="/admin">
          <img
            alt="Logo"
            height={50}
            src={logo}
          />
        </RouterLink>
        <a style={{ marginLeft: 10, fontWeight: 'bold', color: 'white' }}>Pet Health</a>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;
