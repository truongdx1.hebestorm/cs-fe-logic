/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useMediaQuery } from '@material-ui/core';
// import { menuService } from '../../_services'
import { Sidebar, Topbar } from './components';
const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    // [theme.breakpoints.up('sm')]: {
    //   paddingTop: 
    // },
    background: '#f3f6f8'
  },
  shiftContent: {
    // padding:'0 100px'
  },
  content: {
    marginTop: 60,
    // marginLeft: 220,
    height: '100%',
  }
}));

const Upgrade = props => {
  const { children } = props;

  const classes = useStyles();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
    defaultMatches: true
  });

  const [openSidebar, setOpenSidebar] = useState(true);
  const [openDrawer, setOpenDrawer] = useState(true);


  const handleSidebarOpen = () => {
    var element = document.getElementsByClassName('NavParent')
    if (openDrawer) {
      element[0].classList.remove('sidebar-max')
      element[0].classList.add('sidebar-minimized')
    } else {
      element[0].classList.remove('sidebar-minimized')
      element[0].classList.add('sidebar-max')
    }
    setOpenDrawer(!openDrawer);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  const shouldOpenSidebar = openSidebar;

  return (
    <div
      className={clsx({
        [classes.root]: true,
        [classes.shiftContent]: isDesktop
      }) + ' NavParent sidebar-max'}
    >
      <Topbar 
        {...props}
        onSidebarOpen={handleSidebarOpen} 
      />
      
      <main className={classes.content}>
        {children}
        {/* <Footer /> */}
      </main>
    </div>
  );
};

Upgrade.propTypes = {
  children: PropTypes.node
};


export default Upgrade;
