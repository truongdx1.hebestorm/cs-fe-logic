/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import InputIcon from '@material-ui/icons/Input';
import logo from '../../../../assets/images/logos/logo_2.png'
import { history } from '../../../../_helpers'
import { connect } from 'react-redux'
import ReceiptIcon from '@material-ui/icons/Receipt';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';
import PersonIcon from '@material-ui/icons/Person';
import PetsIcon from '@material-ui/icons/Pets';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import BusinessIcon from '@material-ui/icons/Business';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EventNoteIcon from '@material-ui/icons/EventNote';
import { FormattedMessage } from 'react-intl';
import { userService, employeeService } from '../../../../_services';
import toastr from '../../../../common/toastr';
import { userConstants } from '../../../../_constants';
import {
  Button, MenuItem, ClickAwayListener, Grow, AppBar, ListItem,
  Paper, Popper, MenuList, Typography, List, Menu, ListItemText, Tooltip, TextField
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ApartmentIcon from '@material-ui/icons/Apartment';
import HouseIcon from '@material-ui/icons/House';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import './style.scss';
import LoadingBar from 'react-redux-loading-bar'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { userActions } from '../../../../_actions'

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
  },
  flexGrow: {
    flexGrow: 1
  },
  icon: {
    // marginRight: theme.spacing(1),
    color: '#fff'
  },
  iconPop: {
    marginRight: theme.spacing(1),
    color: '#6371c7'
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 13,
    // marginRight: theme.spacing(1),
    textTransform: 'initial',
    color: '#fff',
    // maxWidth: '100px',
    display: '-webkit-box',
    lineClamp: '1',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    boxOrient: 'vertical'
  },
  textMenuItem: {
    fontSize: 14
  },
  toolBar: {
    minHeight: 60,
    backgroundColor: '#ffffff',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
  },
  textLogo: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 15, textAlign: 'center',
    textDecoration: 'none !important',
    display: 'flex',
    alignItems: 'center',
    color: '#fff !important'
  },
  dropdownMenu: {
    backgroundColor: '#2F6737',
    border: 'none',
    marginTop: 5
  }, tabMenu: {
    maxWidth: '160px',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  }, rightMenuBtn: {
    outline: 'none !important'
  }, inputSearch: {
    float: 'left',
    backgroundColor: '#fff'
  }
}));

const allPages = [];



const Topbar = props => {
  const { className, onSidebarOpen, dispatch, ...rest } = props;

  const classes = useStyles();
  const [hover, setHover] = useState(-1);
  const [hoverChild, setHoverChild] = useState(-1);
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [pages, setPages] = useState([]);
  const [widthScreen, setWidthScreen] = useState(window.innerWidth);




  const handleZoom = () => {
    setWidthScreen(window.innerWidth)
  }
  window.addEventListener('resize', handleZoom);


  useEffect(() => {
    // if (props.user != null) {
    //   userService.getDataUser({ public_id: props.user.data.public_id }).then(res => {
    //     if (res.data.success) {
    //       setCurrentUser(res.data.data)
    //     }
    //   })
    //   const user_page = []
    //   allPages.forEach(page => {
    //     if (props.user != null && (props.user.data.admin || props.user.data.rule.includes('group_admin') || props.user.data.rule.some(rule => page.groups && page.groups.includes(rule)))) {
    //       user_page.push(page)
    //     }
    //   })
    //   var widthScreen = window.innerWidth
    //   var unit = 160
    //   var maxUnit = parseInt(((widthScreen - 100) * 70 / 100) / unit)
    //   if (user_page.length > maxUnit) {
    //     const page = user_page.slice(0, maxUnit)
    //     const itemMore = []
    //     user_page.slice(maxUnit).forEach(e => {
    //       const child = []
    //       if (e.items) {
    //         e.items.forEach(ee => child.push({
    //           title: ee.title,
    //           href: ee.href,
    //           groups: ee.groups
    //         }))
    //       }
    //       itemMore.push({
    //         title: e.title,
    //         href: e.href,
    //         child
    //       })
    //     })
    //     page.push({
    //       title: <FormattedMessage id="more" />,
    //       href: '/more',
    //       icon: <ExpandMoreIcon />,
    //       groups: props.user.data.rule,
    //       nameCol: 'more',
    //       items: itemMore
    //     })
    //     setPages(page)
    //   } else {
    //     setPages(user_page)
    //   }
    // }
  }, [props.user, widthScreen])




  const handleClickDepartment = (event) => {
    setAnchorEl(event.currentTarget);
    if (props.user != null) {
      userService.getDataUser({ public_id: props.user.data.public_id }).then(res => {
        if (res.data.success) {
          setCurrentUser(res.data.data)
        }
      })
    }
  };


  const handleCloseDept = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    // dispatch(userActions.logout(1))
    localStorage.removeItem('user_hebecore')
    history.push('/')
  }

  const handleHover = (event) => {
    setHover(event)
  }

  const onClickMenu = (page, index) => {
    if (index == 0) {
      return
    } else {
      handleHover(-1)
    }
    history.push(page.href)
  }


  const handleToggle = (value) => {
    setOpen(value);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleAccount = () => {
    history.push('/admin/accounts/security')
    setOpen(false);
  };

  return (
    <AppBar
      className={clsx(classes.root, className)}
    >
      <div
        style={{
          width: '100vw',
          background: '#43467F',
          borderLeft: 0,
          borderRight: 0,
          borderTop: 0,
          padding: 5,
          height: '100%',
          display: 'inline-flex'
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '10vw' }}>
          <a
            className={classes.textLogo}
            href='/admin/home'
          >
            <img
              alt="Logo"
              height={50}
              src={logo}
            />
          </a>
        </div>
        <div className="menu-bar" style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
          <div style={{ width: '60%' }} className='invisible'>
            <TextField
              variant="outlined"
              className={'invisible'}
              fullWidth
              InputProps={{
                className: classes.inputSearch,
                classes: {
                  placeholder: classes.placeholder,
                }
              }}
              margin="dense"
              placeholder="Search"
            />
          </div>
        </div>
        {/* <div className={classes.flexGrow} style={{ backgroundColor: 'purple' }} /> */}
        <div style={{ display: 'flex', alignItems: 'center', width: '10vw' }}>

          <div style={{ width: '8vw' }}>
            <Button
              className={classes.rightMenuBtn}
              aria-controls={open ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => setOpen(true)}
              ref={anchorRef}
              // onMouseEnter={}
              // onMouseLeave={() => setOpen(false)}
              fullWidth
            // disabled={true}
            >
              <AccountCircleIcon className={classes.icon} />
              <Tooltip title={''}>
                <Typography
                  className={classes.textHeader}
                  component="span"
                  style={{ textTransform: 'uppercase' }}
                >
                  {props.user != null ? (props.user.email) : ''}
                </Typography>
              </Tooltip>
            </Button>
            <Popper
              anchorEl={anchorRef.current}
              disablePortal
              onMouseEnter={() => setOpen(true)}
              // onMouseLeave={() => setOpen(false)}
              open={open}
              role={undefined}
              transition
            >
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleClose}>
                      <MenuList
                        autoFocusItem={open}
                        id="menu-list-grow"
                      // onMouseLeave={() => setOpen(false)}
                      >
                        <MenuItem onClick={handleAccount}>
                          <AccountCircleIcon className={classes.iconPop} />
                          <Typography
                            className={classes.textMenuItem}
                            component="span"
                          >
                            <FormattedMessage id="account" />
                          </Typography>
                        </MenuItem>
                        <MenuItem onClick={handleLogout} >
                          <InputIcon className={classes.iconPop} />
                          <Typography
                            className={classes.textMenuItem}
                            component="span"
                          >
                            <FormattedMessage id="logout" />
                          </Typography>
                        </MenuItem>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        </div>
      </div>
      <LoadingBar showFastActions style={{ backgroundColor: '#33CC33' }} />
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;

  return {
    user
  }
}



export default connect(mapStateToProps)(Topbar);
