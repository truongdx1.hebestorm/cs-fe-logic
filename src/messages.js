export default {
  en: {
    'view': 'View',
    'preview': 'Preview',
    'clone': 'Duplicate',
    'export': 'Export',
    'import': 'Import',
    'cancel': 'Cancel',
    'edit': 'Edit',
    'account': 'Account',
    'logout': 'Logout',
    'more_action': 'More Actions',
    'duplicate': 'Duplicate',
    // Product
    'product': 'Product',
    'add.product': 'Add product',
    'save': 'Save',
    'product.all': 'All product',
    // Collection
    'collection': 'Collection',
    'group': 'Mix product',
    'add.group': 'Add mix product',
    'add.collection': 'Add collection',
    'add.review': 'Add review',
    'add.review.ali': 'Add Link Alixpress',
    'add.another.review': 'Add another review',

    // Discounts
    'discounts': 'Discounts',
    'discount.add': 'Create discounts',
    'discount.volumes': 'Volumes discount',
    'reviews': 'Reviews',
    //Store
    'store': 'Online Store',
    'store.theme': 'Themes',
    'store.page': 'Pages',
    'store.domain': 'Domains',
    'store.info': 'Shipping and return policies',
    'store.font': 'Fonts',
    'add.font': 'Add font',
    //Orders
    'order': 'Orders',
    'create.order': 'Create order',
    'checkouts': 'Abandoned checkouts',
    'refund': 'Refund',
    'restock': 'Restock',
    'draft_orders': 'Drafts',

    //Apps
    'apps': 'Apps',
    'apps.manual.bundles': 'Upsell Page',
    //Pages
    'page.add': 'Add page',
    //Customers
    'customer': 'Customers',
    'add.customer': 'Add Customer',

    //Domains
    'domain.add': 'Add Domain',

    //Analytics
    'analytics': 'Analytics',
    'dashboards': 'Dashboards',
    //Upsell Offer
    'upsell.add': 'Add Offer',
    'create_taxzone': 'Create tax zone',
    'manage_tax': 'Manage Tax',
    'sales_reports':"Sales Reports",
    "seller_report":"Seller Reports",
    "generate_link": "Generate Link Marketing",
    "marketer_report": "Marketer Report",
    "dod": "Design on demand",

    "facebook":"Facebook",
    "change_password": "Change password",
    "new_issues": "New Issues",
    "new_ticket": "New Ticket",
    "today": "Today",
    "yesterday": "Yesterday",
    "last_7_days": "Last 7 days",
    "last_30_days": "Last 30 days",
    "last_90_days": "Last 90 days",
    "new_case": "New case",
  },
  vi: {
    'add.product': 'Add product',
    'save.product': 'Save',
    'create_taxzone': 'Create tax zone'
  }
}
