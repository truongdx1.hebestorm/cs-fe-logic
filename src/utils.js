export const makeId = (length) => {
  var result = "";
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

function padZero(str, len) {
  len = len || 2;
  var zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
}

export const stringToColor = (str, invert=false) => {
  // var hash = 0;
  // for (var i = 0; i < str.length; i++) {
  //   hash = str.charCodeAt(i) + ((hash << 5) - hash);
  // }
  // var colour = '#';
  // for (var i = 0; i < 3; i++) {
  //   var value = (hash >> (i * 8)) & 0xFF;
  //   colour += ('00' + value.toString(16)).substr(-2);
  // }

  var hash = 0;
  for(var i=0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 3) - hash);
  }
  var color = Math.abs(hash).toString(16).substring(0, 6);

  var colour = "#" + '000000'.substring(0, 6 - color.length) + color;

  var color_invert = "";
  if (invert) {
    var color_text = ""
    if (colour.indexOf('#') === 0) {
      color_text = colour.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (color_text.length === 3) {
      color_text = color_text[0] + color_text[0] + color_text[1] + color_text[1] + color_text[2] + color_text[2];
    }
    var r = parseInt(color_text.slice(0, 2), 16),
        g = parseInt(color_text.slice(2, 4), 16),
        b = parseInt(color_text.slice(4, 6), 16);
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    color_invert = '#' + padZero(r) + padZero(g) + padZero(b);
  }

  return invert ? [colour, color_invert] : colour;
}

export const capitalizeStr = (str, text_split=" ") => {
  var splitStr = str.toLowerCase().split(text_split);
  for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
  }
  return splitStr.join(' '); 
}

export const customStyles = {
  control: (provided, state) => ({
    ...provided,
    border: '1px solid #C4C4C4',
    marginTop: 3,
    backgroundColor: 'transparent'
  }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: "none"
  }),
  placeholder: (provided, state) => ({
    ...provided,
    color: 'rgba(0, 0, 0, 0.38)',
    fontSize: '14px'
  }),
  menuList: (provided, state) => ({
    ...provided,
    maxHeight: 200,
    zIndex: 9999
  }),
  menu: (provided, state) => ({
    ...provided,
    zIndex: 9999
  }),
  menuPortal: (provided, state) => ({
    ...provided,
    zIndex: 9999
  })
}

export const disabledStyles = {
  control: (provided, state) => ({
      ...provided,
      border: '1px solid #C4C4C4',
      marginTop: 3,
      backgroundColor: '#EEEEEE'
  }),
  indicatorSeparator: (provided, state) => ({
      ...provided,
      display: "none"
  }),
  placeholder: (provided, state) => ({
      ...provided,
      color: 'rgba(0, 0, 0, 0.38)',
      fontSize: '14px'
  }),
  menuList: (provided, state) => ({
      ...provided,
      maxHeight: 150
  }),
  menu: (provided, state) => ({
      // none of react-select's styles are passed to <Control />
      // width: 200,
      ...provided,
      zIndex: 10000
      // bottom: "100%",
      // top: "auto",
  })
}

export const errorStyles = {
  control: (provided, state) => ({
      ...provided,
      border: '1px solid red',
      marginTop: 3,
      backgroundColor: 'transparent'
  }),
  indicatorSeparator: (provided, state) => ({
      ...provided,
      display: "none"
  }),
  placeholder: (provided, state) => ({
      ...provided,
      color: 'rgba(0, 0, 0, 0.38)',
      fontSize: '14px'
  }),
  menuList: (provided, state) => ({
      ...provided,
      maxHeight: 150
  }),
  menu: (provided, state) => ({
      // none of react-select's styles are passed to <Control />
      // width: 200,
      ...provided,
      zIndex: 10000
      // bottom: "100%",
      // top: "auto",
  })
}