/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import ReactDOM from 'react-dom'
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box, Grid, Chip, Divider,
  Checkbox, List, ListItem, ListItemText,
  InputAdornment,
  TextField, Stepper, Step,
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination
} from "@material-ui/core";
import config from "config";
import { CopyToClipboard, MultiInputPicker, FormModal, FormModalSide, FormModalSideStatic, CustomStep, CustomTimeline } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { makeId } from "../../../utils";
import moment from "moment";
import clsx from 'clsx'
import Dropzone from 'react-dropzone'
import {
  IconPhoto, IconInfoCircle, IconMessages, IconEdit, IconTemplate, IconCornerUpRight,
  IconArrowLeft, IconPencil, IconArrowUp, IconChevronLeft, IconChevronRight
} from "@tabler/icons";
import { Button, Avatar, Input, Toggle, Placeholder } from 'rsuite'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  cardNote: {
    backgroundColor: '#E7F8FB',
    border: '1px solid #B7DBE0'
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    textTransform: 'capitalize',
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    },
    '&.authorized': {
      backgroundColor: '#F4D7AA'
    },
    '&.voided': {
      backgroundColor: '#E4E5E7'
    },
    '&.partially_refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&.refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&> div': {
      width: 10,
      height: 10,
      borderRadius: '50%',
    }
  },
  chip_paid: {
    backgroundColor: '#006F53',
  },
  chip_cancelled: {
    backgroundColor: '#626669',
  },
  chip_pending: {
    border: '2px solid #885E16',
  },
  chip_unfulfilled: {
    border: '2px solid #885E16',
  },
  chip_fulfilled: {
    border: '2px solid #885E16',
  },
  chip_authorized: {
    backgroundColor: '#885E16',
  },
  chip_voided: {
    backgroundColor: '#885E16',
  },
  chip_partially_refunded: {
    border: '2px solid #885E16',
  },
  chip_refunded: {
    border: '2px solid #885E16',
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  textDisabled: {
    backgroundColor: '#EFF1F2'
  },
  notchedOutline: {
    border: '2px solid #449242'
  }
});

class CustomerForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        }
      },
      content: "<p>Báo SUP resend + thuyết phục KH close kiện sau khi nhận TKN replacement</p><p>Báo Kh bên ngoài yêu cầu close kiện</p><p>18/1: TKN chưa active</p>",
      caseResponse: null,
      disabledResponse: true,
      editNote: false,
      currentLength: 0,
      currentTab: 'customer',
      reply: false,
      isLoading: false,
      isLoadingTimeline: false,
      timelines: []
    };
    this.quillRef = React.createRef()
    this.inputRef = React.createRef()
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Customer Form");
    // this.getListItems(this.state.formState.values);
    this.getData(this.props, this.props.match.params.id)
  }

  componentWillUpdate(nextProps) {
    if (nextProps.match.params.id != this.props.match.params.id) {
      this.getData(nextProps, nextProps.match.params.id)
    }
  }

  getOrders = (email) => {
    this.setState({ isLoading: true }, () => {
      request.get(`${config.apiCoreUrl}/api/orders`, {email}).then(res => {
        if (res.data.success) {
          const formState = Object.assign({}, this.state.formState)
          formState['values']['orders'] = res.data.data.result
          formState['values']['total_orders'] = res.data.data.total
          this.setState({ formState, isLoading: false })
        } else {
          this.setState({ isLoading: false })
        }
      }, err => {
        toastr.error(err)
        this.setState({ isLoading: false })
      })
    })
  }

  getTimeline = (order_id) => {
    this.setState({ isLoadingTimeline: true })
    request.get(`${config.apiUrl}/api/logs`, {object_id: order_id, object_type: 'users'}).then(res => {
      if (res.data.success) {
        this.setState({ isLoadingTimeline: false, timelines: res.data.data.result })
      } else {
        toastr.error(res.data.msg)
        this.setState({ isLoadingTimeline: false })
      }
    }, err => {
      toastr.error(err)
      this.setState({ isLoadingTimeline: false })
    })
  }

  getData = (props, id) => {
    const { dispatch } = props;
    dispatch(showLoading())
    request.get(`${config.apiUrl}/api/customer/${id}`, {}).then(res => {
      if (res.data.success) {
        this.getOrders(res.data.data.email)
        this.getTimeline(res.data.data.id)
        const formState = Object.assign({}, this.state.formState)
        formState.values = res.data.data
        this.setState({ formState })
      } else {
        toastr.error(res.data.msg)
        this.setState({ formState: { values: {} }})
      }
      dispatch(hideLoading())
    }, err => {
      toastr.error(err)
      dispatch(hideLoading())
      this.setState({ formState: { values: {} }})
    })
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  calculateOrders = (orders) => {
    const total_price_orders = orders.map(e => e.total_price)
    const total_spend = total_price_orders.reduce((a, b) => a + b, 0)
    const average = orders.length > 0 ? total_spend / orders.length : 0
    return { total_spend, average }
  }

  handleSubmit = (value) => {
    const data = {
      action_code: 'comment',
      action_data: {
        content: value
      },
      object_id: this.state.formState.values.id,
      object_type: 'users'
    }
    request.post_json(`${config.apiUrl}/api/actions/commit`, data).then(res => {
      if (res.data.success) {
        const timelines = Object.assign([], this.state.timelines)
        timelines.push(res.data.data.log)
        this.setState(timelines)
      } else {
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }

  editEmail = () => {
    FormModal.instance.current.showForm({
      data: {
        email: this.state.formState.values.email
      },
      title: 'Edit email',
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Email
            </Box>
            <TextField 
              name="email"
              margin='dense'
              fullWidth
              variant="outlined"
              value={submitData.values.email}
              onChange={handleChange}
              placeholder='Email...'
              error={submitData.errors?.email}
              helperText={submitData.errors?.email ? submitData.errors?.email[0] : ''}
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Save',
        schema: {
          email: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.update_json(`${config.apiUrl}/api/customer/${this.state.formState.values.uuid}`, {
              email: submitData.values.email
            }).then(res => {
              if (res.data.success) {
                toastr.success("Update successful")
                const formState = Object.assign({}, this.state.formState)
                formState.values['email'] = submitData.values.email
                this.setState({ formState })
                this.getOrders(submitData.values.email)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  render() {
    const { classes, user } = this.props;
    const { formState, editNote } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <FormModalSideStatic />
        <div className="col-lg-9 offset-2">
          <Grid container spacing={2} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <Grid item xs={12}>
              <div className="d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center">
                  <div 
                    className="d-flex align-items-center justify-content-center cursor-pointer" 
                    style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}
                    onClick={() => this.props.history.push('/customers')}
                  >
                    <IconArrowLeft size={30} stroke={2} />
                  </div>
                  <div className="ml-2 f-18 font-weight-bold">{formState.values.name}</div>
                </div>
                <div className="d-flex align-items-center">
                  <div className="d-flex align-items-center justify-content-center cursor-pointer" style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}>
                    <IconChevronLeft size={30} stroke={2} />
                  </div>
                  <div className="d-flex align-items-center justify-content-center cursor-pointer ml-1" style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}>
                    <IconChevronRight size={30} stroke={2} />
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={8}>
              <div className={classes.card}>
                <div className="p-4 d-flex flex-column">
                  <h4>{formState.values.name}</h4>
                  <span className="mt-4">{formState.values.addresses && formState.values.addresses.length > 0 ? `${formState.values.addresses[0].city} - ${formState.values.addresses[0].country}` : ''}</span>
                  <span>Customer for {moment(formState.values.created_at).fromNow()}</span>
                </div>
                {this.state.isLoading && <Placeholder.Grid active columns={4} rows={3} />}
                {formState.values.orders && formState.values.orders.length > 0 &&
                <>
                  <Divider />
                  <div className="row col-12 m-4">
                    <div className="col-lg-3 d-flex flex-column">
                      <p className="text-center f-16">Last Order</p>
                      <p className="text-center font-weight-bold f-16">{moment(formState.values.orders[0].ordered_at).fromNow()}</p>
                      <p className="text-center f-14">From Online Store</p>
                    </div>
                    <div className="col-lg-3 d-flex flex-column">
                      <p className="text-center f-16">Total spent to date</p>
                      <p className="text-center font-weight-bold f-16">{this.calculateOrders(formState.values.orders)['total_spend'].toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</p>
                      <p className="text-center f-14">{formState.values.total_orders || 0} order(s)</p>
                    </div>
                    <div className="col-lg-3 d-flex flex-column">
                      <p className="text-center f-16">Average order value</p>
                      <p className="text-center font-weight-bold f-16">{this.calculateOrders(formState.values.orders)['average'].toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</p>
                    </div>
                    <div className="col-lg-3 d-flex flex-column">
                      <p className="text-center f-16">Issues</p>
                      <p className="text-center font-weight-bold f-16"><a href="#">{formState.values.issue_count} Issues</a></p>
                    </div>
                  </div>
                </>
                }
              </div>
              {this.state.isLoading && 
                <div className={clsx(classes.card, 'nt-3')}>
                  <Placeholder.Grid active columns={4} rows={3} />
                </div>
              }
              {formState.values.orders?.length > 0 &&
                <div className={clsx(classes.card, 'mt-3')}>
                  <div className="p-3">
                    <div className="f-16 font-weight-bold">Last order placed</div>
                    <div className="d-flex align-items-center justify-content-between mt-4">
                      <div className="d-flex align-items-center">
                        <a className="f-16" href="#">Order {formState.values.orders[0].name}</a>
                        {formState.values.orders[0].financial_status && <div className={clsx(classes.chip, formState.values.orders[0].financial_status)}><div className={clsx(classes[`chip_${formState.values.orders[0].financial_status}`], 'mr-1')} /> Payment {formState.values.orders[0].financial_status?.replace('_', ' ')}</div>}
                        {formState.values.orders[0].fulfillment_status && <div className={clsx(classes.chip, formState.values.orders[0].fulfillment_status || '')}><div className={clsx(classes[`chip_${formState.values.orders[0].fulfillment_status}`], 'mr-1')} /> {formState.values.orders[0].fulfillment_status.replace('_', ' ')}</div>}
                      </div>
                      <span className="f-16">{moment(formState.values.orders[0].ordered_at).format('llll')}</span>
                    </div>
                    <div className="mt-4 f-16">
                      {formState.values.orders[0].total_price.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })} from Online Store
                    </div>
                    {formState.values.orders[0].items.map(item => (
                      <div className='d-flex' key={item.id} style={{ width: '100%' }}>
                        <div className='d-flex align-items-center justify-content-center' style={{ width: '20%' }}>
                          {item.image ? <img src={item.image} style={{ width: '80%', height: 'auto' }} /> : <Avatar><IconPhoto style={{ width: '100%', height: '100%' }} /></Avatar>}
                        </div>
                        <div className="ml-2" style={{ width: '80%' }}>
                          <div className="f-18 font-weight-bold">{item.lineitem_name}</div>
                          <div className="f-16">SKU: {item.sku}</div>
                        </div>
                      </div>
                    ))}
                  </div>
                  {/* <Divider />
                  <div className="p-3 d-flex align-items-center justify-content-end">
                    <Button className="button-primary font-weight-bold">
                      Create order
                    </Button>
                  </div> */}
                </div>
              }
              {this.state.isLoadingTimeline && <Placeholder.Grid active columns={4} rows={3} />}
              {!this.state.isLoadingTimeline &&
                <CustomTimeline 
                  items={this.state.timelines}
                  title="Timeline"
                  titleCheckbox="Show comments"
                  buttonText={"Send"}
                  itemClassName={'ml-1'}
                  renderItem={(item) => (
                    <div className='d-flex justify-content-between'>
                      <div className='d-flex flex-column'>
                        <strong className='f-16'>{item.data?.name}</strong>
                        <span>{item.data.content}</span>
                      </div>
                      <small>{item.created_at}</small>
                    </div>
                  )}
                  handleSubmit={this.handleSubmit}
                />
              }
            </Grid>
            <Grid item xs={4}>
              <div className={clsx(classes.card, 'p-3')}>
                <div className="d-flex align-items-center justify-content-between">
                  <strong>Customer</strong>
                  <a 
                    href="#" 
                    onClick={(e) => {
                      e.preventDefault()
                      e.stopPropagation()
                      this.editEmail()
                    }}
                  >Edit</a>
                </div>
                <div className='d-flex align-items-center justify-content-between mt-2'>
                  <div>{formState.values.email}</div>
                  <CopyToClipboard text={formState.values.email} />
                </div>
              </div>
              <div className={classes.card}>
                <div className='p-3'>
                  <div className='d-flex align-items-center justify-content-between'>
                    <div style={{ textTransform: 'uppercase' }}>Default address</div>
                    <a 
                      href="#" 
                      onClick={(e) => {
                        e.preventDefault()
                        e.stopPropagation()
                        console.log('1111')
                      }}
                    >Manage</a>
                  </div>
                  
                  <div className='d-flex flex-column mt-2'>
                    <span>{formState.values.name}</span>
                    <span>{formState.values.addresses && formState.values.addresses.length > 0 ? `${formState.values.addresses[0].address1 || ""}` : ""}</span>
                    <span>{formState.values.addresses && formState.values.addresses.length > 0 ? `${formState.values.addresses[0].city || ""}` : ""}</span>
                    <span>{formState.values.addresses && formState.values.addresses.length > 0 ? `${formState.values.addresses[0].country || ""}` : ""}</span>
                    <span>{formState.values.addresses && formState.values.addresses.length > 0 ? `${formState.values.addresses[0].phone || ""}` : ""}</span>
                    <a className="mt-3" href="#">Add new address</a>
                  </div>
                </div>
                <Divider className='mt-2' />
                <div className='p-3 mt-2'>
                  <div className='d-flex align-items-center justify-content-between'>
                    <div style={{ textTransform: 'uppercase' }}>Tax settings</div>
                    <a 
                      href="#" 
                      onClick={(e) => {
                        e.preventDefault()
                        e.stopPropagation()
                        console.log('1111')
                      }}
                    >Manage</a>
                  </div>
                  <div className='d-flex align-items-center justify-content-between mt-2'>
                    <div>No exemptions</div>
                  </div>
                </div>
              </div>
              <div className={classes.card}>
                <div className='p-3'>
                  <div className='d-flex align-items-center justify-content-between'>
                    <strong>Status</strong>
                  </div>
                  <div className='d-flex align-items-center mt-2'>
                    <Toggle 
                      size="lg" 
                      checkedChildren="Active" 
                      unCheckedChildren="Inactive" 
                      checked={formState.values.status == 'active'}
                      onChange={(checked, event) => {
                        const status = checked ? 'active' : 'unactivated'
                        request.update_json(`${config.apiUrl}/api/customer/${formState.values.uuid}`, {status}).then(res => {
                          if (res.data.success) {
                            const formState = Object.assign({}, this.state.formState)
                            formState.values.status = status
                            this.setState({ formState })
                          } else {
                            toastr.error(res.data.msg)
                          }
                        }, err => toastr.error(err))
                      }}
                    />
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}


CustomerForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(CustomerForm)));
export { connectedList as CustomerForm };
