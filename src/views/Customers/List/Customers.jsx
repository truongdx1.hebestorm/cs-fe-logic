/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs, ToolbarAction } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box,
  Checkbox,
  InputAdornment,
  TextField,
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TablePagination
} from "@material-ui/core";
import config from "config";
import { Filter, MultiInputPicker, FormModal, FormModalSide } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import moment from "moment";
import { fields, stateOptions } from './constants'
import {
  IconSearch, IconFilter, IconDots, IconDownload, IconPlus
} from "@tabler/icons";
import { Button, Dropdown } from 'rsuite'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '0 1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  notchedOutline: {
    borderRadius: 6
  },
  inputRoot: {
    "&.MuiInput-underline:hover:before": {
      border: "1px solid #3476D9",
    },
  },
  headName: {
    fontWeight: 'bold',
    borderRight: '1px solid #D8D8D8',
    borderTop: '1px solid #D8D8D8',
    borderBottom: '1px solid #D8D8D8',
    padding: '8px',
    background: '#ECECEC',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    borderRight: '1px solid #D8D8D8',
    padding: '8px',
    '&:last-child': {
      borderRight: 'none'
    }
  }
});

class Customers extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
          page: 1,
          limit: 15,
        }
      },
      selectedItems: [],
      count: {},
      expandFilterRow: false,
      expandFilterColumn: false,
      totals: 0,
      show_keys: [],
      items: []
    }
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Customers");
    this.getListItems(this.state.formState.values);
  }

  getListItems = (query) => {
    const queryObject = Object.assign({}, this.state.queryObject);
    var keys = Object.keys(query);
    keys.map((key, index) => {
      if (typeof query[key] == "array" || query[key] instanceof Array) {
        if (query[key].length > 0) {
          const arr = [];
          query[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format('YYYY-MM-DD'))
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (typeof query[key] == "object" || query[key] instanceof Object) {
        queryObject[key] = query[key].id;
      } else {
        queryObject[key] = query[key];
      }
    });
    this.query(queryObject);
  };

  query = (queryObject) => {
    const { dispatch } = this.props;
    dispatch(showLoading());
    request.get(`${config.apiUrl}/api/customers`, queryObject).then((res) => {
      dispatch(hideLoading());
      if (res.data.success) {
        this.setState({ items: res.data.data.result, totals: res.data.data.total, count: res.data.data.count });
      } else {
        toastr.error(res.data.msg);
        dispatch(hideLoading());
      }
    }, err => {
      toastr.error(err)
      dispatch(hideLoading());
    });
  };

  handlePageChange = (event, page) => {
    const formState = Object.assign({}, this.state.formState)
    formState.values.page = page + 1
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleRowsPerPageChange = (event) => {
    const formState = Object.assign({}, this.state.formState)
    formState.values.page = 1
    formState.values.limit = event.target.value
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  toggleDrawer = (side, open) => (event) => {
    if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }
    this.setState({ [side]: open });
  };

  handleChangeFilter = (event) => {
    const formState = Object.assign({}, this.state.formState);
    const { name, value } = event.target;
    formState["values"][name] = value;
    this.setState({ formState })
  };

  onSearch = () => {
    this.getListItems(this.state.formState.values);
  };

  resetFilter = () => {
    const formState = Object.assign({}, this.state.formState);
    const all_keys = Object.keys(this.state.formState.values)
    all_keys.forEach(e => {
      if (!['page', 'limit', 'tab'].includes(e)) {
        delete formState.values[e]
      }
    })
    formState.values.page = 1
    formState.values.tab = 'all'
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleSelectAll = event => {
    let selectedItems;

    if (event.target.checked) {
      selectedItems = this.state.items.map(t => t.id);
    } else {
      selectedItems = [];
    }
    this.setState({ selectedItems });
  };

  handleSelectOne = (event, id) => {
    const { selectedItems } = this.state;
    const selectedIndex = selectedItems.indexOf(id);
    let newselectedItems = [];

    if (selectedIndex === -1) {
      newselectedItems = newselectedItems.concat(selectedItems, id);
    } else if (selectedIndex === 0) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(1));
    } else if (selectedIndex === selectedItems.length - 1) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(0, -1));
    } else if (selectedIndex > 0) {
      newselectedItems = newselectedItems.concat(
        selectedItems.slice(0, selectedIndex),
        selectedItems.slice(selectedIndex + 1)
      );
    }
    this.setState({ selectedItems: newselectedItems });
  };

  render() {
    const { classes, user } = this.props;
    const { formState, right, items, selectedItems, expandFilterColumn, expandFilterRow } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <Filter
          right={right}
          onClose={(side, open) => this.toggleDrawer(side, open)}
          formState={this.state.formState}
          onChange={this.handleChangeFilter}
          onSearch={this.onSearch}
          resetFilter={this.resetFilter}
          filters={[]}
        />
        <div style={{ paddingTop: 5 }}>
          <div className={classes.card}>
            <div style={{ display: "flex", padding: "5px 10px" }}>
              {selectedItems.length > 0 && 
                <Dropdown 
                  renderTitle={() => (
                    <Button style={{ border: '1px solid #ccc', marginTop: 8, marginRight: 10 }}><IconDots size={20} stroke={2} /></Button>
                  )}
                >
                  <Dropdown.Item>Assign to CS</Dropdown.Item>
                  <Dropdown.Item>Change CS State</Dropdown.Item>
                </Dropdown>
              }
              <TextField
                id="query"
                margin="dense"
                name="query"
                placeholder="Search customers"
                classes={{
                  root: classes.inputRoot,
                }}
                value={formState.values.query}
                onChange={this.handleChangeFilter}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <IconSearch size={20} stroke={1.5} />
                    </InputAdornment>
                  ),
                  classes: {
                    notchedOutline: classes.notchedOutline,
                  },
                }}
                style={{ width: "30%", backgroundColor: "#fff" }}
                variant="outlined"
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    this.onSearch();
                  }
                }}
              />
              <div className="ml-1">
                <MultiInputPicker
                  onDelete={(click) => (this.handleDeleteMultiPicker = click)}
                  valueKey="id"
                  style={{ marginTop: 8 }}
                  getOptionLabel={(item) => item.name}
                  onChange={(items) => {
                    const formState = Object.assign({}, this.state.formState);
                    if (items.length > 0) {
                      formState.values['states'] = items
                    } else {
                      delete formState.values["states"];
                    }
                    this.setState({ formState }, () =>
                      this.getListItems(this.state.formState.values)
                    );
                  }}
                  loadOptions={(inputValue) => {
                    return new Promise((resolve) => {
                      resolve(stateOptions)
                    });
                  }}
                  noOptionsMessage="No results found"
                  renderAriaLabel={(selectedItems) => `${selectedItems.length} states selected`}
                  defaultAriaLabel="Status"
                />
              </div>
              {/* <div className="ml-1">
                <MultiInputPicker
                  onDelete={(click) => (this.handleDeleteMultiPicker = click)}
                  valueKey="id"
                  style={{ marginTop: 8 }}
                  getOptionLabel={(item) => item.name}
                  onChange={(items) => {
                    const formState = Object.assign({}, this.state.formState);
                    if (items.length > 0) {
                      formState.values['shops'] = items
                    } else {
                      delete formState.values["shops"];
                    }
                    this.setState({ formState }, () =>
                      this.getListItems(this.state.formState.values)
                    );
                  }}
                  loadOptions={(inputValue) => {
                    return new Promise((resolve) => {
                      resolve(stateOptions)
                    });
                  }}
                  noOptionsMessage="No results found"
                  renderAriaLabel={(selectedItems) => `${selectedItems.length} shops selected`}
                  defaultAriaLabel="From Shop"
                />
              </div> */}
              {/* <div style={{ marginLeft: 5 }}>
                <Button
                  onClick={() => this.setState({ right: true })}
                  style={{
                    marginTop: 8,
                    border: "1px solid #ccc",
                    fontWeight: "bold",
                  }}
                >
                  <IconFilter size={20} stroke={1.5} /> Filter
                </Button>
              </div> */}
            </div>
            <TableContainer style={{ maxHeight: 'calc(100vh - 200px)' }}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '4%' }} align="center" padding="checkbox" className={classes.headName}>
                      <Checkbox
                        checked={selectedItems.length === items.length}
                        color="secondary"
                        indeterminate={
                          selectedItems.length > 0 &&
                          selectedItems.length < items.length
                        }
                        onChange={this.handleSelectAll}
                      />
                    </TableCell>
                    {fields.map(field => (
                      <TableCell key={field.name} style={{ width: field.width }} align={field.align || 'left'} className={classes.headName}>{field.label}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((item, index) => (
                    <TableRow 
                      key={item.id} 
                      style={{ backgroundColor: index % 2 == 1 ? '#ECECEC' : '#fff'}}
                      className="cursor-pointer"
                      onClick={() => this.props.history.push(`/customers/${item.uuid}`)}
                    >
                      <TableCell onClick={e => e.stopPropagation()} padding="checkbox" align="center" className={classes.cellContent}>
                        <Checkbox
                          checked={selectedItems.indexOf(item.id) !== -1}
                          color="secondary"
                          onChange={event => this.handleSelectOne(event, item.id)}
                          value="true"
                        />
                      </TableCell>
                      {fields.map(field => (
                        <TableCell key={field.name} className={classes.cellContent} align={field.align || 'left'}>
                          {field.renderItem(item)}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              style={{ borderTop: '1px solid #D8D8D8' }}
              component="div"
              count={this.state.totals}
              onPageChange={this.handlePageChange}
              onRowsPerPageChange={this.handleRowsPerPageChange}
              page={formState.values.page - 1}
              rowsPerPage={this.state.formState.values.limit}
              rowsPerPageOptions={[15, 25, 50, 100]}
            />
          </div>
        </div>
      </div>
    );
  }
}


Customers.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(Customers)));
export { connectedList as Customers };
