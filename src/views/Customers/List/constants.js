import React from 'react'
import moment from 'moment'
import { IconEye } from '@tabler/icons'
import { ListItem, ListItemText, ListItemAvatar, Avatar } from '@material-ui/core'

export const stateOptions = [
  {id: 'active', name: 'Active', bgColor: '#9CEFC3', color: '#000'},
  {id: 'unactivated', name: 'Inactive', bgColor: '#DCDCDC', color: '#000'},
]

export const fields = [
  {
    name: 'customers',
    label: 'Customers',
    renderItem: (item) => (
      <ListItem style={{ padding: 0 }}>
        <ListItemAvatar>
          <Avatar alt={item.name} src={item.avatar} />
        </ListItemAvatar>
        <ListItemText primary={item.name} classes={{ primary: 'font-weight-bold' }} secondary={item.addresses && item.addresses.length > 0 ? `${item.addresses[0].city} - ${item.addresses[0].country}` : ''} />
      </ListItem>
    ),
    width: '20%',
  },
  {
    name: 'email',
    label: 'Email',
    renderItem: (item) => item.email,
    width: '20%',
  },
  {
    name: 'created_at',
    label: 'Created at',
    renderItem: (item) => moment(item.created_at).format('LLL'),
    width: '20%',
  },
  {
    name: 'issues',
    label: 'Issues',
    renderItem: (item) => item.issue_count,
    width: '10%',
  },
  {
    name: 'status',
    label: 'Status',
    renderItem: (item) => {
      const option = stateOptions.find(e => e.id == item.status)
      return (
        <div className='f-14 br-10 text-center width-fit-content' style={{ padding: '3px 5px', backgroundColor: option?.bgColor || '#000', color: option?.color || '#fff' }}>{option?.name}</div>
      )
    },
    width: '10%',
  },
  {
    name: 'from_shop',
    label: 'From Shop',
    renderItem: (item) => item.metadata?.shop,
    width: '16%',
  },
]