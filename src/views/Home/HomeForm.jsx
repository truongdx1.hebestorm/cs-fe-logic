/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { cssConstants } from '../../_constants'


const useStyles = theme => ({
    ...cssConstants,
    root: {
        padding: '3rem 8rem',
        background: '#f3f6f8'
    }

});






class HomeForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }


    }

    componentDidMount() {


    }






    render() {
        const { classes, user } = this.props;
        const { formState, visible, total, orders, data, refund, sessionData } = this.state;
        return (
            <div className={classes.root}>
                
            </div >
        )
    }
}

HomeForm.formats = [
    "footer",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "color"
];

HomeForm.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(HomeForm)));
export { connectedList as HomeForm };
