/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants, offerIssues } from "../../../_constants";
import { AntTabs } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box, Grid, Divider, Table, TableRow, TableCell, TableBody, TableHead, Stepper, Step, StepLabel, Button as ButtonMaterial, TextField,
  Popper, ClickAwayListener, Typography,
  MenuItem, MenuList, Grow, Paper, FormGroup, FormControlLabel, Checkbox, CircularProgress,
  List, ListItem, ListItemText
} from "@material-ui/core";
import config from "config";
import { FormModal, FormModalSide, FormModalSideStatic, CustomStep, CustomTimeline } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { capitalizeStr, makeId } from "../../../utils";
import moment from "moment";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import clsx from 'clsx'
import {
  IconEye, IconInfoCircle, IconMessages, IconEdit, IconTemplate, IconCornerUpRight
} from "@tabler/icons";
import { Button, Avatar } from 'rsuite'
import { Conversation, ForwardForm, IssueDetail, OrderDetail } from './components'
import { TinyEditorComponent, ToolBar } from '../../../_components'
import Check from '@material-ui/icons/Check';
import { ColorlibConnector, ColorlibStepIcon, stepsIssues } from './constants'
import './issues.scss'
import CheckIcon from '@material-ui/icons/Check';
import { Alert, AlertTitle } from '@material-ui/lab';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationCircle, faFileAlt, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { layerRender } from '../../../Layer'

import AsyncSelect from 'react-select/async';
import Select from 'react-select'
import { selectStyle, customStyles } from '../../../utils'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Dropzone from 'react-dropzone'
import download from '../../../assets/images/download.svg'
import { labelMap, Log } from './constants'
import { IssueActionPop } from '../../../_components/IssueActions'

import {
  IconArrowUp, IconFile, IconTrash, IconCheck, IconX
} from '@tabler/icons'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    // marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  cardNote: {
    backgroundColor: '#E7F8FB',
    border: '1px solid #B7DBE0'
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    }
  },
  chipPaid: {
    width: 10,
    height: 10,
    backgroundColor: '#006F53',
    borderRadius: '50%'
  },
  chipCancelled: {
    width: 10,
    height: 10,
    backgroundColor: '#626669',
    borderRadius: '50%'
  },
  chipPending: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  chipUnfulfilled: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    fontWeight: 'bold',
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  }, popper: {
    zIndex: 100
  }, dropzone: {
    margin: '10px 0',
    border: '2px dashed #ccc',
    outline: 'none',
    color: '#666'
  }, dropzone_input: {
    outline: 'none',
    visibility: 'hidden'
  }, fileContainer: {
    borderTop: '1px solid #ccc',
    borderBottom: '1px solid #ccc'
  },
  a_back: {
    color: '#637381 !important',
    '&:hover': {
      color: '#000 !important'
    },
    cursor: 'pointer'
  },
  tiltePage: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#3f3f3f',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    lineHeight: '30px'
    // marginTop: theme.spacing(2)
  }
});





class IssueForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        }
      },
      currentTab: 'customer',
      reply: false
    };
    this.quillRef = React.createRef()
    this.moreActions = React.createRef();

  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Issues");
    this.getData(this.props, this.props.match.params.id)
    // this.confirmPop({
    //   title: 'Confirm Design Link',
    //   message: "Hey, make sure you know what you're doing before hitting the button!",
    //   button: "Confirm",
    //   data: null
    // })
  }

  componentWillUpdate(nextProps) {
    if (nextProps.match.params.id != this.props.match.params.id) {
      this.getData(nextProps, nextProps.match.params.id)
    }
  }
  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  forwardMail = () => {
    this.setState({ reply: false })
    FormModalSide.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <ForwardForm />
      )
    })
  }

  editOrder = () => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <OrderDetail />
      )
    })
  }


  getData = (props, id) => {
    const { dispatch } = props;
    dispatch(showLoading())
    request.get(`${config.issueApiUrl}/api/issues/${id}`, {}).then(res => {
      if (res.data.success) {
        const formState = Object.assign({}, this.state.formState)
        formState.values = res.data.data
        this.setState({ formState })
      } else {
        toastr.error(res.data.msg)
        this.setState({ formState: { values: {} } })
      }
      dispatch(hideLoading())
    }, err => {
      toastr.error(err)
      dispatch(hideLoading())
      this.setState({ formState: { values: {} } })
    })

    this.onGetLogs(id)
  }

  onGetLogs = (id, page) => {
    request.get(`${config.issueApiUrl}/api/logs?object_type=issues&object_id=${id}`, { page: page || 1 }).then(res => {
      if (res.data.success) {
        var logs = Object.assign({}, this.state.logs)
        logs = res.data.data
        this.setState({ logs })
      } else {
        this.setState({ logs: null })
      }
    }, err => {
      toastr.error(err)
      this.setState({ logs: null })
    })
  }

  loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  };

  handleInputChange = (newValue) => {
    return newValue;
  };


  getSteps = () => {
    const { formState } = this.state
    var solution_steps = formState.values?.solution_steps?.sort((a, b) => a.sequence < b.sequence ? -1 : 1)
    var kind_steps = formState.values?.kind_steps?.sort((a, b) => a.sequence < b.sequence ? -1 : 1)
    var arrSteps = formState.values?.id ? [...kind_steps || [], ...solution_steps || [], { code: "resolve", label: "Resolve", level: "T5", state: formState.values?.level == "T5" ? "done" : null }] : []
    var array = []
    while (arrSteps.length > 0) {
      var chunk = arrSteps.splice(0, 8)
      array.push(chunk)
    }
    return array
  }



  actionStep = (action) => {
    const { formState } = this.state
    IssueActionPop({
      defaultData: ["add_custom_design_link", 'add_design_link'].includes(action.code) ? {
        link: formState?.values?.extra?.design_link
      } : {},
      action,
      issue: formState.values,
      context: {
        objectId: formState.values.id,
        objectType: 'issues'
      },
      onSubmitDone: () => {
        this.getData(this.props, this.props.match.params.id)
      }
    })
  }





  render() {
    const { classes, user } = this.props;
    const { formState, reply, logs } = this.state;

    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <FormModalSideStatic />
        <div className="d-flex align-items-center justify-content-between mb-4">
          {/* <ToolBar
            type={2}
            formState={{}}
            isSubmitting={false}
            onChange={() => { }}
            onClickSearch={() => { }}
            titlePage={
              <div className="d-flex ml-2 flex-column">
                <Box fontWeight={'bold'} fontSize={20} color={"#007B5C"} >
                  {formState.values?.code || ''}
                </Box>
                <span style={{ fontSize: 12 }}>{`Create at: ${moment(formState.values.created_at).format('YYYY-MM-DD HH:mm:ss')}`}</span>
              </div>}
            actionBack={() => {
              const current_app = JSON.parse(localStorage.getItem('current_app'))
              this.props.history.push(`/issues`)
            }}
            {...this.props}
          /> */}
          
          <div className="d-flex align-items-center">
            <a 
              onClick={() => {
                const from = this.props.location?.state?.from
                if (from) {
                  this.props.history.push(from)
                } else {
                  this.props.history.push('/issues')
                }
              }} 
              className={classes.a_back}
            >
              <ArrowBackIosIcon fontSize='small' />
            </a>

            <Typography
              className={clsx(classes.tiltePage, 'ml-2')}
              gutterBottom
            >
              Issues
            </Typography>
          </div>
          <div className="d-flex align-items-center ml-3 justify-content-between flex-fill">
            <div className="d-flex align-items-start flex-column">
              {
                this.getSteps()?.map((group, k) =>
                  <div className={clsx("ui steps", k > 0 ? 'mt-2' : '')}>
                    {group?.map((step, i) => {
                      if (step.state == "done") {
                        return (
                          <div className="step" key={i}>
                            {<CheckIcon color={'primary'} style={{ fontWeigth: 'bold' }} />}
                            <div className="content">
                              <div className="title">{step?.label || ""}</div>
                            </div>
                          </div>)
                      } else if (!formState.values?.next_actions?.includes(step.code) || (formState.values.next_actions.includes(step.code) && step?.state)) {
                        return (
                          <div className="disabled step" key={i}>
                            <div className="content">
                              <div className="title">{step?.label || ""}</div>
                            </div>
                          </div>)
                      } else if (formState.values.next_actions.includes(step.code) && !step?.state) {
                        return (
                          <a className="active step"
                            key={i}
                            onClick={() => {
                              this.actionStep(step)
                            }}
                          >
                            <div className="content">
                              <div className="title">{step?.label || ""}</div>
                            </div>
                          </a>)
                      }
                    })
                    }
                  </div>
                )
              }
            </div>
            <button className={clsx(classes.button_outline, 'ml-3')}
              ref={this.moreActions}
              onClick={() => {
                this.setState(prevState => ({
                  moreAction: !prevState.moreAction
                }))
              }}
            >
              <MoreVertIcon fontSize='small' />
            </button>
            <Popper
              // placement={comp.placement}
              className={classes.popper}
              open={this.state.moreAction || false}
              anchorEl={this.moreActions.current}
              role={undefined}
              transition disablePortal>
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{
                    transformOrigin: 'center top',
                    marginTop: 5,

                  }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={() => {
                      this.setState({ moreAction: false })
                    }}>
                      <MenuList autoFocusItem={false} id="menu-list-grow"
                        onKeyDown={(event) => {
                          if (event.key === 'Tab') {
                            event.preventDefault();
                            this.setState({ moreAction: false })
                          }
                        }}>
                        <MenuItem onClick={() => {
                          this.setState({ moreAction: false })
                          this.actionStep({ code: "assign" })

                        }}>
                          Assign
                        </MenuItem>
                        <MenuItem onClick={() => {
                          this.setState({ moreAction: false })
                          this.actionStep({ code: "add_followers" })
                        }}>
                          Add Assignment

                        </MenuItem>


                      </MenuList>

                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        </div>

        <Grid container spacing={3}>
          <Grid item xs={6} style={{ overflow: 'auto' }}>
            <CustomStep
              steps={[
                { value: 'T0', label: 'T0. Open' },
                { value: 'T1', label: 'T1. Assigned' },
                { value: 'T2', label: 'T2. Had Offer' },
                { value: 'T3', label: 'T3. Sent Offer' },
                { value: 'T4', label: 'T4. Had Final Decision' },
                { value: 'T5', label: 'T5. Resolved' }

              ]}
              activeStep={formState.values.level || null}
            />

            <div className={clsx(classes.card, 'mt-4')}>
              <div className="p-3 font-weight-bold f-18">Resource</div>
              <Divider />
              <div className="p-3">
                <div style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
                  <Table>
                    <TableBody>
                      <TableRow className={classes.tableRow}>
                        <TableCell className={classes.headName} style={{ width: '20%' }}>Name</TableCell>
                        <TableCell className={classes.headName} style={{ width: '55%' }}>Lineitem Name</TableCell>
                        <TableCell className={classes.headName} style={{ width: '20%' }}>Level</TableCell>
                      </TableRow>
                      {formState.values?.items?.map((item, i) =>
                        <TableRow className={classes.tableRow} key={i}>
                          <TableCell className={classes.cellContent}>{item.order_name}</TableCell>
                          <TableCell className={classes.cellContent}>{item.lineitem_name}</TableCell>
                          <TableCell className={classes.cellContent}>{item.level}</TableCell>
                        </TableRow>)}
                    </TableBody>
                  </Table>
                </div>
              </div>
              {formState.values?.note ? <div className="p-3">
                <div className="p-3" style={{ border: '2px dashed orange', borderRadius: 6, backgroundColor: '#FCF4E9' }}>
                  <div className="d-flex align-items-center justify-content-between">
                    <div className="font-weight-bold f-16">Note</div>
                  </div>
                  <div className="mt-2">
                    <div dangerouslySetInnerHTML={{ __html: formState.values?.note }} />
                  </div>
                </div>
              </div> : null}
            </div>

            <div className={clsx(classes.card, 'mt-4')}>
              {
                <>
                  <div className='ui comments issue-conversation'>
                    <CustomTimeline
                      firstItemDot={<Avatar circle style={{ backgroundColor: '#FFD097', color: '#000', marginLeft: -15 }}>{user?.data?.user_name?.substring(0, 2)?.toUpperCase()}</Avatar>}
                      items={this.state.logs?.result || []}
                      title="History"
                      titleCheckbox="Show History"
                      buttonText={"Post"}
                      itemClassName={'ml-1'}
                      renderItem={(log) => {
                        return (
                          <Log log={log} />
                        )
                      }}
                      onReply={(content) => {
                        return new Promise((resolve) => {
                          if (!content) return;
                          const issue = formState.values

                          request.post_json(`${config.issueApiUrl}/api/actions/commit`, {
                            object_id: issue.id,
                            object_type: 'issues',
                            action_code: "comment",
                            action_data: { content: content }
                          }).then(res => {
                            if (res.data.success) {
                              this.onGetLogs(issue.id)
                              resolve(true)
                            } else {
                              resolve(false)
                              toastr.error(res.data.msg)
                            }
                          }, err => {
                            resolve(false)
                            toastr.error(err)
                          })
                        })
                      }}
                    />
                  </div>
                  {this.state.logs && <div className={clsx(classes.footer, "p-2")}>
                    <div className={classes.actions_ft}>
                      <button className={clsx(classes.button_normal, 'default', logs?.page > 1 ? '' : 'disable')}
                        onClick={() => {
                          this.onGetLogs(issue.id, logs?.page - 1)
                        }}
                        disabled={logs?.page > 1 ? false : true}
                      >
                        <FontAwesomeIcon icon={faArrowLeft}
                          size={'1x'} style={{ fontSize: 14 }} />
                      </button>
                      <button className={clsx(classes.button_normal, 'default', logs?.total_page > logs?.page ? '' : 'disable')}
                        onClick={() => {
                          this.onGetLogs(issue.id, logs?.page + 1)
                        }}
                        disabled={logs?.total_page > logs?.page ? false : true}>
                        <FontAwesomeIcon icon={faArrowRight}
                          size={'1x'} style={{ fontSize: 14 }} />
                      </button>
                    </div>
                  </div>}
                </>
              }
            </div>


          </Grid>
          <Grid item xs={6} style={{ overflow: 'auto' }}>

            <IssueDetail
              editOrder={this.editOrder}
              showHistory
              issue={formState.values}
              assignPopUp={() => {
                this.actionStep({ code: "assign" })
              }}
              logs={this.state.logs || []}
              onGetLogs={this.onGetLogs}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}


IssueForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(IssueForm)));
export { connectedList as IssueForm };
