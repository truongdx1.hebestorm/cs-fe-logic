import React, { useState, useEffect } from 'react'
import { Box, Table, TableRow, TableCell, TableBody } from '@material-ui/core'
import { AntTabs } from '../../../../_components'
import { makeStyles } from '@material-ui/core/styles';
import { TinyEditorComponent } from '../../../../_components'
import {
  IconCornerUpRight, IconDownload, IconTrash
} from "@tabler/icons";
import { Avatar } from 'rsuite'

const useStyles = makeStyles(theme => ({
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  headName: {
    background: '#E8F5FE',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none',
      borderTopRightRadius: 6
    },
    '&:first-child': {
      borderTopLeftRadius: 6
    },
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    background: '#fff',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const Conversation = (props) => {
  const classes = useStyles()
  return (
    <div className="p-3" style={{ background: props.background || '#fff' }}>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Avatar style={{ backgroundColor: '#C5E7BF', color: '#000' }}>A</Avatar>
          <div className="ml-2">
            <p style={{ color: '#1B79E5', fontSize: 16 }}>Alisson Becker</p>
            <small>2 days ago (Fri, 14 Jan 2022 at 1:43 PM)</small>
          </div>
        </div>
        <div className="d-flex align-items-center justify-content-center p-2 cursor-pointer" style={{ border: '1px solid #ccc', borderRadius: 6 }}>
          <IconCornerUpRight size={20} stroke={2} />
        </div>
      </div>
      <div className='mt-3'>
        It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div className='mt-3 d-flex align-item-center'>
        <div className='mail-attachment'>
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center">
              <div className="ml-2">
                <p style={{ color: '#1B79E5', fontSize: 16 }}>Video.mov</p>
                <small>6.69MB</small>
              </div>
            </div>
            <div className="d-flex align-items-center justify-content-center p-2 ml-2">
              <IconDownload size={20} stroke={2} cursor="pointer" />
              <IconTrash size={20} stroke={2} cursor="pointer" />
            </div>
          </div>
        </div>
      </div>
      <div className='mt-3' style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
        <Table>
          <TableBody>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Order ID</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Code</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Title</TableCell>
              <TableCell className={classes.headName} style={{ width: '25%' }}>Note</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Created At</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Forward To</TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.cellContent} style={{ width: '10%', borderBottom: 'none' }}>Order ID</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '10%', borderBottom: 'none' }}>Code</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '5%', borderBottom: 'none' }}>Level</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '15%', borderBottom: 'none' }}>Kind</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '10%', borderBottom: 'none' }}>Title</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '25%', borderBottom: 'none' }}>Note</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '15%', borderBottom: 'none' }}>Created At</TableCell>
              <TableCell className={classes.cellContent} style={{ width: '10%', borderBottom: 'none' }}>Forward To</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    </div>
  )
}

export default Conversation;