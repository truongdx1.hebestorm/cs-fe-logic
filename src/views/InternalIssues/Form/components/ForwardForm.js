import React, { useState, useEffect, useRef } from 'react'
import { Box, Table, TableRow, TableCell, TableBody, Checkbox, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { TinyEditorComponent } from '../../../../_components'
import { cssConstants } from '../../../../_constants'
import { Conversation } from '../components'
import AsyncSelect from 'react-select/async'
import {
  IconCornerUpRight, IconDownload, IconTrash
} from "@tabler/icons";
import { Avatar } from 'rsuite'
import { customStyles, makeId } from '../../../../utils'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));


const ForwardForm = (props) => {
  const classes = useStyles()
  const inputEl = useRef(null);
  const [content, setContent] = useState(null)

  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  }

  const handleInputChange = (newValue) => {
    return newValue;
  }

  return (
    <div className="p-3">
      <div style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontSize: 16, fontWeight: 'bold' }}>
                Order Detail
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '2%' }} padding="checkbox">
                <Checkbox
                  // checked={selectedItems.length === items.length}
                  color="secondary"
                  // indeterminate={
                  //   selectedItems.length > 0 &&
                  //   selectedItems.length < items.length
                  // }
                  // onChange={this.handleSelectAll}
                />
              </TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Lineitem SKU</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Lineitem Name</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Link Design/Ali/Sup</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Supplier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Tracking</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Carrier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN uploaded at</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN status</TableCell>
              <TableCell className={classes.headName} style={{ width: '8%' }}>Est time</TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.cellContent} padding="checkbox">
                <Checkbox
                  // checked={selectedItems.indexOf(item.id) !== -1}
                  color="secondary"
                  // onChange={event => this.handleSelectOne(event, item.id)}
                  value="true"
                />
              </TableCell>
              <TableCell className={classes.cellContent}>POD_202147641</TableCell>
              <TableCell className={classes.cellContent}>ABCC</TableCell>
              <TableCell className={classes.cellContent}>Link</TableCell>
              <TableCell className={classes.cellContent}>L7.8</TableCell>
              <TableCell className={classes.cellContent}>Alex</TableCell>
              <TableCell className={classes.cellContent}>YT83921732</TableCell>
              <TableCell className={classes.cellContent}>YunExpress</TableCell>
              <TableCell className={classes.cellContent}>2022-01-18 14:23:21</TableCell>
              <TableCell className={classes.cellContent}>In transit</TableCell>
              <TableCell className={classes.cellContent}>2022-01-20</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
      <div className='mt-2'>
        <Box fontSize={15}>
          Forward To
        </Box>
        <AsyncSelect
          isSearchable
          cacheOptions
          loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
            return new Promise(resolve => {
              resolve([])
            })
          })}
          defaultOptions
          onInputChange={handleInputChange}
          placeholder={'Forward To'}
          getOptionLabel={({ name }) => name}
          getOptionValue={({ uid }) => uid}
          onChange={(value) => {
            console.log(value)
          }}
          value={null}
          styles={customStyles}
        />
      </div>
      <div className='mt-2 p-3' style={{ border: '1px solid #ECECEC' }}>
        <div className="d-flex align-items-center">
          <Avatar style={{ background: '#FFD2CD', color: '#000' }}>J</Avatar>
          <div className="ml-2">
            <p style={{ color: '#1B79E5', fontSize: 16 }}>Alisson Becker</p>
            <small>Fri, 14 Jan 2022 at 1:43 PM</small>
          </div>
        </div>
        <div className='mt-2'>
          <TinyEditorComponent 
            style={{ width: '100%' }}
            ref={inputEl}
            content={content}
            id={makeId(20)}
            onEditorChange={(value) => {
              setContent(value)
            }}
          />
        </div>
        <div className='mt-3 d-flex justify-content-end align-items-center'>
          <Button
            className={clsx(classes.button_normal, 'primary', 'hb-button')}
            color="primary"
            onClick={() => {}}
            size="small"
            variant="contained"
            startIcon={<IconCornerUpRight size={20} stroke={1.5} />}
          >
            Forward
          </Button>
        </div>
      </div>
      <div className='mt-2'>
        <Conversation background={'#D4F7FF'} />
      </div>
    </div>
  )
}

export default ForwardForm;