import React, { useState, useEffect } from 'react'
import {
  List, ListItem, ListItemText, Divider,
  Table, TableRow, TableCell, TableBody, IconButton, Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
// import { CopyToClipboard } from 'react-copy-to-clipboard'
import { CustomTimeline, CopyToClipboard } from '../../../../common'
import {
  IconExternalLink, IconClipboardList
} from "@tabler/icons";
import Avatar from '@material-ui/core/Avatar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import user_logo from '../../../../assets/images/user_2.png'
import clsx from 'clsx';

import { capitalizeStr } from '../../../../utils'
import { labelMap, Log } from '../constants'
import { request } from "../../../../_services/request";
import toastr from "../../../../common/toastr";
import config from "config";
import follower from '../../../../assets/images/avatars/nan.jpeg'



const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  }, text_button: {
    textTransform: 'none !important'
  },
  close_button: {
    "&:hover": {
      color: '#333'
    }
  }
}));

const ListItemInfo = ({ title, value, icon, divider, fullWidth }) => {
  const classes = useStyles()
  return (
    <>
      <ListItem style={{ padding: '4px 0px' }}>
        <ListItemText
          style={{ width: '40%' }}
          secondary={title}
          classes={{ secondary: classes.textBold }}
        />
        <ListItemText
          style={{ width: '60%' }}
          secondary={
            <div className='d-flex'>
              <div className={fullWidth ? 'w-100' : ''}>{value}</div>
              {icon && <div className='ml-2'>{icon}</div>}
            </div>
          }
        />
      </ListItem>
      {divider && <Divider />}
    </>
  )
}



const IssueDetail = (props) => {
  const classes = useStyles()
  const { issue } = props;
  var { logs } = props;
  return (
    <div>
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2"><strong style={{ color: '#449242' }}>Issue Info</strong></div>
        <Divider />
        <div className="pl-3 pr-3">
          <List>
            <ListItemInfo title="Creator:" value={issue.creator?.name || ''} divider />
            <ListItemInfo title="Kind:" value={issue?.kind?.name || ''} divider />
            <ListItemInfo title="Other Assignee:" value={
              <div>
                {issue?.followers?.map((per, i) => <div className='ui image label mr-2 mb-1'>
                  <img src={follower} />
                  {per.name}
                  <FontAwesomeIcon icon={faTimes} className={clsx('ml-2 cursor-pointer', classes.close_button)}
                    color='#0000004d'
                  />
                </div>)}
              </div>

            }
              divider />
            <ListItemInfo title="Assignee:" value={
              <Button
                onClick={() => {
                  props.assignPopUp()
                }}
                className='ui button'
                classes={
                  {
                    label: classes.text_button
                  }
                }
                startIcon={<AccountCircleIcon />}
              >
                {issue?.assignee?.name}
              </Button>

            } divider />
            <ListItemInfo title="Deadline:" value="" divider />
            <ListItemInfo title="Solution:" value={issue?.solution?.name || ''} divider />
            {/* <ListItemInfo title="Design SKU:" value={""} divider /> */}
            {Object.keys(issue?.extra || {}).map((key, p) =>
              <ListItemInfo title={
                capitalizeStr(key?.replaceAll("_", " "))
              } value={(typeof issue?.extra[key] == 'object' || issue?.extra[key] instanceof Object) ? JSON.stringify(issue?.extra[key]) : issue?.extra[key]}
                divider />
            )}
            {/* < ListItemInfo title="Custom data:" value={""} divider /> */}
            {/* <ListItemInfo title="Check design at:" value={""} divider /> */}
            {/* <ListItemInfo title="Updated solution at:" value={""} divider /> */}
          </List>
        </div>
      </div>

      {/* {props.showHistory &&
        <>
          <div className='ui comments issue-conversation'>
            <CustomTimeline
              items={props.logs?.result || []}
              title="History"
              titleCheckbox="Show History"
              buttonText={"Post"}
              itemClassName={'ml-1'}
              renderItem={(log) => {
                return (
                  <Log log={log} />
                )
              }}
              onReply={(content) => {
                return new Promise((resolve) => {
                  if (!content) return;
                  request.post_json(`${config.issueApiUrl}/api/actions/commit`, {
                    object_id: issue.id,
                    object_type: 'issues',
                    action_code: "comment",
                    action_data: { content: content }
                  }).then(res => {
                    if (res.data.success) {
                      props.onGetLogs(issue.id)
                      resolve(true)
                    } else {
                      resolve(false)
                      toastr.error(res.data.msg)
                    }
                  }, err => {
                    resolve(false)
                    toastr.error(err)
                  })
                })
              }}
            />
          </div>
          {props.logs && <div className={classes.footer}>
            <div className={classes.actions_ft}>
              <button className={clsx(classes.button_normal, 'default', logs?.page > 1 ? '' : 'disable')}
                onClick={() => {
                  props.onGetLogs(issue.id, logs?.page -1)
                }}
                disabled={logs?.page > 1 ? false : true}
              >
                <FontAwesomeIcon icon={faArrowLeft}
                  size={'1x'} style={{ fontSize: 14 }} />
              </button>
              <button className={clsx(classes.button_normal, 'default', logs?.total_page > logs?.page ? '' : 'disable')}
                onClick={() => {
                  props.onGetLogs(issue.id, logs?.page + 1)
                }}
                disabled={logs?.total_page > logs?.page ? false : true}>
                <FontAwesomeIcon icon={faArrowRight}
                  size={'1x'} style={{ fontSize: 14 }} />
              </button>
            </div>
          </div>}
        </>
      } */}

    </div>
  )
}

export default IssueDetail