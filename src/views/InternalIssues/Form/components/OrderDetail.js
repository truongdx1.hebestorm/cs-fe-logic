import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider, Grid
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import { CustomTimeline, CopyToClipboard } from '../../../../common'
import {
  IconCircleDashed, IconCircleCheck, IconClipboardList
} from "@tabler/icons";
import { Button, Dropdown } from 'rsuite'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  container: {
    backgroundColor: '#fff',
    boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    }
  },
  chipPaid: {
    width: 10,
    height: 10,
    backgroundColor: '#006F53',
    borderRadius: '50%'
  },
  chipCancelled: {
    width: 10,
    height: 10,
    backgroundColor: '#626669',
    borderRadius: '50%'
  },
  chipPending: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  chipUnfulfilled: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  textBold: {
    fontWeight: 'bold'
  },
  snackSuccess: {
    backgroundColor: '#449242',
    minWidth: 'auto'
  }
}));

const OrderDetail = (props) => {
  const classes = useStyles()

  return (
    <Grid container spacing={2} className='p-3'>
      <Grid item xs={12}>
        <div className='d-flex align-items-center justify-content-between'>
          <div className='d-flex flex-column'>
            <div className='d-flex align-items-center'>
              <div className='f-24'>#702630-GBS</div>
              <div className={clsx(classes.chip, 'pending')}><div className={clsx(classes.chipPending, 'mr-1')} /> Payment pending</div>
              <div className={clsx(classes.chip, 'unfulfilled')}><div className={clsx(classes.chipUnfulfilled, 'mr-1')} /> Unfulfilled</div>
            </div>
            <div className='text-gray'>October 10, 2021 at 9:06 pm</div>
          </div>
          <div className='d-flex align-items-center'>
            <Button className='mr-1' style={{ border: '1px solid #ccc' }}>
              Edit
            </Button>
            <Button className='mr-1' style={{ border: '1px solid red', color: 'red' }}>
              Refund
            </Button>
            <Dropdown title="More Action" placement='bottomEnd' style={{ border: '1px solid #ccc', borderRadius: 6 }}>
              <Dropdown.Item>New File</Dropdown.Item>
              <Dropdown.Item>New File 2</Dropdown.Item>
            </Dropdown>
          </div>
        </div>
      </Grid>
      <Grid item xs={8}>
        <div className={clsx(classes.card, 'pt-3')}>
          <div className='d-flex align-items-center p-2'>
            <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: 'orange' }}>
              <IconCircleDashed size={20} stroke={3} />
            </div>
            <div className='ml-2'><strong>Unfulfilled (1)</strong> #212704149-GBS-F2</div>
          </div>
          <div className='d-flex align-items-center row mt-3 p-2'>
            <div className='col-lg-2'>
              <img src={'https://d5nunyagcicgy.cloudfront.net/external_assets/hero_examples/hair_beach_v391182663/original.jpeg'} style={{ width: '100%', height: 'auto' }} />
            </div>
            <div className='col-lg-6 f-13'>
              <div><a href="#">HIPPI LIFE 2022</a></div>
              <div>Face Mask 3.5D / 26cm X 16cm / 1PC</div>
              <div>SKU: POD_202013958</div>
              <div>Custom Info: Name: ”PeterParker”</div>
            </div>
            <div className='col-lg-2 f-13 text-right'>$13.99 x 1</div>
            <div className='col-lg-2 f-13 text-right'>$13.99</div>
          </div>
          <div className='d-flex align-items-center justify-content-end mt-2 p-2' style={{ borderTop: '1px solid #DCDCDC'}}>
            <Button
              style={{ background: '#449242', color: '#fff' }}
            >
              Fulfill item
            </Button>
          </div>
        </div>
        <div className={clsx(classes.card, 'pt-3')}>
          <div className='d-flex align-items-center p-2'>
            <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: '#BAE7D2' }}>
              <IconCircleCheck size={20} stroke={3} />
            </div>
            <div className='ml-2'><strong>Fulfilled (3)</strong> #212704149-GBS-F2</div>
          </div>
          <div className='d-flex align-items-center row mt-3 p-2'>
            <div className='col-lg-2'>
              <img src={'https://d5nunyagcicgy.cloudfront.net/external_assets/hero_examples/hair_beach_v391182663/original.jpeg'} style={{ width: '100%', height: 'auto' }} />
            </div>
            <div className='col-lg-6 f-13'>
              <div><a href="#">HIPPI LIFE 2022</a></div>
              <div>Face Mask 3.5D / 26cm X 16cm / 1PC</div>
              <div>SKU: POD_202013958</div>
              <div>Custom Info: Name: ”PeterParker”</div>
            </div>
            <div className='col-lg-2 f-13 text-right'>$13.99 x 1</div>
            <div className='col-lg-2 f-13 text-right'>$13.99</div>
          </div>
        </div>
        <div className={clsx(classes.card, 'pt-3')}>
          <div className='d-flex align-items-center p-2'>
            <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: 'orange' }}>
              <IconCircleDashed size={20} stroke={3} />
            </div>
            <div className='ml-2'><strong>Authorized</strong></div>
          </div>
          <div className='d-flex align-items-center mt-3 p-2'>
            <List className='w-100'>
              <ListItem style={{ padding: 0 }}>
                <ListItemText 
                  style={{ width: '20%' }} 
                  secondary={"Discount"}
                />
                <ListItemText 
                  style={{ width: '60%' }} 
                  secondary={"VT_M4o9pPzk5d"} 
                />
                <ListItemText 
                  style={{ width: '20%', textAlign: 'right' }}
                  secondary={"Applied"}
                />
              </ListItem>
              <ListItem style={{ padding: 0 }}>
                <ListItemText 
                  style={{ width: '20%' }} 
                  secondary={"Subtotal"}
                />
                <ListItemText 
                  style={{ width: '60%' }} 
                  secondary={"1 item"} 
                />
                <ListItemText 
                  style={{ width: '20%', textAlign: 'right' }}
                  secondary={"$13.99"}
                />
              </ListItem>
              <ListItem style={{ padding: 0 }}>
                <ListItemText 
                  style={{ width: '20%' }} 
                  secondary={"Shipping"}
                />
                <ListItemText 
                  style={{ width: '60%' }} 
                  secondary={"Secure Shipping (Include Tracking) (0.05 kg)"} 
                />
                <ListItemText 
                  style={{ width: '20%', textAlign: 'right' }}
                  secondary={"$5.99"}
                />
              </ListItem>
              <ListItem style={{ padding: 0 }}>
                <ListItemText 
                  style={{ width: '20%' }} 
                  secondary={"Total"}
                  classes={{ secondary: classes.textBold }}
                />
                <ListItemText 
                  style={{ width: '60%' }} 
                  secondary={""} 
                />
                <ListItemText 
                  style={{ width: '20%', textAlign: 'right' }}
                  secondary={"$19.98"}
                  classes={{ secondary: classes.textBold }}
                />
              </ListItem>
            </List>
          </div>
          <Divider className='mt-2' />
          <div className='d-flex align-items-center justify-content-between mt-3 mb-3 pl-2 pr-2'>
            <span>Paid by customer</span>
            <span>$19.98</span>
          </div>
          <div className='d-flex align-items-center justify-content-end mt-2 p-2' style={{ borderTop: '1px solid #DCDCDC'}}>
            <Button
              style={{ background: '#449242', color: '#fff' }}
            >
              Capture Payment
            </Button>
          </div>
        </div>
        <CustomTimeline 
          items={[]}
          title="Timeline"
          titleCheckbox="Show comments"
          buttonText={"Send"}
          itemClassName={'ml-1'}
          renderItem={(item) => item.name}
        />
      </Grid>
      <Grid item xs={4}>
        <div className={clsx(classes.card, 'p-3')}>
          <div className="d-flex align-items-center justify-content-between">
            <strong>Notes</strong>
            <a 
              href="#" 
              onClick={(e) => {
                e.preventDefault()
                e.stopPropagation()
                console.log('1111')
              }}
            >Edit</a>
          </div>
          <div className='mt-2'>
            No notes from customer
          </div>
        </div>
        <div className={classes.card}>
          <div className='d-flex flex-column p-3'>
            <strong>Customer</strong>
            <a 
              className='mt-3'
              href="#" 
              onClick={e => {
                e.stopPropagation()
                e.preventDefault()
                console.log('22222')
              }}
            >Barbra Churchill</a>
            <span>1 order</span>
          </div>
          <Divider className='mt-2' />
          <div className='p-3 mt-2'>
            <div className='d-flex align-items-center justify-content-between'>
              <div style={{ textTransform: 'uppercase' }}>Contact Infomation</div>
              <a 
                href="#" 
                onClick={(e) => {
                  e.preventDefault()
                  e.stopPropagation()
                  console.log('1111')
                }}
              >Edit</a>
            </div>
            <div className='d-flex align-items-center justify-content-between mt-2'>
              <div>barbrachurchill@gmail.com</div>
              <CopyToClipboard text={'11111333332222'} />
            </div>
          </div>
          <Divider className='mt-2' />
          <div className='p-3 mt-2'>
            <div className='d-flex align-items-center justify-content-between'>
              <div style={{ textTransform: 'uppercase' }}>Shipping Address</div>
              <a 
                href="#" 
                onClick={(e) => {
                  e.preventDefault()
                  e.stopPropagation()
                  console.log('1111')
                }}
              >Edit</a>
            </div>
            <div className='d-flex justify-content-between mt-2'>
              <div className='d-flex flex-column'>
                <span>Barbra Churchill</span>
                <span>5549 Oldham Street</span>
                <span>Lincoln NE 68506</span>
                <span>United States</span>
                <span>(402) 570-2216</span>
              </div>
              <CopyToClipboard text={'vvvvvvv'} />
            </div>
          </div>
          <Divider className='mt-2' />
          <div className='p-3 mt-2'>
            <div className='d-flex align-items-center justify-content-between'>
              <div style={{ textTransform: 'uppercase' }}>Billing Address</div>
              <a 
                href="#" 
                onClick={(e) => {
                  e.preventDefault()
                  e.stopPropagation()
                  console.log('1111')
                }}
              >Edit</a>
            </div>
            <div className='d-flex align-items-center justify-content-between mt-2'>
              <div>Same as shipping address</div>
              <CopyToClipboard text={'rrrrrrrrr'} />
            </div>
          </div>
        </div>
      </Grid>
    </Grid>
  )
}

export default OrderDetail