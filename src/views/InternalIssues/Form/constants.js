import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { capitalizeStr } from '../../../utils'
import { Tooltip } from '@material-ui/core'
import { withStyles, makeStyles } from "@material-ui/core/styles";
import SettingsIcon from '@material-ui/icons/Settings';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import StepConnector from '@material-ui/core/StepConnector';
import clsx from 'clsx'
import NotificationsIcon from '@material-ui/icons/Notifications';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import SendIcon from '@material-ui/icons/Send';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import {
  faHandshake,
  faTrashAlt
} from '@fortawesome/free-solid-svg-icons'
import avatar from '../../../assets/images/avatars/matthew.png'


export const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22
  },
  active: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)"
    }
  },
  completed: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)"
    }
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: "#eaeaf0",
    borderRadius: 1
  }
})(StepConnector);

export const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: "#ccc",
    zIndex: 1,
    color: "#fff",
    width: 50,
    height: 50,
    display: "flex",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center"
  },
  active: {
    backgroundImage:
      "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)",
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)"
  },
  completed: {
    backgroundImage:
      "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)"
  }
});

export function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <NotificationsIcon />,
    2: <PersonAddIcon />,
    3: <AddShoppingCartIcon />,
    4: <SendIcon />,
    5: <FontAwesomeIcon icon={faHandshake} size={"lg"} />,
    6: <CheckCircleIcon />
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

export const stepsIssues = [
  "T0", "T1", "T2", "T3", "T4", "T5"
]


export const labelMap = {
  customer_decision: 'Customer decsion',
  refund_amount: 'Refund amount ($)',
  refund_percent: 'Refund percent (%)',
  next_action: 'Next action',
  next_action_note: 'Next action note',
  offered_at: 'Offered at',
  offer_name: 'Offer',
  offer_note: 'Offer note',
  offer_refund_percent: 'Offer refund percent (%)',
  refunded_at: 'Refunded at',
  refunded_note: 'Refund note',
  changed_info_at: 'Changed info at',
  changed_info_note: 'Changed info note',
  link_contact_offer: 'Link contact offer',
  action_deadline: 'Action deadline',
  check_tiff_design_at: 'Check tiff design at',
  tiff_link: 'Tiff link',
  checked_design_at: 'Checked design at',
  design_link: 'Design link',
  resend_order_note: 'Resend order notes',
  tracking_number_replacement: 'Tracking number replacement',
  assignee_id: 'Assignee',
  solution: 'Solution',
  variant_options: 'Variant options',
  sku: 'SKU',
  attachments: 'Attachments',
  custom_data: 'Custom data',
  confirmed_custom_design_at: 'Confirmed custom design at',
  product_id: 'Product ID',
  design_sku: 'Design SKU',
  refund_id: 'Refund ID',
  dod_images: 'DOD Image',
  dod_property: 'DOD Property',
  offer_added_at: 'Added offer at',
  updated_solution_at: 'Updated solution at',
  refund_amount: 'Refund Amount($)',
  extra_fee: 'Extra fee($)',
  carrier: 'Carrier',
  solution_updated_at: 'Updated solution at',
  refunded_on: 'Refunded on'
}


const EventLog = (props) => {
  const [log, setLog] = useState()
  useEffect(() => {
    const logData = props.log
    // if (!(context.ticket.isAdminInHbs() ||
    //   context.ticket.isDesignerInHbs() ||
    //   context.ticket.isDesignerLeaderInHbs() ||
    //   logData.action_code === 'add_custom_design_link'
    // ) && 
    if ('design_link' in (logData?.data || {})) {
      delete logData.data.design_link
    }
    setLog(logData)
  }, [props.log])

  return (
    <>
      {log && <div className='comment' style={{ background: '#fff', borderRadius: 6 }}>
        <div className='avatar'>
        <img src={avatar} />
        </div>
        <div className='content'>
          <a className='author'>{log.actor.name}({log.actor.email})</a>
          <div className='metadata'>
            <div>{log.created_at}</div>
            <div>{log.state}</div>
          </div>
          <div className='text'><i>Commit action <b>{log.action_code}</b> on issue</i></div>
          <div className='text'>
            {Object.keys(log?.data || {}).map(field =>
              <div className='content' key={field}>
                &emsp; <b>&bull; {labelMap[field] || field}:</b> <div style={{ display: 'initial' }} dangerouslySetInnerHTML={{ __html: log.data[field] }} />
              </div>
            )}
          </div>
          <div className='actions'>
            <a></a>
          </div>
        </div>
      </div>}
    </>
  )
}

const CommentContent = (props) => {
  const { content } = props

  return (
    <div className='comment-content' dangerouslySetInnerHTML={{ __html: content }}></div>
  )
}

const CommentLog = (props) => {
  const { log } = props

  return (
    <div className='comment' style={{ background: '#fff', borderRadius: 6 }}>
      <div className='avatar'>
        <img src={avatar} />
      </div>
      <div className='content'>
        <a className='author'>{log.actor.name}({log.actor.email})</a>
        <div className='metadata'>
          <div>{log.created_at}</div>
          <div>{log.state}</div>
        </div>
        <div className='text'>
          <CommentContent content={
            log?.data?.content || '.'
          }></CommentContent>
        </div>
        <div className='actions'>
          <a></a>
        </div>
      </div>
    </div>
  )
}

export const Log = (props) => {
  const { log } = props

  switch (log.action_code) {
    case 'comment':
      return <CommentLog log={log} />
    default:
      return <EventLog log={log} />
  }
}
