/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box, Grid, TextField
} from "@material-ui/core";
import AsyncSelect from 'react-select/async'
import config from "config";
import { FormModal, FormModalSide, FormModalSideStatic, ModalViewImage } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { makeId, capitalizeStr, customStyles, errorStyles } from "../../../utils";
import moment from "moment";
import clsx from 'clsx'
import {
  IconEye, IconArrowLeft
} from "@tabler/icons";
import { Button, Placeholder } from 'rsuite'
import { ForwardForm, CsIssueDetail, OrderDetail, OrderItemDetail, IssueDetail } from './components'
import { stateOptions } from '../List/constants'
import { TinyEditorComponent } from '../../../_components'
import { resolve } from "tinymce/themes/silver/theme";
import { CustomerTab, OtherTab } from './tabs'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  cardNote: {
    backgroundColor: '#E7F8FB',
    border: '1px solid #B7DBE0'
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    textTransform: 'capitalize',
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    },
    '&.authorized': {
      backgroundColor: '#F4D7AA'
    },
    '&.voided': {
      backgroundColor: '#E4E5E7'
    },
    '&.partially_refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&.refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&> div': {
      width: 10,
      height: 10,
      borderRadius: '50%',
    }
  },
  chip_paid: {
    backgroundColor: '#006F53',
  },
  chip_cancelled: {
    backgroundColor: '#626669',
  },
  chip_pending: {
    border: '2px solid #885E16',
  },
  chip_unfulfilled: {
    border: '2px solid #885E16',
  },
  chip_authorized: {
    backgroundColor: '#885E16',
  },
  chip_voided: {
    backgroundColor: '#885E16',
  },
  chip_partially_refunded: {
    border: '2px solid #885E16',
  },
  chip_refunded: {
    border: '2px solid #885E16',
  },
  textBold: {
    fontWeight: 'bold'
  },
  input: {
    fontSize: 14
  }
});

class IssueForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        }
      },
      currentTab: 'customer',
      reply: false,
      update: 0,
      content: null,
      tabData: {
        supplier: {
          items: [],
          isLoading: false
        },
        designer: {
          items: [],
          isLoading: false
        },
        seller: {
          items: [],
          isLoading: false
        },
        cs: {
          items: [],
          isLoading: false
        },
      }
    };
    this.quillRef = React.createRef()
    this.templateContentRef = React.createRef()
    this.noteRef = React.createRef()
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Issues");
    // this.getListItems(this.state.formState.values);
    this.getData(this.props, this.props.match.params.id)
    
  }

  componentWillUpdate(nextProps) {
    if (nextProps.match.params.id != this.props.match.params.id) {
      this.getData(nextProps, nextProps.match.params.id)
    }
  }

  getDataTab = (query) => {
    const data = {
      search_option: 'order_items',
      query
    }
    const allKeys = Object.keys(this.state.tabData)
    const tabData = Object.assign({}, this.state.tabData)
    allKeys.map(key => {
      tabData[key].isLoading = true
    })
    this.setState({ tabData }, () => {
      request.get(`${config.apiUrl}/api/issues`, {...data, department: 'seller'}).then(res => {
        if (res.data.success) {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['seller']['isLoading'] = false
          tabData['seller']['items'] = res.data.data.result
          this.setState({ tabData })
        } else {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['seller']['isLoading'] = false
          this.setState({ tabData })
        }
      }, err => toastr.error(err))
      request.get(`${config.apiUrl}/api/issues`, {...data, department: 'designer'}).then(res => {
        if (res.data.success) {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['designer']['isLoading'] = false
          tabData['designer']['items'] = res.data.data.result
          this.setState({ tabData })
        } else {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['designer']['isLoading'] = false
          this.setState({ tabData })
        }
      }, err => toastr.error(err))
      request.get(`${config.apiUrl}/api/issues`, {...data, department: 'supplier'}).then(res => {
        if (res.data.success) {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['supplier']['isLoading'] = false
          tabData['supplier']['items'] = res.data.data.result
          this.setState({ tabData })
        } else {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['supplier']['isLoading'] = false
          this.setState({ tabData })
        }
      }, err => toastr.error(err))
      request.get(`${config.apiUrl}/api/issues`, {...data, department: 'cs'}).then(res => {
        if (res.data.success) {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['cs']['isLoading'] = false
          tabData['cs']['items'] = res.data.data.result
          this.setState({ tabData })
        } else {
          const tabData = Object.assign({}, this.state.tabData)
          tabData['cs']['isLoading'] = false
          this.setState({ tabData })
        }
      }, err => toastr.error(err))
    })
  }

  getData = (props, id) => {
    const { dispatch } = props;
    dispatch(showLoading())
    request.get(`${config.apiUrl}/api/cs_issues/${id}`, {}).then(res => {
      if (res.data.success) {
        dispatch(hideLoading())
        const formState = Object.assign({}, this.state.formState)
        formState.values = res.data.data
        this.getDataTab(formState.values.order_name)
        this.setState({ formState })
      } else {
        toastr.error(res.data.msg)
        dispatch(hideLoading())
        this.setState({ formState: { values: {} }})
      }
    }, err => {
      toastr.error(err)
      dispatch(hideLoading())
      this.setState({ formState: { values: {} }})
    })
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  forwardMail = (item, type) => {
    this.setState({ reply: false })
    FormModalSide.instance.current.showForm({
      data: {
        selectedItems: []
      },
      side: 'right',
      width: '50vw',
      customView: (submitData, handleChange) => (
        <ForwardForm
          issue={this.state.formState.values}
          submitData={submitData}
          handleChange={handleChange}
          log={item}
          type={type}
        />
      ),
      action: {
        titleAction: 'Forward',
        schema: {
          // title: { presence: { allowEmpty: false, message: '^Required' }},
          forward_to: { presence: { allowEmpty: false, message: '^Required' }},
          content: { presence: { allowEmpty: false, message: '^Required' }},
          kind_id: { presence: { allowEmpty: false, message: '^Required' }},
        },
        onAction: (submitData) => {
          if (submitData.values.selectedItems?.length == 0) {
            toastr.error("Please select order item first")
            resolve(false)
          }
          const allKeys = Object.keys(submitData.values)
          const query = {}
          allKeys.map(key => {
            if (key == 'forward_to') {
              query['dest_department'] = submitData.values[key].id
            } else if (key == 'kind_id') {
              query[key] = submitData.values[key].id
            } else if (key == 'extra') {
              const extra = submitData.values[key]
              if (extra.files) {
                extra.files = extra.files.filter(e => e.status == 'uploaded')
              }

              query['extra'] = extra
            } else if (key == 'selectedItems') {
              query['objects'] = submitData.values[key].map(item => ({id: item, type: 'order_items'}))
            } else if (key == 'content') {
              query['note'] = submitData.values[key]
            } else {
              query[key] = submitData.values[key]
            }
          })

          const extend_extra = {
            forward_from_issue: this.state.formState.values.id,
            forward_issue_type: 'cs_issues',
            order_items: submitData.values.selectedItems,
            dispute_payment_gateway: submitData.values.dispute_payment_gateway
          }

          query['extra'] = {
            ...(query['extra'] || {}),
            ...extend_extra
          }

          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/issues`, query).then(res => {
              if (res.data.success) {
                resolve(true)
                FormModalSide.instance.current.setState({ open: false })
                toastr.success("Forward successful")
                this.getDataTab(this.state.formState.values.order_name)
                this.getData(this.props, this.props.match.params.id)
                // add comment to issue
                const data = {
                  action_code: 'comment',
                  action_data: {
                    name: this.props.user.data.name,
                    email: this.props.user.data.email || "",
                    content: submitData.values.content,
                    user_type: 'customer_support'
                  },
                  object_id: res.data.data.id,
                  object_type: 'issues'
                }
                request.post_json(`${config.apiUrl}/api/actions/commit`, data)

              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  editOrder = () => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <OrderDetail 
          order={this.state.formState.values.order}
          user={this.props.user}
        />
      )
    })
  }

  getOptionState = (cs_state) => {
    const option = stateOptions.find(e => e.id == cs_state)
    return option
  }

  detailItem = (item) => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <OrderItemDetail item={item} isClosed={this.isClosed()} />
      )
    })
  }

  onCreateTemplate = (inputValue, callback) => {
    const { classes } = this.props;
    FormModal.instance.current.showForm({
      data: {
        name: inputValue || ''
      },
      size: 'md',
      title: "Create Template",
      customView: (submitData, handleChange) => (
        <div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              Template Name
            </Box>
            <TextField 
              name="name"
              onChange={handleChange}
              value={submitData.values.name}
              variant="outlined"
              margin="dense"
              fullWidth
              placeholder="Template name..."
              InputProps={{
                classes: {
                  input: classes.input
                }
              }}
              error={submitData.errors?.name}
              helperText={submitData.errors?.name ? submitData.errors?.name[0] : ''}
            />
          </div>
          <div className="mt-2 issue-description">
            <Box fontWeight={"bold"} className="f-14">
              Content
            </Box>
            <TinyEditorComponent 
              className={submitData.errors?.content ? 'error' : ''}
              style={{ width: '100%' }}
              ref={this.templateContentRef}
              content={submitData.values.content}
              id={makeId(20)}
              onEditorChange={(content) => {
                const event = {
                  target: {
                    name: 'content',
                    value: content
                  }
                }
                handleChange(event)
              }}
            />
            {submitData.errors?.content && <small style={{ color: 'red'}}>{submitData.errors.content[0]}</small>}
          </div>
        </div>
      ),
      action: {
        titleAction: 'Save',
        schema: {
          name: {
            presence: { allowEmpty: false, message: '^Required' }
          },
          content: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_templates`, submitData.values).then(res => {
              if (res.data.success) {
                // this.setState({ reply: true, content: submitData.values.content })
                resolve(true)
                callback()
              } else {
                toastr.error(res.data.msg)
              }
            }, err => toastr.error(err))
          })
        }
      }
    })
  }

  addReply = () => {
    const { formState } = this.state
    const data = {
      action_code: 'comment',
      action_data: {
        name: this.props.user.data.name,
        email: this.props.user.data.email || "",
        content: this.state.content,
        user_type: 'customer_support'
      },
      object_id: formState.values.id,
      object_type: 'cs_issues'
    }
    request.post_json(`${config.apiUrl}/api/cs_actions/commit`, data).then(res => {
      if (res.data.success) {
        const formState = Object.assign({}, this.state.formState)
        formState['values']['action_logs'].push(res.data.data.log)
        this.setState({ reply: false, content: '', formState })
      } else {
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }

  addNote = () => {
    const { formState } = this.state;
    FormModal.instance.current.showForm({
      data: {},
      title: "Add note",
      customView: (submitData, handleChange) => (
        <div className="issue-description">
          <TinyEditorComponent 
            className={submitData.errors?.note ? 'error' : ''}
            style={{ width: '100%' }}
            ref={this.noteRef}
            content={submitData.values.note}
            id={makeId(20)}
            onEditorChange={(content) => {
              const event = {
                target: {
                  name: 'note',
                  value: content
                }
              }
              handleChange(event)
            }}
          />
          {submitData.errors?.note && <small style={{ color: 'red'}}>{submitData.errors.note[0]}</small>}
        </div>
      ),
      action: {
        titleAction: 'Add',
        schema: {
          note: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          const data = {
            action_code: 'add_note',
            action_data: {
              note: submitData.values.note
            },
            object_id: formState.values.id,
            object_type: 'cs_issues'
          }
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_actions/commit`, data).then(res => {
              if (res.data.success) {
                const formState = Object.assign({}, this.state.formState)
                formState['values']['action_logs'].push(res.data.data.log)
                this.setState({ formState })
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  deleteComment = (item) => {
    const { formState } = this.state;
    FormModal.instance.current.showForm({
      title: 'Delete comment',
      customView: (submitData, handleChange) => (
        <div className="col-12">Are you sure to delete this comment?</div>
      ),
      action: {
        titleAction: 'Delete',
        color: 'secondary',
        onAction: (submitData) => {
          const query = {
            action_code: 'delete_comment',
            action_data: {
              action_log_id: item.id
            },
            object_id: formState.values.id,
            object_type: 'cs_issues'
          }
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_actions/commit`, query).then(res => {
              if (res.data.success) {
                const formState = Object.assign({}, this.state.formState)
                formState['values']['action_logs'] = formState.values.action_logs.map(e => e.id == item.id ? res.data.data.log : e)
                this.setState({ formState })
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  issueDetail = (issue) => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      title: <div className="d-flex flex-column">
        <div className="f-16 font-weight-bold">
          [{issue.code}] {issue.title}
        </div>
        <span className="f-12">{moment(issue.created_at).format('LLLL')}</span>
      </div>,
      customView: () => (
        <IssueDetail 
          issue={issue} 
          user={this.props.user} 
          isClosed={this.isClosed()}
          tab={this.state.currentTab}
          onRefresh={(tab, issue) => {
            const tabData = Object.assign({}, this.state.tabData)
            tabData[tab].items = tabData[tab].items.map(e => e.id == issue.id ? issue : e)
            this.setState({ tabData })
          }}
        />
      )
    })
  }

  viewAsCustomer = () => {
    const { formState } = this.state
    request.post_json(`${config.apiUrl}/api/view_as_customer`, {email: formState.values.customer?.email}).then(res => {
      window.open(res.data.data.url, '_blank')
    }).catch(err => console.log(err))
  }

  viewImage = (img) => {
    ModalViewImage.instance.current.showForm({
      data: img
    })
  }

  loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  }

  handleInputChange = (newValue) => {
    return newValue;
  }
  updateCsState = () => {
    const { formState } = this.state
    FormModal.instance.current.showForm({
      data: {},
      title: 'Update CS State',
      customView: (submitData, handleChange) => (
        <div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              CS State
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => resolve([
                  {key: 0, value: 'open', text: 'Open'},
                  {key: 1, value: 'pending', text: 'Pending'},
                  {key: 2, value: 'resolved', text: 'Resolved'},
                  {key: 3, value: 'wait_for_customer', text: 'Wait for Customer'},
                  {key: 4, value: 'wait_for_third_party', text: 'Wait for Third Party'},
                  {key: 5, value: 'need_1st_response', text: 'Need 1st response'}
                ]))
              })}
              defaultOptions
              onInputChange={this.handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ text }) => text}
              getOptionValue={({ value }) => value}
              placeholder="CS State"
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'cs_state',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.cs_state}
              styles={customStyles}
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Save',
        onAction: (submitData) => {
          const allKeys = Object.keys(submitData.values)
          const query = {}
          allKeys.map(key => {
            query[key] = submitData.values[key].value
          })
          const data = {
            action_code: 'change_cs_state',
            action_data: query,
            object_id: formState.values.id,
            object_type: 'cs_issues'
          }
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_actions/commit`, data).then(res => {
              if (res.data.success) {
                const formState = Object.assign({}, this.state.formState)
                formState['values']['action_logs'].push(res.data.data.log)
                allKeys.map(key => {
                  formState.values[key] = query[key]
                })
                this.setState({ formState })
                toastr.success("Update successfully")
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }
  updateKindLevel = () => {
    const { formState } = this.state
    FormModal.instance.current.showForm({
      data: {},
      title: 'Update Issue Kind',
      customView: (submitData, handleChange) => (
        <div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              Kind level 1
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.apiUrl}/api/ticket_field/level_1`, {}).then(res => {
                    if (res.data.success) {
                      const result = res.data.data.options || []
                      const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                      resolve(options)
                    } else {
                      resolve([])
                    }
                  }, err => resolve([]))
                })
              })}
              defaultOptions
              onInputChange={this.handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ text }) => text}
              getOptionValue={({ key }) => key}
              placeholder="Kind level 1..."
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'kind_level_1',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.kind_level_1}
              styles={customStyles}
            />
          </div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              Kind level 2
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.apiUrl}/api/ticket_field/level_2`, {}).then(res => {
                    if (res.data.success) {
                      const result = res.data.data.options || []
                      const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                      resolve(options)
                    } else {
                      resolve([])
                    }
                  }, err => resolve([]))
                })
              })}
              defaultOptions
              onInputChange={this.handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ text }) => text}
              getOptionValue={({ key }) => key}
              placeholder="Kind level 2..."
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'kind_level_2',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.kind_level_2}
              styles={customStyles}
            />
          </div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              Kind level 3
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.apiUrl}/api/ticket_field/level_3`, {}).then(res => {
                    if (res.data.success) {
                      const result = res.data.data.options || []
                      const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                      resolve(options)
                    } else {
                      resolve([])
                    }
                  }, err => resolve([]))
                })
              })}
              defaultOptions
              onInputChange={this.handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ text }) => text}
              getOptionValue={({ key }) => key}
              placeholder="Kind level 3..."
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'kind_level_3',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.kind_level_3}
              styles={customStyles}
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Save',
        onAction: (submitData) => {
          const allKeys = Object.keys(submitData.values)
          const query = {}
          allKeys.map(key => {
            query[key] = submitData.values[key].value
          })
          const data = {
            action_code: 'update_issue_type',
            action_data: query,
            object_id: formState.values.id,
            object_type: 'cs_issues'
          }
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_actions/commit`, data).then(res => {
              if (res.data.success) {
                const formState = Object.assign({}, this.state.formState)
                formState['values']['action_logs'].push(res.data.data.log)
                allKeys.map(key => {
                  formState.values[key] = query[key]
                })
                this.setState({ formState })
                toastr.success("Update successfully")
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  updateAgent = () => {
    const { formState } = this.state
    FormModal.instance.current.showForm({
      data: {},
      title: 'Update agent',
      customView: (submitData, handleChange) => (
        <div className="mt-2">
          <Box fontWeight={"bold"} className="f-14">
            Agent
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
              return new Promise(resolve => {
                request.get(`${config.apiLoginUrl}/api/v1/users`, {department: 'cs', name: inputValue}).then(res => {
                  if (res.data.success) {
                    resolve(res.data.data.result)
                  } else {
                    resolve([])
                  }
                }, err => resolve([]))
              })
            })}
            defaultOptions
            onInputChange={this.handleInputChange}
            menuPortalTarget={document.body}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ uid }) => uid}
            placeholder="Agent"
            onChange={(value) => {
              const event = {
                target: {
                  name: 'assignee',
                  value
                }
              }
              handleChange(event)
            }}
            value={submitData.values.assignee}
            styles={submitData.errors?.assignee ? errorStyles : customStyles}
          />
        </div>
      ),
      action: {
        titleAction: 'Save',
        schema: {
          assigne: {
            presence: { allowEmpty: false, message: '^Required'}
          }
        },
        onAction: (submitData) => {
          const data = {
            action_code: 'assign',
            action_data: submitData.values,
            object_id: formState.values.id,
            object_type: 'cs_issues'
          }
          return new Promise(resolve => {
            request.post_json(`${config.apiUrl}/api/cs_actions/commit`, data).then(res => {
              if (res.data.success) {
                const formState = Object.assign({}, this.state.formState)
                formState['values']['action_logs'].push(res.data.data.log)
                formState.values['customer_support'] = submitData.values.assignee
                formState.values['customer_support_id'] = submitData.values.assignee.uid
                this.setState({ formState })
                toastr.success("Update successfully")
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  isClosed = () => {
    const { formState } = this.state
    return formState.values.state == 'closed' || formState.values.cs_state == 'closed'
  }

  render() {
    const { classes, user } = this.props;
    const { formState, reply, currentTab } = this.state;

    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <FormModalSideStatic />
        <ModalViewImage />
        {formState.values.id && 
        <Grid container spacing={3}>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <div className="issue-header">
              <div className="d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center">
                  <div 
                    className="d-flex align-items-center justify-content-center cursor-pointer" 
                    style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}
                    onClick={() => this.props.history.push('/issues')}
                  >
                    <IconArrowLeft size={30} stroke={2} />
                  </div>
                  <div className="ml-2 f-18 font-weight-bold" style={{ color: '#006F53' }}>{formState.values.code}</div>
                  <span className="ml-2 f-14">Reported at: {moment(formState.values.created_at).format('lll')}</span>
                </div>
                <div className="d-flex align-items-center">
                  <div className='f-10 br-6 text-center width-fit-content' style={{ padding: '3px 5px', backgroundColor: this.getOptionState(formState.values.cs_state)?.bgColor || '#000', color: this.getOptionState(formState.values.cs_state)?.color || '#fff' }}>{this.getOptionState(formState.values.cs_state)?.name}</div>
                  <div className="f-14 ml-2">Due by {moment(formState.values.created_at).add(24, 'hours').format('llll')}</div>
                </div>
              </div>
              <div className="d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center">
                  <div className="mr-1 f-17">{formState.values.kind ? capitalizeStr(formState.values.kind, '_') : ''} | {formState.values.order?.name}</div>
                  {formState.values.order?.financial_status && <div className={clsx(classes.chip, formState.values.order?.financial_status || '')}><div className={clsx(classes[`chip_${formState.values.order?.financial_status}`], 'mr-1')} /> {formState.values.order?.financial_status.replace('_', ' ')}</div>}
                  {formState.values.order?.cancelled && <div className={clsx(classes.chip, 'cancelled')}><div className={clsx(classes.chip_cancelled, 'mr-1')} /> Cancelled</div>}
                  {formState.values.order?.fulfillment_status && <div className={clsx(classes.chip, formState.values.order?.fulfillment_status || '')}><div className={clsx(classes[`chip_${formState.values.order?.fulfillment_status || ''}`], 'mr-1')} /> {formState.values.order?.fulfillment_status}</div>}
                </div>
                <Button
                  style={{ border: '1px solid #ccc' }}
                  onClick={this.viewAsCustomer}
                >
                  <IconEye className="mr-1" size={20} stroke={2} />Customer screen
                </Button>
              </div>
            </div>
            <div className="issue-note">
              {formState.values.action_logs && formState.values.action_logs.filter(e => e.action_code == 'add_note').map(note => (
                <div key={note.id} className={clsx(classes.card, classes.cardNote, 'p-2')}>
                  <span className="ml-2">
                    <b>Note from {note.actor?.name}</b>
                    <i className="ml-2">({moment(note.created_at).add(7, 'hours').format('LLL')})</i>
                  </span>
                  <div className="ml-2 mt-2" dangerouslySetInnerHTML={{ __html: note.action_data.note }} />
                </div>
              ))}
            </div>
            <div className={classes.card}>
              <Box style={{ borderBottom: "0.5px solid #ccc" }}>
                <AntTabs
                  currentTab={this.state.currentTab}
                  handleChangeTab={this.handleChangeTab}
                  items={[
                    { value: 'customer', label: 'Customer' },
                    { value: 'supplier', label: 'Supplier' },
                    { value: 'designer', label: 'Designer' },
                    { value: 'seller', label: 'Seller' },
                    { value: 'cs', label: 'CS Team' },
                  ]}
                />
              </Box>
              {currentTab == 'customer' &&
                <CustomerTab 
                  reply={reply}
                  formState={formState}
                  forwardMail={this.forwardMail}
                  deleteComment={this.deleteComment}
                  content={this.state.content}
                  setState={(data) => this.setState(data)}
                  addReply={this.addReply}
                  addNote={this.addNote}
                  onCreateTemplate={this.onCreateTemplate}
                  viewImage={this.viewImage}
                  isClosed={this.isClosed()}
                />
              }
              {["designer", "seller", "supplier", "cs"].includes(currentTab) &&
                <div className="p-3">
                  <OtherTab 
                    data={this.state.tabData[currentTab]}
                    issueDetail={this.issueDetail}
                    isClosed={this.isClosed()}
                  />
                </div>
              }
              
            </div>
          </Grid>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            {formState.values.id ?
              <CsIssueDetail
                editOrder={this.editOrder}
                showHistory
                issue={formState.values}
                detailItem={this.detailItem}
                updateKindLevel={this.updateKindLevel}
                updateCsState={this.updateCsState}
                updateAgent={this.updateAgent}
                isClosed={this.isClosed()}
              /> : <Placeholder.Grid rows={20} columns={2} active />
            }
          </Grid>
        </Grid>
        }
      </div>
    );
  }
}


IssueForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(IssueForm)));
export { connectedList as IssueForm };
