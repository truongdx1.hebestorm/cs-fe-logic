import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
  IconCornerUpRight, IconTrash, IconPaperclip, IconExternalLink, IconDownload
} from "@tabler/icons";
import { Avatar } from 'rsuite'
import { stringToColor } from '../../../../utils'
import moment from 'moment';
import { history } from '../../../../_helpers'
import {Table, TableHead, TableRow, TableCell, TableBody} from '@material-ui/core'


const useStyles = makeStyles(theme => ({
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  headName: {
    background: '#E8F5FE',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none',
      borderTopRightRadius: 6
    },
    '&:first-child': {
      borderTopLeftRadius: 6
    },
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    background: '#fff',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellClick: {
    color: '#4CA75E',
    cursor: 'pointer',
    '&:hover': {
      textDecoration: 'underline'
    }
  }
}));

const Conversation = (props) => {
  const { data, type, noAction } = props;
  const classes = useStyles()

  const detailIssue = (issue) => {
    history.push(`/issues/${issue.id}`, { from: window.location.pathname })
  }

  return (
    <div className="p-3" style={{ background: props.background || '#fff', borderBottom: '1px solid #D8D8D8' }}>
      {type == 'issue' && 
        <>
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center">
              <Avatar style={{ backgroundColor: stringToColor(data?.customer?.name, true)[0], color: stringToColor(data?.customer?.name, true)[1] }}>{data?.customer?.name[0]}</Avatar>
              <div className="ml-2">
                <p style={{ color: '#1B79E5', fontSize: 16 }}>{data?.customer?.name}</p>
                <small>{moment(data?.created_at).fromNow()} ({moment(data?.created_at).format('LLL')})</small>
              </div>
            </div>
            {!noAction && <div 
              className="d-flex align-items-center justify-content-center p-2" 
              style={{ border: '1px solid #ccc', borderRadius: 6, cursor: props.isClosed ? 'not-allowed' : 'pointer' }}
              onClick={() => {
                if (!props.isClosed) {
                  props.actionForward()
                }
              }}
            >
              <IconCornerUpRight size={20} stroke={2} />
            </div>}
          </div>
          <div className='mt-3' dangerouslySetInnerHTML={{ __html: data?.content }} />
          {data.extras && data.extras.attachments?.length > 0 &&
            <div className='mt-3 attachment-wrapper'>
              {data.extras.attachments.map((file, index) => (
                <div className='p-2 mr-1 attachment-inline' key={index}>
                  <div className='d-flex align-items-center'>
                    <IconPaperclip size={20} stroke={2} />
                    <div className='ml-2'>{file.filename}</div>
                    <div className='ml-4'>
                      {file.type.split('/')[0] == 'image' && 
                        <IconExternalLink 
                          cursor={"pointer"} size={20} stroke={2} 
                          // onClick={() => props.viewImage(file.file_url)}
                        />
                      }
                      <IconDownload className='ml-1' cursor={"pointer"} size={20} stroke={2} />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          }

          {props.forwardIssues && props.forwardIssues.length > 0 &&
            <div className='mt-2' style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
              <Table className='table-list'>
                <TableBody>
                  <TableRow className={classes.tableRow}>
                    <TableCell className={classes.headName} style={{ width: '10%' }}>Order Item ID</TableCell>
                    <TableCell className={classes.headName} style={{ width: '12%' }}>Code</TableCell>
                    <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
                    <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
                    <TableCell className={classes.headName} style={{ width: '10%' }}>Order ID</TableCell>
                    <TableCell className={classes.headName} style={{ width: '20%' }}>Note</TableCell>
                    <TableCell className={classes.headName} style={{ width: '15%' }}>Created At</TableCell>
                    <TableCell className={classes.headName} style={{ width: '13%' }}>Forward To</TableCell>
                  </TableRow>
                  {props.forwardIssues.map((forwardIssue, index) => 
                    <TableRow className={classes.tableRow} key={index}>
                      <TableCell className={classes.cellContent}>{forwardIssue.extra?.order_items?.join(', ')}</TableCell>
                      <TableCell className={classes.cellContent}><span onClick={() => detailIssue(forwardIssue)} className={classes.cellClick}>{forwardIssue.code}</span></TableCell>
                      <TableCell className={classes.cellContent}>{forwardIssue.level}</TableCell>
                      <TableCell className={classes.cellContent}>{forwardIssue.kind?.name}</TableCell>
                      <TableCell className={classes.cellContent}>{props.orderName}</TableCell>
                      <TableCell className={classes.cellContent}><div className='line-clamp-1' dangerouslySetInnerHTML={{ __html: forwardIssue.note }} /></TableCell>
                      <TableCell className={classes.cellContent}>{moment(forwardIssue.created_at).format('lll')}</TableCell>
                      <TableCell className={classes.cellContent}>{forwardIssue.assignee?.name || forwardIssue.assignee_id}</TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          }
        </>
      }
      {type == 'action_logs' && 
        <>
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center">
              <Avatar style={{ textTransform: 'uppercase', backgroundColor: stringToColor(data?.action_data.name, true)[0], color: stringToColor(data?.action_data.name, true)[1] }}>{data?.action_data.name[0]}</Avatar>
              <div className="ml-2">
                <p style={{ color: '#1B79E5', fontSize: 16 }}>{data?.action_data.name}</p>
                <small>{moment(data?.created_at).fromNow()} ({moment(data?.created_at).format('LLL')})</small>
              </div>
            </div>
            {!noAction && <div className='d-flex'>
              {data?.action_data.user_type == 'customer_support' && 
              <div 
                className="d-flex align-items-center justify-content-center p-2" 
                style={{ border: '1px solid #ccc', borderRadius: 6, cursor: props.isClosed ? 'not-allowed' : 'pointer' }}
                onClick={() => {
                  if (!props.isClosed) {
                    props.actionDelete()
                  }
                }}
              >
                <IconTrash size={20} stroke={2} />
              </div>
              }
              <div 
                className="d-flex align-items-center justify-content-center p-2 ml-1" 
                style={{ border: '1px solid #ccc', borderRadius: 6, cursor: props.isClosed ? 'not-allowed' : 'pointer' }}
                onClick={() => {
                  if (!props.isClosed) {
                    props.actionForward()
                  }
                }}
              >
                <IconCornerUpRight size={20} stroke={2} />
              </div>
            </div>
            }
            
          </div>
          <div className='mt-3' dangerouslySetInnerHTML={{ __html: data?.action_data.content }} />
          {data?.action_data?.data && data.action_data.data.attachments?.length > 0 &&
            <div className='mt-3 attachment-wrapper'>
              {data.action_data.data.attachments.map((file, index) => (
                <div className='p-2 mr-1 attachment-inline' key={index}>
                  <div className='d-flex align-items-center'>
                    <IconPaperclip size={20} stroke={2} />
                    <div className='ml-2'>{file.filename}</div>
                    <div className='ml-4'>
                      {file.type.split('/')[0] == 'image' && 
                        <IconExternalLink cursor={"pointer"} size={20} stroke={2} />
                      }
                      <IconDownload className='ml-1' cursor={"pointer"} size={20} stroke={2} />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          }
        </>
      }
    </div>
  )
}

export default Conversation;