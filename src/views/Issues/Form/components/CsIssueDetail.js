import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider,
  Table, TableRow, TableCell, TableBody, Tooltip
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import clsx from 'clsx'
import { CopyToClipboard } from '../../../../common'
import toastr from '../../../../common/toastr'
import { history } from '../../../../_helpers'
import {
  IconExternalLink
} from "@tabler/icons";
import { request } from '../../../../_services/request'
import moment from 'moment';
import config from 'config';
import { Button, Dropdown } from 'rsuite'
import { stateOptions } from '../../List/constants'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  tableRow: {
    '&:hover': {
      backgroundColor: '#F5F5F5'
    }
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const ListItemInfo = ({ title, value, icon, divider, fullWidth }) => {
  const classes = useStyles()
  return (
    <>
      <ListItem style={{ padding: '4px 0px' }}>
        <ListItemText 
          style={{ width: '40%' }} 
          secondary={title}
          classes={{ secondary: classes.textBold }}
        />
        <ListItemText 
          style={{ width: '60%' }} 
          disableTypography
          secondary={
            <div className='d-flex'>
              <div className={fullWidth ? 'w-100' : ''}>{value}</div>
              {icon && <div className='ml-2'>{icon}</div>}
            </div>
          } 
        />
      </ListItem>
      {divider && <Divider />}
    </>
  )
}

const CsIssueDetail = (props) => {
  const classes = useStyles()
  const { issue } = props;
  const [issueReference, setIssueReference] = useState([])
  const [isLoadingIssue, setLoadingIssue] = useState(false)

  useEffect(() => {
    if (issue.order_name) {
      setLoadingIssue(true)
      request.get(`${config.apiUrl}/api/cs_issues`, {query: issue.order_name, search_option: 'order_name', exclude_ids: JSON.stringify([issue.id])}).then(res => {
        setLoadingIssue(false)
        if (res.data.success) {
          setIssueReference(res.data.data.result)
        } else {
          toastr.error(res.data.msg)
        }
      }, err => toastr.error(err))
    } else {
      setIssueReference([])
    }
  }, [issue]);

  const getOptionState = (cs_state) => {
    const option = stateOptions.find(e => e.id == cs_state)
    return option
  }

  return (
    <div>
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2 d-flex align-items-center justify-content-between">
          <strong className='f-16' style={{ color: '#449242' }}>Issue Info</strong>
          {/* <Button 
            appearance='link'
            style={{ color: '#3788E8', fontSize: 15 }}
            onClick={() => props.editIssue()}
            disabled={props.isClosed}
          >
            Edit
          </Button> */}
          <Dropdown 
            className='issue-edit-dropdown'
            noCaret 
            disabled={props.isClosed} 
            title="Edit"
            placement='bottomEnd'
          >
            <Dropdown.Item onSelect={props.updateAgent}>Agent</Dropdown.Item>
            <Dropdown.Item onSelect={props.updateKindLevel}>Kind level</Dropdown.Item>
            <Dropdown.Item onSelect={props.updateCsState}>CS State</Dropdown.Item>
          </Dropdown>
        </div>
        <Divider />
        <div className="pl-3 pr-3">
          <List>
            <ListItemInfo title="Company:" value={issue.company_id?.name} divider />
            <ListItemInfo title="Agent:" value={issue.customer_support?.name} divider />
            <ListItemInfo title="Level 1:" value={issue.kind_level_1} divider />
            <ListItemInfo title="Level 2:" value={issue.kind_level_2} divider />
            <ListItemInfo title="Level 3:" value={issue.kind_level_3} />
          </List>
        </div>
      </div>
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2 d-flex align-items-center justify-content-between">
          <strong className='f-16' style={{ color: '#449242' }}>General Info</strong>
          <Button 
            appearance='link'
            style={{ color: '#3788E8', fontSize: 15 }}
            onClick={props.editOrder}
            // disabled={props.isClosed}
          >
            Edit
          </Button>
        </div>
        <Divider />
        <div className="pl-3 pr-3">
          <List>
            <ListItemInfo 
              title="Order Name:" 
              value={issue.order_name}
              divider 
              icon={<IconExternalLink size={20} stroke={2} />}
              action={() => console.log('1111')}
            />
            <ListItemInfo 
              title="Created At:" 
              value={<span>{issue.order?.ordered_at ? moment(issue.order?.ordered_at).format('LLL') : ''} from <a href={`https://${issue?.order?.store?.domain}`} target="_blank">{issue.order?.store?.name}</a></span>} 
              divider
            />
            <ListItemInfo 
              title="Customer Name:" 
              value={<a href='#'>{issue.order?.shipping_address?.name}</a>}
              divider
            />
            <ListItemInfo 
              title="Contact Info:" 
              value={issue.order?.customer_info?.email}
              divider
              icon={<CopyToClipboard text={issue.order?.customer_info?.email} />}
            />
            <ListItemInfo 
              title="Phone:" 
              value={issue.order?.shipping_address?.phone}
              divider
              icon={<CopyToClipboard text={issue.order?.shipping_address?.phone} />}
            />
            <ListItemInfo 
              title="Shipping Address:" 
              value={
                <div>
                  <p>{issue.order?.shipping_address?.name}</p>
                  <p>{issue.order?.shipping_address?.address1}</p>
                  <p>{issue.order?.shipping_address?.zip} - {issue.order?.shipping_address?.city}</p>
                  <p>{issue.order?.shipping_address?.country}</p>
                </div>
              } 
              divider
              icon={<CopyToClipboard text={`${issue.order?.shipping_address?.name} ${issue.order?.shipping_address?.address1} ${issue.order?.shipping_address?.zip} ${issue.order?.shipping_address?.city} ${issue.order?.shipping_address?.country}`} />}
            />
            <ListItemInfo 
              title="Subtotal:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span>{issue.order_items?.length || 0} Item(s)</span>
                  <span>{issue.order?.subtotal_price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Shipping Fee:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span></span>
                  <span>{issue.order?.total_shipping?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Tip:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span></span>
                  <span>{issue.order?.total_tip?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Total:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span></span>
                  <span>{issue.order?.total_price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
              divider
            />
            <ListItemInfo 
              title="Note:" 
              value={<i>{issue.order?.note}</i>}
            />
          </List>
        </div>
      </div>
      <div className={classes.card}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontSize: 16, fontWeight: 'bold', color: '#449242' }}>
                Order Detail
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Lineitem SKU</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Lineitem Name</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Link Design/Ali/Sup</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Supplier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Tracking</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Carrier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN uploaded at</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN status</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Est time</TableCell>
            </TableRow>
            {issue.order_items && issue.order_items.map(item => (
              <TableRow 
                className={clsx(classes.tableRow, 'cursor-pointer')} 
                key={item.id}
                onClick={() => props.detailItem(item)}
              >
                <TableCell className={classes.cellContent}>{item.sku}</TableCell>
                <TableCell className={classes.cellContent}>
                  <Tooltip placement='top' arrow title={item.lineitem_name}>
                    <span className='line-clamp-1'>{item.lineitem_name}</span>
                  </Tooltip>
                </TableCell>
                <TableCell className={classes.cellContent}>Link</TableCell>
                <TableCell className={classes.cellContent}>{item.level}</TableCell>
                <TableCell className={classes.cellContent}>{item.supplier?.name}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.name).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.carrier).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.imported_at).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.status).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <div className={clsx(classes.card, 'p-3')}>
        <div className="f-16 font-weight-bold" style={{ color: '#449242'}}>Issues Timeline</div>
        {issueReference.length > 0 && issueReference.map(issue => (
          <div className={clsx(classes.card, 'p-3 mt-2')} key={issue.id}>
            <div className='d-flex'>
              <span 
                className='cursor-pointer' 
                onClick={() => {
                  history.push(`/issues/${issue.id}`)
                }}
              >
                {issue.code}
              </span>
              <div 
                className='ml-2 f-12 br-6 text-center width-fit-content' 
                style={{ padding: '3px 5px', backgroundColor: getOptionState(issue.cs_state)?.bgColor || '#000', color: getOptionState(issue.cs_state)?.color || '#fff' }}
              >
                {getOptionState(issue.cs_state).name}
              </div>
            </div>
          </div>
        ))}
        {issueReference.length == 0 && 
          <div className='mt-2'>No issue reference</div>
        }
      </div>
    </div>
  )
}

export default CsIssueDetail