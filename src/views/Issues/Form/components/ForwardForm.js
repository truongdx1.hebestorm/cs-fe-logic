import React, { useRef, useEffect } from 'react'
import { Box, Table, TableRow, TableCell, TableBody, Checkbox, Button, Tooltip, TextField, FormGroup, FormControlLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { TinyEditorComponent } from '../../../../_components'
import { cssConstants } from '../../../../_constants'
import { request } from '../../../../_services/request'
import { Conversation } from '../components'
import AsyncSelect from 'react-select/async'
import { customStyles, makeId } from '../../../../utils'
import config from 'config'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const forwardTo = [
  {id: 'supplier', name: 'Supplier'},
  {id: 'designer', name: 'Designer'},
  {id: 'seller', name: 'Seller'},
  {id: 'cs', name: 'Customer Support'},
]

const ForwardForm = (props) => {
  const classes = useStyles()
  const inputEl = useRef(null);
  const { issue, submitData, handleChange, type, log } = props;

  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  }

  const handleInputChange = (newValue) => {
    return newValue;
  }

  useEffect(() => {
    if (log?.action_data?.content){
      handleChange({
        target: {
          name: 'content',
          value: log.action_data.content
        }
      })
    } else if (issue.content){
      handleChange({
        target: {
          name: 'content',
          value: issue.content
        }
      })
    }
  }, [log?.action_data?.content, issue.content])

  return (
    <div className="p-3">
      <div style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontSize: 16, fontWeight: 'bold' }}>
                Order Detail
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '2%' }} padding="checkbox">
                <Checkbox
                  checked={submitData.values.selectedItems.length === issue.order_items.length}
                  color="secondary"
                  indeterminate={
                    submitData.values.selectedItems.length > 0 &&
                    submitData.values.selectedItems.length < issue.order_items.length
                  }
                  onChange={(e) => {
                    var selected_items = Object.assign([], submitData.values.selectedItems)
                    if (e.target.checked) {
                      selected_items = issue.order_items.map(ev => ev.id)
                    } else {
                      selected_items = []
                    }
                    const event = {
                      target: {
                        name: 'selectedItems',
                        value: selected_items
                      }
                    }
                    handleChange(event)
                  }}
                />
              </TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Lineitem SKU</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Lineitem Name</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Link Design/Ali/Sup</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Supplier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Tracking</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Carrier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN uploaded at</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN status</TableCell>
              <TableCell className={classes.headName} style={{ width: '8%' }}>Est time</TableCell>
            </TableRow>
            {issue.order_items.map(item => (
              <TableRow className={classes.tableRow} key={item.id}>
                <TableCell className={classes.cellContent} padding="checkbox">
                  <Checkbox
                    checked={submitData.values.selectedItems.indexOf(item.id) !== -1}
                    color="secondary"
                    onChange={e => {
                      var selected_items = Object.assign([], submitData.values.selectedItems)
                      if (e.target.checked) {
                        selected_items.push(item.id)
                      } else {
                        const index = selected_items.indexOf(item.id);
                        if (index > -1) {
                          selected_items.splice(index, 1)
                        }
                      }
                      const event = {
                        target: {
                          name: 'selectedItems',
                          value: selected_items
                        }
                      }
                      handleChange(event)
                    }}
                    value="true"
                  />
                </TableCell>
                <TableCell className={classes.cellContent}>{item.sku}</TableCell>
                <TableCell className={classes.cellContent}>
                  <Tooltip placement='top' arrow title={item.lineitem_name}>
                    <span className='line-clamp-1'>{item.lineitem_name}</span>
                  </Tooltip>
                </TableCell>
                <TableCell className={classes.cellContent}></TableCell>
                <TableCell className={classes.cellContent}>{item.level}</TableCell>
                <TableCell className={classes.cellContent}>{item.supplier?.name}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.name).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.carrier).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.imported_at).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.status).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      
      {!props.isPaypalDisputeIssue &&
        <>
        {/* <div className='mt-2'>
          <Box fontSize={15} fontWeight={'bold'}>
            Title
          </Box>
          <TextField 
            name="title"
            fullWidth
            margin='dense'
            variant="outlined"
            placeholder='Title...'
            value={submitData.values.title || ''}
            onChange={handleChange}
            InputProps={{
              classes: {
                input: 'f-14'
              }
            }}
          />
        </div> */}
        <div className='mt-2'>
          <Box fontSize={15} fontWeight={'bold'}>
            Kind
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
              return new Promise(resolve => {
                request.get(`${config.apiUrl}/api/kinds`, {name: inputValue, object_type: 'issues', purpose: 'create'}).then(res => {
                  if (res.data.success) {
                    resolve(res.data.data.result)
                  } else {
                    resolve([])
                  }
                }, err => resolve([]))
              })
            })}
            defaultOptions
            onInputChange={handleInputChange}
            placeholder={'Kind'}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ id }) => id}
            onChange={(value) => {
              const event = {
                target: {
                  name: 'kind_id',
                  value
                }
              }
              handleChange(event)
            }}
            value={submitData.values.kind_id}
            styles={customStyles}
          />
        </div>
        </>
      }
      <div className='mt-2'>
        <Box fontSize={15} fontWeight={'bold'}>
          Forward To
        </Box>
        <AsyncSelect
          isSearchable
          cacheOptions
          loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
            return new Promise(resolve => {
              const result = forwardTo.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
              resolve(result)
            })
          })}
          defaultOptions
          onInputChange={handleInputChange}
          placeholder={'Forward To'}
          getOptionLabel={({ name }) => name}
          getOptionValue={({ id }) => id}
          onChange={(value) => {
            const event = {
              target: {
                name: 'forward_to',
                value
              }
            }
            handleChange(event)
          }}
          value={submitData.values.forward_to}
          styles={customStyles}
        />
      </div>
      <div className='mt-2 issue-description'>
        <Box fontSize={15} fontWeight={'bold'}>
          Description
        </Box>
        <TinyEditorComponent 
          style={{ width: '100%' }}
          ref={inputEl}
          content={submitData.values.content}
          id={makeId(20)}
          placeholder="Write some description for this issue..."
          onEditorChange={(value) => {
            const event = {
              target: {
                name: 'content',
                value
              }
            }
            handleChange(event)
          }}
        />
      </div>
      <div className='mt-2'>
        <FormGroup row>
          <FormControlLabel label="Dispute payment gateway"
            control={
              <Checkbox
                checked={submitData.values.dispute_payment_gateway}
                onChange={(e) => {
                  handleChange({
                    target: {
                      name: 'dispute_payment_gateway',
                      value: e.target.checked
                    }
                  })
                }}
                color="primary"
              />
            }
          />
        </FormGroup>
      </div>
      <div className='mt-2'>
        {type == 'issue' && 
          <Conversation 
            data={issue}
            background={'#D4F7FF'} 
            type={'issue'}
            noAction
          />
        }
        {type == 'action_logs' && 
          <Conversation 
            data={log}
            background={'#D4F7FF'} 
            type={'action_logs'}
            noAction
          />
        }
        {type == 'all' && 
          <>
            <Conversation 
              data={issue}
              background="#D4F7FF" 
              type={'issue'}
              noAction
            />
            {issue.action_logs && issue.action_logs.filter(e => e.action_code == 'comment' && e.action_data.deleted != true).map((log, index) => (
              <div key={index}>
                <Conversation
                  data={log} 
                  background={'#D4F7FF'}
                  type={'action_logs'}
                  noAction
                />
              </div>
              
            ))}
          </>
        }
        
      </div>
    </div>
  )
}

export default ForwardForm;