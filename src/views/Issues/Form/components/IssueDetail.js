import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider, Box, TextField
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import { Uploader } from '../../../../_components'
import { stringToColor, capitalizeStr, customStyles, errorStyles } from '../../../../utils'
import clsx from 'clsx'
import { CustomStep, CustomTimeline, FormModal } from '../../../../common'
import toastr from '../../../../common/toastr'
import { request } from '../../../../_services/request'
import moment from 'moment';
import { IconDotsVertical } from '@tabler/icons'
import { offerIssues, customerDecisions } from '../../List/constants'
import config from 'config';
import CheckIcon from '@material-ui/icons/Check'
import { Placeholder, Avatar, DatePicker, Dropdown } from 'rsuite'
import AsyncSelect from 'react-select/async'
import './style.scss'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  tableRow: {
    '&:hover': {
      backgroundColor: '#F5F5F5'
    }
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const departments = [
  {id: 'supplier', name: 'Supplier'},
  {id: 'designer', name: 'Designer'},
  {id: 'seller', name: 'Seller'},
  {id: 'cs', name: 'Customer Support'},
]

const ListItemInfo = ({ title, value, icon, divider, fullWidth }) => {
  const classes = useStyles()
  return (
    <>
      <ListItem style={{ padding: '4px 0px' }}>
        <ListItemText 
          style={{ width: '40%' }} 
          secondary={title}
          classes={{ secondary: classes.textBold }}
        />
        <ListItemText 
          style={{ width: '60%' }} 
          disableTypography
          secondary={
            <div className='d-flex'>
              <div className={fullWidth ? 'w-100' : ''}>{value}</div>
              {icon && <div className='ml-2'>{icon}</div>}
            </div>
          } 
        />
      </ListItem>
      {divider && <Divider />}
    </>
  )
}

const IssueDetail = (props) => {
  const [issue, setIssue] = useState(null)
  const [isLoading, setLoading] = useState(false)
  const [logs, setLogs] = useState([])
  const [isLoadingLog, setLoadingLog] = useState(false)

  useEffect(() => {
    if (props.issue) {
      getIssue(props.issue.id)
    }
  }, [props.issue]);

  const getIssue = (id) => {
    setLoading(true)
    request.get(`${config.apiUrl}/api/issues/${id}`).then(res => {
      if (res.data.success) {
        setIssue(res.data.data)
        setLoading(false)
        if (res.data.data.level != props.issue.level) {
          props.onRefresh(props.tab, res.data.data)
        }
      } else {
        toastr.error(res.data.msg)
        setLoading(false)
      }
    }, err => toastr.error(err))
  }

  useEffect(() => {
    if (props.issue) {
      setLoadingLog(true)
      request.get(`${config.apiUrl}/api/logs`, {object_id: props.issue.id, object_type: 'issues'}).then(res => {
        if (res.data.success) {
          setLogs(res.data.data.result)
          setLoadingLog(false)
        } else {
          toastr.error(res.data.msg)
          setLoadingLog(false)
        }
      }, err => toastr.error(err))
    }
  }, [props.issue]);

  const handleSubmit = (value) => {
    const data = {
      action_code: 'comment',
      action_data: {
        content: value,
      },
      object_id: issue.id,
      object_type: 'issues'
    }
    request.post_json(`${config.apiUrl}/api/actions/commit`, data).then(res => {
      if (res.data.success) {
        const logs_ = Object.assign([], logs)
        logs_.push(res.data.data.log)
        setLogs(logs_)
      } else {
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }

  const getSteps = () => {
    var solution_steps = issue.solution_steps?.sort((a, b) => a.sequence < b.sequence ? -1 : 1)
    var kind_steps = issue.kind_steps?.sort((a, b) => a.sequence < b.sequence ? -1 : 1)
    const result = [...kind_steps || [], ...solution_steps || [], { code: "resolve", label: "Resolve", level: "T5", state: issue.level == "T5" ? "done" : null }]
    return result
  }

  const loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  }

  const handleInputChange = (newValue) => {
    return newValue;
  }

  const addAssign = (action) => {
    if (!issue.dest_department) {
      toastr.info("Please add department for this issue first.")
      return
    }
    FormModal.instance.current.showForm({
      data: {
        assignee_id: action == 'add_followers' ? [] : null
      },
      title: capitalizeStr(action, '_'),
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <Box fontSize={15} fontWeight={'bold'} style={{ textTransform: 'capitalize' }}>
            Select
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
              return new Promise(resolve => {
                request.get(`${config.apiLoginUrl}/api/v1/users`, {department: issue.dest_department, name: inputValue}).then(res => {
                  if (res.data.success) {
                    resolve(res.data.data.result)
                  } else {
                    resolve([])
                  }
                }, err => resolve([]))
              })
            })}
            defaultOptions
            isMulti={action == 'add_followers'}
            onInputChange={handleInputChange}
            menuPortalTarget={document.body}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ uid }) => uid}
            onChange={(value) => {
              const event = {
                target: {
                  name: 'assignee_id',
                  value
                }
              }
              handleChange(event)
            }}
            value={submitData.values.assignee_id}
            styles={customStyles}
          />
          {submitData.errors?.assignee_id && <small style={{ color: 'red'}}>{submitData.errors.assignee_id[0]}</small>}
        </div>
      ),
      action: {
        titleAction: 'Add',
        schema: {
          assignee_id: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: action,
              action_data: action == 'add_followers' ? { follower_ids: submitData.values.assignee_id.map(x => x.uid) } : { assignee_id: submitData.values.assignee_id.uid }
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const addDepartment = () => {
    FormModal.instance.current.showForm({
      data: {
        department: null
      },
      title: "Add department",
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <Box fontSize={15} fontWeight={'bold'} style={{ textTransform: 'capitalize' }}>
            Department
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
              return new Promise(resolve => {
                const result = departments.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
                resolve(result)
              })
            })}
            defaultOptions
            onInputChange={handleInputChange}
            menuPortalTarget={document.body}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ id }) => id}
            onChange={(value) => {
              const event = {
                target: {
                  name: 'department',
                  value
                }
              }
              handleChange(event)
            }}
            value={submitData.values.department}
            styles={submitData.errors?.department ? errorStyles : customStyles}
          />
        </div>
      ),
      action: {
        titleAction: 'Add',
        schema: {
          department: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: "update_department",
              action_data: {dest_department: submitData.values['department']['id']}
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const addOffer = () => {
    FormModal.instance.current.showForm({
      title: 'Add offer',
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Offer
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  const result = offerIssues.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
                  resolve(result)
                })
              })}
              defaultOptions
              onInputChange={handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ id }) => id}
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'offer',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.offer}
              styles={customStyles}
            />
            {submitData.errors?.offer && <small style={{ color: 'red'}}>{submitData.errors.offer[0]}</small>}
          </div>
          <div className='mt-2'>
            <Box fontSize={15} fontWeight={'bold'}>
              Offer Note
            </Box>
            <TextField 
              name="offer_note"
              variant="outlined"
              value={submitData.values.offer_note}
              onChange={handleChange}
              margin='dense'
              fullWidth
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Add',
        schema: {
          offer: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: "add_offer",
              action_data: {
                offer: submitData.values.offer.name,
                offer_note: submitData.values.offer_note || ''
              }
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const confirmSendOffer = () => {
    FormModal.instance.current.showForm({
      data: {
        offered_at: new Date()
      },
      title: 'Confirm send offer',
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Link contact offer
            </Box>
            <TextField 
              name="link_contact_offer"
              margin='dense'
              fullWidth
              variant="outlined"
              value={submitData.values.link_contact_offer}
              onChange={handleChange}
              placeholder='Link contact offer'
              error={submitData.errors?.link_contact_offer}
              helperText={submitData.errors?.link_contact_offer ? submitData.errors.link_contact_offer[0] : ''}
            />
          </div>
          <div className='mt-2'>
            <Box fontSize={15} fontWeight={'bold'}>
              Offered at
            </Box>
            <DatePicker
              style={{ width: '100%', border: submitData.errors?.offered_at ? '1px solid red' : '1px solid #999999', borderRadius: 5, color: 'black' }}
              appearance="subtle"
              format="DD/MM/YYYY HH:mm"
              placeholder="Offered at"
              oneTap
              name="offered_at"
              onChange={(date) => {
                const event = {
                  target: {
                    name: 'offered_at',
                    value: date
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.offered_at}
            />
            {submitData.errors?.offered_at && <small style={{ color: 'red'}}>{submitData.errors.offered_at[0]}</small>}
          </div>
        </div>
      ),
      action: {
        titleAction: 'Confirm',
        schema: {
          link_contact_offer: {
            presence: { allowEmpty: false, message: '^Required' }
          },
          offered_at: {
            presence: { allowEmpty: false, message: '^Required' }
          },
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: "confirm_sent_offer",
              action_data: {...submitData.values}
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const confirmPopup = (data) => {
    FormModal.instance.current.showForm({
      title: data.title,
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          {data.message}
        </div>
      ),
      action: {
        titleAction: data.titleAction,
        onAction: (submitData) => {
          return new Promise((resolve) => {
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: data.action_code,
              action_data: {}
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const updateCustomerDecision = () => {
    FormModal.instance.current.showForm({
      title: 'Update customer decision',
      customView: (submitData, handleChange, onDrop) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Customer decision
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  const result = customerDecisions.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
                  resolve(result)
                })
              })}
              defaultOptions
              onInputChange={handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ id }) => id}
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'customer_decision',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.customer_decision}
              styles={submitData.errors?.customer_decision ? errorStyles : customStyles}
            />
            {submitData.errors?.customer_decision && <small style={{ color: 'red'}}>{submitData.errors.customer_decision[0]}</small>}
          </div>
          <div className='mt-2'>
            <Box fontSize={15} fontWeight={'bold'}>
              Solution
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.apiUrl}/api/solutions`, {name: inputValue}).then(res => {
                    if (res.data.success) {
                      resolve(res.data.data.result)
                    } else {
                      resolve([])
                    }
                  }, err => resolve([]))
                })
              })}
              defaultOptions
              onInputChange={handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ id }) => id}
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'solution_id',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.solution_id}
              styles={submitData.errors?.solution_id ? errorStyles : customStyles}
            />
            {submitData.errors?.solution_id && <small style={{ color: 'red'}}>{submitData.errors.solution_id[0]}</small>}
          </div>
          <div className='mt-2'>
            <Box fontSize={15} fontWeight={'bold'}>
              Assignee
            </Box>
            <AsyncSelect
              isSearchable
              cacheOptions
              loadOptions={(inputValue) => loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise(resolve => {
                  request.get(`${config.apiLoginUrl}/api/v1/users`, {name: inputValue}).then(res => {
                    if (res.data.success) {
                      resolve(res.data.data.result)
                    }
                  })
                })
              })}
              defaultOptions
              onInputChange={handleInputChange}
              menuPortalTarget={document.body}
              getOptionLabel={({ name }) => name}
              getOptionValue={({ uid }) => uid}
              onChange={(value) => {
                const event = {
                  target: {
                    name: 'assignee_id',
                    value
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.assignee_id}
              styles={customStyles}
            />
          </div>
          <div className='mt-2'>
            <Box fontSize={15} fontWeight={'bold'}>
              Next action note
            </Box>
            <TextField 
              name="next_action_note"
              margin='dense'
              variant="outlined"
              fullWidth
              placeholder='Note...'
              multiline
              minRows={3}
              maxRows={5}
              value={submitData.values.next_action_note}
              onChange={handleChange}
            />
          </div>
          <div className='uploader'>
            <Uploader 
              submitData={submitData}
              handleChange={handleChange}
              namespace={'actions/update_customer_decision'}
              uploadOnPicked
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Update',
        schema: {
          customer_decision: {
            presence: { allowEmpty: false, message: '^Required' }
          },
          solution_id: {
            presence: { allowEmpty: false, message: '^Required' }
          },
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            const query = {}
            Object.keys(submitData.values).map(key => {
              if (key == 'files') {
                query['attachments'] = submitData.values[key]
              } else if (key == 'assignee_id') {
                query[key] = submitData.values[key].uid
              } else if (submitData.values[key] instanceof Object || typeof submitData.values[key] == 'object') {
                query[key] = submitData.values[key].id
              } else {
                query[key] = submitData.values[key]
              }
            })
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: 'update_customer_decision',
              action_data: query
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const confirmChangeInfo = () => {
    FormModal.instance.current.showForm({
      data: {
        changed_info_at: new Date()
      },
      title: 'Confirm change info',
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Changed info at
            </Box>
            <DatePicker
              style={{ width: '100%', border: submitData.errors?.offered_at ? '1px solid red' : '1px solid #999999', borderRadius: 5, color: 'black' }}
              appearance="subtle"
              format="DD/MM/YYYY HH:mm"
              placeholder="Changed info at"
              oneTap
              name="changed_info_at"
              onChange={(date) => {
                const event = {
                  target: {
                    name: 'changed_info_at',
                    value: date
                  }
                }
                handleChange(event)
              }}
              value={submitData.values.changed_info_at}
            />
            {submitData.errors?.changed_info_at && <small style={{ color: 'red'}}>{submitData.errors.changed_info_at[0]}</small>}
          </div>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Note
            </Box>
            <TextField 
              name="note"
              margin='dense'
              fullWidth
              variant="outlined"
              value={submitData.values.note}
              onChange={handleChange}
              placeholder='Note...'
              multiline
              minRows={3}
              maxRows={5}
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Ok',
        schema: {
          changed_info_at: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: "confirm_change_info",
              action_data: submitData.values
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  const addCustomDesignLink = () => {
    FormModal.instance.current.showForm({
      title: 'Add custom design link',
      customView: (submitData, handleChange) => (
        <div className='p-2'>
          <div>
            <Box fontSize={15} fontWeight={'bold'}>
              Custom design link
            </Box>
            <TextField 
              name="design_link"
              margin='dense'
              fullWidth
              variant="outlined"
              value={submitData.values.design_link}
              onChange={handleChange}
              placeholder='Design link...'
              error={submitData.errors?.design_link}
              helperText={submitData.errors?.design_link ? submitData.errors?.design_link[0] : ''}
            />
          </div>
        </div>
      ),
      action: {
        titleAction: 'Add',
        schema: {
          design_link: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise((resolve) => {
            if (!submitData.isValid) {
              resolve(false)
              return
            }
            request.post_json(`${config.apiUrl}/api/actions/commit`, {
              object_id: issue.id,
              object_type: 'issues',
              action_code: "add_custom_design_link",
              action_data: submitData.values
            }).then(res => {
              if (res.data.success) {
                toastr.success("Action successful")
                const logs_ = Object.assign([], logs)
                logs_.push(res.data.data.log)
                setLogs(logs_)
                getIssue(issue.id)
                resolve(true)
              } else {
                resolve(false)
                toastr.error(res.data.msg)
              }
            }, err => {
              resolve(false)
              toastr.error(err)
            })
          })
        }
      }
    })
  }

  return (
    <div>
      {isLoading && <Placeholder.Paragraph rows={10} active />}
      {issue &&
        <div className="p-3">
          <div className='d-flex justify-content-end'>
            <div className="ui steps">
              {getSteps().map((step, i) => {
                if (step.state == "done") {
                  return (
                    <div className="step" key={i}>
                      {<CheckIcon color={'primary'} style={{ fontWeigth: 'bold' }} />}
                      <div className="content">
                        <div className="title">{step?.label || ""}</div>
                      </div>
                    </div>)
                } else if (!issue.next_actions?.includes(step.code) || (issue.next_actions.includes(step.code) && step?.state)) {
                  return (
                    <div className="disabled step" key={i}>
                      <div className="content">
                        <div className="title">{step?.label || ""}</div>
                      </div>
                    </div>)
                } else if (issue.next_actions.includes(step.code) && !step?.state) {
                  return (
                    <a className={`step ${props.isClosed ? 'disabled' : 'active'}`}
                      key={i}
                      onClick={() => {
                        if (step.code == 'confirm_sent_offer') {
                          confirmSendOffer()
                        } else if (step.code == "assign") {
                          addAssign('assignee')
                        } else if (step.code == "resolve") {
                          confirmPopup({
                            title: 'Resolve issue?',
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Done",
                            action_code: step.code
                          })
                        } else if (step.code == "add_offer") {
                          addOffer()
                        } else if (step.code == "update_customer_decision") {
                          updateCustomerDecision()
                        } else if (step.code == "check_design") {
                          confirmPopup({
                            title: "Check design",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Ok",
                            action_code: step.code
                          })
                        } else if (step.code == 'confirm_change_info') {
                          confirmChangeInfo()
                        } else if (step.code == 'resend_order') {
                          confirmPopup({
                            title: "Resend order",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Ok",
                            action_code: step.code
                          })
                        } else if (step.code == 'confirm_refund') {
                          confirmPopup({
                            title: "Confirm refund",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Ok",
                            action_code: step.code
                          })
                        } else if (step.code == 'check_tiff_design') {
                          confirmPopup({
                            title: "Check Tiff design",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Ok",
                            action_code: step.code
                          })
                        } else if (step.code == 'add_custom_design_link') {
                          addCustomDesignLink()
                        } else if (step.code == 'confirm_custom_design_link') {
                          confirmPopup({
                            title: "Confirm custom design link",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Confirm",
                            action_code: step.code
                          })
                        } else if (step.code == 'update_solution') {

                        } else if (step.code == 'paypal_confirm_refund') {
                          confirmPopup({
                            title: "Paypal confirm refund?",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Confirm",
                            action_code: step.code
                          })
                        } else if (step.code == "paypal_dispute_resolve") {
                          confirmPopup({
                            title: "Paypal dispute resolve?",
                            message: "Hey, make sure you know what you're doing before hitting the button!",
                            titleAction: "Resolve",
                            action_code: step.code
                          })
                        }
                      }}
                    >
                      <div className="content">
                        <div className="title">{step?.label || ""}</div>
                      </div>
                    </a>)
                }
              })}
            </div>
            {issue.level != 'T5' && <div className="ml-2">
              <Dropdown 
                className='issue-dropdown-step' 
                noCaret 
                disabled={props.isClosed}
                icon={<IconDotsVertical size={30} stroke={3} />}
                placement='bottomEnd'
              >
                <Dropdown.Item onSelect={() => addAssign('assign')}>Add Assign</Dropdown.Item>
                <Dropdown.Item onSelect={() => addAssign('add_followers')}>Add Followers</Dropdown.Item>
                <Dropdown.Item onSelect={() => addDepartment()}>Add Department</Dropdown.Item>
              </Dropdown>
            </div>}
          </div>
          <CustomStep 
            steps={[
              {value: 'T0', label: 'T0. Open'},
              {value: 'T1', label: 'T1. Assigned'},
              {value: 'T2', label: 'T2. Had Offer'},
              {value: 'T3', label: 'T3. Sent Offer'},
              {value: 'T4', label: 'T4. Had Final Decision'},
              {value: 'T5', label: 'T5. Resolved'},
            ]}
            activeStep={issue.level}
          />
          {issue.note && 
            <div className='p-3 br-6 d-flex flex-column' style={{ border: '2px dashed orange', backgroundColor: '#FCF4E9' }}>
              <strong>Note:</strong>
              <div dangerouslySetInnerHTML={{ __html: issue.note }} />
            </div>
          }
          <List>
            <ListItemInfo title={"Department"} value={issue.dest_department} divider />
            <ListItemInfo title={"Creator"} value={issue.creator?.name} divider />
            <ListItemInfo title={"Kind"} value={issue.kind?.name} divider />
            <ListItemInfo 
              title={"Assignee"} 
              value={issue.assignee ? 
                <div className='d-flex align-items-center'>
                  <Avatar circle size="sm" style={{ textTransform: 'uppercase', backgroundColor: stringToColor(issue.assignee?.name, true)[0], color: stringToColor(issue.assignee?.name, true)[1] }}>{issue.assignee?.name[0]}</Avatar>
                  <span className='ml-1'>{issue.assignee?.name}</span>
                </div> : ''
              } 
              divider 
            />
            <ListItemInfo 
              title={"Other assignee"} 
              value={issue.followers && issue.followers.length > 0 ? 
                <div className='d-flex'>
                  {issue.followers.map((user, index) => (
                    <div className={`d-flex align-items-center ${index == 0 ? '' : 'ml-2'}`} key={user.name}>
                      <Avatar circle size="sm" style={{ textTransform: 'uppercase', backgroundColor: stringToColor(user.name, true)[0], color: stringToColor(user.name, true)[1] }}>{user.name[0]}</Avatar>
                      <span className='ml-1'>{user.name}</span>
                    </div>
                  ))}
                </div> : ""
              } 
              divider 
            />
            <ListItemInfo title={"Deadline"} value={""} divider />
            <ListItemInfo title={"Solution"} value={issue.solution?.name} divider />
            {Object.keys(issue.extra || {}).map(key => {
              if (key == 'files') {
                return (
                  <ListItemInfo 
                    key={key}
                    title={capitalizeStr(key, '_')} 
                    fullWidth
                    value={
                      <div className='d-flex flex-column'>
                        {issue.extra[key].map(file => (
                          <a href={file.url} key={file.id}>
                            <span className='line-clamp-1'>{file.url}</span>
                          </a>
                        ))}
                      </div>
                    } 
                    divider 
                  />
                )
              } else {
                return (
                  <ListItemInfo 
                    key={key}
                    title={capitalizeStr(key, '_')} 
                    fullWidth
                    value={issue.extra[key]} 
                    divider 
                  />
                )
              }
            })}
          </List>
        </div>
      }
      {isLoadingLog ? <Placeholder.Paragraph rows={4} active /> : 
        <CustomTimeline 
          items={logs}
          title="History"
          titleCheckbox="Show History"
          buttonText={"Post"}
          itemClassName={'ml-1'}
          handleSubmit={handleSubmit}
          disabledButton={props.isClosed}
          renderItem={(item) => (
            <div className='d-flex w-100'>
              <Avatar circle size="sm" style={{ textTransform: 'uppercase', backgroundColor: stringToColor(item.actor ? item.actor?.name : props.user.data.name, true)[0], color: stringToColor(item.actor ? item.actor?.name : props.user.data.name, true)[1] }}>{item.actor ? item.actor?.name[0] : props.user.data.name[0]}</Avatar>
              <div className='d-flex flex-column ml-2' style={{ width: '80%' }}>
                <div className='d-flex align-items-center'>
                  <strong className='f-14'>{item.actor ? item.actor?.name : props.user.data.name} </strong>
                  <small className='ml-2'>{item.created_at}</small>
                </div>
                <div className='mt-2'>
                  <i>Commit action <strong>{item.action_code}</strong> on issue</i>
                  <ul className='mt-2 ml-4'>
                    {Object.keys(item.data || {}).map(key => {
                      if (key == 'attachments') {
                        return (
                          <li key={key}>
                            <strong className='mr-2'>{capitalizeStr(key, '_')}:</strong>
                            {item.data[key].map(file => (
                              <span key={file.id}><a href={file.url}>{file.filename}</a>, </span>
                            ))}
                          </li>
                        )
                      } else if (key == 'extra' && item.data[key].files) {
                        return (
                          <li key={key}>
                            <strong className='mr-2'>Files:</strong>
                            {item.data[key].files.map(file => (
                              <span key={file.id}><a href={file.url}>{file.filename}</a>, </span>
                            ))}
                          </li>
                        )
                      } else {
                        return (
                          <li key={key}>
                            <strong>{capitalizeStr(key, '_')}:</strong>
                            <span className='ml-1'>{item.data[key]}</span>
                          </li>
                        )
                      }
                    })}
                  </ul>
                </div>
              </div>
            </div>
          )}
          // handleSubmit={handleSubmit}
        />
      }
    </div>

  )
}

export default IssueDetail