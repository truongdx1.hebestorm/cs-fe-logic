import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider, Grid
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import { CustomTimeline, CopyToClipboard } from '../../../../common'
import toastr from '../../../../common/toastr'
import {
  IconCircleDashed, IconCircleCheck, IconPhoto
} from "@tabler/icons";
import { Avatar, Placeholder } from 'rsuite'
import clsx from 'clsx'
import moment from 'moment'
import config from 'config'
import { request } from '../../../../_services/request'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  container: {
    backgroundColor: '#fff',
    boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    textTransform: 'capitalize',
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    },
    '&.authorized': {
      backgroundColor: '#F4D7AA'
    },
    '&.voided': {
      backgroundColor: '#E4E5E7'
    },
    '&.partially_refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&.refunded': {
      backgroundColor: '#E4E5E7'
    },
    '&> div': {
      width: 10,
      height: 10,
      borderRadius: '50%',
    }
  },
  chip_paid: {
    backgroundColor: '#006F53',
  },
  chip_cancelled: {
    backgroundColor: '#626669',
  },
  chip_pending: {
    border: '2px solid #885E16',
  },
  chip_unfulfilled: {
    border: '2px solid #885E16',
  },
  chip_authorized: {
    backgroundColor: '#885E16',
  },
  chip_voided: {
    backgroundColor: '#885E16',
  },
  chip_partially_refunded: {
    border: '2px solid #885E16',
  },
  chip_refunded: {
    border: '2px solid #885E16',
  },
  textBold: {
    fontWeight: 'bold'
  },
  snackSuccess: {
    backgroundColor: '#449242',
    minWidth: 'auto'
  }
}));

const OrderDetail = (props) => {
  const classes = useStyles()
  const [order, setOrder] = useState(null)
  const [isLoading, setLoading] = useState(false)
  const [timelines, setTimelines] = useState([])

  useEffect(() => {
    if (props.order) {
      setLoading(true)
      const order_name = props.order.name.replace('#', '%23')
      request.get(`${config.apiCoreUrl}/api/order/${order_name}`, {}).then(res => {
        if (res.data.success) {
          setLoading(false)
          setOrder(res.data.data)
        } else {
          toastr.error(res.data.msg)
          setOrder(null)
        }
      }, err => {
        toastr.error(err)
      })
    }
  }, [props.order]);

  useEffect(() => {
    if (props.order.id) {
      request.get(`${config.apiUrl}/api/logs`, {object_id: props.order.id, object_type: 'orders'}).then(res => {
        if (res.data.success) {
          setTimelines(res.data.data.result)
        } else {
          toastr.error(res.data.msg)
          setTimelines([])
        }
      }, err => toastr.error(err))
    } else {
      setTimelines([])
    }
  }, [props.order]);

  const handleSubmit = (value) => {
    const data = {
      action_code: 'comment',
      action_data: {
        name: props.user.data.name,
        email: props.user.data.email || "",
        content: value,
        user_type: "customer_support"
      },
      object_id: props.order.id,
      object_type: 'orders'
    }
    request.post_json(`${config.apiUrl}/api/actions/commit`, data).then(res => {
      if (res.data.success) {
        const timelines_ = Object.assign([], timelines)
        timelines_.push(res.data.data.log)
        setTimelines(timelines_)
      } else {
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }
  

  return (
    <Grid container spacing={2} className='p-3'>
      {isLoading && <Placeholder.Paragraph rows={10} active />}
      {order &&
        <>
          <Grid item xs={12}>
            <div className='d-flex align-items-center justify-content-between'>
              <div className='d-flex flex-column'>
                <div className='d-flex align-items-center'>
                  <div className='f-24'>{order.name}</div>
                  {order.financial_status && <div className={clsx(classes.chip, order.financial_status)}><div className={clsx(classes[`chip_${order?.financial_status}`], 'mr-1')} /> Payment {order.financial_status?.replace('_', ' ')}</div>}
                  {order.fulfillment_status && <div className={clsx(classes.chip, order.fulfillment_status || '')}><div className={clsx(classes[`chip_${order?.fulfillment_status}`], 'mr-1')} /> {order.fulfillment_status.replace('_', ' ')}</div>}
                </div>
                <div className='text-gray'>{moment(order.created_at).format('LLL')}</div>
              </div>
              {/* <div className='d-flex align-items-center'>
                <Button className='mr-1' style={{ border: '1px solid #ccc' }}>
                  Edit
                </Button>
                <Button className='mr-1' style={{ border: '1px solid red', color: 'red' }}>
                  Refund
                </Button>
                <Dropdown title="More Action" placement='bottomEnd' style={{ border: '1px solid #ccc', borderRadius: 6 }}>
                  <Dropdown.Item>New File</Dropdown.Item>
                  <Dropdown.Item>New File 2</Dropdown.Item>
                </Dropdown>
              </div> */}
            </div>
          </Grid>
          <Grid item xs={8}>
            {order.items.filter(e => e.fulfillment_status != 'fulfilled').length > 0 &&
              <div className={clsx(classes.card, 'pt-3')}>
                <div className='d-flex align-items-center p-2'>
                  <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: 'orange' }}>
                    <IconCircleDashed size={20} stroke={3} />
                  </div>
                  <div className='ml-2 font-weight-bold'>Unfulfilled ({order.items.filter(e => e.fulfillment_status != 'fulfilled').length})</div>
                </div>
                <div style={{ width: '100%' }}>
                  {order.items.filter(e => e.fulfillment_status != 'fulfilled').map(item => (
                    <div className='d-flex w-100 p-2' key={item.id}>
                      <div className='d-flex align-items-center justify-content-center' style={{ width: '20%' }}>
                        {item.image ? <img src={item.image} style={{ width: '100%', height: 'auto' }} /> : <Avatar><IconPhoto style={{ width: '100%', height: '100%' }} /></Avatar>}
                      </div>
                      <div className='f-13 ml-2' style={{ width: '45%' }}>
                        <div>{item.lineitem_name}</div>
                        <div>SKU: {item.sku}</div>
                      </div>
                      <div className='f-13 text-right' style={{ width: '20%'}}>{item.price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })} x {item.quantity}</div>
                      <div className='f-13 text-right' style={{ width: '15%' }}>{(item.price * item.quantity).toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</div>
                    </div>
                  ))}
                </div>
              </div>
            }
            {order.items.filter(e => e.fulfillment_status == 'fulfilled').length > 0 && 
              <div className={clsx(classes.card, 'pt-3')}>
                <div className='d-flex align-items-center p-2'>
                  <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: '#BAE7D2' }}>
                    <IconCircleCheck size={20} stroke={3} />
                  </div>
                  <div className='ml-2 font-weight-bold'>Fulfilled ({order.items.filter(e => e.fulfillment_status != 'fulfilled').length})</div>
                </div>
                <div style={{ width: '100%' }}>
                  {order.items.filter(e => e.fulfillment_status == 'fulfilled').map(item => (
                    <div className='d-flex w-100 p-2' key={item.id}>
                      <div className='d-flex align-items-center justify-content-center' style={{ width: '20%' }}>
                        {item.image ? <img src={item.image} style={{ width: '100%', height: 'auto' }} /> : <Avatar><IconPhoto style={{ width: '100%', height: '100%' }} /></Avatar>}
                      </div>
                      <div className='f-13 ml-2' style={{ width: '45%' }}>
                        <div>{item.lineitem_name}</div>
                        <div>SKU: {item.sku}</div>
                      </div>
                      <div className='f-13 text-right' style={{ width: '20%'}}>{item.price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })} x {item.quantity}</div>
                      <div className='f-13 text-right' style={{ width: '15%' }}>{(item.price * item.quantity).toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</div>
                    </div>
                  ))}
                </div>
              </div>
            }
            <div className={clsx(classes.card, 'pt-3')}>
              <div className='d-flex align-items-center p-2'>
                <div className='d-flex align-items-center justify-content-center' style={{ width: 24, height: 24, borderRadius: '50%', background: 'orange' }}>
                  <IconCircleDashed size={20} stroke={3} />
                </div>
                <div className='ml-2'><strong>Authorized</strong></div>
              </div>
              <div className='d-flex align-items-center p-2'>
                <List className='w-100'>
                  <ListItem style={{ padding: 0 }}>
                    <ListItemText 
                      style={{ width: '20%' }} 
                      secondary={"Subtotal"}
                    />
                    <ListItemText 
                      style={{ width: '60%' }} 
                      secondary={`${order.items.length} item(s)`} 
                    />
                    <ListItemText 
                      style={{ width: '20%', textAlign: 'right' }}
                      secondary={order.subtotal_price.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}
                    />
                  </ListItem>
                  <ListItem style={{ padding: 0 }}>
                    <ListItemText 
                      style={{ width: '20%' }} 
                      secondary={"Discount"}
                    />
                    <ListItemText 
                      style={{ width: '60%' }} 
                      secondary={""} 
                    />
                    <ListItemText 
                      style={{ width: '20%', textAlign: 'right' }}
                      secondary={order.total_discounts?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}
                    />
                  </ListItem>
                  <ListItem style={{ padding: 0 }}>
                    <ListItemText 
                      style={{ width: '20%' }} 
                      secondary={"Shipping"}
                    />
                    <ListItemText 
                      style={{ width: '60%' }} 
                      secondary={""} 
                    />
                    <ListItemText 
                      style={{ width: '20%', textAlign: 'right' }}
                      secondary={order.total_shipping.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}
                    />
                  </ListItem>
                  <ListItem style={{ padding: 0 }}>
                    <ListItemText 
                      style={{ width: '20%' }} 
                      secondary={"Tip"}
                    />
                    <ListItemText 
                      style={{ width: '60%' }} 
                      secondary={""} 
                    />
                    <ListItemText 
                      style={{ width: '20%', textAlign: 'right' }}
                      secondary={order.total_tip.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}
                    />
                  </ListItem>
                  <ListItem style={{ padding: 0 }}>
                    <ListItemText 
                      style={{ width: '20%' }} 
                      secondary={"Total"}
                      classes={{ secondary: classes.textBold }}
                    />
                    <ListItemText 
                      style={{ width: '60%' }} 
                      secondary={""} 
                    />
                    <ListItemText 
                      style={{ width: '20%', textAlign: 'right' }}
                      secondary={order.total_price.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}
                      classes={{ secondary: classes.textBold }}
                    />
                  </ListItem>
                </List>
              </div>
              <Divider className='mt-2' />
              <div className='d-flex align-items-center justify-content-between mt-3 mb-3 pl-2 pr-2'>
                <span>Paid by customer</span>
                <span>{order.total_price.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
              </div>
            </div>
            <CustomTimeline 
              items={timelines}
              title="Timeline"
              titleCheckbox="Show comments"
              buttonText={"Send"}
              itemClassName={'ml-1'}
              renderItem={(item) => (
                <div className='d-flex justify-content-between'>
                  <div className='d-flex flex-column'>
                    <strong className='f-16'>{item.data?.name}</strong>
                    <span>{item.data.content}</span>
                  </div>
                  <small>{item.created_at}</small>
                </div>
              )}
              handleSubmit={handleSubmit}
            />
          </Grid>
          <Grid item xs={4}>
            <div className={clsx(classes.card, 'p-3')}>
              <div className="d-flex align-items-center justify-content-between">
                <strong>Notes</strong>
                <a 
                  href="#" 
                  onClick={(e) => {
                    e.preventDefault()
                    e.stopPropagation()
                  }}
                >Edit</a>
              </div>
              <div className='mt-2'>
                {order.note || "No note from customer"}
              </div>
            </div>
            <div className={classes.card}>
              <div className='d-flex flex-column p-3'>
                <strong>Customer</strong>
                <a 
                  className='mt-2'
                  href="#" 
                  onClick={e => {
                    e.stopPropagation()
                    e.preventDefault()
                  }}
                >
                  {order.shipping_address?.name}
                </a>
                {/* <span>1 order</span> */}
              </div>
              <Divider className='mt-2' />
              <div className='p-3 mt-2'>
                <div className='d-flex align-items-center justify-content-between'>
                  <div style={{ textTransform: 'uppercase' }}>Contact Infomation</div>
                  <a 
                    href="#" 
                    onClick={(e) => {
                      e.preventDefault()
                      e.stopPropagation()
                    }}
                  >Edit</a>
                </div>
                <div className='d-flex align-items-center justify-content-between mt-2'>
                  <div>{order.email}</div>
                  <CopyToClipboard text={order.email} />
                </div>
              </div>
              <Divider className='mt-2' />
              <div className='p-3 mt-2'>
                <div className='d-flex align-items-center justify-content-between'>
                  <div style={{ textTransform: 'uppercase' }}>Shipping Address</div>
                  <a 
                    href="#" 
                    onClick={(e) => {
                      e.preventDefault()
                      e.stopPropagation()
                      console.log('1111')
                    }}
                  >Edit</a>
                </div>
                <div className='d-flex justify-content-between mt-2'>
                  <div className='d-flex flex-column'>
                    <span>{order.shipping_address?.name}</span>
                    <span>{order.shipping_address?.address1}</span>
                    <span>{order.shipping_address?.zip} - {order.shipping_address?.city} - {order.shipping_address?.province}</span>
                    <span>{order.shipping_address?.country}</span>
                    <span>{order.shipping_address?.phone}</span>
                  </div>
                  <CopyToClipboard text={`${order.shipping_address?.name} ${order.shipping_address?.address1} ${order.shipping_address?.zip} - ${order.shipping_address?.city} - ${order.shipping_address?.province} ${order.shipping_address?.country} ${order.shipping_address?.phone}`} />
                </div>
              </div>
              <Divider className='mt-2' />
              <div className='p-3 mt-2'>
                <div className='d-flex align-items-center justify-content-between'>
                  <div style={{ textTransform: 'uppercase' }}>Billing Address</div>
                  <a 
                    href="#" 
                    onClick={(e) => {
                      e.preventDefault()
                      e.stopPropagation()
                      console.log('1111')
                    }}
                  >Edit</a>
                </div>
                <div className='d-flex justify-content-between mt-2'>
                  <div className='d-flex flex-column'>
                    <span>{order.billing_address?.name}</span>
                    <span>{order.billing_address?.address1}</span>
                    <span>{order.billing_address?.zip} - {order.billing_address?.city} - {order.billing_address?.province}</span>
                    <span>{order.billing_address?.country}</span>
                    <span>{order.billing_address?.phone}</span>
                  </div>
                  <CopyToClipboard text={`${order.billing_address?.name} ${order.billing_address?.address1} ${order.billing_address?.zip} - ${order.billing_address?.city} - ${order.billing_address?.province} ${order.billing_address?.country} ${order.billing_address?.phone}`} />
                </div>
              </div>
            </div>
          </Grid>
        </>
      }
    </Grid>
  )
}

export default OrderDetail