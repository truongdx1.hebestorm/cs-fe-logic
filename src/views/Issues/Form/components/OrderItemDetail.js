import React, { useState, useEffect } from 'react'
import { 
  Grid, Table, TableRow, TableCell, TableBody
} from '@material-ui/core'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import clsx from 'clsx'
import { CustomTimeline } from '../../../../common'
import toastr from '../../../../common/toastr'
import { request } from '../../../../_services/request'
import moment from 'moment';
import config from 'config';
import { Placeholder } from 'rsuite'

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const OrderItemDetail = (props) => {
  const classes = useStyles()
  const { item } = props;

  const [issues, setIssues] = useState([])
  const [isLoading, setLoading] = useState(false)
  const [timelines, setTimelines] = useState([])
  const [isLoadingTimeline, setLoadingTimeline] = useState(false)

  useEffect(() => {
    if (item.id) {
      setLoading(true)
      request.get(`${config.apiUrl}/api/issues`, {query: item.id, search_option: 'order_item_id'}).then(res => {
        setLoading(false)
        if (res.data.success) {
          setIssues(res.data.data.result)
        } else {
          toastr.error(res.data.msg)
          setIssues([])
        }
      }, err => toastr.error(err))
    } else {
      setIssues([])
    }
  }, [item]);

  useEffect(() => {
    if (item.id) {
      setLoadingTimeline(true)
      request.get(`${config.apiUrl}/api/logs`, {object_id: item.id, object_type: 'order_items'}).then(res => {
        setLoadingTimeline(false)
        if (res.data.success) {
          setTimelines(res.data.data.result)
        } else {
          toastr.error(res.data.msg)
          setTimelines([])
        }
      }, err => toastr.error(err))
    } else {
      setTimelines([])
    }
  }, [item]);

  const handleSubmit = (value) => {
    const data = {
      action_code: 'comment',
      action_data: {
        name: props.user.data.name,
        email: props.user.data.email || "",
        content: value,
        user_type: "customer_support"
      },
      object_id: item.id,
      object_type: 'order_items'
    }
    request.post_json(`${config.apiUrl}/api/actions/commit`, data).then(res => {
      if (res.data.success) {
        const timelines_ = Object.assign([], timelines)
        timelines_.push(res.data.data.log)
        setTimelines(timelines_)
      } else {
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }

  return (
    <Grid container spacing={2} className='p-3'>
      <Grid item xs={12}>
        <div className='d-flex align-items-center font-weight-bold'>
          <div className='f-18'>{item.order_name} - {item.lineitem_name}</div>
        </div>
        <div className={clsx(classes.card, 'mt-4')}>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell colSpan={8} style={{ fontSize: 16, fontWeight: 'bold', color: '#449242' }}>
                  Related Issues
                </TableCell>
              </TableRow>
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.headName} style={{ width: '8%' }}>Order ID</TableCell>
                <TableCell className={classes.headName} style={{ width: '12%' }}>Code</TableCell>
                <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
                <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
                <TableCell className={classes.headName} style={{ width: '12%' }}>Title</TableCell>
                <TableCell className={classes.headName} style={{ width: '20%' }}>Note</TableCell>
                <TableCell className={classes.headName} style={{ width: '18%' }}>Created At</TableCell>
                <TableCell className={classes.headName} style={{ width: '10%' }}>Forward To</TableCell>
              </TableRow>
              {isLoading && 
                <TableRow>
                  <TableCell colSpan={8}>
                    <Placeholder.Grid rows={8} columns={8} active />
                  </TableCell>
                </TableRow>
              }
              {issues.length > 0 && issues.map(issue => (
                <TableRow className={classes.tableRow} key={issue.id}>
                  <TableCell className={classes.cellContent}>{issue.items?.map(e => e.id).join(', ')}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.code}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.level}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.kind?.name}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.items?.map(e => e.order_name).join(', ')}</TableCell>
                  <TableCell className={classes.cellContent}><div dangerouslySetInnerHTML={{ __html: issue.note }} /></TableCell>
                  <TableCell className={classes.cellContent}>{moment(issue.created_at).format('lll')}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.assignee?.name}</TableCell>
                </TableRow>
              ))}
              {issues.length == 0 && !isLoading &&
                <TableRow className={classes.tableRow}>
                  <TableCell colSpan={8}>No issue related</TableCell>
                </TableRow>
              }
            </TableBody>
          </Table>
        </div>
        {isLoadingTimeline ?
          <Placeholder.Grid rows={8} columns={8} active /> : 
          <CustomTimeline 
            items={timelines}
            title="History"
            titleCheckbox="Show History"
            buttonText={"Post"}
            disabledButton={props.isClosed}
            itemClassName={'ml-1'}
            renderItem={(item) => (
              <div className='d-flex justify-content-between'>
                <div className='d-flex flex-column'>
                  <strong className='f-16'>{item.data?.name}</strong>
                  <span>{item.data.content}</span>
                </div>
                <small>{item.created_at}</small>
              </div>
            )}
            handleSubmit={handleSubmit}
          />
        }
      </Grid>
    </Grid>
  )
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

export default connect(mapStateToProps)(OrderItemDetail);