import React, { useState } from 'react'
import {
  IconCornerUpRight, IconMessages, IconEdit, IconTemplate, 
} from "@tabler/icons";
import { Avatar, Button } from 'rsuite'
import { stringToColor, makeId } from '../../../../utils'
import { TinyEditorComponent } from '../../../../_components'
import { SingleInputPicker } from '../../../../common'
import { request } from '../../../../_services/request'
import { Conversation } from '../components'
import { connect } from 'react-redux'
import config from 'config'

const CustomerTab = (props) => {
  const { formState, user, reply } = props;
  const quillRef = React.useRef(null)
  const forwardIssues = formState.values.forward_issues || []

  return (
    <div className='tab-customer'>
      <Conversation 
        data={formState.values}
        background="#fff" 
        type={'issue'}
        actionForward={() => props.forwardMail(null, 'issue')}
        viewImage={props.viewImage}
        isClosed={props.isClosed}
        forwardIssues={forwardIssues}
      />
      {formState.values.action_logs && formState.values.action_logs.filter(e => e.action_code == 'comment' && e.action_data.deleted != true && e.state == 'finished').map((log, index) => (
        <div key={index}>
          <Conversation 
            data={log}
            orderName={formState.values.order_name}
            background={log.action_data.user_type == 'customer_support'  ? '#F4F6F8' : '#fff'}
            type={'action_logs'}
            actionForward={() => props.forwardMail(log, 'action_logs')}
            actionDelete={() => props.deleteComment(log)}
            viewImage={props.viewImage}
            isClosed={props.isClosed}
          />
        </div>
      ))}
      
      {reply &&
        <div className="d-flex flex-column p-3">
          <div className="d-flex issue-detail">
            <Avatar style={{ backgroundColor: stringToColor(user?.data?.name || '', true)[0], color: stringToColor(user?.data?.name || '', true)[1], marginRight: 10, textTransform: 'uppercase' }}>
              {user?.data?.name ? user.data.name[0] : ''}
            </Avatar>
            <TinyEditorComponent 
              style={{ width: '100%' }}
              ref={quillRef}
              content={props.content}
              id={makeId(20)}
              onEditorChange={(content) => {
                props.setState({ content })
              }}
            />
          </div>
          <div className="d-flex justify-content-end mt-2">
            <Button 
              className="button-default"
              onClick={() => props.setState({ reply: false })}
            >
              Cancel
            </Button>
            <Button 
              className="button-primary ml-2"
              disabled={props.content == null || props.content == ''}
              onClick={() => props.addReply()}
            >
              Reply
            </Button>
          </div>
        </div>
      }
      <div className="d-flex p-3">
        <Button
          style={{ border: '1px solid #ccc', marginRight: 10 }}
          onClick={() => {
            props.setState({ reply: true })
          }}
          disabled={reply || props.isClosed}
        >
          <IconMessages size={20} stroke={2} /> Reply
        </Button>
        <Button
          style={{ border: '1px solid #ccc', marginRight: 10 }}
          onClick={props.addNote}
          disabled={props.isClosed}
        >
          <IconEdit size={20} stroke={2} /> Add note
        </Button>
        {reply &&
          <SingleInputPicker 
            icon={<IconTemplate size={20} stroke={2} />}
            title="Template"
            placement="top"
            creatable
            onCreateOption={(inputValue, callback) => {
              props.onCreateTemplate(inputValue, callback)
            }}
            className="mr-2"
            onChange={(item) => {
              quillRef.current.onSetContent(item.content)
              props.setState({ content: item.content })
            }}
            loadOptions={(inputValue) => {
              return new Promise((resolve) => {
                request.get(`${config.apiUrl}/api/cs_templates`, {name: inputValue || ''}).then(res => {
                  if (res.data.success) {
                    resolve(res.data.data.result)
                  } else {
                    resolve([])
                  }
                }, err => resolve([]))
              });
            }}
          />
        }
        <Button
          style={{ border: '1px solid #ccc', marginRight: 10 }}
          onClick={() => props.forwardMail(null, 'all')}
          disabled={props.isClosed}
        >
          <IconCornerUpRight size={20} stroke={2} /> Forward
        </Button>
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

export default connect(mapStateToProps)(CustomerTab);