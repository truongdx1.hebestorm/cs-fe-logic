import React from 'react'
import { Table, TableContainer, TableBody, TableHead, TableCell, TableRow } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import moment from 'moment'
import { Placeholder } from 'rsuite'

const levelOptions = [
  {label: 'T0', color: '#F3BF4F'},
  {label: 'T1', color: '#A1E5CC'},
  {label: 'T2', color: '#1B79E5'},
  {label: 'T3', color: '#8257F9'},
  {label: 'T4', color: '#11A35B'},
  {label: 'T5', color: '#636B72'},
]

const useStyles = makeStyles(theme => ({
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
  },
  headName: {
    background: '#FFF',
    fontSize: 14,
    textTransform: 'uppercase',
    padding: 10,
    fontWeight: 'bold'
  },
  cellContent: {
    fontSize: 13,
    padding: 10,
  },
  tableRow: {
    cursor: 'pointer',
    backgroundColor: '#fff',
    '&:hover': {
      backgroundColor: '#F4F6F8'
    }
  }
}));

const OtherTab = (props) => {
  const { data } = props;
  const classes = useStyles()
  return (
    <div className='tab'>
      {/* {data.isLoading && <Placeholder.Paragraph rows={5} active />} */}
      {data.items && data.items.length == 0 && <div className='p-3'>No issue found</div>}
      {data.items && data.items.length > 0 &&
        <TableContainer style={{ maxHeight: '50vh' }} className='table-issue'>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '25%' }} className={classes.headName}>Title</TableCell>
                <TableCell style={{ width: '20%' }} className={classes.headName}>Kind</TableCell>
                <TableCell style={{ width: '20%' }} className={classes.headName}>Created At</TableCell>
                <TableCell style={{ width: '15%' }} className={classes.headName}>Assignee</TableCell>
                <TableCell style={{ width: '5%' }} className={classes.headName}>Level</TableCell>
                <TableCell style={{ width: '15%' }} className={classes.headName}>Note</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.items.map(issue => (
                <TableRow key={issue.id} className={classes.tableRow} onClick={() => props.issueDetail(issue)}>
                  <TableCell className={classes.cellContent} style={{ borderLeft: `5px solid ${levelOptions.find(e => e.label == issue.level)?.color || '#ccc'}`}}><span className='line-clamp-1'>{`[${issue.code}] ${issue.title || ''}`}</span></TableCell>
                  <TableCell className={classes.cellContent}>{issue.kind?.name}</TableCell>
                  <TableCell className={classes.cellContent}>{moment(issue.created_at).format('lll')}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.assignee?.name}</TableCell>
                  <TableCell className={classes.cellContent}>{issue.level}</TableCell>
                  <TableCell className={classes.cellContent}>
                    <span className='line-clamp-1' dangerouslySetInnerHTML={{ __html: issue.note}} />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      }
    </div>
  )
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

export default connect(mapStateToProps)(OtherTab);