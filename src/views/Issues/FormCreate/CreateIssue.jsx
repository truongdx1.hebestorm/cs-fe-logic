/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box, Grid, TextField, CircularProgress
} from "@material-ui/core";
import config from "config";
import { FormModal, FormModalSide, FormModalSideStatic } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { makeId, customStyles, disabledStyles, errorStyles } from "../../../utils";
import clsx from 'clsx'
import AsyncSelect from 'react-select/async'
import AsyncCreatableSelect from 'react-select/async-creatable';
import {
  IconArrowLeft
} from "@tabler/icons";
import { Button } from 'rsuite'
import { OrderDetail } from '../Form/components'
import { IssueOrder } from './components'
import { TinyEditorComponent } from '../../../_components'
import { stateOptions } from '../List/constants'
import validate from "validate.js";

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  input: {
    fontSize: 14,
  },
  textBold: {
    fontWeight: 'bold'
  },
  disabled: {
    backgroundColor: '#F1F3F4',
    borderRadius: 4,
    color: '#000'
  }
});

class CreateIssue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        },
        isLoading: false
      },
      order: null,
      isLoading: false,
      currentTab: 'customer',
      reply: false,
      optionLevel1: [],
      optionLevel2: [],
      optionLevel3: [],
    };
    this.quillRef = React.createRef()
    this.schema = {
      order_name: {
        presence: {allowEmpty: false, message: '^Required'}
      },
      kind_level_1: {
        presence: {allowEmpty: false, message: '^Required'}
      },
      kind_level_2: {
        presence: {allowEmpty: false, message: '^Required'}
      },
      kind_level_3: {
        presence: {allowEmpty: false, message: '^Required'}
      },
      subject: {
        presence: {allowEmpty: false, message: '^Required'}
      },
      content: {
        presence: {allowEmpty: false, message: '^Required'}
      },
    }
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Create Issue");
    // this.getListItems(this.state.formState.values);
    request.get(`${config.apiUrl}/api/ticket_field/level_1`).then(res => {
      if (res.data.success) {
        this.setState({ optionLevel1: res.data.data.options || [] })
      }
    })
    request.get(`${config.apiUrl}/api/ticket_field/level_2`).then(res => {
      if (res.data.success) {
        this.setState({ optionLevel2: res.data.data.options || [] })
      }
    })
    request.get(`${config.apiUrl}/api/ticket_field/level_3`).then(res => {
      if (res.data.success) {
        this.setState({ optionLevel3: res.data.data.options || [] })
      }
    })
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  forwardMail = () => {
    this.setState({ reply: false })
    FormModalSide.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <ForwardForm />
      )
    })
  }

  editOrder = () => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <OrderDetail />
      )
    })
  }

  validateForm = (formState) => {
    const errors = validate(formState.values, this.schema);
    formState['isValid'] = errors ? false : true
    formState['errors'] = errors || {}
    return formState
  }

  hasError = (field) => {
    return this.state.formState.errors && this.state.formState.errors[field]
  }

  handleChange = (event) => {
    const formState = Object.assign({}, this.state.formState)
    const { name, value } = event.target
    formState['values'][event.target.name] = event.target.value
    this.setState({ formState })
  }

  loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  };

  handleInputChange = (newValue) => {
    return newValue;
  };

  editOrder = () => {
    FormModalSideStatic.instance.current.showForm({
      side: 'right',
      width: '50vw',
      customView: () => (
        <OrderDetail />
      )
    })
  }

  handleCreate = () => {
    const formState = this.validateForm(this.state.formState)
    if (!formState.isValid) {
      this.setState({ formState })
      return
    }
    if (!this.state.order) {
      toastr.error("Order is invalid")
      return
    }
    formState['isLoading'] = true
    this.setState({ formState })
    const query = {}
    const allKeys = Object.keys(formState.values)
    allKeys.map(key => {
      if (key == 'cs_state') {
        query[key] = formState.values[key].id
      } else if (['kind_level_1', 'kind_level_2', 'kind_level_3'].includes(key)) {
        query[key] = formState.values[key].value
      } else {
        query[key] = formState.values[key]
      }
    })
    const { dispatch } = this.props
    dispatch(showLoading())
    request.post_json(`${config.apiUrl}/api/cs_issues`, query).then(res => {
      if (res.data.success) {
        toastr.success("Create issue successful")
        dispatch(hideLoading())
        this.props.history.push(`/issues/${res.data.data.id}`)
      } else {
        toastr.error(res.data.msg)
        dispatch(hideLoading())
        const formState = Object.assign({}, this.state.formState)
        formState.isLoading = false
        this.setState({ formState })
      }
    }, err => {
      toastr.error(err)
      dispatch(hideLoading())
      const formState = Object.assign({}, this.state.formState)
      formState.isLoading = false
      this.setState({ formState })
    })
  }

  render() {
    const { classes, user } = this.props;
    const { formState, reply } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <FormModalSideStatic />
        <Grid container spacing={3}>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <div className="d-flex align-items-center justify-content-between">
              <div className="d-flex align-items-center">
                <div 
                  className="d-flex align-items-center justify-content-center cursor-pointer" 
                  style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}
                  onClick={() => this.props.history.push('/issues')}
                >
                  <IconArrowLeft size={30} stroke={2} />
                </div>
                <div className="ml-2 f-18 font-weight-bold">New Ticket</div>
              </div>
              <div className="d-flex align-items-center">
                <Button 
                  className="button-primary"
                  onClick={() => this.handleCreate()}
                  disabled={formState.isLoading || !this.state.order}
                >
                  {formState.isLoading ? <CircularProgress style={{ width: 10, height: 10 }} /> : 'Save'}
                </Button>
                <Button
                  className="button-default ml-2"
                  onClick={() => this.props.history.push('/issues')}
                  disabled={formState.isLoading}
                >
                  {formState.isLoading ? <CircularProgress style={{ width: 10, height: 10 }} /> : 'Cancel'}
                </Button>
              </div>
            </div>
            
            <div className={clsx(classes.card, 'mt-2 p-3')}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <div>
                    <Box fontWeight="bold" className="f-14">
                      Order Number <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <TextField 
                      name="order_name"
                      variant="outlined"
                      value={formState.values.order_name || ''}
                      onChange={this.handleChange}
                      margin="dense"
                      placeholder="Order Number..."
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.input
                        }
                      }}
                      onKeyPress={(event) => {
                        if (event.key === "Enter") {
                          this.setState({ isLoading: true }, () => {
                            const order_name = this.state.formState.values.order_name.replace('#', '%23')
                            request.get(`${config.apiCoreUrl}/api/order/${order_name}`, {}).then(res => {
                              if (res.data.success) {
                                this.setState({ order: res.data.data, isLoading: false })
                              } else {
                                toastr.error(res.data.msg)
                                this.setState({ isLoading: false, order: null })
                              }
                            }, err => {
                              toastr.error(err)
                              this.setState({ isLoading: false, order: null })
                            })
                          })
                        }
                      }}
                      error={this.hasError('order_name')}
                      helperText={this.hasError('order_name') ? formState.errors.order_name[0] : ''}
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div>
                    <Box fontWeight="bold" className="f-14">
                      From
                    </Box>
                    <AsyncSelect
                      isDisabled
                      cacheOptions
                      defaultOptions
                      placeholder={''}
                      getOptionLabel={({ name, email }) => `${name} <${email}>`}
                      getOptionValue={({ id }) => id}
                      value={{id: 1, name: 'HEBECARES', email: 'support@hebecares.com'}}
                      styles={disabledStyles}
                    />
                  </div>
                  <div className="mt-2">
                    <Box fontWeight="bold" className="f-14">
                      To
                    </Box>
                    <AsyncSelect
                      isDisabled
                      defaultOptions
                      placeholder={''}
                      getOptionLabel={({ email }) => email}
                      getOptionValue={({ id }) => id}
                      value={{id: 1, email: this.state.order?.email }}
                      styles={disabledStyles}
                    />
                  </div>
                  {/* <div className="mt-2">
                    <Box fontWeight="bold" className="f-14">
                      CS State <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <AsyncSelect
                      isSearchable
                      cacheOptions
                      loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                        return new Promise(resolve => {
                          const result = stateOptions.filter(e => e.name.toLowerCase().includes(inputValue.toLowerCase()))
                          resolve(result)
                        })
                      })}
                      defaultOptions
                      onInputChange={this.handleInputChange}
                      placeholder={''}
                      getOptionLabel={({ name }) => name}
                      getOptionValue={({ id }) => id}
                      onChange={(value) => {
                        const event = {
                          target: {
                            name: 'cs_state',
                            value
                          }
                        }
                        this.handleChange(event)
                      }}
                      value={formState.values.cs_state || null}
                      styles={this.hasError('cs_state') ? errorStyles : customStyles}
                    />
                    {this.hasError('cs_state') && <small style={{ color: 'red'}}>{formState.errors.cs_state[0]}</small>}
                  </div> */}
                </Grid>
                <Grid item xs={6}>
                  <div>
                    <Box fontWeight="bold" className="f-14">
                      Level 1 <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <AsyncCreatableSelect
                      cacheOptions
                      defaultOptions={this.state.optionLevel1}
                      loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                        return new Promise(resolve => {
                          request.get(`${config.apiUrl}/api/ticket_field/level_1`, {}).then(res => {
                            if (res.data.success) {
                              const result = res.data.data.options || []
                              const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                              resolve(options)
                            } else {
                              resolve([])
                            }
                          }, err => resolve([]))
                        })
                      })}
                      styles={this.hasError('kind_level_1') ? errorStyles : customStyles}
                      getOptionLabel={({ text }) => text}
                      getOptionValue={({ key }) => key}
                      placeholder="Kind level 1..."
                      getNewOptionData={(inputValue, optionLabel) => ({
                        key: optionLabel,
                        text: 'Create \"' + inputValue + '\"...',
                        __isNew__: true
                      })}
                      onCreateOption={(inputValue) => {
                        const optionLevel1 = Object.assign([], this.state.optionLevel1)
                        const newOption = {
                          key: makeId(10),
                          text: inputValue,
                          value: inputValue
                        }
                        optionLevel1.push(newOption)
                        const query = {
                          options: optionLevel1
                        }
                        request.update_json(`${config.apiUrl}/api/ticket_field/level_1`, query).then(res => {
                          if (res.data.success) {
                            const formState = Object.assign({}, this.state.formState)
                            formState.values['kind_level_1'] = newOption
                            this.setState({ formState, optionLevel1 })
                          } else {
                            toastr.error("Create option failed")
                          }
                        }, err => toastr.error(err))
                      }}
                      value={formState.values.kind_level_1 || null}
                      onChange={(value) => {
                        const event = {
                          target: {
                            name: 'kind_level_1',
                            value
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                    {this.hasError('kind_level_1') && <small style={{ color: 'red'}}>{formState.errors.kind_level_1[0]}</small>}
                  </div>
                  <div className="mt-2">
                    <Box fontWeight="bold" className="f-14">
                      Level 2 <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <AsyncCreatableSelect
                      cacheOptions
                      defaultOptions={this.state.optionLevel2}
                      loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                        return new Promise(resolve => {
                          request.get(`${config.apiUrl}/api/ticket_field/level_2`, {}).then(res => {
                            if (res.data.success) {
                              const result = res.data.data.options || []
                              const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                              resolve(options)
                            } else {
                              resolve([])
                            }
                          }, err => resolve([]))
                        })
                      })}
                      styles={this.hasError('kind_level_2') ? errorStyles : customStyles}
                      getOptionLabel={({ text }) => text}
                      getOptionValue={({ key }) => key}
                      placeholder="Kind level 2..."
                      getNewOptionData={(inputValue, optionLabel) => ({
                        key: optionLabel,
                        text: 'Create \"' + inputValue + '\"...',
                        __isNew__: true
                      })}
                      onCreateOption={(inputValue) => {
                        const optionLevel2 = Object.assign([], this.state.optionLevel2)
                        const newOption = {
                          key: makeId(10),
                          text: inputValue,
                          value: inputValue
                        }
                        optionLevel2.push(newOption)
                        const query = {
                          options: optionLevel2
                        }
                        request.update_json(`${config.apiUrl}/api/ticket_field/level_2`, query).then(res => {
                          if (res.data.success) {
                            const formState = Object.assign({}, this.state.formState)
                            formState.values['kind_level_2'] = newOption
                            this.setState({ formState, optionLevel2 })
                          } else {
                            toastr.error("Create option failed")
                          }
                        }, err => toastr.error(err))
                      }}
                      value={formState.values.kind_level_2 || null}
                      onChange={(value) => {
                        const event = {
                          target: {
                            name: 'kind_level_2',
                            value
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                    {this.hasError('kind_level_2') && <small style={{ color: 'red'}}>{formState.errors.kind_level_2[0]}</small>}
                  </div>
                  <div className="mt-2">
                    <Box fontWeight="bold" className="f-14">
                      Level 3 <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <AsyncCreatableSelect
                      cacheOptions
                      defaultOptions={this.state.optionLevel3}
                      loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                        return new Promise(resolve => {
                          request.get(`${config.apiUrl}/api/ticket_field/level_3`, {}).then(res => {
                            if (res.data.success) {
                              const result = res.data.data.options || []
                              const options = result.filter(e => e.text.toLowerCase().includes(inputValue.toLowerCase()))
                              resolve(options)
                            } else {
                              resolve([])
                            }
                          }, err => resolve([]))
                        })
                      })}
                      styles={this.hasError('kind_level_3') ? errorStyles : customStyles}
                      getOptionLabel={({ text }) => text}
                      getOptionValue={({ key }) => key}
                      placeholder="Kind level 3..."
                      getNewOptionData={(inputValue, optionLabel) => ({
                        key: optionLabel,
                        text: 'Create \"' + inputValue + '\"...',
                        __isNew__: true
                      })}
                      onCreateOption={(inputValue) => {
                        const optionLevel3 = Object.assign([], this.state.optionLevel3)
                        const newOption = {
                          key: makeId(10),
                          text: inputValue,
                          value: inputValue
                        }
                        optionLevel3.push(newOption)
                        const query = {
                          options: optionLevel3
                        }
                        request.update_json(`${config.apiUrl}/api/ticket_field/level_3`, query).then(res => {
                          if (res.data.success) {
                            const formState = Object.assign({}, this.state.formState)
                            formState.values['kind_level_3'] = newOption
                            this.setState({ formState, optionLevel3 })
                          } else {
                            toastr.error("Create option failed")
                          }
                        }, err => toastr.error(err))
                      }}
                      value={formState.values.kind_level_3 || null}
                      onChange={(value) => {
                        const event = {
                          target: {
                            name: 'kind_level_3',
                            value
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                    {this.hasError('kind_level_3') && <small style={{ color: 'red'}}>{formState.errors.kind_level_3[0]}</small>}
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div>
                    <Box fontWeight="bold" className="f-14">
                      Subject <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <TextField  
                      name="subject"
                      variant="outlined"
                      value={formState.values.subject || ''}
                      onChange={this.handleChange}
                      margin="dense"
                      placeholder="Subject..."
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.input
                        }
                      }}
                      error={this.hasError('subject')}
                      helperText={this.hasError('subject') ? formState.errors.subject[0] : ''}
                    />
                  </div>
                  <div className="mt-2 issue-description">
                    <Box fontWeight="bold" className="f-14">
                      Description <span style={{ color: 'red' }}>*</span>
                    </Box>
                    <TinyEditorComponent 
                      className={this.hasError('content') ? 'error' : ''}
                      style={{ width: '100%', marginTop: 10 }}
                      ref={this.quillRef}
                      content={formState.values.content}
                      id={makeId(20)}
                      onEditorChange={(content) => {
                        const event = {
                          target: {
                            name: 'content',
                            value: content
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                    {this.hasError('content') && <small style={{ color: 'red'}}>{formState.errors.content[0]}</small>}
                  </div>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <IssueOrder
              editOrder={this.editOrder}
              order={this.state.order}
              isLoading={this.state.isLoading}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}


CreateIssue.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(CreateIssue)));
export { connectedList as CreateIssue };
