import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider,
  Table, TableRow, TableCell, TableBody, Tooltip,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
import { CopyToClipboard } from '../../../../common'
import { request } from '../../../../_services/request'
import { Placeholder } from 'rsuite'
import moment from 'moment'
import config from 'config'
import {
  IconExternalLink, IconClipboardList
} from "@tabler/icons";

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const ListItemInfo = ({ title, value, icon, divider, fullWidth }) => {
  const classes = useStyles()
  return (
    <>
      <ListItem style={{ padding: '4px 0px' }}>
        <ListItemText 
          style={{ width: '40%' }} 
          secondary={title}
          classes={{ secondary: classes.textBold }}
        />
        <ListItemText 
          style={{ width: '60%' }} 
          secondary={
            <div className='d-flex'>
              <div className={fullWidth ? 'w-100' : ''}>{value}</div>
              {icon && <div className='ml-2'>{icon}</div>}
            </div>
          } 
        />
      </ListItem>
      {divider && <Divider />}
    </>
  )
}

const IssueOrder = (props) => {
  const classes = useStyles()
  const { order, isLoading } = props;
  const [issues, setIssues] = useState([])
  const [isLoadingIssue, setLoadingIssue] = useState(false)

  useEffect(() => {
    if (order && order.name) {
      setLoadingIssue(true)
      request.get(`${config.apiUrl}/api/issues`, {query: order.name, search_option: 'order_name'}).then(res => {
        setLoadingIssue(false)
        if (res.data.success) {
          setIssues(res.data.data.result)
        } else {
          toastr.error(res.data.msg)
        }
      }, err => toastr.error(err))
    } else {
      setIssues([])
    }
  }, [order]);
  

  return (
    <div>
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2 d-flex align-items-center justify-content-between">
          <strong style={{ color: '#449242' }} className='f-16'>General Info</strong>
          {order && <div 
            className="cursor-pointer" 
            style={{ color: '#3788E8', fontSize: 15 }}
            onClick={props.editOrder}
          >
            Edit
          </div>}
        </div>
        <Divider />
        <div className="pl-3 pr-3">
          {isLoading && 
            <Placeholder.Grid rows={11} columns={2} active />
          }
          {order && !isLoading &&
          <List>
            <ListItemInfo 
              title="Order Name:" 
              value={order.name} 
              divider 
              icon={<IconExternalLink size={20} stroke={2} />}
              action={() => console.log('1111')}
            />
            <ListItemInfo 
              title="Created At:" 
              // value={<span>January 14, 2022 at 9:24 pm from <a href='#'>Gasbystore</a></span>} 
              value={<span>{moment(order.ordered_at).format('LLL')} from <a href={`https://${order.store.domain}`} target="_blank">{order.store?.name}</a></span>}
              divider
            />
            <ListItemInfo 
              title="Customer Name:" 
              value={<a href='#'>{order.shipping_address?.name}</a>} 
              divider
            />
            <ListItemInfo 
              title="Contact Info:" 
              value={order.customer_info?.email}
              divider
              icon={<CopyToClipboard text={order.customer_info?.email} />}
            />
            <ListItemInfo 
              title="Phone:" 
              value={order.shipping_address?.phone}
              divider
              icon={<CopyToClipboard text={order.shipping_address?.phone} />}
            />
            <ListItemInfo 
              title="Shipping Address:" 
              value={
                <div>
                  <p>{order.shipping_address?.name}</p>
                  <p>{order.shipping_address?.address1}</p>
                  <p>{order.shipping_address?.zip} - {order.shipping_address?.city}</p>
                  <p>{order.shipping_address?.country}</p>
                </div>
              } 
              divider
              icon={<CopyToClipboard text={`${order.shipping_address?.name} ${order.shipping_address?.address1} ${order.shipping_address?.zip} ${order.shipping_address?.city} ${order.shipping_address?.country}`} />}
            />
            <ListItemInfo 
              title="Subtotal:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span>{order.items.length} Item(s)</span>
                  <span>{order.subtotal_price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Shipping Fee:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span>Secured Shipping (include Tracking)</span>
                  <span>{order.total_shipping?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Tip:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span></span>
                  <span>{order.total_tip?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
            />
            <ListItemInfo 
              title="Total:" 
              fullWidth
              value={
                <div className='d-flex align-items-center justify-content-between'>
                  <span></span>
                  <span>{order.total_price?.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: 2 })}</span>
                </div>
              }
              divider
            />
            <ListItemInfo 
              title="Note:" 
              value={<i>{order.note}</i>}
            />
          </List>
          }
        </div>
      </div>
      <div className={classes.card}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontSize: 16, fontWeight: 'bold', color: '#449242' }}>
                Order Detail
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Lineitem SKU</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Lineitem Name</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Link Design/Ali/Sup</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Supplier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Tracking</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Carrier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN uploaded at</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN status</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Est time</TableCell>
            </TableRow>
            {isLoading && 
              <TableRow>
                <TableCell colSpan={10}>
                  <Placeholder.Grid rows={8} columns={10} active />
                </TableCell>
              </TableRow>
            }
            {order && order.items.map(item => (
              <TableRow className={classes.tableRow} key={item.id}>
                <TableCell className={classes.cellContent}>{item.sku}</TableCell>
                <TableCell className={classes.cellContent}>
                  <Tooltip placement='top' arrow title={item.lineitem_name}>
                    <span className='line-clamp-1'>{item.lineitem_name}</span>
                  </Tooltip>
                </TableCell>
                <TableCell className={classes.cellContent}>Link</TableCell>
                <TableCell className={classes.cellContent}>{item.level}</TableCell>
                <TableCell className={classes.cellContent}>{item.supplier?.name}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.name).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.carrier).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.imported_at).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}>{item.tracking_numbers?.map(e => e.status).join(', ')}</TableCell>
                <TableCell className={classes.cellContent}></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <div className={classes.card}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={8} style={{ fontSize: 16, fontWeight: 'bold', color: '#449242' }}>
                Related Issues
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Order ID</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Code</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Title</TableCell>
              <TableCell className={classes.headName} style={{ width: '20%' }}>Note</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Created At</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Forward To</TableCell>
            </TableRow>
            {isLoadingIssue && 
              <TableRow>
                <TableCell colSpan={8}>
                  <Placeholder.Grid rows={8} columns={8} active />
                </TableCell>
              </TableRow>
            }
            {issues.length > 0 && issues.map(issue => (
              <TableRow className={classes.tableRow} key={issue.id}>
                <TableCell className={classes.cellContent}>{issue.issue_ref?.order_item?.id}</TableCell>
                <TableCell className={classes.cellContent}>{issue.code}</TableCell>
                <TableCell className={classes.cellContent}>{issue.level}</TableCell>
                <TableCell className={classes.cellContent}>{issue.kind?.name}</TableCell>
                <TableCell className={classes.cellContent}>{issue.issue_ref?.order_item?.order_name}</TableCell>
                <TableCell className={classes.cellContent}>{issue.note}</TableCell>
                <TableCell className={classes.cellContent}>{moment(issue.created_at).format('LLL')}</TableCell>
                <TableCell className={classes.cellContent}></TableCell>
              </TableRow>
            ))}
            {issues.length == 0 && !isLoadingIssue &&
              <TableRow className={classes.tableRow}>
                <TableCell colSpan={8}>No issue related</TableCell>
              </TableRow>
            }
          </TableBody>
        </Table>
      </div>
    </div>
  )
}

export default IssueOrder