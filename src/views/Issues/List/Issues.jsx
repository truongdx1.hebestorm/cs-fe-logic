/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs, ToolbarAction } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box,
  Checkbox,
  InputAdornment,
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
} from "@material-ui/core";
import config from "config";
import {
  Filter,
  MultiInputPicker,
  FormModal,
  FormModalSide,
} from "../../../common";
import { request } from "../../../_services/request";
import AsyncSelect from "react-select/async";
import toastr from "../../../common/toastr";
import { customStyles } from "../../../utils";
import moment from "moment";
import { fields, stateOptions, issueFields } from "./constants";
import { IconSearch, IconDots, IconDownload, IconPlus } from "@tabler/icons";
import { Button, Dropdown, Input } from "rsuite";
import { QuickFilter, QuickFilterIssue } from "./components";
import queryString from "query-string";

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: "0 1rem",
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: "1px solid #D8D8D8",
    backgroundColor: "#fff",
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  notchedOutline: {
    borderRadius: 6,
  },
  inputRoot: {
    "&.MuiInput-underline:hover:before": {
      border: "1px solid #3476D9",
    },
  },
  input: {
    fontSize: 14,
  },
  headName: {
    fontWeight: "bold",
    borderRight: "1px solid #D8D8D8",
    borderTop: "1px solid #D8D8D8",
    borderBottom: "1px solid #D8D8D8",
    background: "#ECECEC",
    "&:last-child": {
      borderRight: "none",
    },
  },
  cellContent: {
    borderRight: "1px solid #D8D8D8",
    padding: "8px",
    "&:last-child": {
      borderRight: "none",
    },
  },
});

class Issues extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formState: {
        values: {
          page: 1,
          limit: 15,
          tab: this.getTabFromUrl(),
          // query: ""
        },
      },
      date_action: "",
      selectedItems: [],
      count: {},
      expandFilterRow: false,
      expandFilterColumn: false,
      totals: 0,
      show_keys: [],
      items: [],
      type: "",
      levelCount: {},
      dataFilters: {},
      isLoadingFilter: false,
    };
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Issues");
    this.getListItems(this.state.formState.values);
  }

  getQuickFilters = (query) => {
    const queryObject = Object.assign({}, this.state.queryObject);
    var keys = Object.keys(query);
    keys.map((key, index) => {
      if (typeof query[key] == "array" || query[key] instanceof Array) {
        if (query[key].length > 0) {
          const arr = [];
          query[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format("YYYY-MM-DD"));
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (
        typeof query[key] == "object" ||
        query[key] instanceof Object
      ) {
        queryObject[key] = query[key].id;
      } else {
        queryObject[key] = query[key];
      }
    });
    this.setState({ isLoadingFilter: true }, () => {
      request
        .get(`${config.apiUrl}/api/cs_issues/realtime_report`, queryObject)
        .then(
          (res) => {
            this.setState({ isLoadingFilter: false });
            if (res.data.success) {
              this.setState({ dataFilters: res.data.data.result });
            } else {
              toastr.error(res.data.msg);
              this.setState({ dataFilters: {} });
            }
          },
          (err) => {
            toastr.error(err);
            this.setState({ isLoadingFilter: false, dataFilters: {} });
          }
        );
    });
  };

  getListItems = (query) => {
    const queryObject = Object.assign({}, this.state.queryObject);
    var keys = Object.keys(query);
    keys.map((key, index) => {
      if (typeof query[key] == "array" || query[key] instanceof Array) {
        if (query[key].length > 0) {
          const arr = [];
          query[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format("YYYY-MM-DD"));
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (
        typeof query[key] == "object" ||
        query[key] instanceof Object
      ) {
        queryObject[key] = query[key].id;
      } else {
        queryObject[key] = query[key];
      }
    });
    if (this.state.formState.values.tab == "customer") {
      this.query(queryObject);
      this.getQuickFilters(this.state.formState.values);
    } else {
      this.queryIssue(queryObject);
    }
  };

  getTabFromUrl = () => {
    const query = queryString.parse(window.location.search);
    var tab = "customer";
    if (
      query["tab"] &&
      ["customer", "system", "supplier", "seller", "designer"].includes(
        query["tab"]
      )
    ) {
      tab = query["tab"];
    } else {
      query["tab"] = tab;
    }
    this.hashUrl(query);
    return tab;
  };

  hashUrl = (query) => {
    const newUrl =
      window.location.pathname + "?" + queryString.stringify(query);
    window.history.pushState({}, "", newUrl);
  };

  query = (queryObject) => {
    const { dispatch } = this.props;
    dispatch(showLoading());
    request.get(`${config.apiUrl}/api/cs_issues`, queryObject).then(
      (res) => {
        if (res.data.success) {
          dispatch(hideLoading());
          this.setState({
            items: res.data.data.result,
            totals: res.data.data.total,
            count: res.data.data.count,
            type: "cs_issues",
          });
        } else {
          dispatch(hideLoading());
          toastr.error(res.data.msg);
        }
      },
      (err) => {
        toastr.error(err);
        dispatch(hideLoading());
      }
    );
  };

  queryIssue = (queryObject) => {
    const { dispatch } = this.props;
    dispatch(showLoading());
    request.get(`${config.apiUrl}/api/issues`, queryObject).then(
      (res) => {
        if (res.data.success) {
          dispatch(hideLoading());
          this.setState({
            items: res.data.data.result,
            totals: res.data.data.total,
            count: res.data.data.count,
            type: "issues",
            levelCount: res.data.data.level_count,
          });
        } else {
          dispatch(hideLoading());
          toastr.error(res.data.msg);
        }
      },
      (err) => {
        toastr.error(err);
        dispatch(hideLoading());
      }
    );
  };

  handlePageChange = (event, page) => {
    const formState = Object.assign({}, this.state.formState);
    formState.values.page = page + 1;
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleRowsPerPageChange = (event) => {
    const formState = Object.assign({}, this.state.formState);
    formState.values.page = 1;
    formState.values.limit = event.target.value;
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  toggleDrawer = (side, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    this.setState({ [side]: open });
  };

  handleChangeTab = (event, newValue) => {
    const formState = Object.assign({}, this.state.formState);
    formState.values.tab = newValue;
    this.setState({ formState }, () => {
      const query = queryString.parse(window.location.search);
      query["tab"] = newValue;
      this.hashUrl(query);
      this.getListItems(this.state.formState.values);
    });
  };

  handleChangeFilter = (event) => {
    const formState = Object.assign({}, this.state.formState);
    const { name, value } = event.target;
    formState["values"][name] = value;
    this.setState({ formState });
  };

  onSearch = () => {
    this.getListItems(this.state.formState.values);
  };

  resetFilter = () => {
    const formState = Object.assign({}, this.state.formState);
    const all_keys = Object.keys(this.state.formState.values);
    all_keys.forEach((e) => {
      if (!["page", "limit", "tab"].includes(e)) {
        delete formState.values[e];
      }
    });
    formState.values.page = 1;
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleSelectAll = (event) => {
    let selectedItems;

    if (event.target.checked) {
      selectedItems = this.state.items.map((t) => t.id);
    } else {
      selectedItems = [];
    }
    this.setState({ selectedItems });
  };

  handleSelectOne = (event, id) => {
    const { selectedItems } = this.state;
    const selectedIndex = selectedItems.indexOf(id);
    let newselectedItems = [];

    if (selectedIndex === -1) {
      newselectedItems = newselectedItems.concat(selectedItems, id);
    } else if (selectedIndex === 0) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(1));
    } else if (selectedIndex === selectedItems.length - 1) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(0, -1));
    } else if (selectedIndex > 0) {
      newselectedItems = newselectedItems.concat(
        selectedItems.slice(0, selectedIndex),
        selectedItems.slice(selectedIndex + 1)
      );
    }
    this.setState({ selectedItems: newselectedItems });
  };

  filterDateAction = (date_action) => {
    const formState = Object.assign({}, this.state.formState);
    switch (date_action) {
      case "today":
        formState.values["created_at"] = [
          moment(new Date()).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "yesterday":
        formState.values["created_at"] = [
          moment(new Date(moment().subtract(1, "days"))).format("YYYY-MM-DD"),
          moment(new Date(moment().subtract(1, "days"))).format("YYYY-MM-DD"),
        ];
        break;
      case "last_7_days":
        formState.values["created_at"] = [
          moment(new Date(moment().subtract(6, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "last_30_days":
        formState.values["created_at"] = [
          moment(new Date(moment().subtract(29, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "last_90_days":
        formState.values["created_at"] = [
          moment(new Date(moment().subtract(89, "days"))).format("YYYY-MM-DD"),
          moment(new Date()).format("YYYY-MM-DD"),
        ];
        break;
      case "":
        delete formState.values["created_at"];
        break;
      default:
        break;
    }
    this.setState({ formState, date_action }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue);
  };

  handleInputChange = (newValue) => {
    return newValue;
  };

  onClickCellFilter = (agent, cs_state, realtime_overdue) => {
    const formState = Object.assign({}, this.state.formState);
    formState.values["agent_id"] = agent;
    formState.values["cs_states"] = cs_state;
    formState.values["realtime_overdue"] = realtime_overdue;
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  assignToCS = (selectedItems) => {
    FormModal.instance.current.showForm({
      title: "Assign",
      customView: (submitData, handleChange) => (
        <div className="col-12 pb-3" style={{ zIndex: 1100 }}>
          <Box fontWeight={"bold"} className="f-14">
            Agent
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            menuPortalTarget={document.body}
            loadOptions={(inputValue) =>
              this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise((resolve) => {
                  request.get(`${config.apiLoginUrl}/api/v1/users`, {}).then(
                    (res) => {
                      if (res.data.success) {
                        resolve(res.data.data.result);
                      } else {
                        resolve([]);
                      }
                    },
                    (err) => resolve([])
                  );
                });
              })
            }
            defaultOptions
            onInputChange={this.handleInputChange}
            placeholder={""}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ uid }) => uid}
            onChange={(value) => {
              const event = {
                target: {
                  name: "assignee",
                  value,
                },
              };
              handleChange(event);
            }}
            value={submitData.values.assignee || null}
            styles={customStyles}
          />
        </div>
      ),
      action: {
        titleAction: "Confirm",
        onAction: (submitData) => {
          return new Promise((resolve) => {
            request
              .post_json(`${config.apiUrl}/api/cs_actions/commit`, {
                action_code: "assign",
                action_data: {
                  assignee: submitData.values.assignee,
                },
                object_type: "cs_issues",
                object_ids: selectedItems,
              })
              .then(
                (res) => {
                  if (res.data.success) {
                    resolve(true);
                    toastr.success("Assign successful");
                  } else {
                    toastr.error(res.data.msg);
                    resolve(false);
                  }
                  this.onSearch();
                },
                (err) => {
                  toastr.error(err);
                  resolve(false);
                }
              );
          });
        },
      },
    });
  };

  changeCsState = (selectedItems) => {
    FormModal.instance.current.showForm({
      title: "Change CS state",
      customView: (submitData, handleChange) => (
        <div className="col-12 pb-3" style={{ zIndex: 1100 }}>
          <Box fontWeight={"bold"} className="f-14">
            CS state
          </Box>
          <AsyncSelect
            isSearchable
            cacheOptions
            menuPortalTarget={document.body}
            loadOptions={(inputValue) =>
              this.loadOptions(inputValue, function loadingData(inputValue) {
                return new Promise((resolve) => {
                  resolve([
                    {
                      key: 0,
                      name: "Need 1st response",
                      value: "need_1st_response",
                    },
                    {
                      key: 1,
                      name: "Open",
                      value: "open",
                    },
                    {
                      key: 2,
                      name: "Wait for customer",
                      value: "wait_for_customer",
                    },
                    {
                      key: 3,
                      name: "Wait for 3rd party",
                      value: "wait_for_third_party",
                    },
                    {
                      key: 4,
                      name: "Pending",
                      value: "pending",
                    },
                    {
                      key: 5,
                      name: "Resolved",
                      value: "resolved",
                    },
                    {
                      key: 6,
                      name: "Closed",
                      value: "closed",
                    },
                  ]);
                });
              })
            }
            defaultOptions
            onInputChange={this.handleInputChange}
            placeholder={""}
            getOptionLabel={({ name }) => name}
            getOptionValue={({ value }) => value}
            onChange={(value) => {
              const event = {
                target: {
                  name: "cs_state",
                  value,
                },
              };
              handleChange(event);
            }}
            value={submitData.values.cs_state || null}
            styles={customStyles}
          />
        </div>
      ),
      action: {
        titleAction: "Confirm",
        onAction: (submitData) => {
          return new Promise((resolve) => {
            request
              .post_json(`${config.apiUrl}/api/cs_actions/commit`, {
                action_code: "change_cs_state",
                action_data: {
                  cs_state: submitData.values.cs_state,
                },
                object_type: "cs_issues",
                object_ids: selectedItems,
              })
              .then(
                (res) => {
                  if (res.data.success) {
                    resolve(true);
                    toastr.success("Assign successful");
                  } else {
                    toastr.error(res.data.msg);
                    resolve(false);
                  }
                  this.onSearch();
                },
                (err) => {
                  toastr.error(err);
                  resolve(false);
                }
              );
          });
        },
      },
    });
  };

  onClickFilterIssue = (level) => {
    const formState = Object.assign({}, this.state.formState);
    formState.values["levels"] = JSON.stringify(level);
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  render() {
    const { classes, user } = this.props;
    const {
      formState,
      right,
      items,
      selectedItems,
      expandFilterColumn,
      expandFilterRow,
      date_action,
    } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <Filter
          right={right}
          onClose={(side, open) => this.toggleDrawer(side, open)}
          formState={this.state.formState}
          onChange={this.handleChangeFilter}
          onSearch={this.onSearch}
          resetFilter={this.resetFilter}
          filters={[]}
        />
        <div style={{ paddingTop: 5 }}>
          {formState.values.tab == "customer" && (
            <QuickFilter
              expandFilterColumn={expandFilterColumn}
              expandFilterRow={expandFilterRow}
              setState={(data) => this.setState(data)}
              data={this.state.dataFilters}
              isLoading={this.state.isLoadingFilter}
              onClickCell={(agent, cs_state, realtime_overdue) =>
                this.onClickCellFilter(agent, cs_state, realtime_overdue)
              }
            />
          )}
          {formState.values.tab != "customer" && (
            <QuickFilterIssue
              data={this.state.levelCount}
              setState={(data) => this.setState(data)}
              onClickCell={(level) => this.onClickFilterIssue(level)}
            />
          )}
          <ToolbarAction
            selectedAction={date_action}
            leftActions={[
              {
                id: "today",
                text: "today",
                visible: true,
                action: () => this.filterDateAction("today"),
              },
              {
                id: "yesterday",
                text: "yesterday",
                visible: true,
                action: () => this.filterDateAction("yesterday"),
              },
              {
                id: "last_7_days",
                text: "last_7_days",
                visible: true,
                action: () => this.filterDateAction("last_7_days"),
              },
              {
                id: "last_30_days",
                text: "last_30_days",
                visible: true,
                action: () => this.filterDateAction("last_30_days"),
              },
              {
                id: "last_90_days",
                text: "last_90_days",
                visible: true,
                action: () => this.filterDateAction("last_90_days"),
              },
            ]}
            leftResetAction={{
              title: "Clear",
              visible: date_action != "",
              action: () => this.filterDateAction(""),
            }}
            rightActions={[
              {
                text: "export",
                color: "default",
                visible: true,
                action: () => {},
                icon: <IconDownload size={20} stroke={2} />,
              },
              {
                text: "new_ticket",
                color: "primary",
                visible: true,
                action: () => this.props.history.push("/create_issue/-1"),
                icon: <IconPlus size={20} stroke={2} />,
              },
            ]}
          />
          <div className={classes.card}>
            <Box style={{ borderBottom: "0.5px solid #ccc" }}>
              <AntTabs
                currentTab={formState.values.tab || "customer"}
                handleChangeTab={this.handleChangeTab}
                items={[
                  { value: "customer", label: "Customer" },
                  { value: "system", label: "System" },
                  { value: "supplier", label: "Supplier" },
                  { value: "designer", label: "Design" },
                  { value: "seller", label: "Seller" },
                ]}
              />
            </Box>
            <div style={{ display: "flex", padding: "5px 10px" }}>
              {selectedItems.length > 0 && formState.values.tab == "customer" && (
                <Dropdown
                  renderTitle={() => (
                    <Button
                      style={{
                        border: "1px solid #ccc",
                        marginTop: 8,
                        marginRight: 10,
                      }}
                    >
                      <IconDots size={20} stroke={2} />
                    </Button>
                  )}
                >
                  <Dropdown.Item
                    onSelect={() => this.assignToCS(selectedItems)}
                  >
                    Assign to CS
                  </Dropdown.Item>
                  <Dropdown.Item
                    onSelect={() => this.changeCsState(selectedItems)}
                  >
                    Change CS State
                  </Dropdown.Item>
                </Dropdown>
              )}
              <TextField
                id="query"
                margin="dense"
                name="query"
                placeholder="Search by issues, order name, customer message, agent note..."
                classes={{
                  root: classes.inputRoot,
                }}
                value={formState.values.query}
                onChange={(e) => {
                  e.persist();
                  this.handleChangeFilter(e);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <IconSearch size={20} stroke={1.5} />
                    </InputAdornment>
                  ),
                  classes: {
                    notchedOutline: classes.notchedOutline,
                    input: classes.input,
                  },
                }}
                style={{ width: "30%", backgroundColor: "#fff" }}
                variant="outlined"
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    this.onSearch();
                  }
                }}
              />
              {formState.values.tab == "customer" && (
                <div style={{ marginLeft: 5 }}>
                  <MultiInputPicker
                    onDelete={(click) => (this.handleDeleteMultiPicker = click)}
                    valueKey="id"
                    style={{ marginTop: 8 }}
                    getOptionLabel={(item) => item.name}
                    onChange={(items) => {
                      const formState = Object.assign({}, this.state.formState);
                      if (items.length > 0) {
                        formState.values["cs_states"] = items;
                      } else {
                        delete formState.values["cs_states"];
                      }
                      this.setState({ formState }, () =>
                        this.getListItems(this.state.formState.values)
                      );
                    }}
                    loadOptions={(inputValue) => {
                      return new Promise((resolve) => {
                        resolve(stateOptions);
                      });
                    }}
                    noOptionsMessage="No results found"
                    renderAriaLabel={(selectedItems) =>
                      `${selectedItems.length} states selected`
                    }
                    defaultAriaLabel="CS State"
                  />
                </div>
              )}
            </div>
            <TableContainer
              style={{
                maxHeight: expandFilterRow
                  ? "calc(100vh - 550px)"
                  : "calc(100vh - 380px)",
              }}
            >
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell
                      style={{ width: "3%" }}
                      align="center"
                      padding="checkbox"
                      className={classes.headName}
                    >
                      <Checkbox
                        checked={selectedItems.length === items.length}
                        color="secondary"
                        indeterminate={
                          selectedItems.length > 0 &&
                          selectedItems.length < items.length
                        }
                        onChange={this.handleSelectAll}
                      />
                    </TableCell>
                    {this.state.type == "cs_issues" &&
                      fields.map((field) => (
                        <TableCell
                          key={field.name}
                          style={{ width: field.width }}
                          align="left"
                          className={classes.headName}
                        >
                          {field.label}
                        </TableCell>
                      ))}
                    {this.state.type == "issues" &&
                      issueFields.map((field) => (
                        <TableCell
                          key={field.name}
                          style={{ width: field.width }}
                          align="left"
                          className={classes.headName}
                        >
                          {field.label}
                        </TableCell>
                      ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((item, index) => (
                    <TableRow
                      key={item.id}
                      className="cursor-pointer"
                      style={{
                        backgroundColor: index % 2 == 1 ? "#ECECEC" : "#fff",
                      }}
                      onClick={() =>
                        this.props.history.push(
                          `/${this.state.type}/${item.id}`,
                          {
                            from:
                              window.location.pathname + window.location.search,
                          }
                        )
                      }
                    >
                      <TableCell
                        onClick={(e) => e.stopPropagation()}
                        padding="checkbox"
                        align="center"
                        className={classes.cellContent}
                      >
                        <Checkbox
                          checked={selectedItems.indexOf(item.id) !== -1}
                          color="secondary"
                          onChange={(event) =>
                            this.handleSelectOne(event, item.id)
                          }
                          value="true"
                        />
                      </TableCell>
                      {this.state.type == "cs_issues" &&
                        fields.map((field) => (
                          <TableCell
                            key={field.name}
                            className={classes.cellContent}
                          >
                            {field.renderItem(item)}
                          </TableCell>
                        ))}
                      {this.state.type == "issues" &&
                        issueFields.map((field) => (
                          <TableCell
                            key={field.name}
                            className={classes.cellContent}
                          >
                            {field.renderItem(item)}
                          </TableCell>
                        ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              style={{ borderTop: "1px solid #D8D8D8" }}
              component="div"
              count={this.state.totals}
              onPageChange={this.handlePageChange}
              onRowsPerPageChange={this.handleRowsPerPageChange}
              page={formState.values.page - 1}
              rowsPerPage={this.state.formState.values.limit}
              rowsPerPageOptions={[15, 25, 50, 100]}
            />
          </div>
        </div>
      </div>
    );
  }
}

Issues.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(
  connect(mapStateToProps)(withStyles(useStyles)(Issues))
);
export { connectedList as Issues };
