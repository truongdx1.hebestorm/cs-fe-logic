import React, { useState, useEffect } from 'react'
import { Table, TableRow, TableCell, TableBody, CircularProgress, TableContainer, TableHead } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import {
  IconArrowUp, IconArrowDown, IconChevronLeft, IconChevronRight, IconChevronsLeft
} from "@tabler/icons";
import { Badge } from 'rsuite'
import clsx from  'clsx'

const useStyles = makeStyles(theme => ({
  filterRoot: {
    backgroundColor: '#fff', 
    border: '1px solid #D8D8D8', 
    borderRadius: 6
  },
  filterRootExpand: {
    backgroundColor: '#fff', 
    border: '1px solid #D8D8D8', 
    borderRadius: 6,
    height: 250,
    overflow: 'auto'
  },
  cellHeaderFilterCollapse: {
    borderRight: '1px solid #D8D8D8',
    borderBottom: 'none',
    fontWeight: 'bold',
    background: '#ECECEC',
    padding: '8px 10px',
    '&:first-child': {
      borderBottomLeftRadius: 6
    },
    '&:last-child': {
      borderBottomRightRadius: 6,
      borderRight: 'none'
    },
    '&:nth-child(n+8)': {
      background: '#FF5E4D',
      color: '#fff'
    },
    '&:nth-child(8)': {
      cursor: 'pointer',
      borderTop: '1px solid #FF5E4D'
    }
  },
  cellHeaderFilterExpand: {
    borderRight: '1px solid #D8D8D8',
    borderBottom: '1px solid #E2E2E2',
    fontWeight: 'bold',
    background: '#ECECEC',
    padding: '8px 10px',
    '&:last-child': {
      borderRight: 'none'
    },
    '&:nth-child(n+8)': {
      background: '#FF5E4D',
      color: '#fff'
    },
    '&:nth-child(8)': {
      cursor: 'pointer',
      borderTop: '1px solid #FF5E4D'
    }
  },
  cellContentFilter: {
    borderRight: '1px solid #D8D8D8',
    cursor: 'pointer',
    padding: '8px 10px',
    '&:last-child': {
      borderRight: 'none'
    },
    '&:nth-child(n+8)': {
      color: '#FF5E4D'
    },
  },
  cellContentSelected: {
    border: '2px solid red',
    cursor: 'pointer',
    padding: '8px 10px',
    '&:nth-child(n+8)': {
      color: '#FF5E4D'
    },
  },
  badge: {
    background: '#fff',
    color: '#000'
  },
  tableRow: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#F8F8F8'
    }
  }
}));

const QuickFilter = (props) => {
  const { expandFilterColumn, expandFilterRow, isLoading, data } = props;
  const [dataSelect, setDataSelect] = useState(null)
  // const [expandFilterColumn, setExpandFilterColumn] = useState(false)
  // const [expandFilterRow, setExpandFilterRow] = useState(false)

  const onClickUnresolve = (agent_id, cs_states) => {
    if (dataSelect == `${agent_id}_unresolved_${cs_states}`) {
      setDataSelect(null)
      props.onClickCell(0, [], "")
    } else {
      setDataSelect(`${agent_id}_unresolved_${cs_states}`)
      const states = cs_states == 'all' ? ['need_1st_response', 'open', 'wait_for_customer', 'wait_for_third_party', 'pending'] : [cs_states]
      props.onClickCell(agent_id, states, "")
    }
  }
  
  const onClickOverdue = (agent_id, realtime_overdue) => {
    if (dataSelect == `${agent_id}_overdue_${realtime_overdue}`) {
      setDataSelect(null)
      props.onClickCell(0, [], "")
    } else {
      setDataSelect(`${agent_id}_overdue_${realtime_overdue}`)
      props.onClickCell(agent_id, [], realtime_overdue)
    }
  }

  const classes = useStyles()
  return (
    <div className={expandFilterRow ? classes.filterRootExpand : classes.filterRoot}>
      <div 
        className="d-flex align-items-center justify-content-between cursor-pointer pl-2 pr-2"
        onClick={() => {
          props.setState({ expandFilterRow: !expandFilterRow })
        }}
        style={{ height: 40 }}
      >
        <strong>Quick Filter</strong>
        <div className='d-flex align-items-center justify-content-center'>
          {isLoading && <CircularProgress className='mr-2' style={{ width: 16, height: 16 }} color="primary" />}
          {expandFilterRow ? <IconArrowUp size={16} stroke={2} /> : <IconArrowDown size={16} stroke={2} />}
        </div>
      </div>
      <TableContainer style={{ maxHeight: 210 }}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Agent</TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Unresolved <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.count || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Need 1st response <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.need_1st_response || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Open <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.open || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Wait for Customer <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.wait_for_customer || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Wait for 3rd party <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.wait_for_third_party || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Pending <Badge className={classes.badge} maxCount={999} content={data?.count?.unresolved?.pending || 0} /></TableCell>
              <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse} onClick={() => props.setState({ expandFilterColumn: !expandFilterColumn })}>Overdue <Badge className={classes.badge} maxCount={999} content={data?.count?.overdue?.count || 0} /> {expandFilterColumn ? <IconChevronLeft size={20} stroke={2} /> : <IconChevronRight size={20} stroke={2} />}</TableCell>
              {expandFilterColumn && 
                <>
                  <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Need 1st response <Badge className={classes.badge} maxCount={999} content={data?.count?.overdue?.need_1st_response || 0} /></TableCell>
                  <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>Open <Badge className={classes.badge} maxCount={999} content={data?.count?.overdue?.open || 0} /></TableCell>
                  <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>{'Unresolved (>20 days)'} <Badge className={classes.badge} maxCount={999} content={data?.count?.overdue?.unresolved || 0} /></TableCell>
                  <TableCell align="center" className={expandFilterRow ? classes.cellHeaderFilterExpand : classes.cellHeaderFilterCollapse}>{'Unclosed (>60 days)'} <Badge className={classes.badge} maxCount={999} content={data?.count?.overdue?.unclosed || 0} /></TableCell>
                </>
              }
            </TableRow>
          </TableHead>
          <TableBody>
            {expandFilterRow && data.agents?.length > 0 && data.agents.map((item, index) => (
              <TableRow key={item.agent.id} style={{ backgroundColor: index % 2 == 0 ? '#fff' : '#EFEFEF' }}>
                <TableCell align="center" className={classes.cellContentFilter}>{item.agent.name}</TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'all')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_all` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.count}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'need_1st_response')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_need_1st_response` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.need_1st_response}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'open')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_open` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.open}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'wait_for_customer')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_wait_for_customer` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.wait_for_customer}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'wait_for_third_party')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_wait_for_third_party` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.wait_for_third_party}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickUnresolve(item.agent.id, 'pending')} 
                  className={clsx(dataSelect == `${item.agent.id}_unresolved_pending` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.unresolved.pending}
                </TableCell>
                <TableCell 
                  align="center" 
                  onClick={() => onClickOverdue(item.agent.id, 'all')} 
                  className={clsx(dataSelect == `${item.agent.id}_overdue_all` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                >
                  {item.overdue.count}
                </TableCell>
                {expandFilterColumn && 
                  <>
                    <TableCell 
                      align="center" 
                      onClick={() => onClickOverdue(item.agent.id, 'need_1st_response')} 
                      className={clsx(dataSelect == `${item.agent.id}_overdue_need_1st_response` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                    >
                      {item.overdue.need_1st_response}
                    </TableCell>
                    <TableCell 
                      align="center" 
                      onClick={() => onClickOverdue(item.agent.id, 'open')} 
                      className={clsx(dataSelect == `${item.agent.id}_overdue_open` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                    >
                      {item.overdue.open}
                    </TableCell>
                    <TableCell 
                      align="center" 
                      onClick={() => onClickOverdue(item.agent.id, 'unresolved')} 
                      className={clsx(dataSelect == `${item.agent.id}_overdue_unresolved` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                    >
                      {item.overdue.unresolved}
                    </TableCell>
                    <TableCell 
                      align="center" 
                      onClick={() => onClickOverdue(item.agent.id, 'unclosed')} 
                      className={clsx(dataSelect == `${item.agent.id}_overdue_unclosed` ? classes.cellContentSelected : classes.cellContentFilter, 'cursor-pointer')}
                    >
                      {item.overdue.unclosed}
                    </TableCell>
                  </>
                }
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default QuickFilter;