import React, { useState, useEffect } from 'react'
import { Table, TableRow, TableCell, TableBody, CircularProgress, TableContainer, TableHead } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import {
  IconArrowUp, IconArrowDown, IconChevronLeft, IconChevronRight, IconChevronsLeft
} from "@tabler/icons";
import { Badge } from 'rsuite'
import clsx from  'clsx'

const useStyles = makeStyles(theme => ({
  filterRoot: {
    backgroundColor: '#fff', 
    border: '1px solid #D8D8D8', 
    borderRadius: 6,
    padding: "15px 120px"
  },
  filterSelected: {
    color: '#D7782D'
  }
}));

const levelOptions = [
  {id: 'T0', label: "Need assigning"},
  {id: 'T1', label: "Need an offer"},
  {id: 'T2', label: "Offer to customer"},
  {id: 'T3', label: "Update next action"},
  {id: 'T4', label: "Process next action"},
  {id: 'T5', label: "Closed"},
]

const QuickFilterIssue = (props) => {
  const { data } = props;
  const [dataSelect, setDataSelect] = useState(null)

  const onClickFilter = (key) => {
    if (key == dataSelect) {
      props.onClickCell([])
      setDataSelect(null)
    } else {
      setDataSelect(key)
      props.onClickCell([key])
    }
  }

  const classes = useStyles()
  return (
    <div className={clsx(classes.filterRoot, 'd-flex align-items-center justify-content-between')}>
      {Object.keys(data || {}).map(key => (
        <div 
          className={clsx(dataSelect == key ? classes.filterSelected : '', 'd-flex flex-column cursor-pointer')}
          onClick={() => onClickFilter(key)}
        >
          <span className='text-center font-weight-bold f-24'>{data ? data[key] : 0}</span>
          <span className='text-center text-upper f-16 font-weight-bold'>{key} - {levelOptions.find(e => e.id == key).label}</span>
        </div>
      ))}
    </div>
  )
}

export default QuickFilterIssue;