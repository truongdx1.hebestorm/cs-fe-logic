import React from 'react'
import moment from 'moment'
import { capitalizeStr } from '../../../utils'
import { Tooltip } from '@material-ui/core'

export const stateOptions = [
  {id: 'new', name: 'New', bgColor: '#A1E5CC', color: '#000'},
  {id: 'open', name: 'Open', bgColor: '#11A35B', color: '#fff'},
  {id: 'pending', name: 'Pending', bgColor: '#F3BF4F', color: '#000'},
  {id: 'need_1st_response', name: 'Need 1st Response', bgColor: '#C992EA', color: '#000'},
  {id: 'wait_for_customer', name: 'Wait for Customer', bgColor: '#1B79E5', color: '#fff'},
  {id: 'wait_for_third_party', name: 'Wait on 3rd party', bgColor: '#8257F9', color: '#fff'},
  {id: 'resolved', name: 'Resolved', bgColor: '#795514', color: '#fff'},
  {id: 'closed', name: 'Closed', bgColor: '#636B72', color: '#fff'},
]

export const kindOptions = [
  {id: 'address_confirmation', name: 'Address confirmation'},
  {id: 'ask_for_help', name: 'Ask for help'},
  {id: 'cancel_order', name: 'Cancel order'},
  {id: 'change_product', name: 'Change product'},
  {id: 'change_shipping_information', name: 'Change shipping information'},
  {id: 'check', name: 'Check'},
  {id: 'cs_change_item_info', name: 'CS change item info'},
  {id: 'cs_change_shipping_info', name: 'CS change shipping info'},
  {id: 'dispute', name: 'Dispute'},
  {id: 'fraud', name: 'Fraud'},
  {id: 'item_not_as_described_merchadise', name: 'Item not as described merchandise'},
  {id: 'item_not_received', name: 'Item not received'},
  {id: 'other', name: 'Other'},
  {id: 'out_of_stock', name: 'Out of stock'},
  {id: 'pending', name: 'Pending'},
  {id: 'pickup_notify', name: 'Pickup notify'},
  {id: 'fraud', name: 'Fraud'},
]

export const fields = [
  {
    name: 'code',
    label: 'Code',
    renderItem: (item) => item.code,
    width: '7%',
  },
  {
    name: 'order_name',
    label: 'Order Name',
    renderItem: (item) => item.order_name,
    width: '7%',
  },
  {
    name: 'content',
    label: 'Customer Message',
    renderItem: (item) => <Tooltip arrow placement='top' title={<div dangerouslySetInnerHTML={{ __html: item.content }}  />}><div dangerouslySetInnerHTML={{ __html: item.content }} className='line-clamp-1' /></Tooltip>,
    width: '10%',
  },
  {
    name: 'kind',
    label: 'Kind',
    renderItem: (item) => <Tooltip arrow placement='top' title={item.kind ? capitalizeStr(item.kind, "_") : ''}><span className="line-clamp-1">{item.kind ? capitalizeStr(item.kind, "_") : ''}</span></Tooltip>,
    width: '7%',
  },
  {
    name: 'created_at',
    label: 'Created at',
    renderItem: (item) => moment(item.created_at).format('lll'),
    width: '7%',
  },
  {
    name: 'note',
    label: 'Agent Note',
    renderItem: (item) => <div className="line-clamp-1" dangerouslySetInnerHTML={{ __html: item.note }} />,
    width: '9%',
  },
  {
    name: 'time_remaining',
    label: 'Time Remaining',
    renderItem: (item) => {
      const due = new Date(moment(item.cs_state_overdue_at))
      const now = new Date()
      var hours = 0;
      var active = ''
      if (due >= now) {
        hours = (Math.abs(due - now) / 36e5).toFixed(2);
        active = 'positive'
      } else {
        hours = (Math.abs(now - due) / 36e5).toFixed(2);
        active = 'negative'
      }
      return (
        <div className="d-flex align-items-center">
          <div className={`mr-2 time-remaining-${active}`} />
          <div className='line-clamp-1'>{active == 'positive' ? `${hours} hour(s)` : `${hours} hour(s) ago`}</div>
        </div>
      )
    },
    width: '8%',
  },
  {
    name: 'cs_state',
    label: 'CS State',
    // renderItem: (item) => <Tooltip arrow placement='top' title={stateOptions.find(e => e.id == item.cs_state).name}><span className="line-clamp-1">{stateOptions.find(e => e.id == item.cs_state).name}</span></Tooltip>,
    renderItem: (item) => {
      const option = stateOptions.find(e => e.id == item.cs_state)
      return (
        <div className='f-10 br-6 text-center width-fit-content' style={{ padding: '3px 5px', backgroundColor: option?.bgColor || '#000', color: option?.color || '#fff' }}>{option?.name}</div>
      )
    },
    width: '7%',
  },
  {
    name: 'agent',
    label: 'Agent',
    renderItem: (item) => item.customer_support?.name,
    width: '7%',
  },
  {
    name: 'kind_level_1',
    label: 'Kind Level 1',
    renderItem: (item) => item.kind_level_1,
    width: '7%',
  },
  {
    name: 'kind_level_2',
    label: 'Kind Level 2',
    renderItem: (item) => item.kind_level_2,
    width: '7%',
  },
  {
    name: 'kind_level_3',
    label: 'Kind Level 3',
    renderItem: (item) => item.kind_level_3,
    width: '7%',
  }
]

export const issueFields = [
  {
    name: 'code',
    label: 'Code',
    renderItem: (item) => item.code,
    width: '8%',
  },
  {
    name: 'title',
    label: 'Title',
    renderItem: (item) => item.title,
    width: '15%',
  },
  {
    name: 'level',
    label: 'Level',
    renderItem: (item) => item.level,
    width: '5%',
  },
  {
    name: 'assignee',
    label: 'Assignee',
    renderItem: (item) => item.assignee?.name,
    width: '10%',
  },
  {
    name: 'kind',
    label: 'Kind',
    renderItem: (item) => item.kind?.name,
    width: '15%',
  },
  {
    name: 'note',
    label: 'Note',
    renderItem: (item) => <div className="line-clamp-1" dangerouslySetInnerHTML={{ __html: item.note }} />,
    width: '15%',
  },
  {
    name: 'created_at',
    label: 'Created at',
    renderItem: (item) => moment(item.created_at).format('lll'),
    width: '12%',
  },
  {
    name: 'solution',
    label: 'Solution',
    renderItem: (item) => item.solution?.name,
    width: '20%',
  },
]

export const offerIssues = [
  {id: 1, name: 'None'},
  {id: 2, name: "Hold order and wait on Customer's feedback"},
  {id: 3, name: "Refund"},
  {id: 4, name: "Resend"},
  {id: 5, name: "Resend + Refund"},
  {id: 6, name: "Cancel"},
  {id: 7, name: "Cancel + Refund"},
  {id: 8, name: "Change info"},
  {id: 9, name: "Change info + Refund"},
  {id: 10, name: "Check information"},
  {id: 11, name: "Contact PO"},
  {id: 12, name: "No Action"},
  {id: 13, name: "Offer resend/refund"},
]

export const customerDecisions = [
  {id: 'agree', name: 'Agree'},
  {id: 'disagree', name: 'Disagree'},
  {id: 'no_response', name: 'No response'},
]

export const nextActions = [
  {id: 'A1', name: 'A1. Process order'},
  {id: 'A2', name: 'A2. Change info + Not Refund => Process'},
  {id: 'A3', name: 'A3. Change info + Refund => Process'},
  {id: 'A4', name: 'A4. Process Resend + Not Refund'},
  {id: 'A5', name: 'A5. Process Resend + Refund'},
  {id: 'B1', name: 'B1. Cancel original order + Not Refund'},
  {id: 'B2', name: 'B2. Cancel original order + Refund'},
  {id: 'B3', name: 'B3. Cancel original item + Change info + Not refund => Process new item'},
  {id: 'B4', name: 'B4. Cancel original item + Change info + Refund => Process new item'},
  {id: 'C1', name: 'C1. Refund Only'},
  {id: 'D2', name: 'D2. Create New Offer'},
  {id: 'E1', name: 'E1. No action'},
]