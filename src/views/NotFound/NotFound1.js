import React from 'react';
import { makeStyles } from '@material-ui/styles';
import config from 'config'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    display: 'flex',
    justifyContent:'center',
    alignItems: 'center',
    flexDirection: 'column'
  }
}));

const NotFound1 = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <p className='nf-p'>Welcome to 403:</p>
      <h1 className='nf-h1'>
        <span style={{ transform: 'skew(-15deg) scale(1, 1)', letterSpacing: '5px' }}>F</span>
        <span style={{ transform: 'skew(-13.2353deg) scale(1, 1.11765)', letterSpacing: '5.88235px'}}>O</span>
        <span style={{ transform: 'skew(-11.4706deg) scale(1, 1.23529)', letterSpacing: '6.76471px'}}>R</span>
        <span style={{ transform: 'skew(-9.70588deg) scale(1, 1.35294)', letterSpacing: '7.64706px'}}>B</span>
        <span style={{ transform: 'skew(-7.94118deg) scale(1, 1.47059)', letterSpacing: '8.52941px'}}>I</span>
        <span style={{ transform: 'skew(-6.17647deg) scale(1, 1.58824)', letterSpacing: '9.41176px'}}>D</span>
        <span style={{ transform: 'skew(-4.41176deg) scale(1, 1.70588)', letterSpacing: '10.2941px'}}>D</span>
        <span style={{ transform: 'skew(-2.64706deg) scale(1, 1.82353)', letterSpacing: '11.1765px'}}>E</span>
        <span style={{ transform: 'skew(-0.882353deg) scale(1, 1.94118)', letterSpacing: '12.0588px'}}>N</span>
        <span style={{ transform: 'skew(0.882353deg) scale(1, 2.05882)', letterSpacing: '12.9412px'}}>&nbsp;</span>
        <span style={{ transform: 'skew(2.64706deg) scale(1, 1.82353)', letterSpacing: '11.1765px'}}>R</span>
        <span style={{ transform: 'skew(4.41176deg) scale(1, 1.70588)', letterSpacing: '10.2941px'}}>E</span>
        <span style={{ transform: 'skew(6.17647deg) scale(1, 1.58824)', letterSpacing: '9.41176px'}}>S</span>
        <span style={{ transform: 'skew(7.94118deg) scale(1, 1.47059)', letterSpacing: '8.52941px'}}>O</span>
        <span style={{ transform: 'skew(9.70588deg) scale(1, 1.35294)', letterSpacing: '7.64706px'}}>U</span>
        <span style={{ transform: 'skew(11.4706deg) scale(1, 1.23529)', letterSpacing: '6.76471px'}}>R</span>
        <span style={{ transform: 'skew(13.2353deg) scale(1, 1.11765)', letterSpacing: '5.88235px'}}>C</span>
        <span style={{ transform: 'skew(15deg) scale(1, 1)', letterSpacing: '5px'}}>E</span>
      </h1>
      <p className='nf-p'>The server understood the request but refuses to authorize it.</p>
      <a className='nf-back' href={config.accountUrl}>Go Back</a>
    </div>
  );
};

export default NotFound1;
