/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component, useReducer } from "react";
import ReactDOM from 'react-dom'
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box, Grid, Chip, Divider,
  Checkbox, List, ListItem, ListItemText,
  InputAdornment,
  TextField, Stepper, Step,
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination,
  Avatar as AvatarMUI
} from "@material-ui/core";
import config from "config";
import { CopyToClipboard, MultiInputPicker, FormModal, FormModalSide, FormModalSideStatic, CustomStep, CustomTimeline } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { makeId } from "../../../utils";
import moment from "moment";
import clsx from 'clsx'
import Dropzone from 'react-dropzone'
import {
  IconEye, IconInfoCircle, IconMessages, IconEdit, IconTemplate, IconCornerUpRight,
  IconArrowLeft, IconPencil, IconArrowUp
} from "@tabler/icons";
import { Button, Avatar, Input } from 'rsuite'
import { OrderDetail } from '../../Issues/Form/components'
import { CaseDetail } from './components'
import { TinyEditorComponent } from '../../../_components'
import { LEVEL_TO_STATE } from './constants'
import issue_request from '../../../_services/issue.service'
import { IssueActionButton } from '../../../_components/IssueActions'
import Uploader from '../../../_components/Uploader'
import AttachmentIcon from '@material-ui/icons/Attachment';
import ClearIcon from '@material-ui/icons/Clear';
import { ForwardForm } from '../../Issues/Form/components'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  cardNote: {
    backgroundColor: '#E7F8FB',
    border: '1px solid #B7DBE0'
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    }
  },
  chipPaid: {
    width: 10,
    height: 10,
    backgroundColor: '#006F53',
    borderRadius: '50%'
  },
  chipCancelled: {
    width: 10,
    height: 10,
    backgroundColor: '#626669',
    borderRadius: '50%'
  },
  chipPending: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  chipUnfulfilled: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  textDisabled: {
    backgroundColor: '#EFF1F2'
  },
  notchedOutline: {
    border: '2px solid #449242'
  },
  dot: {

  }
});

class PaygateDisputeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        }
      },
      // content: "<p>Báo SUP resend + thuyết phục KH close kiện sau khi nhận TKN replacement</p><p>Báo Kh bên ngoài yêu cầu close kiện</p><p>18/1: TKN chưa active</p>",
      content: '',
      caseResponse: null,
      disabledResponse: true,
      editNote: false,
      currentLength: 0,
      currentTab: 'customer',
      reply: false,
      issue: {},
      currentSteps: [],
      agentCommentLogs: [],
      uploader: { values: {} },
      forwardIssues: [],
      newForwardIssueCode: null,
      // forwardMail: {values: { selectedItems: [] }}
    };
    this.quillRef = React.createRef()
    this.inputRef = React.createRef()
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Paygate Dispute");
    // this.getListItems(this.state.formState.values);
    this.getIssue().then(data => this.getAgentCommentLogs(data))
  }

  getIssue = () => {
    const id = this.props.match.params.id

    return issue_request.getIssue(id).then(data => {
      const updators = { issue: data }

      if (data.extra?.agent_note){
        updators.content = data.extra.agent_note
      }

      if (data.extra?.case_response){
        updators.caseResponse = data.extra.case_response
        updators.currentLength = data.extra.case_response.length
      }

      // if (data.extra?.attachments){
      //   const attachments = data.extra.attachments.map((att, index) => ({...att, id: index}))
      //   updators.uploader = {
      //     values: { files: attachments }
      //   }
      // }

      this.setState(updators)

      this.addCurrentSteps(data)

      return data
    }).catch((err) => {
      toastr.error(err)
    })
  }

  getAgentCommentLogs = (issue) => {
      issue_request.fetchLogs(issue, {
        objectType: 'issues',
        actionCode: 'agent_comment'
      }).then(data => {
        this.setState({ agentCommentLogs: data.result })
      })
  }

  addCurrentSteps = (issue) => {
    const steps = issue.kind_steps.concat((issue.solution_steps || {}))
    const nextSteps = steps.filter(step => issue.next_actions?.includes(step.code))
    this.setState({ currentSteps: nextSteps })
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  forwardMail = () => {
    this.setState({ reply: false })
    FormModalSide.instance.current.showForm({
      side: 'right',
      width: '50vw',
      data: { selectedItems: [] },
      customView: (submitData, handleChange) => {
        const { issue } = this.state

        return <ForwardForm issue={issue}
          submitData={submitData}
          handleChange={handleChange}/>
      },
      action: {
        titleAction: 'Submit',
        onAction: (submitData, context) => {
          if (!submitData.values?.selectedItems || submitData.values.selectedItems.length == 0){
            toastr.error("Not select order item")
            return Promise.reject("Error")
          }

          return request.post_json(`${config.apiUrl}/api/issues`, {
            title: submitData.values.title,
            kind_id: submitData.values.kind_id.id,
            note: submitData.values.content,
            forward_to: submitData.values.forward_to.id,
            dest_department: submitData.values.forward_to.id,
            extra: {
              forward_from_issue: this.state.issue.id,
              order_ids: submitData.values.selectedItems
            },
            objects: submitData.values.selectedItems.map((orderItemId) => ({ id: orderItemId, type: 'order_items' }))
          }).then(res => {
            toastr.success("Forward Success!!")
            this.setState({ newForwardIssueCode: res.data.data.code })
            return res
          })
        }
      }
    })
  }

  handleAgentNote = (e) => {
    const { issue, content } = this.state
    issue_request.updateIssue(issue.id, {
      extra: { agent_note: content }
    }).then(data => {
      toastr.success('Updated agent note')
      this.setState({ editNote: false })
    }).catch(error => {
      toastr.error('Failed')
      console.log(error)
      this.setState({ editNote: false })
    })
  }

  handleSaveCaseResponse = (e) => {
    const { currentLength, caseResponse, issue } = this.state

    issue_request.updateIssue(issue.id, {
      extra: { case_response: caseResponse }
    }).then(data => {
      toastr.success('Updated case response')
      this.setState({ disabledResponse: true })
    }).catch(error => {
      toastr.error('Failed')
      console.log(error)
      this.setState({ disabledResponse: true })
    })
  }

  updateIssue = (data) => {
    const { issue } = this.state

    return issue_request.updateIssue(issue.id, data).then(data => {
      this.setState({ issue: data })
    })
  }

  handleSubmitAgentComment = (text) => {
    if (text == null || text == ''){
      return
    }

    const { issue } = this.state
    return issue_request.commitAction({
      code: 'agent_comment',
      data: { text },
      objectType: 'paypal_dispute_issues',
      objectId: issue.id
    }).then(res => {
      this.getAgentCommentLogs(issue)
    }).catch(error => {
      console.log(error)
    })
  }

  handleUploadChange = (e) => {
    const { uploader } = this.state
    uploader.values[e.target.name] = e.target.value

    this.setState({ uploader: {...uploader} })
  }

  handleSuccessUploader = (data) => {
    const attachments = this.state.issue.extra?.attachments || []
    const uploadFiles = data.map(dt => ({ filename: dt.filename, url: dt.url }))
    this.updateIssue({
      extra: { attachments: [...attachments, ...uploadFiles] }
    })
  }

  render() {
    const { classes, user } = this.props;
    const { formState, editNote, issue } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);

    return (
      <div className={classes.root}>
        <FormModalSide />
        <IssueActionButton issue={issue} steps={this.state.currentSteps} objectType="paypal_dispute_issues"
          onSubmitDone={(submitData, res) => this.getIssue()}
        />
        <Grid container spacing={3}>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <div className="d-flex align-items-center justify-content-between">
              <div className="d-flex align-items-center">
                <div className="d-flex align-items-center justify-content-center cursor-pointer" style={{ width: 40, height: 40, borderRadius: 6, border: '1px solid #ccc' }}>
                  <IconArrowLeft size={30} stroke={2} onClick={() => this.props.history.push('/paygate_disputes')}/>
                </div>
                <div className="ml-2 f-18 font-weight-bold" style={{ color: '#449242' }}>{issue.code}</div>
                <div className="ml-2 f-14">Reported at: { issue.extra?.case_filing_date }</div>
              </div>
              <div className="d-flex align-items-center">
                <div className="p-1 pl-2 pr-2 f-12" style={{ backgroundColor: '#11A35B', color: '#fff', borderRadius: 6 }}>
                  { issue.resolved_at ? 'Done' : 'Doing' }
                </div>
                { issue.extra?.response_due_date &&
                  <div className="f-14 ml-2">Due by { issue.extra.response_due_date }</div>
                }
              </div>
            </div>
            <div className='mt-4 mb-4 f-18 font-weight-bold'>
              {issue.extra?.case_type} | { issue.extra?.case_reason }
            </div>
            <CustomStep 
              steps={[
                {value: 'need_checking', label: 'Need Checking'},
                {value: 'negotiating', label: 'Negotiating'},
                {value: 'solving', label: 'Solving'},
                {value: 'reviewing', label: 'Reviewing'},
                {value: 'closed', label: 'Closed'}
              ]}
              activeStep={LEVEL_TO_STATE[issue.level]}
            />
            <div className={clsx(classes.card, 'mt-4')}>
              <div className="p-3 font-weight-bold f-16">Buyer Comment</div>
              <Divider />
              <div className="p-3 d-flex justify-content-between">
                <div style={{ width: '90%' }}>
                  { issue.note }
                </div>
                <div style={{ width: '10%' }} className="d-flex justify-content-end">
                  <div className="d-flex align-items-center justify-content-center cursor-pointer" style={{ width: 30, height: 30, borderRadius: 6, border: '1px solid #ccc' }}>
                    <IconCornerUpRight size={20} stroke={2} onClick={this.forwardMail}/>
                  </div>
                </div>
              </div>
              <div className="p-3">
                <div style={{ border: '1px solid #D8D8D8', borderRadius: 6 }}>
                  <Table>
                    <TableBody>
                      <TableRow className={classes.tableRow}>
                        <TableCell className={classes.headName} style={{ width: '10%' }}>Order Item ID</TableCell>
                        <TableCell className={classes.headName} style={{ width: '10%' }}>Code</TableCell>
                        <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
                        <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
                        <TableCell className={classes.headName} style={{ width: '15%' }}>Title</TableCell>
                        <TableCell className={classes.headName} style={{ width: '20%' }}>Note</TableCell>
                        <TableCell className={classes.headName} style={{ width: '15%' }}>Created At</TableCell>
                        <TableCell className={classes.headName} style={{ width: '10%' }}>Forward To</TableCell>
                      </TableRow>
                      {this.state.forwardIssues.map((forwardIssue, index) => 
                        <TableRow className={classes.tableRow}>
                          <TableCell className={classes.cellContent}>{forwardIssue.extra?.order_ids}</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.code}</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.level}</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.kind?.name}</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.title}*</TableCell>
                          <TableCell className={classes.cellContent}>{ forwardIssue.note }</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.created_at}</TableCell>
                          <TableCell className={classes.cellContent}>{forwardIssue.assignee?.name || forwardIssue.assignee_id}</TableCell>
                        </TableRow>
                      )}
                    </TableBody>
                  </Table>
                </div>
              </div>
              <div className="p-3">
                {!editNote &&
                  <div className="p-3" style={{ border: '2px dashed orange', borderRadius: 6, backgroundColor: '#FCF4E9' }}>
                    <div className="d-flex align-items-center justify-content-between">
                      <div className="font-weight-bold f-16">Agent Note</div>
                      { issue.assignee?.email && issue.assignee.email == user?.data.email &&
                        <Button
                          style={{ backgroundColor: 'orange', color: '#fff' }}
                          onClick={() => this.setState({ editNote: true })}
                        >
                          <IconPencil color="#fff" size={20} stroke={2} /> Edit
                        </Button>
                      }
                    </div>
                    <div className="mt-2">
                      <div dangerouslySetInnerHTML={{ __html: this.state.content }} />
                    </div>
                  </div>
                }
                {editNote && 
                  <div className="d-flex flex-column">
                    <TinyEditorComponent 
                      style={{ width: '100%' }}
                      ref={this.quillRef}
                      content={this.state.content}
                      id={makeId(20)}
                      onEditorChange={(content) => {
                        this.setState({ content })
                      }}
                    />
                    <div className="d-flex align-items-center justify-content-end mt-1">
                      <Button style={{ border: '1px solid #ccc' }} onClick={() => this.setState({ editNote: false })}>
                        Cancel
                      </Button>
                      <Button className="ml-1" onClick={this.handleAgentNote} style={{ backgroundColor: '#449242', border: '1px solid #449242', color: '#fff' }}>
                        Save
                      </Button>
                    </div>
                  </div>
                }
              </div>
            </div>
            <div className={clsx(classes.card, 'mt-2')}>
              <div className="p-3 f-16 font-weight-bold">
                Agent Comments
              </div>
              <Divider />
              <CustomTimeline 
                items={this.state.agentCommentLogs}
                containerClass="p-3"
                firstItemDot={<Avatar circle style={{ backgroundColor: '#FFD097', color: '#000', marginLeft: -15 }}>JS</Avatar>}
                buttonText={'Post'}
                itemClassName="pt-2 ml-2"
                renderItem={(item) => (
                  <div className="p-3" style={{ marginLeft: 4, border: '1px solid #ccc', borderRadius: 6 }}>
                    <Grid container direction="column">
                      <Grid item>
                        <Grid container>
                          <AvatarMUI aria-label="recipe" style={{
                            marginRight: '8px',
                            width: '22px',
                            height: '22px',
                            fontSize: '0.75rem',
                            backgroundColor: 'rgb(255, 208, 151)',
                            color: '#000'
                          }}>
                              CS
                          </AvatarMUI>
                          <div style={{ marginRight: '5px' }}>
                            <b>{ item.actor?.name }</b>
                          </div>
                          <div>{ item.created_at }</div>
                        </Grid>
                      </Grid>
                      <Grid item style={{ marginTop: '5px' }}>
                        {item.data?.text}
                      </Grid>
                    </Grid>
                  </div>)}
                submitText={(text, callback) => this.handleSubmitAgentComment(text).then(() => callback())}
              />
            </div>
            <div className={classes.card}>
              <div className='p-3 f-16 font-weight-bold'>
                Case Response
              </div>
              <Divider />
              <div className="pt-3 pl-3 pr-3">
                <TextField 
                  margin="dense"
                  fullWidth
                  ref={(el) => this.inputRef = el}
                  // ref={this.inputRef}
                  variant="outlined"
                  autoFocus
                  disabled={this.state.disabledResponse}
                  value={this.state.caseResponse || ''}
                  name="case_response"
                  onChange={(e) => this.setState({ caseResponse: e.target.value, currentLength: e.target.value.length })}
                  multiline
                  minRows={5}
                  maxRows={10}
                  inputProps={{ maxLength: 2000 }}
                  InputProps={{
                    classes: {
                      disabled: classes.textDisabled,
                      notchedOutline: classes.notchedOutline
                    }
                  }}
                />
              </div>
              <div className="pl-3 pr-3 pb-3 d-flex justify-content-between">
                <span>{this.state.currentLength}/2000</span>
                <div className="d-flex">
                  <CopyToClipboard text={this.state.caseResponse} titleButton="Copy content" component="button" buttonStyle={{ border: '1px solid #ccc' }} />
                  {this.state.disabledResponse ? 
                    <Button 
                      className="ml-1" 
                      style={{ border: '1px solid #ccc' }} 
                      onClick={() => this.setState({ disabledResponse: false })}
                    >
                      <IconPencil size={20} stroke={2} /> Edit
                    </Button> : 
                    <Button className="ml-1"
                      style={{ border: '1px solid #449242', backgroundColor: '#449242', color: '#fff' }}
                      onClick={this.handleSaveCaseResponse}>
                      Save message
                    </Button>
                  }
                </div>
              </div>
              <div className="p-3">
                {/* <Dropzone
                  multiple={false}
                  accept={'image/*'}
                  onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
                >
                  {({getRootProps, getInputProps, isDragAccept, isDragReject, acceptedFiles, rejectedFiles}) => (
                    <section>
                      <div 
                        className="p-3 br-6 d-flex align-items-center justify-content-center" 
                        style={{ border: '2px dashed #ccc' }}
                        {...getRootProps()} 
                      >
                        <input {...getInputProps()}  />
                        {isDragReject && <div style={{ color: 'red' }}>Please upload a valid type</div>}
                        {acceptedFiles.length == 0 && !isDragReject &&
                          <div className="d-flex align-items-center justify-content-center flex-column">
                            <div style={{ width: 40, height: 40, backgroundColor: '#000' }} className="d-flex align-items-center justify-content-center br-circle">
                              <IconArrowUp size={30} stroke={2} color="#fff" />
                            </div>
                            <Button className="br-6 mt-2 font-weight-bold" style={{ backgroundColor: '#fff', border: '1px solid #ccc' }}>
                              Add file
                            </Button>
                            <span className="mt-2">or drop files to upload</span>
                          </div>
                        }

                        {acceptedFiles && acceptedFiles.length > 0 && <div>ACCBCBCBCBCB</div>}
                      </div>
                    </section>
                  )}
                </Dropzone> */}
                <Uploader handleChange={this.handleUploadChange}
                  submitData={this.state.uploader}
                  uploadOnPicked
                  namespace="hbc/paypal_disputes/uploader"
                  onUploadSuccess={this.handleSuccessUploader}/>
              </div>
              <div style={{ padding: '0 1rem' }}>
                <ul style={{ listStyleType: 'none' }}>
                  {issue.extra?.attachments && issue.extra.attachments.map((attachment, index) =>
                    <li>
                      <AttachmentIcon />
                      <a target='_blank' href={attachment.url}>{ attachment.filename }</a>
                      <ClearIcon style={{ cursor: 'pointer' }} onClick={() => {
                        issue.extra.attachments.splice(index, 1)
                        this.updateIssue({ extra: {
                          attachments: issue.extra.attachments
                        }})
                      }}/>
                    </li>
                  )}
                </ul>
              </div>
            </div>
          </Grid>
          <Grid item xs={6} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <CaseDetail issue={this.state.issue}
              onUpdate={this.updateIssue}
              onRelatedIssueUpdator={(relatedData) => {
                const { issue } = this.state
                issue.order_items = relatedData.order_items

                this.setState({ issue: {...issue} })
              }}
              onUpdateForwardIssues={(forwardIssues) => this.setState({ forwardIssues })}
              newForwardIssueCode={this.state.newForwardIssueCode}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}


PaygateDisputeForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(PaygateDisputeForm)));
export { connectedList as PaygateDisputeForm };
