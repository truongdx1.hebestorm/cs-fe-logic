import React, { useState, useEffect } from 'react'
import { 
  List, ListItem, ListItemText, Divider,
  Table, TableRow, TableCell, TableBody
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { cssConstants } from '../../../../_constants'
// import { CopyToClipboard } from 'react-copy-to-clipboard'
import { CustomTimeline, CopyToClipboard } from '../../../../common'
import {
  IconExternalLink, IconClipboardList
} from "@tabler/icons";
import { Input } from 'rsuite'
import AsyncSelect from 'react-select/async';
import { userService } from '../../../../_services/user.service'
import issue_request from '../../../../_services/issue.service'
import { OrderDetail } from '../../../Issues/Form/components'
import { FormModalSideStatic } from '../../../../common'
import toastr from "../../../../common/toastr";

const useStyles = makeStyles(theme => ({
  ...cssConstants,
  card: {
    borderRadius: 6,
    marginBottom: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
}));

const ListItemInfo = ({ title, value, icon, divider, fullWidth }) => {
  const classes = useStyles()
  return (
    <>
      <ListItem style={{ padding: '4px 0px' }}>
        <ListItemText 
          style={{ width: '40%' }} 
          secondary={title}
          classes={{ secondary: classes.textBold }}
        />
        <ListItemText 
          style={{ width: '60%' }} 
          secondary={
            <div className='d-flex'>
              <div className={fullWidth ? 'w-100' : ''}>{value}</div>
              {icon && <div className='ml-2'>{icon}</div>}
            </div>
          } 
        />
      </ListItem>
      {divider && <Divider />}
    </>
  )
}

const CaseDetail = (props) => {
  const classes = useStyles()
  const [disabled, setDisabled] = useState(true)
  const { issue } = props
  const [updators, setUpdators] = useState({})
  const [userOptions, setUserOptions] = useState([])
  const [orderItems, setOrderItems] = useState(null)
  const [relatedIssues, setRelatedIssues] = useState(null)
  const [order, setOrder] = useState(null)

  const handleUpdate = () => {
    if (Object.keys(updators).length > 0){
      props.onUpdate({
        extra: {
          case_link: updators.case_link,
          link_email: updators.link_email
        },
        assignee_id: updators.assignee?.value
      })
    }
    setDisabled(true)
  }

  useEffect(() => {
    setUpdators({
      case_link: issue.extra?.case_link,
      link_email: issue.extra?.link_email
    })

    if (Object.keys(issue).length > 0 && !orderItems && !relatedIssues){
      fetchRelatedResources()
    }
  }, [props.issue])

  const fetchRelatedResources = () => {
    issue_request.fetchRelatedResources(issue).then(data => {
      setOrderItems(data.order_items || [])
      setRelatedIssues(data.related_issues || [])
      setOrder(data.order || {})

      if (props.onRelatedIssueUpdator) props.onRelatedIssueUpdator(data)
      if (props.onUpdateForwardIssues && data.related_issues.length > 0) {
        const forwardIssues = data.related_issues.filter(forwardIssue => forwardIssue.extra?.forward_from_issue == issue.id)
        if (forwardIssues.length > 0){
          props.onUpdateForwardIssues(forwardIssues)
        }
      }
    })
  }

  useEffect(() => {
    if (props.newForwardIssueCode){
      fetchRelatedResources()
    }
  }, [props.newForwardIssueCode])
  

  useEffect(() => {
    fetchUsers().then(data => {
      const options = data.result.map((dt) => ({ value: dt.uid, label: dt.name }))
      setUserOptions(options)
    })
  }, [])

  const fetchUsers = (filters = {}) => {
    return userService.fetchUsers(filters).then((res) => res.data.data)
  }

  const promiseAssigneeOptions = (inputValue) => {
    return new Promise((resolve, reject) => {
      fetchUsers({ name: inputValue }).then(data => {
        const options = data.result.map((dt) => ({ value: dt.uid, label: dt.name }))
        resolve(options)
      })
    })
  }

  const handleAssigneeChange = (newValue, actionData) => {
    setUpdators({
      ...updators,
      assignee: newValue
    })
  }

  const  editOrder = () => {
    if (order && Object.keys(order).length > 0){
      FormModalSideStatic.instance.current.showForm({
        side: 'right',
        width: '50vw',
        customView: () => (
          <OrderDetail order={order}/>
        )
      })
    } else {
      toastr.error("Not found order")
    }
  }

  return (
    <div>
      <FormModalSideStatic />
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2 d-flex align-items-center justify-content-between">
          <strong style={{ color: '#449242' }}>Case Detail</strong>
          {disabled ? 
            <div 
              className="cursor-pointer" 
              style={{ color: '#3788E8', fontSize: 15 }}
              onClick={() => setDisabled(false)}
            >
              Edit
            </div> : 
            <div 
              className="cursor-pointer font-weight-bold" 
              style={{ color: '#3788E8', fontSize: 15 }}
              onClick={handleUpdate}
            >
              Save
          </div>
          }
        </div>
        <Divider />
        <div className="pl-3 pr-3">
          <List>
            <ListItemInfo title="Company:" value={issue.src_company?.name} divider />
            <ListItemInfo title="Agent:" fullWidth 
              value={disabled ? issue.assignee?.name :
                <AsyncSelect cacheOptions
                  isClearable
                  value={updators.assignee}
                  loadOptions={promiseAssigneeOptions}
                  defaultOptions={userOptions}
                  default
                  onChange={handleAssigneeChange}
                  // onInputChange={handleAssigneeInputChange}
                />}
              divider />
            <ListItemInfo 
              title="Buyer Details:" 
              value={
                <div className='d-flex flex-column'>
                  <span>{issue.extra?.claimant_name}</span>
                  <span>{issue.extra?.claimant_email_address}</span>
                </div>
              } 
              divider 
            />
            <ListItemInfo title="Disputed Amount:" value={issue.extra?.disputed_amount} divider />
            <ListItemInfo title="Transaction ID" value={<a className='font-weight-bold' href="#">{issue.extra?.transaction_id}</a>} divider />
            <ListItemInfo title="Link Case" divider fullWidth
              value={disabled ? 
                <a className='font-weight-bold' href="#">{issue.extra?.paypal_case_link}</a> : 
                <Input value={updators?.case_link} style={{ width: '100%' }} onChange={(value, e) => setUpdators({...updators, case_link: value})}/>}
              />
            <ListItemInfo title="Link Email" divider fullWidth
              value={disabled ?
                <a className='font-weight-bold' href="#">{issue.extra?.link_email}</a> :
                <Input value={updators.link_email} style={{ width: '100%' }} onChange={(value, e) => setUpdators({ ...updators, link_email: value })}/>} />
            <ListItemInfo title="Check Date" divider value={issue.extra?.paypal_check_date} />
            <ListItemInfo title="Shipping Label" value={issue.extra?.shipping_label} />
          </List>
        </div>
      </div>
      <div className={classes.card}>
        <div className="pl-3 pr-3 pt-2 pb-2 d-flex align-items-center justify-content-between">
          <strong style={{ color: '#449242' }}>General Info</strong>
          <div 
            className="cursor-pointer" 
            style={{ color: '#3788E8', fontSize: 15 }}
            onClick={editOrder}
          >
            Edit
          </div>
        </div>
        <Divider />
        <div className="pl-3 pr-3">
          {order && Object.keys(order).length > 0 && 
            <List>
              <ListItemInfo 
                title="Order Name:" 
                value={order.name}
                divider 
                icon={<IconExternalLink size={20} stroke={2} />}
                action={() => console.log('1111')}
              />
              <ListItemInfo 
                title="Created At:" 
                value={<span>{order.ordered_at} from <a href='#'>{order.store?.name}</a></span>} 
                divider
              />
              <ListItemInfo 
                title="Customer Name:" 
                value={<a href='#'>{order.customer_info.last_name} {order.customer_info.first_name}</a>} 
                divider
              />
              <ListItemInfo 
                title="Contact Info:" 
                value={order.customer_info.email}
                divider
                icon={<CopyToClipboard text={order.customer_info.email} />}
              />
              <ListItemInfo 
                title="Phone:" 
                value={ order.customer_info.phone }
                divider
                icon={<CopyToClipboard text={'33333'} />}
              />
              <ListItemInfo 
                title="Shipping Address:" 
                value={
                  <div>
                    <p>{order.shipping_address.address1}</p>
                    <p>{ order.shipping_address.address2 }</p>
                    <p>{order.shipping_address.province}</p>
                    <p>{order.shipping_address.city}</p>
                    <p>{ order.shipping_address.country }</p>
                  </div>
                } 
                divider
                icon={<CopyToClipboard text={'44444'} />}
              />
              <ListItemInfo 
                title="Subtotal:" 
                fullWidth
                value={
                  <div className='d-flex align-items-center justify-content-between'>
                    {/* <span>3 Items</span> */}
                    <span>{order.subtotal_price}</span>
                  </div>
                }
              />
              <ListItemInfo 
                title="Shipping Fee:" 
                fullWidth
                value={
                  <div className='d-flex align-items-center justify-content-between'>
                    {/* <span>Secured Shipping (include Tracking) (0.04 kg)</span> */}
                    <span>{order.total_shipping}</span>
                  </div>
                }
              />
              <ListItemInfo 
                title="Tip:" 
                fullWidth
                value={
                  <div className='d-flex align-items-center justify-content-between'>
                    {/* <span></span> */}
                    <span>{order.total_tip}</span>
                  </div>
                }
              />
              <ListItemInfo 
                title="Total:" 
                fullWidth
                value={
                  <div className='d-flex align-items-center justify-content-between'>
                    {/* <span></span> */}
                    <span>{order.total_price}</span>
                  </div>
                }
                divider
              />
              <ListItemInfo 
                title="Note:" 
                value={<i>{order.note}</i>}
              />
            </List>
          }
        </div>
      </div>
      <div className={classes.card}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontSize: 16, fontWeight: 'bold' }}>
                Order Detail
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Lineitem SKU</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Lineitem Name</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Link Design/Ali/Sup</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Supplier</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Tracking</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Carrier</TableCell>
              {/* <TableCell className={classes.headName} style={{ width: '10%' }}>TKN uploaded at</TableCell> */}
              <TableCell className={classes.headName} style={{ width: '10%' }}>TKN status</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Est time</TableCell>
            </TableRow>
            {orderItems && orderItems.map((orderItem, index) => {
              let tkn = orderItem.tracking_numbers?.filter(tkn => tkn.active)?.first
              return (
                <TableRow className={classes.tableRow} key={index}>
                  <TableCell className={classes.cellContent}>{ orderItem.sku }</TableCell>
                  <TableCell className={classes.cellContent}>{ orderItem.lineitem_name }</TableCell>
                  <TableCell className={classes.cellContent}>{ orderItem.design_link }</TableCell>
                  <TableCell className={classes.cellContent}>{ orderItem.level }</TableCell>
                  <TableCell className={classes.cellContent}>{ orderItem.supplier?.name }</TableCell>
                  <TableCell className={classes.cellContent}>{ tkn && tkn.name }</TableCell>
                  <TableCell className={classes.cellContent}>{ tkn && tkn.carrier }</TableCell>
                  {/* <TableCell className={classes.cellContent}>{tkn && tkn.created_at}</TableCell> */}
                  <TableCell className={classes.cellContent}>{ tkn && tkn.status }</TableCell>
                  <TableCell className={classes.cellContent}>{ orderItem.estimated_delivery_at }</TableCell>
                </TableRow>)
            }
            )}
          </TableBody>
        </Table>
      </div>
      <div className={classes.card}>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell colSpan={8} style={{ fontSize: 16, fontWeight: 'bold' }}>
                Related Issues
              </TableCell>
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Issue ID</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Code</TableCell>
              <TableCell className={classes.headName} style={{ width: '5%' }}>Level</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Kind</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Title</TableCell>
              <TableCell className={classes.headName} style={{ width: '20%' }}>Note</TableCell>
              <TableCell className={classes.headName} style={{ width: '15%' }}>Created At</TableCell>
              <TableCell className={classes.headName} style={{ width: '10%' }}>Forward To</TableCell>
            </TableRow>
            {relatedIssues && relatedIssues.map((relatedIssue, index) =>
              <TableRow className={classes.tableRow} key={index}>
                <TableCell className={classes.cellContent}>{relatedIssue.id}</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.code }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.level }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.kind?.name }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.title }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.note }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.created_at }</TableCell>
                <TableCell className={classes.cellContent}>{ relatedIssue.assignee?.name }</TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    </div>
  )
}

export default CaseDetail