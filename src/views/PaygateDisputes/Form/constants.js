export const LEVEL_TO_STATE = {
  'T1': 'need_checking',
  'T2': 'negotiating',
  'T3': 'solving',
  'T4': 'reviewing',
  'T5': 'closed'
}