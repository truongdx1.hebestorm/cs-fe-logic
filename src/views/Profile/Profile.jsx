import React, { Component } from 'react'
import { Box, Grid, Card, TextField, Checkbox, ListItem, List, ListItemText, ListItemAvatar, Avatar } from "@material-ui/core";
import { request } from '../../_services/request'
import { ToolBar } from '../../_components'
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone";
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import axios from 'axios'
import { AntTabs } from '../../_components'
import toastr from '../../common/toastr'
import { FormModal } from '../../common'
import avatar from '../../assets/images/avatars/avatar.png'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import config from 'config'
import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import TelegramIcon from '@material-ui/icons/Telegram';
import LockIcon from '@material-ui/icons/Lock';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import BlockIcon from '@material-ui/icons/Block';

const useStyles = theme => ({
  root: {
    padding: '10px 30px',
    position: 'relative'
  },
  textField: {
    border: 'none',
  },
  textInput: {
    fontSize: 45,
    padding: 0,
    fontWeight: 'bold',
    textAlign: 'center',
    '&:hover>.Mui-focused': {
        border: '1px dashed #ccc'
    }
  },
  textFocus: {
    border: '1px dashed #ccc'
  },
  titleName: {
    marginLeft: -5,
    flexGrow: '1'
  },
  textInputItem: {
    fontSize: 14,
    padding: 5,
    '&:hover>.Mui-focused': {
        border: '1px dashed #ccc'
    }
  },
});

class Profile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      formState: {
        values: {},
        changes: {}
      },
      currentTab: 'overview'
    }
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Profile");
    const { dispatch } = this.props;
    dispatch(showLoading())
    request.get(`${config.apiLoginUrl}/api/v1/user_profile`, {}).then(res => {
      if (res.data.success) {
        dispatch(hideLoading())
        const formState = Object.assign({}, this.state.formState)
        formState.values = res.data.data
        if (!formState.values.extra_info?.display_name) {
          formState.values.extra_info['display_name'] = formState.values.name
        }
        this.setState({ formState })
      } else {
        dispatch(hideLoading())
        toastr.error(res.data.msg)
      }
    }, err => toastr.error(err))
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  handleChange = (event) => {
    const formState = Object.assign({}, this.state.formState)
    formState['values'][event.target.name] = event.target.value
    formState['changes'][event.target.name] = event.target.value
    this.setState({ formState })
  }

  onDropImage = (accepted, rejected) => {
    if (Object.keys(rejected).length !== 0) {
      
    } else {
      const user = JSON.parse(localStorage.getItem('user_hebecore'))
      var formData = new FormData();
      formData.append('file', accepted[0]);
      formData.append("namespace", "users/avatar")
      axios({
        method: "POST",
        url: `${config.apiCoreUrl}/api/upload_file`,
        headers: {
          'x-access-token': user?.data.token
        },
        data: formData
      }).then(res => {
        if (res.data.success) {
          const extra_info = Object.assign({}, this.state.formState.values.extra_info)
          extra_info['avatar'] = res.data.data.url
          const event = {
            target: {
              name: 'extra_info',
              value: extra_info
            }
          }
          this.handleChange(event)
        } else {
          toastr.error(res.data.msg)
        }
      }, err => toastr.error(err))
    }
  }

  saveChanges = () => {
    const query = this.state.formState.changes
    const formSubmit = {}
    var keys = Object.keys(query);
    keys.map((key, index) => {
      if (key == 'extra_info') {
        formSubmit[key] = JSON.stringify(query[key])
      } else if (typeof query[key] == "object" || query[key] instanceof Object) {
        formSubmit[key] = query[key].id;
      } else {
        formSubmit[key] = query[key];
      }
    });
    console.log(formSubmit, 'sssss')
  }

  render() {
    const { formState } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <FormModal />
        <div style={{ paddingTop: 0 }}>
          <ToolBar
            {...this.props}
            formState={null}
            actions={[
              {
                text: "change_password",
                color: "primary",
                size: "small",
                variant: 'contained',
                visible: true,
                action: () => this.changePassword(),
                icon: <LockIcon />,
              },
              {
                text: "save",
                color: "primary",
                size: "small",
                variant: 'contained',
                visible: Object.keys(formState.changes).length > 0,
                action: () => this.saveChanges(),
                icon: <SaveAltIcon />,
              },
              {
                text: "cancel",
                color: "default",
                size: "small",
                variant: 'contained',
                visible: Object.keys(formState.changes).length > 0,
                action: () => this.discardChanges(),
                icon: <BlockIcon />,
              },
            ]}
          />
        </div>
        <Grid container spacing={3}>
          <Grid item xs={7}>
            <Card style={{ height: '80vh', borderRadius: 12 }} className='card-profile'>
              <section className='section-profile'>
                <div className='p-name'>
                  <div className={classes.titleName}>
                    <TextField         
                      // onBlur={() => this.onSubmitData('display_name')}
                      fullWidth
                      type='text'
                      name="display_name"
                      variant="outlined"
                      InputProps={{
                        classes: {
                          notchedOutline: classes.textField,
                          focused: classes.textFocus,
                          input: classes.textInput,
                          // root: classes.rootMargin
                        },
                        style: {
                          fontSize: 40,
                          padding: 0,
                          textAlign: 'center',
                          color: '#fff'
                        }
                      }}
                      value={formState.values.extra_info?.display_name || ''}
                      onChange={(e) => {
                        const extra_info = Object.assign({}, this.state.formState.values.extra_info)
                        extra_info[e.target.name] = e.target.value
                        const event = {
                          target: {
                            name: 'extra_info',
                            value: extra_info
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                  </div>
                </div>
                
              </section>
              <div className='d-flex justify-content-center' style={{ marginTop: -80, zIndex: 2 }}>
                <div style={{ width: 160, height: 160, backgroundColor: '#F5F5F5', zIndex: 2, borderRadius: '50%', position: 'relative' }}>
                  {formState.values.extra_info?.avatar ? 
                    <img style={{ width: 160, height: 160, borderRadius: '50%' }} src={formState.values.extra_info?.avatar} /> : 
                    <img style={{ width: 160, height: 160, borderRadius: '50%', transform: 'scale(0.8)' }} src={avatar} />
                  }
                  <Dropzone
                    multiple={false}
                    accept="image/*"
                    onDrop={(accepted, rejected) => this.onDropImage(accepted, rejected)}
                  >
                    {({getRootProps, getInputProps, isDragAccept, isDragReject, acceptedFiles, rejectedFiles}) => (
                      <section>
                        <div 
                          {...getRootProps()}
                          className='p-avatar d-flex align-items-center justify-content-center'
                        >
                          <input {...getInputProps()}  />
                          <PhotoCameraIcon />
                        </div>
                      </section>
                    )}
                  </Dropzone>
                </div> 
              </div>
              <div className='col-8 offset-2 mt-2'>
                <Box>
                  <AntTabs 
                    currentTab={this.state.currentTab || 'overview'}
                    items={[
                      {value: 'overview', label: 'Overview'},
                      {value: 'access_group', label: 'Access Group'},
                      // {value: 'password', label: 'Password'}
                    ]}
                    centered={true}
                    handleChangeTab={this.handleChangeTab}
                  />
                </Box>
                {this.state.currentTab == 'overview' &&
                <div className='mt-4'>
                  <div className='row'>
                    <div className='col-4' style={{ textAlign: 'right' }}>
                      <AccountCircleIcon />
                    </div>
                    <div className='col-8'>
                      {formState.values.name}
                    </div>
                  </div>
                  <div className='row mt-4'>
                    <div className='col-4' style={{ textAlign: 'right' }}>
                      <EmailIcon />
                    </div>
                    <div className='col-8'>
                      {formState.values.email}
                    </div>
                  </div>
                  <div className='row mt-4'>
                    <div className='col-4' style={{ textAlign: 'right' }}>
                      <PhoneIcon />
                    </div>
                    <div className='col-8'>
                      <TextField         
                        fullWidth
                        name="phone"
                        variant="outlined"
                        className="col-lg-12"
                        InputProps={{
                          classes: {
                            notchedOutline: classes.textField,
                            focused: classes.textFocus,
                            input: classes.textInputItem,
                            root: classes.rootMargin
                          },
                          style: {
                            color: '#34363A',
                            fontWeight: 'normal',
                            fontSize: 16
                          }
                        }}
                        placeholder='Phone...'
                        value={formState.values.extra_info?.phone || ''}
                        onChange={(event) => {
                          const extra_info = Object.assign({}, formState.values.extra_info)
                          extra_info[event.target.name] = event.target.value
                          const e = {
                            target: {
                              name: 'extra_info',
                              value: extra_info
                            }
                          }
                          this.handleChange(e)
                        }}
                      />
                    </div>
                  </div>
                  <div className='row mt-4'>
                    <div className='col-4' style={{ textAlign: 'right' }}>
                      <LocationOnIcon />
                    </div>
                    <div className='col-8'>
                      {formState.values.company_id?.name}
                    </div>
                  </div>
                  <div className='row mt-4'>
                    <div className='col-4' style={{ textAlign: 'right' }}>
                      <TelegramIcon />
                    </div>
                    <div className='col-8'>
                      {/* {formState.values.company_id?.name} */}
                      <TextField         
                        fullWidth
                        name="telegram"
                        variant="outlined"
                        className="col-lg-12"
                        InputProps={{
                          classes: {
                            notchedOutline: classes.textField,
                            focused: classes.textFocus,
                            input: classes.textInputItem,
                            root: classes.rootMargin
                          },
                          style: {
                            color: '#34363A',
                            fontWeight: 'normal',
                            fontSize: 16
                          }
                        }}
                        placeholder='Telegram...'
                        value={formState.values.telegram}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>
                }
                {this.state.currentTab == 'access_group' &&
                  <div className='mt-4 col-11 offset-3 row'>
                    {formState.values.groups?.map((rule, index) => (
                      <div className='row align-items-center col-lg-6'>
                        <Checkbox
                          disabled
                          style={{ padding: 4 }}
                          size="small"
                          checked={true}
                          color="primary"
                          name='rule'
                          className="col-lg-3"
                        />
                        <Box 
                          fontWeight="fontWeightBold"
                          fontSize="12"
                          className='col-lg-9'
                        >
                          {rule.name}
                        </Box>
                      </div>
                    ))}
                  </div>
                }
              </div>
            </Card>
          </Grid>
          <Grid item xs={5}>
            <Card style={{ maxHeight: '80vh', overflow: 'auto', borderRadius: 12 }}>
              <h5 className='p-4' style={{ borderBottom: '1px solid #ccc' }}>{`Users invited by me (${formState.values.user_invited?.length || 0})`}</h5>
              {formState.values.user_invited?.length == 0 && 
                <div className='d-flex align-items-center justify-content-center p-4' style={{ color: '#ccc' }}>
                  No user invited
                </div>
              }
              <List style={{ width: '100%' }}>
                {formState.values.user_invited?.length > 0 && formState.values.user_invited.map(user => (
                  <ListItem key={user.uid}>
                    <ListItemAvatar>
                      <Avatar
                        alt={`Avatar n°${user.uid + 1}`}
                        src={user.extra_info?.avatar}
                      />
                    </ListItemAvatar>
                    <ListItemText primary={user.name} />
                    <ListItemText secondary={user.company_id?.name} />
                  </ListItem>
                ))}
              </List>
              
            </Card>
          </Grid>
        </Grid>
      </div>
    )
  }
}

const connectedList = injectIntl(connect(null)(withStyles(useStyles)(Profile)));
export { connectedList as Profile };
