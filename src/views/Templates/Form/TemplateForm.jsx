/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import ReactDOM from 'react-dom'
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { AntTabs } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import AsyncSelect from 'react-select/async'
import {
  Box, Grid, Chip, Divider, Radio, RadioGroup, FormControlLabel,
  Checkbox, List, ListItem, ListItemText,
  InputAdornment,
  TextField, Stepper, Step,
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination
} from "@material-ui/core";
import config from "config";
import { CopyToClipboard, MultiInputPicker, FormModal, FormModalSide, FormModalSideStatic, CustomStep, CustomTimeline } from "../../../common";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import { makeId, customStyles } from "../../../utils";
import moment from "moment";
import clsx from 'clsx'
import Dropzone from 'react-dropzone'
import {
  IconEye, IconInfoCircle, IconMessages, IconEdit, IconTemplate, IconCornerUpRight,
  IconArrowLeft, IconPencil, IconArrowUp, IconChevronLeft, IconChevronRight
} from "@tabler/icons";
import { Button, Avatar, Input } from 'rsuite'
import { OrderDetail } from '../../Issues/Form/components'
import { TinyEditorComponent } from '../../../_components'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  cardNote: {
    backgroundColor: '#E7F8FB',
    border: '1px solid #B7DBE0'
  },
  chip: {
    display: 'flex',
    alignItems: 'center',
    padding: '3px 12px',
    borderRadius: 12,
    margin: '0 5px',
    '&.paid': {
      backgroundColor: '#5EC790'
    },
    '&.pending': {
      backgroundColor: '#FFD097'
    },
    '&.cancelled': {
      backgroundColor: '#DCDCDC'
    },
    '&.unfulfilled': {
      backgroundColor: '#FFE586'
    },
    '&.fulfilled': {
      backgroundColor: '#E0E1E3'
    }
  },
  chipPaid: {
    width: 10,
    height: 10,
    backgroundColor: '#006F53',
    borderRadius: '50%'
  },
  chipCancelled: {
    width: 10,
    height: 10,
    backgroundColor: '#626669',
    borderRadius: '50%'
  },
  chipPending: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  chipUnfulfilled: {
    width: 10,
    height: 10,
    border: '2px solid #885E16',
    borderRadius: '50%'
  },
  chipFulfilled: {
    width: 10,
    height: 10,
    backgroundColor: '#626669',
    borderRadius: '50%'
  },
  textBold: {
    fontWeight: 'bold'
  },
  headName: {
    background: '#ECECEC',
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    fontSize: 11,
    padding: 10,
    borderRight: '1px solid #D8D8D8',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  textDisabled: {
    backgroundColor: '#EFF1F2'
  },
  notchedOutline: {
    border: '2px solid #449242'
  }
});

class TemplateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
        }
      },
      content: "<p>Báo SUP resend + thuyết phục KH close kiện sau khi nhận TKN replacement</p><p>Báo Kh bên ngoài yêu cầu close kiện</p><p>18/1: TKN chưa active</p>",
      caseResponse: null,
      disabledResponse: true,
      editNote: false,
      currentLength: 0,
      currentTab: 'customer',
      reply: false
    };
    this.quillRef = React.createRef()
    this.inputRef = React.createRef()
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Template Form");
    // this.getListItems(this.state.formState.values);
  }

  handleChange = (event) => {
    const formState = Object.assign({}, this.state.formState)
    formState.values[event.target.name] = event.target.value
    this.setState({ formState })
  }

  handleChangeTab = (event, newValue) => {
    this.setState({ currentTab: newValue })
  }

  loadOptions = (inputValue, loadingData) => {
    return loadingData(inputValue)
  }

  handleInputChange = (newValue) => {
    return newValue;
  }

  render() {
    const { classes, user } = this.props;
    const { formState, editNote } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <FormModalSideStatic />
        <div className="col-lg-8 offset-2">
          <Grid container spacing={2} style={{ maxHeight: 'calc(100vh - 60px)', overflow: 'auto' }}>
            <Grid item xs={12}>
              <div className="d-flex align-items-center justify-content-center cursor-pointer br-6" style={{ width: 40, height: 40, border: '1px solid #ccc' }}>
                <IconArrowLeft size={30} stroke={2} />
              </div>
            </Grid>
            <Grid item xs={8}>
              <div className={clsx(classes.card, 'p-4')}>
                <div>
                  <Box fontWeight="bold" className="f-14">
                    Title
                  </Box>
                  <TextField 
                    fullWidth
                    name='title'
                    margin="dense"
                    variant="outlined"
                    placeholder="Title..."
                    onChange={this.handleChange}
                    value={formState.values.title || ''}
                  />
                </div>
                <div className="mt-2">
                  <Box fontWeight="bold" className="f-14">
                    Short description
                  </Box>
                  <TextField 
                    fullWidth
                    name='short_description'
                    margin="dense"
                    variant="outlined"
                    placeholder="Short description..."
                    onChange={this.handleChange}
                    value={formState.values.short_description || ''}
                  />
                </div>
                <div className="mt-2">
                  <Box fontWeight="bold" className="f-14">
                    Available for
                  </Box>
                  <RadioGroup
                    className='MuiFormControl-marginDense'
                    row 
                    aria-label="available_for" 
                    name="available_for" 
                    value={formState.values.available_for || ''}
                    onChange={this.handleChange}
                  >
                    <FormControlLabel style={{ marginBottom: 0 }} value={'in_company'} control={<Radio color="primary" />} label="Agents in company" />
                    <FormControlLabel style={{ marginBottom: 0 }} value={'all'} control={<Radio color="primary" />} label="All agents" />
                    <FormControlLabel style={{ marginBottom: 0 }} value={'only_me'} control={<Radio color="primary" />} label="Only me" />
                  </RadioGroup>
                </div>
                <div className="mt-2">
                  <Box fontWeight="bold" className="f-14">
                    Issue kind
                  </Box>
                  <TextField 
                    fullWidth
                    name='issue_kind'
                    margin="dense"
                    variant="outlined"
                    placeholder="Issue Kind"
                    onChange={this.handleChange}
                    value={formState.values.issue_kind || ''}
                  />
                </div>
                <div className="mt-2">
                  <Box fontWeight="bold" className="f-14">
                    Description
                  </Box>
                  <div className="mt-1">
                    <TinyEditorComponent 
                      style={{ width: '100%' }}
                      ref={this.inputRef}
                      content={formState.values.description || ''}
                      id={makeId(20)}
                      onEditorChange={(value) => {
                        const event = {
                          target: {
                            name: 'description',
                            value
                          }
                        }
                        this.handleChange(event)
                      }}
                    />
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={4}>
              <div className={clsx(classes.card, 'p-4')}>
                <div>
                  <Box fontWeight="bold" className="f-14">
                    Status
                  </Box>
                  <AsyncSelect
                    isSearchable
                    cacheOptions
                    loadOptions={(inputValue) => this.loadOptions(inputValue, function loadingData(inputValue) {
                      return new Promise(resolve => {
                        resolve([])
                      })
                    })}
                    defaultOptions
                    onInputChange={this.handleInputChange}
                    placeholder={'Status'}
                    getOptionLabel={({ name }) => name}
                    getOptionValue={({ id }) => id}
                    onChange={(value) => {
                      console.log(value)
                    }}
                    value={null}
                    styles={customStyles}
                  />
                </div>
              </div>
              <div className={clsx(classes.card, 'mt-2 p-4')}>
                <strong>Ticket Templates</strong>
                <div className="mt-2">
                  Templates allow you to pre-fill new tickets and outgoing emails with a single click. You can save time spent on typing out the same content again and again, and quickly create tickets or send outbound emails. <br /><br />For example, you can create a template called 'Initiate Refund' by filling the body of the ticket and setting the priority 'medium'. So the next time you receive a phone call about a refund, you can create a medium priority ticket for your customer and initiate the refund request with a single click.
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}


TemplateForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(TemplateForm)));
export { connectedList as TemplateForm };
