/* eslint-disable react/no-set-state */
/* eslint-disable no-redeclare */
import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { cssConstants } from "../../../_constants";
import { TinyEditorComponent } from "../../../_components";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  Box,
  Checkbox,
  InputAdornment,
  TextField,
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TablePagination
} from "@material-ui/core";
import config from "config";
import { Filter, FormModal, FormModalSide } from "../../../common";
import { makeId, capitalizeStr } from "../../../utils";
import { request } from "../../../_services/request";
import toastr from "../../../common/toastr";
import moment from "moment";
import { fields } from './constants'
import {
  IconSearch, IconPlus
} from "@tabler/icons";
import { Button, Dropdown } from 'rsuite'

const useStyles = (theme) => ({
  ...cssConstants,
  root: {
    padding: '0 1rem'
  },
  content: {
    marginTop: theme.spacing(2),
  },
  card: {
    borderRadius: 6,
    marginTop: 5,
    border: '1px solid #D8D8D8',
    backgroundColor: '#fff',
    // boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px'
  },
  notchedOutline: {
    borderRadius: 6
  },
  inputRoot: {
    "&.MuiInput-underline:hover:before": {
      border: "1px solid #3476D9",
    },
  },
  headName: {
    fontWeight: 'bold',
    borderRight: '1px solid #D8D8D8',
    borderTop: '1px solid #D8D8D8',
    borderBottom: '1px solid #D8D8D8',
    padding: '8px',
    background: '#ECECEC',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  cellContent: {
    borderRight: '1px solid #D8D8D8',
    padding: '8px',
    '&:last-child': {
      borderRight: 'none'
    }
  },
  input: {
    fontWeight: 14
  }
});

class Templates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: {
        values: {
          page: 1,
          limit: 15,
          tab: 'all'
        }
      },
      selectedItems: [],
      count: {},
      expandFilterRow: false,
      expandFilterColumn: false,
      totals: 0,
      show_keys: [],
      items: []
    };
  }

  componentDidMount() {
    this.props.onRouteChange(this.props.match, "Templates");
    this.getListItems(this.state.formState.values);
  }

  getListItems = (query) => {
    const queryObject = Object.assign({}, this.state.queryObject);
    var keys = Object.keys(query);
    keys.map((key, index) => {
      if (typeof query[key] == "array" || query[key] instanceof Array) {
        if (query[key].length > 0) {
          const arr = [];
          query[key].map((item) => {
            if (typeof item == "date" || item instanceof Date) {
              arr.push(moment(item).format('YYYY-MM-DD'))
            } else if (typeof item == "object" || item instanceof Object) {
              var valueKey = "id";
              arr.push(item[valueKey]);
            } else {
              arr.push(item);
            }
          });
          queryObject[key] = JSON.stringify(arr);
        } else {
          delete queryObject[key];
        }
      } else if (typeof query[key] == "object" || query[key] instanceof Object) {
        queryObject[key] = query[key].id;
      } else {
        queryObject[key] = query[key];
      }
    });
    this.query(queryObject);
  };

  query = (queryObject) => {
    const { dispatch } = this.props;
    dispatch(showLoading());
    request.get(`${config.apiUrl}/api/cs_templates`, queryObject).then((res) => {
      dispatch(hideLoading());
      if (res.data.success) {
        this.setState({ items: res.data.data.result, totals: res.data.data.total, count: res.data.data.count });
      } else {
        toastr.error(res.data.msg);
      }
    }, err => {
      toastr.error(err)
      dispatch(hideLoading())
    });
  };

  handlePageChange = (event, page) => {
    const formState = Object.assign({}, this.state.formState)
    formState.values.page = page + 1
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleRowsPerPageChange = (event) => {
    const formState = Object.assign({}, this.state.formState)
    formState.values.page = 1
    formState.values.limit = event.target.value
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  toggleDrawer = (side, open) => (event) => {
    if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }
    this.setState({ [side]: open });
  };

  handleChangeFilter = (event) => {
    const formState = Object.assign({}, this.state.formState);
    const { name, value } = event.target;
    formState["values"][name] = value;
    this.setState({ formState })
  };

  onSearch = () => {
    this.getListItems(this.state.formState.values);
  };

  resetFilter = () => {
    const formState = Object.assign({}, this.state.formState);
    const all_keys = Object.keys(this.state.formState.values)
    all_keys.forEach(e => {
      if (!['page', 'limit', 'tab'].includes(e)) {
        delete formState.values[e]
      }
    })
    formState.values.page = 1
    formState.values.tab = 'all'
    this.setState({ formState }, () => {
      this.getListItems(this.state.formState.values);
    });
  };

  handleSelectAll = event => {
    let selectedItems;

    if (event.target.checked) {
      selectedItems = this.state.items.map(t => t.id);
    } else {
      selectedItems = [];
    }
    this.setState({ selectedItems });
  };

  handleSelectOne = (event, id) => {
    const { selectedItems } = this.state;
    const selectedIndex = selectedItems.indexOf(id);
    let newselectedItems = [];

    if (selectedIndex === -1) {
      newselectedItems = newselectedItems.concat(selectedItems, id);
    } else if (selectedIndex === 0) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(1));
    } else if (selectedIndex === selectedItems.length - 1) {
      newselectedItems = newselectedItems.concat(selectedItems.slice(0, -1));
    } else if (selectedIndex > 0) {
      newselectedItems = newselectedItems.concat(
        selectedItems.slice(0, selectedIndex),
        selectedItems.slice(selectedIndex + 1)
      );
    }
    this.setState({ selectedItems: newselectedItems });
  };

  handleAction = (item, action) => {
    const { classes } = this.props;
    FormModal.instance.current.showForm({
      data: {
        name: item.name,
        content: item.content
      },
      size: 'md',
      title: `${capitalizeStr(action)} Template`,
      customView: (submitData, handleChange) => (
        <div>
          <div className="mt-2">
            <Box fontWeight={"bold"} className="f-14">
              Template Name
            </Box>
            <TextField 
              name="name"
              onChange={handleChange}
              value={submitData.values.name}
              variant="outlined"
              margin="dense"
              fullWidth
              placeholder="Template name..."
              InputProps={{
                classes: {
                  input: classes.input
                }
              }}
              error={submitData.errors?.name}
              helperText={submitData.errors?.name ? submitData.errors?.name[0] : ''}
            />
          </div>
          <div className="mt-2 issue-description">
            <Box fontWeight={"bold"} className="f-14">
              Content
            </Box>
            <TinyEditorComponent 
              className={submitData.errors?.content ? 'error' : ''}
              style={{ width: '100%' }}
              ref={this.templateContentRef}
              content={submitData.values.content}
              id={makeId(20)}
              onEditorChange={(content) => {
                const event = {
                  target: {
                    name: 'content',
                    value: content
                  }
                }
                handleChange(event)
              }}
            />
            {submitData.errors?.content && <small style={{ color: 'red'}}>{submitData.errors.content[0]}</small>}
          </div>
        </div>
      ),
      action: {
        titleAction: 'Save',
        schema: {
          name: {
            presence: { allowEmpty: false, message: '^Required' }
          },
          content: {
            presence: { allowEmpty: false, message: '^Required' }
          }
        },
        onAction: (submitData) => {
          return new Promise(resolve => {
            if (['clone', 'create'].includes(action)) {
              request.post_json(`${config.apiUrl}/api/cs_templates`, submitData.values).then(res => {
                if (res.data.success) {
                  resolve(true)
                  const items = Object.assign([], this.state.items)
                  items.push(res.data.data.result)
                  this.setState({ items })
                } else {
                  toastr.error(res.data.msg)
                }
              }, err => toastr.error(err))
            } else if (action == 'edit') {
              request.update_json(`${config.apiUrl}/api/cs_template/${item.id}`, submitData.values).then(res => {
                if (res.data.success) {
                  resolve(true)
                  const items = this.state.items.map(e => e.id == item.id ? res.data.data : e)
                  this.setState({ items })
                } else {
                  toastr.error(res.data.msg)
                }
              }, err => toastr.error(err))
            }
          })
        }
      }
    })
  }

  handleDelete = (item) => {
    FormModal.instance.current.showForm({
      title: `Delete Template`,
      customView: (submitData, handleChange) => (
        <div className="col-12">
          Are you sure to delete this template?
        </div>
      ),
      action: {
        titleAction: 'Delete',
        color: 'secondary',
        onAction: (submitData) => {
          return new Promise(resolve => {
            request._delete(`${config.apiUrl}/api/cs_template/${item.id}`).then(res => {
              if (res.data.success) {
                const items = this.state.items.filter(e => e.id != item.id)
                this.setState({ items })
                toastr.success("Delete successful")
                resolve(true)
              } else {
                toastr.error(res.data.msg)
                resolve(false)
              }
            }, err => {
              toastr.error(err)
              resolve(false)
            })
          })
        }
      }
    })
  }

  render() {
    const { classes, user } = this.props;
    const { formState, right, items, selectedItems, expandFilterColumn, expandFilterRow } = this.state;
    // const search_key = this.getFormSearch(this.state.show_keys);
    return (
      <div className={classes.root}>
        <FormModal />
        <FormModalSide />
        <Filter
          right={right}
          onClose={(side, open) => this.toggleDrawer(side, open)}
          formState={this.state.formState}
          onChange={this.handleChangeFilter}
          onSearch={this.onSearch}
          resetFilter={this.resetFilter}
          filters={[]}
        />
        <div style={{ paddingTop: 5 }}>
          <div className={classes.card}>
            <div className="d-flex align-items-center justify-content-between">
              <div style={{ display: "flex", padding: "5px 10px", width: '100%' }}>
                <TextField
                  id="query"
                  margin="dense"
                  name="query"
                  placeholder="Search template"
                  classes={{
                    root: classes.inputRoot,
                  }}
                  value={formState.values.query}
                  onChange={this.handleChangeFilter}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <IconSearch size={20} stroke={1.5} />
                      </InputAdornment>
                    ),
                    classes: {
                      notchedOutline: classes.notchedOutline,
                    },
                  }}
                  style={{ width: "30%", backgroundColor: "#fff" }}
                  variant="outlined"
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      this.onSearch();
                    }
                  }}
                />
              </div>
              <div className="d-flex align-items-center">
                <Button 
                  className="button-primary mr-2"
                  onClick={() => this.handleAction({}, 'create')}
                >
                  <IconPlus size={16} stroke={2} /> New
                </Button>
              </div>
            </div>
            
            <TableContainer style={{ maxHeight: 'calc(100vh - 200px)' }}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '4%' }} align="center" padding="checkbox" className={classes.headName}>
                      <Checkbox
                        checked={selectedItems.length === items.length}
                        color="secondary"
                        indeterminate={
                          selectedItems.length > 0 &&
                          selectedItems.length < items.length
                        }
                        onChange={this.handleSelectAll}
                      />
                    </TableCell>
                    {fields.map(field => (
                      <TableCell key={field.name} style={{ width: field.width }} align={field.align || 'left'} className={classes.headName}>{field.label}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((item, index) => (
                    <TableRow key={item.id} style={{ backgroundColor: index % 2 == 1 ? '#ECECEC' : '#fff'}}>
                      <TableCell onClick={e => e.stopPropagation()} padding="checkbox" align="center" className={classes.cellContent}>
                        <Checkbox
                          checked={selectedItems.indexOf(item.id) !== -1}
                          color="secondary"
                          onChange={event => this.handleSelectOne(event, item.id)}
                          value="true"
                        />
                      </TableCell>
                      {fields.map(field => (
                        <TableCell key={field.name} className={classes.cellContent} align={field.align || 'left'}>
                          {field.renderItem(this, item)}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              style={{ borderTop: '1px solid #D8D8D8' }}
              component="div"
              count={this.state.totals}
              onPageChange={this.handlePageChange}
              onRowsPerPageChange={this.handleRowsPerPageChange}
              page={formState.values.page - 1}
              rowsPerPage={this.state.formState.values.limit}
              rowsPerPageOptions={[15, 25, 50, 100]}
            />
          </div>
        </div>
      </div>
    );
  }
}


Templates.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

const connectedList = injectIntl(connect(mapStateToProps)(withStyles(useStyles)(Templates)));
export { connectedList as Templates };
