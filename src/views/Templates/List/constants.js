import React from 'react'
import moment from 'moment'
import { IconCopy, IconPencil, IconTrash } from '@tabler/icons'

export const fields = [
  {
    name: 'title',
    label: 'Title',
    renderItem: (ctx, item) => item.name,
    width: '30%',
  },
  {
    name: 'description',
    label: 'Description',
    renderItem: (ctx, item) => <div className="line-clamp-1" dangerouslySetInnerHTML={{ __html: item.content }} />,
    width: '35%',
  },
  {
    name: 'created_at',
    label: 'Created at',
    renderItem: (ctx, item) => moment(item.created_at).format('LLL'),
    width: '20%',
  },
  {
    name: 'action',
    label: 'Action',
    renderItem: (ctx, item) => (
      <div className='d-flex align-items-center'>
        <div 
          className='d-flex align-items-center justify-content-center cursor-pointer' 
          style={{ width: 30, height: 30, borderRadius: 6, border: '1px solid #ccc' }}
          onClick={() => ctx.handleAction(item, 'clone')}
        >
          <IconCopy size={20} stroke={2} />
        </div>
        <div 
          className='d-flex align-items-center justify-content-center cursor-pointer ml-2' 
          style={{ width: 30, height: 30, borderRadius: 6, border: '1px solid #ccc' }}
          onClick={() => ctx.handleAction(item, 'edit')}
        >
          <IconPencil size={20} stroke={2} />
        </div>
        <div 
          className='d-flex align-items-center justify-content-center cursor-pointer ml-2' 
          style={{ width: 30, height: 30, borderRadius: 6, border: '1px solid #ccc' }}
          onClick={() => ctx.handleDelete(item)}
        >
          <IconTrash size={20} stroke={2} />
        </div>
      </div>
    ),
    width: '15%',
  },
]

export const stateOptions = [
  {id: 'all', name: 'All'},
  {id: 'new', name: 'New'},
  {id: 'open', name: 'Open'},
  {id: 'pending', name: 'Pending'},
  {id: 'wait_on_customer', name: 'Wait on Customer'},
  {id: 'wait_on_3rd_party', name: 'Wait on 3rd party'},
  {id: 'resolved', name: 'Resolved'},
  {id: 'closed', name: 'Closed'},
]

export const dataItems = [
  {
    id: 1,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 2,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 3,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 4,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 5,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 6,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 7,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 8,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 9,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 10,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 11,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 12,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 13,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 14,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 15,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  },
  {
    id: 16,
    title: 'Order not found ver 2',
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
    creator: 'Celina V',
    issue_kind: 'L202'
  }
]