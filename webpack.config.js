var HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
var path = require('path')
require('dotenv').config()
var webpack = require('webpack');

module.exports = (env, argv) => {
  return {
    mode: 'development',
    resolve: {
      extensions: ['.js', '.jsx']
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: argv.mode == 'development' ? `[name].bundle.version_1.0.2.js` : `[contenthash].bundle.version_1.0.2.js`,
      chunkFilename: argv.mode == 'development' ? `[name].bundle.version_1.0.2.js` : `[contenthash].bundle.version_1.0.2.js`,

    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },
    module: {
      rules: [{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: ['file-loader']
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }]
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/octet-stream'
          }
        }]
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml'
          }
        }]
      },
      {
        test: /\.(jpe?g|png|gif|ico)$/i,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[contenthash].[ext]'
          }
        }]
      },
      {
        test: /(\.css|\.scss|\.sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader'
          }, {
            loader: 'sass-loader',
            options: {
              includePaths: [path.resolve(__dirname, 'src')],
              sourceMap: true
            }
          }
        ]
      }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html',
        baseUrl: process.env.BASE_URL,
        excludeChunks: Object.keys({})
      }),
      new MiniCssExtractPlugin({
        filename: argv.mode == 'development' ? '[name].css' : '[contenthash].css',
        chunkFilename: argv.mode == 'development' ? '[name].css' : '[contenthash].css',
      }),
      new webpack.ProvidePlugin({
        'window.Quill': 'quill/dist/quill.js',
        'Quill':'quill/dist/quill.js',
      })
    ],
    devServer: {
      historyApiFallback: true,
      contentBase: './',
      hot: true,
      port: 8082,
      // compress: true,
      disableHostCheck: true,   // That solved it
    },
    externals: {
      // global app config object
      config: JSON.stringify({
        apiUrl: argv.mode == 'development' ? process.env.DEV_API_URL : process.env.API_URL,
        apiCoreUrl: argv.mode == 'development' ? process.env.DEV_API_CORE_URL : process.env.API_CORE_URL,
        apiLoginUrl: argv.mode == 'development' ? process.env.DEV_API_URL_LOGIN : process.env.API_URL_LOGIN,
        accountUrl: argv.mode == 'development' ? process.env.DEV_ACCOUNT_URL : process.env.ACCOUNT_URL,
        sellerUrl: argv.mode == 'development' ? process.env.DEV_SELLER_URL : process.env.SELLER_URL,
        supplierUrl: argv.mode == 'development' ? process.env.DEV_SUPPLIER_URL : process.env.SUPPLIER_URL,
        issueApiUrl: argv.mode == 'development' ? process.env.DEV_API_ISSUE_URL : process.env.API_ISSUE_URL,
      })
    }
  }
}
